# Campra : L’Europe Galante
Matériel complet de Europe Galante de Campra,
créé pour la Simphonie du Marais – Hugo Reyne (2017).

Pour construire le matériel :
```
$ make all
```
