\key sol \major
\time 2/2 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*21 \bar "|."
