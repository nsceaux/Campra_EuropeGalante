C’est Vul -- cain qui fait le ton -- ner -- re,
dont le Maî -- tre des Dieux é -- pou -- van -- te le ter -- re ;
Mais ce sont les Plai -- sirs, les Gra -- ces, & les Ris
qui for -- ment les traits de mon fils :
Jeu -- nes cœurs, es -- say -- ez la dou -- ceur de ses ar -- mes ;
Qui s’en lais -- se bles -- ser e -- prou -- ve mil -- le char -- mes.
Jeu -- nes cœurs, es -- say -- ez la dou -- ceur de ses ar -- mes ;
Qui s’en lais -- se bles -- ser e -- prou -- ve mil -- le char -- mes.
