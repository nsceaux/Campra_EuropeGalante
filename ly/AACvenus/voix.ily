\clef "vbas-dessus"
r4 sol'8 sol' re''8. re''16 si'8\trill si'16 si' |
mi''8 mi'' r do''16 re'' mi''8 mi''16 fad'' |
sol''8 si'16 re'' sol'8 sol'16 si' mi'8 mi' r4 |
do''8. do''16 la'4\trill la'8 si' |
sold'4.\trill mi'8 la'8. sol'?16 sol'8[ fad'16] sol' |
fad'8.\trill fad'16 sol'8 sol'16 la' si'4\trill si'8 dod'' |
re''2 la'8 si' |
\appoggiatura si'8 do''2 do''8 re'' |
si'4\trill si' mi'' |
\appoggiatura re''8 dod''4 red'' mi'' |
red''4. dod''?8( si'4) |
r sol'' fad'' |
mi'' mi'' si' |
do''4. mi''8 \appoggiatura re''8 do''4 |
si' la'4.\trill sol'8 |
sol'4( fad'2)\trill |
mi' si'8 do'' |
re''2 la'8 si' |
do''4 do'' la' |
si' si' \appoggiatura la'8 sol'4 |
re''2 si'4\trill |
r sol'' fad'' |
mi'' re'' do'' |
si'4.\trill re''8 do''4\trill |
\appoggiatura si'8 la'4 la'( si'8) do'' |
si'4( la'2)\trill |
sol'2. |
