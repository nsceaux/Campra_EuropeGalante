\clef "basse" sol1 |
do2. |
si,2 do |
la,4 fa4. re8 |
mi2 dod |
re8. do16 si,8. la,16 sol,8. fad,16 mi,4 |
re,2 re4 |
mi4 fad re |
sol8 la sol fad mi4 |
la8 sol fad4 mi |
si, si8 la sol fad |
mi4 mi' re' |
do' sold2 |
la la,4 |
si, do la, |
si,2. |
mi2 mi'4 |
si4. do'8 re'4 |
la la re' |
sol8 la sol fad mi4 |
re2 sol8 fad |
mi4 mi re |
do si, la, |
sol,4. si,8 do4 |
re fad, sol, |
do, re,2 |
sol,2. |
