\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Venus
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basse continue" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2. s1\break s2. s1*2 s2.\pageBreak
        \grace s8 s2.*8\break s2.*8\break
      }
    >>
  >>
  \layout {
    indent = \largeindent
    system-count = 6
  }
  \midi { }
}