\clef "haute-contre"
\setMusic #'refrain {
  la'4 la' |
  la' la' re'' |
  dod'' la' la' |
  sol' sol' la' |
  la' la' la' |
  la' la' re'' |
  dod'' la' la' |
  sol' sol' la' |
  la' dod'' re'' |
  mi'' fa'' la' |
  la' sol' la' |
  sib' la'4. la'8 |
  la'4 dod'' re'' |
  mi'' fa'' la' |
  la' sol' la' |
  sib' la'4. la'8 |
  la'4
}
r4 r4 R2.*15 r4
\keepWithTag#'() \refrain
r4 r4 R2.*15 r4
\refrain
r4 r4 R2.*15 r4
\refrain

