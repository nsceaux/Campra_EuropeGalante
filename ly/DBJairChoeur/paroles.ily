\setMusic #'refrain {
  Li -- vrons- nous aux plai -- sirs, il n’est rien de plus doux ;
  Li -- vrons- nous aux plai -- sirs, il n’est rien de plus doux ;
  Pour qui se -- roient- ils faits, si ce n’es -- toit pour nous ?
  Pour qui se -- roient- ils faits, si ce n’es -- toit pour nous ?
}
\tag #'(vdessus basse) \keepWithTag#'() \refrain
\refrain
\tag #'(vdessus basse) {
  Mille a -- mours dé -- gui -- sez dans ce char -- mant sé -- jour,
  com -- blent nos cœurs d’u -- ne dou -- ceur ex -- trê -- me ;
  Si quel -- qu’un en ces lieux, est en -- tré sans a -- mour,
  ne crai -- gnons pas qu’il en sor -- te de mê -- me.
}
\refrain
\tag #'(vdessus basse) {
  L’A -- mour, jeu -- nes beau -- tez, ac -- com -- pa -- gne vos pas ;
  pour tout sou -- mettre, il vous prê -- te ses ar -- mes :
  C’est vai -- ne -- ment qu’aux yeux vous ca -- chez mille ap -- pas,
  à tous les cœurs il ré -- ve -- le ses char -- mes.
}
\refrain
