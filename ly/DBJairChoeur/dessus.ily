\clef "dessus"
\setMusic #'refrain {
  re''4 mi'' |
  fa'' mi''\trill re'' |
  mi'' la' re'' |
  si' dod'' re'' |
  dod''\trill re'' mi'' |
  fa'' mi''\trill re'' |
  mi'' la' re'' |
  si' dod'' re'' |
  dod'' mi'' fa'' |
  sol'' fa'' mi'' |
  fa'' dod'' re'' |
  mi'' dod''4.\trill re''8 |
  re''4 mi'' fa'' |
  sol'' fa'' mi'' |
  fa'' dod'' re'' |
  mi'' dod''4.\trill re''8 |
  re''4
}
r4 r4 R2.*15 r4
\keepWithTag#'() \refrain
r4 r4 R2.*15 r4
\refrain
r4 r4 R2.*15 r4
\refrain

