\clef "quinte"
\setMusic #'refrain {
  fa'4 mi' |
  re' re' re' |
  la' la la |
  si mi' la |
  la fa' mi' |
  re' re' re' |
  la' la la |
  si mi' la |
  la mi' re' |
  dod' re' mi' |
  la mi' la' |
  sol' sol' mi' |
  fa' mi' re' |
  dod' re' mi' |
  la mi' la' |
  sol' sol' mi' |
  fa'
}
r4 r4 R2.*15 r4
\keepWithTag#'() \refrain
r4 r4 R2.*15 r4
\refrain
r4 r4 R2.*15 r4
\refrain

