\clef "basse"
\setMusic #'refrain {
  re'4 dod' |
  re' do' sib |
  la fa re |
  sol mi\trill re |
  la re' dod' |
  re' do' sib |
  la fa re |
  sol mi re |
  la sol fa |
  mi re dod |
  re mi fa |
  sol la la, |
  re sol fa |
  mi re dod |
  re mi fa |
  sol la la, |
  re
}
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue \keepWithTag #'() \refrain
>>
\refrain
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue {
    re4 re |
    la fad re |
    sol la si |
    do' si do' |
    sol mi re8 do |
    sol4 sol la |
    si do' fa |
    sol sol,2 |
    do4 do' si |
    la sold la |
    mi do si, |
    la, sol, fa, |
    mi, mi'4. re'8 |
    do'4 si la |
    sold mi do |
    re mi mi, |
    la,
  }
>>
\refrain
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue {
    re'4 sib |
    mib' mib' do' |
    re' sib la |
    sol fa mib |
    re sib, do8 re |
    do4 re mib |
    re sib, sol, |
    re re,2 |
    sol,4 sol fa |
    mi fa re |
    la sol fa |
    mi fa fa, |
    do mi re8 do |
    fa4 sol la |
    sib do' fa |
    sib, do do, |
    fa,
  }
>>
\refrain

