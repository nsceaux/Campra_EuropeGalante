\clef "taille"
\setMusic #'refrain {
  fa'4 sol' |
  fa' fa' sol' |
  mi' fa' fa' |
  re' sol' fa' |
  mi' fa' sol' |
  fa' fa' sol' |
  mi' fa' fa' |
  re' sol' fa' |
  mi' la' la' |
  sol' la' sol' |
  fa' sol' fa' |
  mi' mi'4.\trill re'8 |
  re'4 la' la' |
  sol' la' sol' |
  fa' sol' fa' |
  mi' mi'4.\trill re'8 |
  re'4
}
r4 r4 R2.*15 r4
\keepWithTag#'() \refrain
r4 r4 R2.*15 r4
\refrain
r4 r4 R2.*15 r4
\refrain

