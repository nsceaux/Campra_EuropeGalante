\setMusic#'refrain <<
  \tag #'(vdessus basse) {
    re''4 mi'' |
    fa'' mi''\trill re'' |
    mi'' la' re'' |
    si' dod'' re'' |
    dod'' re'' mi'' |
    fa'' mi''\trill re'' |
    mi'' la' re'' |
    si' dod'' re'' |
    dod''\trill mi'' fa'' |
    sol'' fa'' mi'' |
    fa'' dod'' re'' |
    mi'' dod''4.\trill re''8 |
    re''4 mi'' fa'' |
    sol'' fa'' mi'' |
    fa'' dod'' re'' |
    mi'' dod''4.\trill re''8 |
    re''4
  }
  \tag #'vhaute-contre {
    la'4 sol' |
    fa' fa' sol' |
    mi'\trill fa' fa' |
    sol' sol' fa' |
    mi'\trill la' sol' |
    fa' fa' sol' |
    mi'\trill fa' fa' |
    sol' sol' fa' |
    mi'\trill mi' la' |
    sol' la' sol' |
    fa' sol' fa' |
    mi' mi'4.\trill mi'8 |
    fa'4 mi' la' |
    sol' la' sol' fa' sol' fa' |
    mi' mi'4.\trill re'8 |
    re'4
  }
  \tag #'vtaille {
    fa'4 mi' |
    re' la re' |
    dod' re' re' |
    re' mi' la |
    la fa' mi' |
    re' la re' |
    dod'\trill re' re' |
    re' mi' la |
    la dod' re' |
    mi' la la |
    la sol la |
    sib la4. la8 |
    la4 dod' re' |
    mi' la la |
    la sol la |
    sib la4. la8 |
    la4
  }
  \tag #'vbasse {
    re'4 dod' |
    re' do' sib |
    la fa re |
    sol mi\trill re |
    la re' dod' |
    re' do' sib |
    la fa re |
    sol mi re |
    la sol fa |
    mi re dod |
    re mi fa |
    sol la la, |
    re sol fa |
    mi re dod |
    re mi fa |
    sol la la, |
    re
  }
>>

<<
  \tag #'(vdessus basse) {
    \clef "vbas-dessus"
    <>^\markup\character-text Une autre Venitienne déguisée
    re''4 mi'' |
    fa'' mi''\trill re'' |
    mi'' la' re'' |
    si' dod'' re'' |
    dod'' re'' mi'' |
    fa'' mi''\trill re'' |
    mi'' la' re'' |
    si' dod'' re'' |
    dod''\trill mi'' fa'' |
    sol'' fa'' mi'' |
    fa'' dod'' re'' |
    mi'' dod''4.\trill re''8 |
    re''4 mi'' fa'' |
    sol'' fa'' mi'' |
    fa'' dod'' re'' |
    mi'' dod''4.\trill re''8 |
    re''4
  }
  \tag #'vhaute-contre { \clef "vhaute-contre" r4 r R2.*15 r4 }
  \tag #'vtaille { \clef "vtaille" r4 r R2.*15 r4 }
  \tag #'vbasse { \clef "vbasse" r4 r R2.*15 r4 }
>>
\keepWithTag #'(vdessus vhaute-contre vtaille vbasse) \refrain
<<
  \tag #'(vdessus basse) {
    <>^\markup\character La Venitienne
    la'4 si' |
    do'' do'' re'' |
    si'\trill do'' re'' |
    mi'' fa'' mi'' |
    re''\trill sol'' fa''8\trill mi'' |
    re''4 si' do'' |
    re'' mi'' fa'' |
    mi''( re''2)\trill |
    do''4 mi'' re'' |
    do'' si' la' |
    sold'\trill la' si' |
    do'' mi' la' |
    sold'\trill si' la'8 sold' |
    la'4 si' do'' |
    si' sold' la' |
    la'2( sold'4) |
    la'
  }
  \tag #'(vhaute-contre vtaille vbasse) { r4 r R2.*15 r4 }
>>
\refrain
<<
  \tag #'(vdessus basse) {
    <>^\markup\character La Venitienne
    la'4 sib' |
    sol' sol' la' |
    fad'\trill sol' la' |
    sib' la' sol' |
    re'' re'' mi''8 fa'' |
    mi''4 fad'' sol'' |
    fad''\trill \appoggiatura mi''8 re''4 \appoggiatura do''8 sib'4 |
    sib'( la'2)\trill |
    sol'4 sol' la' |
    sib' la' sib' |
    do'' sib' la' |
    sol' la' fa' |
    mi'\trill sol' la'8 sib' |
    la'4\trill sib' do'' |
    re'' sol' la' |
    la'( sol'2)\trill |
    fa'4
  }
  \tag #'(vhaute-contre vtaille vbasse) { r4 r R2.*15 r4 }
>>
\refrain
