\clef "basse" sol,2 sol4. sol8 |
do'8. sib16 la8.\trill sol16 re4 re8. do16 |
si,4 do8. sib,?16 la,4. sol,8 |
re4 mib8 do re4 re, |
sol,2 sol4. sol8 |
re4. re8 mib8. re16 do8. sib,16 |
fa8. mib16 re8.\trill do16 sib,4. sib8 |
re8. mib16 fa8 sib, fa,2 |
sib,8. do16 sib,8. la,16 sol,4 sol |
do'8. sib16 la8. sol16 re4 re8. do16 |
si,4 do8. sib,?16 la,4. sol,8 |
re4 mib8 do re4 re, |
sol,2
