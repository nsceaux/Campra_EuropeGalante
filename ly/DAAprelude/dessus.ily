\clef "dessus" r2 r4 re''8. re''16 |
\appoggiatura re''16 mi''8 mi''16 mi'' fad''8. sol''16 fad''2\trill |
sol''8 sol''16 fa'' mib''8. re''16 do''4\trill la'8 re'' |
fad'4 sol'8. la'16 sib'4( la'8.)\trill sol'16 |
sol'4. sol''8 re''8.\trill re''16 re''8. mib''16 |
fa''4 fa''8 fa'' sol''4 la''8. sib''16 |
la''8.\trill sol''16 fa''4 r8 sib'' re''8.\trill mib''16 |
fa''8. sol''16 do''8 re'' re''4( do''8.)\trill sib'16 |
sib'2 r4 re''8. re''16 |
\appoggiatura re''8 mi'' mi''16 mi'' fad''8. sol''16 fad''2\trill |
sol''8 sol''16 fa'' mib''8. re''16 do''4\trill la'8 re'' |
fad'4 sol'8. la'16 sib'4( la'8.)\trill sol'16 |
sol'2