\clef "haute-contre" r2 r4 sib'8. sib'16 |
sol'8. sol'16 do''8. re''16 re''2~ |
re''4 do''8 sol' la'4. re'8 |
re'4 sol'2 fad'8.\trill sol'16 |
sol'2 sib'8. sib'16 sib'8. do''16 |
re''4. re''8 sib'4 mib''8. re''16 |
do''4\trill re''8 la' sib'4. sib'8 |
sib'4 la'8. sib'16 sib'4( la'8.)\trill sib'16 |
sib'2 r4 si'8. si'16 |
do''8. sol'16 do''8. re''16 re''2 |
re''4 do''8. sol'16 la'4 do'8 re' |
re'4 sol'8 mib' re'4. re'8 |
re'2
