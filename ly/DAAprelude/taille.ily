\clef "taille" r2 r4 sol'8. sol'16 |
sol'8 mi' la'8. sib'16 la'2 |
re'4 do'8. sol16 la4. sib8 |
la8 re' sib mib' re'4. re'8 |
re'2 sol'4. sol'8 |
fa'4. fa'8 mib'4 mib'8. fa'16 |
fa'2 fa'4. fa'8 |
fa'8. sib16 fa'4. mib'8 mib'8. fa'16 |
re'2\trill r4 sol'8. sol'16 |
sol'4 la'8. re'16 re'2 |
sol'4 sol'8 do' do'4. sib8 |
la re' sib do' do'4 la |
sib2
