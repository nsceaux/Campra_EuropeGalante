\clef "quinte" r2 r4 re'8. re'16 |
do'8. sol16 la8. re'16 re'2 |
sol4. sol8 fad4. sol8 |
la4 sol do' la |
sib2. re'4 |
sib2. do'8. fa16 |
fa4 fa'8. mib'16 re'8.\trill do'16 sib4~ |
sib8. sol16 la8 fa fa4. fa8 |
fa2 r4 re'8. re'16 |
do'4 do'8. sib16 la2\trill |
sol4 sol8. sol16 fad4.\trill sol8 |
la4 sol8 sol sol4( fad8.) sol16 |
sol2
