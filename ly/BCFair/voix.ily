\clef "vbas-dessus" <>^\markup\character Une autre Bergere
re''4 |
sol' la' sib' |
la'4.\trill sib'8 sol'4 |
fad'2\trill re''4 |
mib''2 do''4 |
\appoggiatura sib'8 la'4 \appoggiatura sol'8 fad'4 sol' |
la' sib'( la')\trill |
sol'2 re''4 |
mib'' mib'' re'' |
do''4.\trill re''8 sib'4 |
la'2.\trill |
fa''4 \appoggiatura mib''8 re''4 \appoggiatura do''8 sib'4 |
sol'4.\trill sol'8 la'4 |
sib' sib'( la') |
sib'2. |
r4 r re'' |
sib'2 re''4 |
sol'' sol'' mi'' |
dod''2\trill re''4 |
mi''2 la'4 |
r re'' mi'' |
fa''2 re''4 |
mi'' dod''4.\trill re''8 |
re''4 re'' do'' |
sib' la' sol' |
fad'2 sol'4 |
la'2\trill sib'4 |
do''4. re''8 sib'4\trill~ |
sib'8 la' la'4.\trill sol'8 |
sol'2 re''4 |
sol'4 re'' do'' |
sib' la' sol' |
fad'2 sol'4 |
la'2\trill sib'4 |
do''4. re''8 sib'4\trill~ |
sib'8 la' la'4.\trill sol'8 |
sol'2