\key re \minor \midiTempo#160 \beginMark "Air"
\digitTime\time 3/4 \partial 4
s4 \bar ".!:" s2.*13 \alternatives s2.*2 s2.
\bar ".!:" s2.*12 \alternatives s2. s2. s2.*5 s2 \bar "|."
