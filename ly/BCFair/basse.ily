\clef "basse" sol4 |
sib la sol |
fad4. sol8 mib4 |
re2 sib,4 |
do2 la,4 |
re2 mib4 |
do re re, |
sol,2 sol4 |
do' do' sib |
la4. sib8 sol4 |
fa( fa mib) |
re2. |
mib4. re8 do4 |
sib, fa fa, |
sib,4. do8 sib, la, |
sol,2 sol4 |
sib,4 sib8 do' sib la |
sol4 sib sol |
la2 re4 |
la, la sol |
fa fa mi |
re la, sib, |
sol, la,2 |
re2. |
re'4 do' sib |
la2 sol4 |
fad2 sol4 |
mi fad sol |
do re re, |
sol, sib8 do' sib la |
sol,2. |
re'4 do' sib |
la2 sol4 |
fad2 sol4 |
mi fad sol |
do re re, |
sol,2