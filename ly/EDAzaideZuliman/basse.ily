\clef "basse" mi4 |
mi' red' mi' |
la si do' |
si4. la8 sold4 |
la4. sol8 fad re |
sol do re4 re, |
sol, sol fad8 mi |
re4. re8 dod si, |
fad4 re2 |
sol4 fad mi |
fad fad,2 |
si, si4 |
mi'8 re' do'2 |
si4 sol mi |
red mi la, |
si,2. |
mi, |
mi |
red |
mi2 re4 |
do2. |
si, |
mi4. re8 do si, |
la,2. |
si, |
do |
si, |
mi4. re8 do si, |
la,2. |
si,2 la,4 |
si,2. |
mi,2. | \allowPageTurn
mi4 fad sol |
red4. dod8 si,4 |
mi4 do' la |
si2 si4 |
sol mi re |
dod red mi |
si,2. |
mi, |
mi'4 re' do' |
si4. la8 sol4 |
do' la sol |
fad2\trill re4 |
sol sol fa |
mi mi do |
re re,2 |
sol,4 sol sol |
fad fad sol |
mi2 la4 |
re2 re4 |
re' dod' si |
mi2 mi4 |
mi fad fad, |
si,2. |
fad2 fad4 |
sol sol mi |
si2 mi'4 |
si2 si8 la |
sol4 la si |
do'2 sol4 |
la si si, |
mi2. |
mi4 la la |
la2 sol4 |
la si si, |
mi,1 |
fad, |
sol, |
la,4 si,2 |
mi, mi8 re dod si, |
la,1 |
