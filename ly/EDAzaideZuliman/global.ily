\key mi \minor \midiTempo#120
\digitTime\time 3/4 \partial 4 \beginMark "Air"
s4 s2.*15 s4 \beginMark "Air" s2 s2.*15 \bar "||"
\digitTime\time 3/4 \midiTempo#160 s2.*34
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.
\time 4/4 \grace s8 s1*2 \bar "|."
