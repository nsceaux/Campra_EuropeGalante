<<
  \tag #'(recit0 basse) {
    \clef "vbas-dessus" <>^\markup\character Zayde
    r8 si' |
    mi''4 fad'' sol'' |
    fad''8.\trill mi''16( red''4) mi''8 mi''( |
    red''2)\trill mi''8 si' |
    do''2 do''8 do'' |
    si'\trill do'' si'4( la')\trill |
    sol'4 r8 si' si' dod'' |
    re''4 re'' mi''8 fad'' |
    lad'4\trill fad'' \appoggiatura mi''16 re''4 |
    \appoggiatura dod''16 si'4 dod''! re'' |
    re''( dod''2)\trill |
    si'4 r8 si' si' fad' |
    sol'4. sol'8 fad'\trill mi' |
    si'4 mi'' \appoggiatura re''8 do''4 |
    \appoggiatura si'8 la'4 sol'\trill fad' |
    sol'( fad'2)\trill |
    mi'4
    \ffclef "vbasse" <>^\markup\character Zuliman
    mi4 mi8 fad |
    sol4. sol8 la si |
    fad2\trill fad4 |
    sol4. sol8 la si |
    si2( la4) |
    si2 si4 |
    sold4.\trill sold8 la si |
    \appoggiatura si16 do'4. si8( la4) |
    fad4 fad sol |
    mi4.( red8) mi4 |
    red2\trill si4 |
    sold4.\trill sold8 la si |
    do'4. si8( la4) |
    fad4 fad sol |
    sol( fad4.)\trill mi8 |
    mi2. |
  }
  \tag #'recit1 {
    \clef "vbas-dessus" r4 R2.*31
  }
>>
%%
<<
  \tag #'(recit1 basse) {
    \tag #'basse \clef "vbas-dessus"
    <>^\markup\character Zayde
    sol'4 fad'\trill mi' |
    si'2. |
    si'4 mi'' fad'' |
    red''4.\trill dod''?8( si'4) |
    r mi'' si' |
    la'\trill fad' sol' |
    sol'( fad'4.)\trill mi'8 |
    mi'2. |
    si'4 si' do'' |
    re''2. |
    mi''4 fad'' sol'' |
    re''2( do''8.)[\trill si'16] |
    si'4\trill si' re'' |
    sol' sol' la' |
    si'( la'4.)\trill sol'8 |
    sol'2. |
    r4 re'' re'' |
    re''2 dod''4 |
    re''2 re''4 |
    la' lad'\trill si' |
    \appoggiatura si'8 dod''2 dod''4~ |
    dod''8 re'' re''4( dod'') |
    si'2. |
    r4 red'' red'' |
    mi'' mi'' mi'' |
    fad''2 sol''4 |
    fad''4.\trill mi''8( red''4) |
    si'4 dod'' red'' |
    mi''2 mi''4 |
    fad'' sol''( fad'')\trill |
    mi''2. |
    sol''4 \appoggiatura fad''8 mi''4 \appoggiatura re''16 dod''4 |
    \appoggiatura dod''16 red''2 mi''4~ |
    mi''8 sol' sol'4( fad')\trill |
    mi'4
  }
  \tag #'recit0 {
    <>^\markup\character Zuliman
    mi4 fad sol |
    red2.\trill |
    mi4 do' la |
    si2 si4 |
    r mi re |
    dod red mi |
    si,2~ si,8 si, |
    mi2. |
    mi'4 re' do' |
    \appoggiatura do'16 si2. |
    do'4 la sol |
    fad2\trill fad4 |
    r sol fa |
    mi mi do |
    re2~ re8 re |
    sol,4 sol sol |
    \appoggiatura sol16 fad4 fad sol |
    \appoggiatura fad16 mi2 la4 |
    re2 re4 |
    re' dod'\trill si |
    mi2 mi4 |
    mi fad2 |
    si,2. |
    r4 fad fad |
    sol sol mi |
    si2 mi'4 |
    red'4.\trill do'8( si4) |
    sol4 la si |
    do'2 sol4 |
    la si( si,) |
    mi2. |
    mi4 la la |
    la2 sol4 |
    la si( si,) |
    mi4
  }
>>
%%
<<
  \tag #'(recit0 basse) {
    \tag #'basse { \clef "vbasse" <>^\markup\character Zuliman }
    r8 sol sol sol la si |
    red4\trill red8 red red4 red8 fad |
    si, si, r si si\trill si si si |
    do' la fad\trill fad fad sol |
    \appoggiatura fad16 mi4 mi r2 |
    R1 |
  }
  \tag #'recit1 { r4 r2 | R1*2 R2. R1*2 }
>>
