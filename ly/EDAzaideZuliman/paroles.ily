\tag #'(recit0 basse) {
  Au nom de nos ten -- dres ar -- deurs,
  ou -- bli -- ez sa ja -- lou -- se ra -- ge ;
  Ne vous van -- gez de ses fu -- reurs,
  qu’en m’ai -- mant d’a -- van -- ta -- ge.
  Ne vous van -- gez de ses fu -- reurs,
  qu’en m’ai -- mant d’a -- van -- ta -- ge.
  
  Je suis é -- pris de vos at -- traits
  au -- tant qu’on le peut ê -- tre ;
  Mon feu ne sçau -- roit croî -- tre,
  ny s’af -- foi -- blir ja -- mais :
  Mon feu ne sçau -- roit croî -- tre,
  ny s’af -- foi -- blir ja -- mais.
}

Li -- vrons nos cœurs à la ten -- dres -- se,
ne for -- mons que d’heu -- reux de -- sirs ;
Li -- vrons nos cœurs à la ten -- dres -- se,
ne for -- mons que d’heu -- reux de -- sirs ;
Ai -- mons- nous, \tag #'recit0 { ai -- mons- nous } sans ces -- se,
comp -- tons nos jours par nos plai -- sirs.
Ai -- mons- nous, ai -- mons- nous sans ces -- se,
comp -- tons nos jours par nos plai -- sirs.
Comp -- tons nos jours par nos plai -- sirs.

\tag #'(recit0 basse) {
  Que tout si -- gnale i -- cy nos ar -- deurs mu -- tu -- el -- les,
  qu’on offre à nos re -- gards les Fê -- tes les plus bel -- les.
}
