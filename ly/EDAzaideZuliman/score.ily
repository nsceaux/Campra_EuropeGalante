\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag#'recit1 \includeNotes "voix"
    >> \keepWithTag#'recit1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag#'recit0 \includeNotes "voix"
    >> \keepWithTag#'recit0 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*5\break s2.*6\break s2.*6\break
        s2.*8\break s2.*6\pageBreak
        s2.*9\break \grace s16 s2.*8\break
        \grace s16 s2.*9\break s2.*8\pageBreak
        s1*3 s4 \bar "" \break
      }
      \modVersion { s4 s2.*31\break }
    >>
  >>
  \layout { }
  \midi { }
}