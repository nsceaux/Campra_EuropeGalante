\clef "taille" si'4 |
la'4.\trill sol'8 fad'4 |
sol'2 mi'4 |
mi' re'4. re'8 |
re'2 mi'4 |
la re'2 |
re' re'4 |
mi'2 re'4 |
re' re' dod' |
re'2 re'4 |
do'2 si4 |
mi' re'2 |
re' re'4 |
do'2 do'4 |
si2 si4 |
re' mi'2 |
fad' sol'4 |
fad' fad'2\trill |
mi' mi'4 |
si' si'2 |
si'4 la' la' |
la'2 la'4 |
sol' si'2 |
la' la'4 |
si' la'4. la'8 |
la'2 si'4 |
la'4.\trill sol'8 fad'4 |
sol'2 mi'4 |
mi' re'4. re'8 |
re'2 mi'4 |
la re'2 |
re' re'4 |
mi'2 re'4 |
re' re' dod' |
re'2 re'4 |
do'4. do'8 si4 |
mi' re'2 |
re' re'4 |
do'4. re'8 mi'4 |
re'2 re'4 |
re' mi'2 |
fad' fad'4 |
fad'2 mi'4 |
mi'2 mi'4 |
mi' mi'2 |
mi'4 mi' fad' |
re'2 mi'4 |
re' re'2 |
re' re'4 |
mi' re'4. re'8 |
re'2 si'4 |
la'4.\trill sol'8 fad'4 |
sol'2 mi'4 |
mi' re'4. re'8 |
re'2 mi'4 |
la re'2 |
re' re'4 |
mi'2 re'4 |
re' re' dod' |
re'2 re'4 |
do'2 si4 |
mi' re'2 |
re'2. |
