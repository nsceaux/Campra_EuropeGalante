\clef "basse" sol4 |
re'4. re'8 do'4 |
si2 do'4 |
la re' re |
sol4. fad8 mi4 |
re sol,2 |
re, re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol, sol4 |
la4. si8 do'4 |
red2 mi4 |
re do2\trill |
si,4. la,8 sol,4 |
la, si,2 |
mi4. fad8 sol la |
si4 si,2 |
mi4 dod la, |
re4. mi8 fad4 |
sol mi2 |
la re4 |
sol, la,2 |
re, sol4 |
re'4. re'8 do'4 |
si2 do'4 |
la re' re |
sol4. fad8 mi4 |
re sol,2 |
re, re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re re, |
sol,2 sol4 |
do'4. si8 la4 |
fad2 sol4 |
fad mi2 |
re2 re4 |
si,2 mi4 |
la,4. si,8 do re |
mi4 mi,2 |
la,4 la fad |
sol4. fad8 mi4 |
si sol2 |
re' sol4 |
do re re, |
sol,2 sol4 |
re'4. re'8 do'4 |
si2 do'4 |
la re' re |
sol4. fad8 mi4 |
re sol,2 |
re, re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol,2. |
