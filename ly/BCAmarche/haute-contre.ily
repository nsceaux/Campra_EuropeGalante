\clef "haute-contre" re''4 |
re''4. re''8 la'4 |
si'2 sol'4 |
sol' fad'4.\trill fad'8 |
sol'2 sol'4 |
la' sol'2 |
fad'2\trill la'4 |
la'2 la'4 |
sol' re' mi' |
fad'2 fad'4 |
mi'2 re'4 |
sol' fad'2\trill |
sol' sol'4 |
mi'2 mi'4 |
la'2 sol'4 |
fad'4\trill mi'2 |
si' mi''4 |
mi'' red''4. red''8 |
mi''2 mi''4 |
mi'' red''2 |
mi'' mi''4 |
re'' re''4. do''8 |
si'4 mi''2 |
dod''2 re''4 |
re'' dod''4.\trill re''8 |
re''2 re''4 |
re''4. re''8 la'4 |
si'2 sol'4 |
sol' fad'4.\trill fad'8 |
sol'2 sol'4 |
la' sol'2 |
fad'\trill la'4 |
la'2 la'4 |
sol' re' mi' |
fad'2 fad'4 |
mi'2 re'4 |
sol' fad'4.\trill sol'8 |
sol'2 sol'4 |
sol'2 la'4 |
la'2 si'4 |
re''4 dod''2 |
re''4 la'2 |
la'4. la'8 sold'4 |
la'2 la'4 |
la' sold'2 |
la' la'4 |
sol'2 sol'4 |
sol' si'2 |
la'2\trill si'4 |
do'' la' re'' |
si'2\trill re''4 |
re''4. re''8 la'4 |
si'2 sol'4 |
sol' fad'4.\trill fad'8 |
sol'2 sol'4 |
la' sol'2 |
fad'2\trill la'4 |
la'2 la'4 |
sol' re' mi' |
fad'2 fad'4 |
mi'2 re'4 |
sol' fad'2\trill |
sol'2. |
