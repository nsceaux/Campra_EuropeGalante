\clef "quinte" re'4 |
re' re'4. re'8 |
re'2 do'4 |
do'4. si8 la4 |
si4. la8 sol4 |
fad\trill sol2 |
la la4 |
la2 la4 |
si re' sol |
fad2\trill la4 |
sol2 sol4 |
la la4.(\trill sol16 la) |
si2 sol4 |
do'4 la2 |
fad2 sol4 |
si do'2 |
fad2 mi4 |
do' si2 |
si si4 |
si si2 |
si4 dod'4. dod'8 |
re'2 re'4 |
re'4 sol mi' |
mi'2 re'4 |
re' la4. sol8 |
fad2\trill re'4 |
re' re'4. re'8 |
re'2 do'4 |
do'4. si8 la4 |
si4. la8 sol4 |
fad sol2 |
la la4 |
la2 la4 |
si re' sol |
fad2\trill la4 |
sol2 sol4 |
la la2\trill |
si si4 |
do'2 do'4 |
la2 sol4 |
la sol2 |
la re'4 |
re' si2\trill |
la2 la4 |
mi' mi'2 |
mi'4 do' la |
si2 si4 |
si re'2 |
re'4. do'8 si4 |
la4 la4.\trill sol8 |
sol2 re'4 |
re' re'4. re'8 |
re'2 do'4 |
do'4. si8 la4 |
si4. la8 sol4 |
fad\trill sol2 |
la la4 |
la2 la4 |
si re' sol |
fad2\trill la4 |
sol2 sol4 |
la la4.(\trill sol16 la) |
si2. |
