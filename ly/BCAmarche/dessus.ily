\clef "dessus" sol''4 |
fad''4.\trill sol''8 la''4 |
re''2 mi''4 |
do'' la' re'' |
si'\trill sol' do'' |
do'' si'2\trill |
la'\trill la'4 |
do''4. re''8 do''4 |
si' la'\trill sol' |
la'2 fad'4\trill |
sol'4. la'8 si'4 |
do'' la'2\trill |
sol' si'4 |
do''4. si'8 la'4 |
si'2 si'4 |
si' mi''2 |
red''4\trill si' si'' |
la''4.\trill sol''8 fad''4 |
sol''4. la''8 sol''4 |
fad'' si''2 |
sold''4 mi'' la'' |
fad''4.\trill sol''8 la''4 |
re'' sol''2 |
mi''2\trill fad''4 |
sol'' mi''4.\trill re''8 |
re''2 sol''4 |
fad''4.\trill sol''8 la''4 |
re''2 mi''4 |
do'' la' re'' |
si' sol' do'' |
do'' si'2\trill |
la'\trill la'4 |
do''4. re''8 do''4 |
si'4 la'\trill sol' |
la'2 fad'4\trill |
sol'4. la'8 si'4 |
do'' la'4.\trill sol'8 |
sol'2 re''4 |
mi''4. re''8 do''4 |
re''2 re''4 |
re'' sol''2 |
fad''4\trill re'' la' |
re''4. do''8 si'4 |
do''4. re''8 do''4 |
si' mi''2 |
dod''4 la' re'' |
si'4.\trill la'8 sol'4 |
re'' sol''2 |
fad''2\trill sol''4 |
la'' fad''4.\trill sol''8 |
sol''2 sol''4 |
fad''4.\trill sol''8 la''4 |
re''2 mi''4 |
do'' la' re'' |
si'\trill sol' do'' |
do'' si'2\trill |
la'\trill la'4 |
do''4. re''8 do''4 |
si' la'\trill sol' |
la'2 fad'4\trill |
sol'4. la'8 si'4 |
do'' la'2\trill |
sol'2. |
