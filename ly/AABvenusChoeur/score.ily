\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'venus \includeNotes "voix"
    >> \keepWithTag #'venus \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*8\pageBreak
        s1*7\break s1*8\pageBreak
        s1*4\break s1*7\pageBreak
        s1*8\break s1*7\pageBreak
        s1*8\break s1*5\pageBreak
        \grace s8 s1*3 s2. \bar "" \pageBreak
        s4 s1*5\pageBreak
        s1*7\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*5 s2. \bar "" \pageBreak
        s4 s1*5 s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
