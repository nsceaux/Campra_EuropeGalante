\clef "haute-contre" r8 sol' |
sol'2. r8 re' |
re'2. r8 re' |
sol'2. r8 sol' |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. la'8 sol'4. sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'4. re''8 re''4. mi''8 |
re''4. re''8 mi''4. mi''8 |
re''4. si'8 si'4. si'8 |
mi''4. si'8 si'4. si'8 |
la'2 la' |
la' la' |
la'4. dod''8 dod''4. dod''8 |
re''4. re''8 re''4. re''8 |
re''2 dod'' |
re''2. r8 fad' |
la'4. la'8 mi'4. mi'8 |
sol'2 sol' |
sol'4. si'8 si'4. si'8 |
la'2 re'' |
re'' re'' |
re''4. re''8 si'4. si'8 |
si'4. sol'8 sol'4. sol'8 |
sol'4. fad'8 sol'4. sol'8 |
fad'4. fad'8 mi'4. sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'2. r8 re' |
re'2. r8 re' |
re'2. r8 re' |
si2. r8 si |
si2 re' |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 re'4. do'8 |
do'2 la |
si4. si'8\fortSug si'4. si'8 |
la'4. si'8\douxSug si'4. si'8 |
la'4. re''8\fortSug re''4. re''8 |
si'4. do''8 la'4. re''8 |
si'4.\trill re''8\douxSug re''4. re''8 |
si'4. do''8 la'4 re'' |
si'2 sol'4\douxSug sol' |
sol'2. sol'4 |
sol'2. sol'4 |
sol' sol' la' re' |
fad'4. la'8 la'4. la'8 |
si'4. sol'8 sol'4. sol'8 |
mi'2 fad'4. fad'8 |
la2 la4 la |
la2. la4 |
la4. fad'8\fortSug fad'4. la'8 |
la'4. fad'8\douxSug fad'4. la'8 |
la'4. dod''8\fortSug dod''4. dod''8 |
re''4. re''8 dod''4. re''8 |
re''4. dod''8\douxSug dod''4. dod''8 |
re''4. re''8 dod''4.\trill re''8 |
re''2. r8 re'\douxSug |
re'2. r8 re' |
re'2. r8 re' |
si2. r8 si |
si2 re' |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4 mi'8 mi' re'4 sol' |
re'2. re'4 |
re'2. r8 sol' |
sol'2. r8 re' |
re'2. r8 re' |
sol'2. r8 sol' |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. la'8 sol'4 sol' |
sol'2( fad')\trill |
sol'4. si'8\fortSug si'4. si'8 |
la'4. si'8\douxSug si'4. si'8 |
la'4.\trill re''8\fortSug re''4. re''8 |
si'4. do''8 la'4. re''8 |
si'4.\trill re''8\douxSug re''4 re'' |
si'4. do''8 la'4.\trill re''8 |
si'2.\trill r8 sol'\fortSug |
sol'2. r8 sol' |
do''4. do''8 do''4. do''8 |
si'4 si'8 si' si'4. si'8 |
la'4. la'8 si'4. si'8 |
la'4. la'8\douxSug si'4. si'8 |
la'4. la'8 la'4. la'8 |
sol'4. sol'8 mi'4. mi'8 |
si'2. si'4 |
si'4. si'8 si'4. do''8 |
si'4. si'8 si'4.\douxSug do''8 |
si'4. sol'8\fortSug sol'4. sol'8 |
do''4. do''8 la'4. la'8 |
la'2( sold'4.) la'8 |
la'4. do''8\fortSug do''4. mi''8 |
mi''4. do''8\douxSug do''4. mi''8 |
mi''4. si'8\fortSug si'4. mi''8 |
do''4. re''8 si'4.\trill la'8 |
la'4. si'8\douxSug si'4. mi''8 |
do''4. re''8 si'4.\trill la'8 |
la'2 la'4\fortSug la' |
la'2 la'4 la' |
sol'2. si'4 |
do''4 sol' do'' re'' |
re''4. la'8 la'4. la'8 |
si'4. si'8 si'4. si'8 |
la'2 la'4 la' |
mi'2 mi'4 re' |
re'2( dod'4.) re'8 |
re'4. fad'8 fad'4. la'8 |
la'2 la' |
la' la' |
la'4. dod''8 dod''4. dod''8 |
re''4. re''8 re''4. re''8 |
re''2 dod''4.\trill re''8 |
re''2. r8 fad' |
sol'2. r8 re' |
fad'2. r8 re' |
sol'2. r8 sol' |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'4 la'8 la' sol'4. sol'8 |
sol'2( fad'4.) sol'8 |
sol'1 |
