\tag #'(venus basse) {
  Fra -- pez, fra -- pez, ne vous las -- sez ja -- mais :
  Qu’à vos tra -- vaux l’E -- cho ré -- pon -- de.
  Pour le fils de Ve -- nus, for -- gez de nou -- veaux traits,
  qu’ils por -- tent dans les cœurs une at -- tein -- te pro -- fon -- de.
  Fra -- pez, fra -- pez, ne vous las -- sez ja -- mais,
  vous tra -- vail -- lez pour le bon -- heur du mon -- de.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Fra -- pons, Fra -- pons, ne nous las -- sons ja -- mais :
  Qu’à nos tra -- vaux l’E -- cho ré -- pon -- de.
  Fra -- pons, Fra -- pons, ne nous las -- sons ja -- mais :
  Qu’à nos tra -- vaux l’E -- cho ré -- pon -- de.
  Qu’à nos tra -- vaux l’E -- cho ré -- pon -- de.
  Pour le fils de Ve -- nus, for -- geons de nou -- veaux traits ;
  qu’ils por -- tent dans les cœurs une at -- tein -- te pro -- fon -- de.
  Fra -- pons, fra -- pons, ne nous las -- sons ja -- mais,
  nous tra -- vail -- lons pour le bon -- heur du mon -- de.
}
