\clef "basse" r8 sol |
sol2. r8 sol |
re2. r8 re |
mi4 mi8 fad sol4. mi8 |
si2 si |
si4. si,8 si,4. si,8 |
do4. la,8 si,4. do8 |
re2 re, |
sol,4. sol8 sol4. do'8 |
si4. si8 do'4. do'8 |
sol4. sol8 sol4. sol8 |
mi4. mi8 mi4. mi8 |
la2 la |
la la |
la4. fad8 fad4. fad8 |
si4. sol8 sol4. sol8 |
la2 la, |
re2. r8 re' |
la4. la8 do'4. do'8 |
sol2 sol4. sol8 |
si4. si8 si4. sol8 |
re'2 re' |
re' re' |
re'4. re'8 mi'4. mi'8 |
si4. si8 do'4. do'8 |
sol4. la8 si4. do'8 |
re'4. re8 mi4. do8 |
re2 re, |
sol,2. r8 sol |
sol2. r8 sol |
re4. re8 re4. re8 |
mi4 mi8 fad sol4. mi8 |
si2 si |
si4. si,8 si,4. si,8 |
do4. la,8 si,4. do8 |
re2 re, |
sol,4. sol8\fortSug sol4. sol8 |
re4. sol8\douxSug sol4. sol8 |
re4. re8\fortSug re4. re8 |
mi4. do8 re4 re, |
sol,4. re8\douxSug re4. re8 |
mi4. do8 re4 re, |
sol,1 |
sol2. mi4 |
si,2. si,4 |
do si, la, sol, |
re4. re'8 re'4. re'8 |
si4. si8 sol4. sol8 |
la2 fad4. fad8 |
dod2 dod4 re |
la,1 |
re,4. re8\fortSug re4. re8 |
la,4. re8\douxSug re4. re8 |
la,4. la8\fortSug la4. la8 |
si4. sol8 la4 la, |
re4. la8^\douxSug la4. la8 |
si4. sol8 la4 la, |
re2. r8 re\douxSug |
sol2. r8 sol |
re4. re8 re4. re8 |
mi4 mi8 fad sol4. mi8 |
si2 si |
si4. si,8 si,4. si,8 |
do4. do8 do4. do8 |
la,4 la,8 la, si,4. do8 |
re2 re, |
sol,2. r8 sol |
sol2. r8 sol |
re4. re8 re4. re8 |
mi4 mi8 fad sol4. mi8 |
si2 si |
si4. si8 si4. si8 |
do'4. la8 si4. do'8 |
re'2 re |
sol,4. sol8\fortSug sol4. sol8 |
re'4. sol8\douxSug sol4. sol8 |
re'4. re8\fortSug re4. re8 |
mi4. do8 re4 re, |
sol,4. re8\douxSug re4. re8 |
mi4. do8 re4 re, |
sol,2. r8 sol\fortSug |
mi2. r8 mi |
do4. do8 do4. do8 |
sol4 sol8 la si4. sol8 |
re'2 re' |
re' re'^\douxSug |
re'4. re'8 re'4. re'8 |
sol4. sol8 do'4. la8 |
si2 si, |
mi4. mi8 mi4. la8 |
sold4. mi8 mi4.^\douxSug la8 |
sold4. mi8^\fortSug mi4. mi8 |
do4. do8 fa4. re8 |
mi1 |
la,4. la8\fortSug la4. la8 |
mi'4. la8 la4.\douxSug la8 |
mi'4. mi8\fortSug mi4. mi8 |
fa4. re8 mi4. mi,8 |
la,4. mi8^\douxSug mi4. mi8 |
fa4. re8 mi4 mi, |
la,2 la4\fortSug la |
fad2\trill fad4 fad |
sol2. sol4 |
do'4 si la sol |
re'2. re'4 |
si4. si8 sol4. sol8 |
la2 fad4 fad |
dod2 dod4 re |
la,1 |
re4. re8 re4. re8 |
la2 la |
la la |
la4. fad8 fad4. fad8 |
si4. sol8 sol4. sol8 |
la2 la, |
re2. r8 re' |
sol2. r8 sol |
re4. re8 re4. re8 |
mi4 mi8 fad sol4. mi8 |
si2 si |
si,4. si8 si4. si8 |
do'4. do'8 do'4. do'8 |
la4 la8 la si4. do'8 |
re'2 re |
sol,1 |
