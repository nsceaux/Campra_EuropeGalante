\clef "quinte" r8 si |
si2. r8 sol |
la2. r8 la |
sol2 sol |
sol sol |
sol4. si8 si4. si8 |
sol4. do'8 si4. mi'8 |
re'2. re'4 |
re'4. si8 si4. do'8 |
re'4. re'8 do'4. do'8 |
si4. si8 si4. si8 |
si4. si8 sol4. sol8 |
la4. la8 re'4. la8 |
la4. la8 re'4. re'8 |
la4. la8 la4. la8 |
si4. si8 si4. si8 |
la2 la |
la2. r8 re' |
do'4. do'8 do'4. do'8 |
si4. si8 si4. si8 |
si2 re' |
re' re' |
re'4. re'8 sol'4. sol'8 |
re'4. fad'8 mi'4. mi'8 |
sol'4. re'8 do'4. do'8 |
re'4. la8 sol4. sol'8 |
re'4. re'8 re'4. do'8 |
do'2~ do'4. re'8 |
si2. r8 sol |
sol2. r8 sol |
la2. r8 fad |
si2. r8 sol |
sol2 sol |
sol4. si8 si4. si8 |
sol4. do'8 sol4. sol8 |
re2. re4 |
re4. sol8\fortSug sol4. sol8 |
la4. sol8\douxSug sol4. sol8 |
la4. la8\fortSug la4. re'8 |
sol4. sol8 re'4. re'8 |
re'4. la8\douxSug la4. re'8 |
sol4. sol8 re'4. re'8 |
re'2 si4\douxSug si |
si2. sol4 |
sol2. sol4 |
la si do' sol |
la4. re'8 re'4. re'8 |
re'4. re'8 si4. si8 |
dod'2 re'4 re |
mi2 mi4 la |
la2 mi |
fad4. la8\fortSug la4. la8 |
la4. la8\douxSug la4. la8 |
la4. la8\fortSug la4. la8 |
re'4. re'8 la4. la8 |
la4. la8\douxSug la4. la8 |
re'4. re'8 la4. la8 |
la2. r8 la\douxSug |
sol2. r8 sol |
la2. r8 fad |
sol2 sol |
sol sol |
sol4. sol8 sol4. sol8 |
sol4. mi8 mi4. mi8 |
la4 la8 la re4 re |
re2. re4 |
re2. r8 si |
si2. r8 sol |
la2. r8 la |
sol2 sol |
sol sol |
sol4. si8 si4. re'8 |
do'4. do'8 si4. mi'8 |
re'2. la4 |
si4. re'8\fortSug re'4. re'8 |
re'4. re'8\douxSug re'4. re'8 |
re'4. la8\fortSug la4. re'8 |
sol4. sol8 re'4. re'8 |
re'4. la8\douxSug la4. re'8 |
sol4. sol8 re'4. re'8 |
re'2. r8 si\fortSug |
si2. r8 sol |
sol4. do'8 do'4. do'8 |
re'4 re'8 re' re'4. re'8 |
re'4. re'8 sol'4. sol'8 |
re'4. re'8\douxSug sol'4. sol'8 |
re'4. re'8 re'4. re'8 |
re'4. re'8 do'4. do'8 |
si2. si4 |
sold4. sold8 sold4. la8 |
si4. sold8 sold4.\douxSug la8 |
si4. si8\fortSug si4. sol8 |
sol4. sol8 la4. la8 |
mi'2 si |
do' mi'\fortSug |
mi' mi'\douxSug |
mi'4. mi'8\fortSug mi'4. mi'8 |
la4. re'8 re'4 si |
do'4. mi'8\douxSug mi'4. mi'8 |
la4. re'8 re'4 si |
do'2 do'4\fortSug do' |
la2\trill la4 re' |
re'2. re'4 |
do'4 do' do' si |
re'4. re'8 re'4. re'8 |
re'4. si8 si4 mi' |
mi'2 la4 fad |
mi2 mi4 la |
la2 mi |
fad la |
la la |
la4. la8 re'4. re'8 |
la4. la8 la4. la8 |
si4. si8 si4. si8 |
la2 la |
la2. r8 re' |
re'2. r8 sol |
la2. r8 la |
sol2 sol |
sol sol |
sol4. si8 si4. re'8 |
do'4. do'8 do'4. do'8 |
do'4 do'8 do' si4. mi'8 |
re'2. la4 |
si1 |
