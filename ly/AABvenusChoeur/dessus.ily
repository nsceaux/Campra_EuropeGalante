\clef "dessus" <>^\markup\italic { Détaché & piqué }
_\markup\whiteout Violons
r8 re'' |
si'2.\trill r8 re'' |
fad'1\trill |
sol'4 sol'8 la' si'4. sol'8 |
re''1 |
r4 r8 re'' re''4. sol''8 |
mi''4. do''8 re''4. mi''8 |
si'2( la'4.)\trill sol'8 |
sol'2 sol'' |
sol'' sol'' |
sol''4. re''8 re''4. re''8 |
sol''4. sol''8 mi''4. mi''8 |
dod''4. la''8 la''4. la''8 |
mi''4. mi''8 fad''4. fad''8 |
dod''4. la''8 la''4. la''8 |
fad''4. si''8 si''4. si''8 |
mi''4. fad''8 sol''4. la''8 |
fad''4.\trill re''8 la'4. la'8 |
do''4. do''8 sol'4. sol'8 |
si'4. si'8 si'4. sol'8 |
re''4. re''8 re''4. sol''8 |
fad''2\trill la'' |
la''4. la''8 si''4. si''8 |
fad''4.\trill fad''8 sol''4. sol''8 |
re''4. re''8 mi''4. mi''8 |
si'4.\trill do''8 re''4. mi''8 |
la'4. re''8 sol'4. la'8 |
si'2( la'4.)\trill sol'8 |
sol'2. r8 sol' |
sol'2.\doux r8 re' |
re'2. r8 re' |
sol'2. r8 sol' |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. la'8 sol'4. sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'4. re''8\fort re''4. sol''8 |
fad''4.\trill re''8\doux re''4. sol''8 |
fad''4.\trill la''8\fort la''4. si''8 |
sol''4. la''8 la''4.\trill sol''8 |
sol''4. la''8\doux la''4. si''8 |
sol''4. la''8 la''4.\trill sol''8 |
sol''2 sol'4\doux sol' |
sol'2. sol'4 |
si'2. re''4 |
do'' sol' do'' si' |
la'4.\trill re''8 re''4. re''8 |
re''4. si'8 si'4. si'8 |
la'2 la'4. la'8 |
mi'2 mi'4 re' |
re'2( dod'4.) re'8 |
re'4. la'8\fort la'4. re''8 |
dod''4. la'8\doux la'4. re''8 |
dod''4. mi''8\fort mi''4. fad''8 |
re''4. mi''8 mi''4.\trill re''8 |
re''4. mi''8\doux mi''4. fad''8 |
re''4. mi''8 mi''4.\trill re''8 |
re''2. r8 fad'\doux |
sol'2. r8 re' |
re'2. r8 re' |
sol'2. r8 sol' |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'4 la'8 la' sol'4 sol' |
sol'2( fad'4.)\trill sol'8 |
sol'2. r8 re'' |
si'2.\trill r8 re'' |
fad'1\trill |
sol'4 sol'8 la' si'4. sol'8 |
re''2 re'' |
re''4. re''8 re''4. sol''8 |
mi''4. do''8 re''4. mi''8 |
si'2( la'4.)\trill sol'8 |
sol'4. re''8\fort re''4. sol''8 |
fad''4.\trill re''8\doux re''4. sol''8 |
fad''4.\trill la''8\fort la''4. si''8 |
sol''4. la''8 la''4.\trill sol''8 |
sol''4. la''8\doux la''4. si''8 |
sol''4. la''8 la''4.\trill sol''8 |
sol''2. r8 si'\fort |
si'2. r8 si' |
mi''4. mi''8 mi''4. mi''8 |
re''4 re''8 re'' re''4. sol''8 |
fad''4.\trill re''8 re''4. sol''8 |
fad''4.\trill re''8\doux re''4. sol''8 |
fad''4.\trill re''8 re''4. re''8 |
si'4.\trill si'8 mi''4. mi''8 |
mi''2 red'' |
mi'' mi'' |
mi'' mi''\doux |
mi''4. si'8\fort si'4. si'8 |
mi''4. mi''8 do''4. re''8 |
do''2( si'4.)\trill la'8 |
la'4. mi''8\fort mi''4. la''8 |
sold''4. mi''8\doux mi''4. la''8 |
sold''4.\trill mi''8\fort mi''4. mi''8 |
la''4. si''8 sold''4.\trill la''8 |
la''4. mi''8\doux mi''4. mi''8 |
la''4. si''8 sold''4.\trill la''8 |
la''2 do''4\fort do'' |
do''2 do''4 re'' |
si'2.\trill re''4 |
mi''4 mi'' fad'' sol'' |
fad''4.\trill re''8 re''4. re''8 |
sol''4. sol''8 mi''4 sol'' |
dod''2 la'4 re'' |
sol'2 sol'4 fad' |
fad'2( mi'4.)\trill re'8 |
re'4. la'8 la'4. re''8 |
dod''4. la''8 la''4. la''8 |
mi''4. mi''8 fad''4. fad''8 |
dod''4.\trill la''8 la''4. la''8 |
fad''4.\trill si''8 si''4. si''8 |
mi''4. fad''8 sol''4. la''8 |
fad''2.\trill r8 re'' |
si'2.\trill r8 re'' |
fad'1\trill |
sol'4 sol'8 la' si'4. sol'8 |
re''2 re'' |
re''4. re''8 re''4. sol''8 |
sol''4. mi''8 mi''4. mi''8 |
do''4 do''8 do'' re''4. mi''8 |
si'2( la'4.)\trill sol'8 |
sol'1 |
