<<
  \tag #'(venus vdessus basse) \clef "vbas-dessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
>>
%% Prélude
r4 |
R1*27
%% Vénus
<<
  \tag #'(venus basse) {
    <>^\markup\character Venus
    r2 r4 r8 re'' |
    si'2.\trill r8 re'' |
    fad'2\trill r |
    sol'4 sol'8 la' si'4. sol'8 |
    re''2 r |
    r4 r8 re'' re''4. sol''8 |
    mi''4. do''8 re''4. mi''8 |
    si'2( la')\trill |
    sol'2 r |
    R1*5 |
    r2 si'4 si' |
    si'2\trill si'4 do'' |
    re''2. re''4 |
    mi'' mi'' fad'' sol'' |
    fad''2.\trill r8 re'' |
    sol''4. sol''8 mi''4. sol''8 |
    dod''2\trill la'4 re'' |
    sol'2 sol'4 fad' |
    fad'2( mi')\trill |
    re'2 r |
    R1*5 |
    r2 r4 r8 re'' |
    si'2.\trill r8 re'' |
    fad'2\trill r |
    sol'4 sol'8 la' si'4. sol'8 |
    re''2 r |
    r4 r8 re'' re''4. sol''8 |
    \appoggiatura fad''8 mi''2 r |
    do''4 do''8 do'' re''4. mi''8 |
    si'2( la')\trill |
    sol'2.
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*38 r2 r4
  }
>>
%% Chœur
<<
  \tag #'venus { r4 R1*58 }
  \tag #'(vdessus basse) {
    <>^\markup\character Chœur
    r8 re'' |
    si'2.\trill r8 re'' |
    fad'2\trill r |
    sol'4 sol'8 la' si'4. sol'8 |
    re''2 r |
    r4 r8 re'' re''4. sol''8 |
    mi''4. do''8 re''4. mi''8 |
    si'2( la')\trill |
    sol'2 r |
    R1*5 |
    r2 r4 r8 si' |
    si'2. r8 si' |
    mi''2 r |
    re''4 re''8 re'' re''4. sol''8 |
    fad''2\trill r |
    R1 |
    r4 r8 re'' re''4. re''8 |
    si'4.\trill si'8 mi''4. mi''8 |
    mi''2( red'') |
    mi''2 r |
    R1 |
    r4 r8 si' si'4. si'8 |
    mi''4. mi''8 do''4. re''8 |
    do''2( si')\trill |
    la'2 r |
    R1*5 |
    r2 do''4 do'' |
    do''2 do''4 re'' |
    si'2.\trill re''4 |
    mi'' mi'' fad'' sol'' |
    fad''2.\trill re''4 |
    sol''4. sol''8 mi''4 sol'' |
    dod''2 la'4 re'' |
    sol'2 sol'4 fad' |
    fad'2( mi')\trill |
    re' r |
    R1*5 |
    r2 r4 r8 re'' |
    si'2.\trill r8 re'' |
    fad'2\trill r |
    sol'4 sol'8 la' si'4. sol'8 |
    re''2 r |
    r4 r8 re'' re''4. sol''8 |
    \appoggiatura fad''16 mi''2 r |
    do''4 do''8 do'' re''4. mi''8 |
    si'2( la')\trill |
    sol'1 |
  }
  \tag #'vhaute-contre {
    r8 sol' |
    sol'2. r8 sol' |
    la'2 r |
    sol'4 sol'8 la' re'4. sol'8 |
    sol'2 r |
    r4 r8 sol' sol'4. sol'8 |
    sol'4. la'8 sol'4. sol'8 |
    sol'2( fad') |
    sol'2 r |
    R1*5 |
    r2 r4 r8 sol' |
    sol'2. r8 sol' |
    sol'2 r |
    sol'4 sol'8 sol' sol'4. si'8 |
    la'2 r |
    R1 |
    r4 r8 fad' fad'4. fad'8 |
    sol'4. sol'8 sol'4. la'8 |
    sol'2( fad')\trill |
    mi'2 r |
    R1 |
    r4 r8 mi' mi'4. mi'8 |
    mi'4. mi'8 fa'4. fa'8 |
    mi'2.( si4) |
    do'2 r |
    R1*5 |
    r2 la'4 la' |
    la'2 la'4 la' |
    sol'2. sol'4 |
    sol'4 sol' la' si' |
    la'2\trill r4 la' |
    si'4. si'8 si'4 sol' |
    mi'2\trill fad'4 fad' |
    mi'2 mi'4 re' |
    re'2( dod') |
    re'2 r |
    R1*5 |
    r2 r4 r8 fad' |
    sol'2. r8 sol' |
    la'2 r |
    sol'4 sol'8 la' re'4. sol'8 |
    sol'2 r |
    r4 r8 sol' sol'4. sol'8 |
    sol'2 r |
    la'4 la'8 la' sol'4. sol'8 |
    sol'2( fad') |
    sol'1 |
  }
  \tag #'vtaille {
    r8 re' |
    re'2. r8 re' |
    re'2 r |
    si4 si8 la sol4. si8 |
    si2 r |
    r4 r8 re' re'4. re'8 |
    do'4. mi'8 re'4. sol'8 |
    re'1 |
    si2\trill r |
    R1*5 |
    r2 r4 r8 re' |
    mi'2. r8 si |
    mi'2 r |
    si4 si8 do' re'4. re'8 |
    re'2 r |
    R1 |
    r4 r8 re' re'4. re'8 |
    re'4. re'8 do'4. do'8 |
    si1 |
    si2 r |
    R1 |
    r4 r8 sol sol4. sol8 |
    do'4. do'8 la4. la8 |
    la2( sold) |
    la r |
    R1*5 |
    r2 mi'4 mi' |
    re'2 re'4 re' |
    re'2. si4 |
    do'4 sol do' re' |
    re'2 r4 re' |
    re'4. si8 si4. si8 |
    la2 la4 la |
    la2 la4 la |
    la1 |
    fad2\trill r |
    R1*5 |
    r2 r4 r8 re' |
    re'2. r8 re' |
    re'2 r |
    si4 si8 la sol4. si8 |
    si2 r |
    r4 r8 re' re'4. re'8 |
    do'2 r |
    do'4 do'8 do' si4. mi'8 |
    re'1 |
    si\trill |
  }
  \tag #'vbasse {
    r8 sol |
    sol2. r8 sol |
    re2 r |
    mi4 mi8 fad sol4. mi8 |
    si2 r |
    r4 r8 si si4. si8 |
    do'4. la8 si4.\trill do'8 |
    re'2( re) |
    sol2 r |
    R1*5 |
    r2 r4 r8 sol |
    mi2. r8 mi |
    do2 r |
    sol4 sol8 la si4. sol8 |
    re'2 r |
    R1 |
    r4 r8 re' re'4. re'8 |
    sol4. sol8 do'4. la8 |
    si1 |
    mi2 r |
    R1 |
    r4 r8 mi mi4. mi8 |
    do4. do8 fa4. re8 |
    mi1 |
    la,2 r |
    R1*5 |
    r2 la4 la |
    fad2\trill fad4 fad |
    sol2. sol4 |
    do'4 si la sol |
    re'2. re'4 |
    si4. si8 sol4. sol8 |
    la2 fad4 fad |
    dod2 dod4 re |
    la,1 |
    re2 r |
    R1*5 |
    r2 r4 r8 re' |
    sol2. r8 sol |
    re2 r |
    mi4 mi8 fad sol4. mi8 |
    si2 r |
    r4 r8 si si4. si8 |
    do'2 r |
    la4 la8 la si4. do'8 |
    re'2( re) |
    sol1 |
  }
>>