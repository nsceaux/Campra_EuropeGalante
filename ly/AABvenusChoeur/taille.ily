\clef "taille" r8 re' |
re'2. r8 si |
la2. r8 re' |
si2 si |
si re' |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 re'4. sol8 |
re'2~ re'4. re'8 |
si4. sol'8 sol'4. sol'8 |
sol'2 sol' |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
mi'4. mi'8 fad'4. fad'8 |
dod'4. dod'8 re'4. re'8 |
mi'4. fad'8 fad'4. fad'8 |
fad'4. sol'8 sol'4. sol'8 |
sol'4. fad'8 mi'4.\trill re'8 |
re'2. r8 re' |
mi'4. do'8 do'4. do'8 |
re'2 re' |
re'4. re'8 sol'4. sol'8 |
la'2 fad' |
fad'4. fad'8 sol'4. sol'8 |
la'4. la'8 sol'4. sol'8 |
sol'4. sol'8 mi'4. mi'8 |
re'4. do'8 si4. mi'8 |
re'4. re'8 si4. mi'8 |
re'2~ re'4. re'8 |
re'2. r8 si |
si2. r8 si |
la2. r8 la |
sol2. r8 sol |
sol2 si |
si4. re'8 re'4. re'8 |
do'4. do'8 si4. sol8 |
re'2. re'4 |
re'4. re'8\fortSug re'4. re'8 |
re'4. re'8\douxSug re'4. re'8 |
re'4. fad'8\fortSug fad'4. fad'8 |
sol'4. sol'8 fad'4.\trill sol'8 |
sol'4. fad'8\douxSug fad'4. fad'8 |
sol'4. sol'8 fad'4.\trill sol'8 |
sol'2 re'4\douxSug re' |
re'2. mi'4 |
re'2. sol'4 |
mi' re' do' re' |
re'4. fad'8 fad'4. fad'8 |
sol'4. re'8 mi'4. mi'8 |
mi'2 re'4 sol |
la2 mi4 fad |
la2. la4 |
la4. re'8\fortSug re'4. re'8 |
mi'4. re'8\douxSug re'4. re'8 |
mi'4. la'8\fortSug la'4. la'8 |
fad'4. sol'8 mi'4 la' |
fad'4.\trill la'8\douxSug la'4. la'8 |
fad'4. sol'8 mi'4 la' |
fad'2.\trill r8 re'\douxSug |
re'2. r8 si |
la2. r8 la |
sol2. r8 sol |
sol2 si |
si4. si8 si4. re'8 |
do'4. do'8 do'4. do'8 |
do'4 do'8 do' si4 sol |
do'2. re'4 |
si2.\trill r8 re' |
re'2. r8 si |
la2. r8 re' |
si2 si |
si re' |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 re'4. sol'8 |
re'2~ re'4. re'8 |
re'4. sol'8\fortSug sol'4. sol'8 |
la'4. sol'8\douxSug sol'4. sol'8 |
la'4. fad'8\fortSug fad'4. fad'8 |
sol'4. sol'8 fad'4.\trill sol'8 |
sol'4. fad'8\douxSug fad'4. fad'8 |
sol'4. sol'8 fad'4.\trill sol'8 |
sol'2. r8 re'\fortSug |
mi'2. r8 mi' |
mi'4. sol'8 sol'4. sol'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
la'4. la'8 sol'4. sol'8 |
la'4. fad'8\douxSug sol'4. sol'8 |
la'4. fad'8 fad'4. fad'8 |
sol'4. sol'8 sol'4. la'8 |
sol'2( fad'4.)\trill mi'8 |
mi'2 mi' |
mi'2 mi'\douxSug |
mi'4. mi'8\fortSug mi'4. mi'8 |
mi'4. mi'8 fa'4. fa'8 |
mi'1 |
mi'4. la'8\fortSug la'4. la'8 |
si'4. la'8\douxSug la'4. la'8 |
si'4. sold'8\fortSug sold'4. sold'8 |
la'4. fa'8 mi'4. mi'8 |
mi'4. sold'8\douxSug sold'4. sold'8 |
la'4. fa'8 mi'4. mi'8 |
mi'2 mi'4\fortSug mi' |
re'2 re'4 re' |
re'2. sol'4 |
sol'4 sol' la' si' |
la'4.\trill fad'8 fad'4. fad'8 |
re'4. sol'8 sol'4. sol'8 |
mi'2\trill re'4 la |
la2 la4 la |
la2. la4 |
la2 re'4. re'8 |
mi'4. mi'8 fad'4. fad'8 |
dod'4. dod'8 re'4. re'8 |
mi'4. fad'8 fad'4. fad'8 |
fad'4. sol'8 sol'4. sol'8 |
sol'4. fad'8 mi'4.\trill re'8 |
re'2. r8 re' |
re'2. r8 si |
la2. r8 re' |
si2 si |
si re' |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4 mi'8 mi' re'4. sol'8 |
re'2. re'4 |
re'1 |

