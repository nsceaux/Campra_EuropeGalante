\act Prologue
\markup\fill-line\fontsize#4 { OU PREMIERE ENTRÉE }
\markup\vspace#1
\sceneDescription\markup\wordwrap-center {
  Les Forges Galantes de l’Amour.
}
%% 1-1
\pieceToc "Ouverture"
\includeScore "AAouverture"
\newBookPart #'(full-rehearsal)

\scene "Scene premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Venus, les Graces, les Plaisirs, et les Ris.
}
%% 1-2
\pieceToc\markup\wordwrap {
  Venus, chœur :
  \italic { Frappez, frappez, ne vous lassez jamais }
}
\includeScore "AABvenusChoeur"
\newBookPart #'(full-rehearsal)
%% 1-3
\pieceToc\markup\wordwrap {
  Venus : \italic { C'est Vulcain qui fait le Tonnerre }
}
\includeScore "AACvenus"
\newBookPart #'(full-rehearsal)
%% 1-4
\pieceToc\markup\wordwrap { Premier air, pour les plaisirs }
\includeScore "AADair"
\newBookPart #'(full-rehearsal)
%% 1-5
\pieceToc "Menuets"
\includeScore "AAEmenuet"
\includeScore "AAFmenuet"
\newBookPart #'(full-rehearsal)
%% 1-6
\pieceToc\markup\wordwrap {
  Deux Graces : \italic { Souffrez que l'Amour vous blesse }
}
\includeScore "AAGair"
\reIncludeScore "AAFmenuet" "AAHmenuet"
\includeScore "AAIair"
\reIncludeScore "AAEmenuet" "AAJmenuet"
\newBookPart #'(full-rehearsal)
%% 1-7
\pieceToc\markup\wordwrap {
  Une Grace : \italic { C'est dans une tendresse extrême }
}
\includeScore "AAKgrace"
\newBookPart #'(full-rehearsal)
%% 1-8
\pieceToc "Gavotte"
\includeScore "AALgavotte"
\newBookPart #'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  La Discorde, Venus, & leur suite.
}
%% 1-9
\pieceToc\markup\wordwrap {
  Venus, la Discorde :
  \italic { Quelle soudaine horreur ! & quels terribles bruits ! }
}
\includeScore "ABAvenusDiscorde"
\newBookPart #'(full-rehearsal)

%% 1-10
\pieceToc\markup\wordwrap {
  Venus, chœur :
  \italic { Faisons regner l'Amour, faisons briller ses charmes }
}
\includeScore "ABBvenus"
\includeScore "ABCvenusChoeur"
\newBookPart #'(full-rehearsal)
%% 1-11
\pieceToc\markup\wordwrap {
  Loure pour les Ris et les Plaisirs
}
\includeScore "ABDloure"
\newBookPart #'(full-rehearsal)
%% 1-12
\pieceToc\markup\wordwrap {
  Une Grace : \italic { Ah ! que l’Amour }
}
\includeScore "ABEair"
\newBookPart #'(full-rehearsal)
\reIncludeScore "ABDloure" "EBFloure"
\newBookPart #'(full-rehearsal)
\includeScore "ABGair"
\newBookPart #'(full-rehearsal)
%% 1-13
\pieceToc "Canaries"
\includeScore "ABHcanaries"
\newBookPart #'(full-rehearsal)
%% 1-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Mortels, que l'Amour vous entraîne }
}
\includeScore "ABIchoeur"
%% 1-15
\pieceToc\markup\wordwrap {
  Venus : \italic { Commence à ressentir l’effet de ma vengeance }
}
\includeScore "ABJrecit"
\newBookPart #'(full-rehearsal)
%% 1-16
\pieceToc "Ouverture"
\reIncludeScore "AAouverture" "ABKouverture"
\actEnd "FIN DE LA PREMIERE ENTRÉE"
