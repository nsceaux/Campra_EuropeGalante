\clef "taille" sib4. do'8 re'4 |
re' la2 |
sib4. do'8 re'4 |
re' do'2\trill |
re' sol'8 la' |
sib'4 la'2\trill |
sol'4. la'8 sib'4 |
la' la'4.\trill sol'8 |
fa'2 fa'4 |
fa' re'2~ |
re'4 do' re' |
sib fa'4. fa'8 |
re'2.\trill |
re'\trill |
re'4. mi'8 fad'4 |
sol' sol'2 |
sol'4 mi'2\trill |
fa'4 do'4. re'8 |
mi'2 do'4 |
re' do'4. do'8 |
do'2 do'4 |
do' re'2 |
re'4 mi'2 |
mi'2. |
fa'2 fa'4 |
fa' sol'2 |
fa'4 mi' la' |
la' sol'4. la'8 |
fad'2 re'4 |
re'2 re'4 |
do'2 do'4 |
do'2 fa'8 mib' |
re'4 fa' sib' |
sib' la'2 |
la'4 fad' sol' |
do' sib do' |
la2\trill re'4 |
sib re' sol' |
fad' re' do' |
sib re' do' |
mib' re'4. re'8 |
re'2. |
re'2 re'4 |
re'2 re'4 |
do'2 do'4 |
do'2 fa'8 mib' |
re'4 fa' sib' |
sib' la'2 |
la'4 fad' sol' |
do' sib do' |
la2\trill re'4 |
sib re' sol' |
fad' re' do' |
sib re' do' |
mib' re'4. re'8 |
re'2. |
