\clef "dessus" sol'4. la'8 sib'4 |
la'4 re''2 |
sib'4.\trill la'8 sol'4 |
re'' sol''2 |
fad''4.\trill mi''8 re''4 |
sol'' la''4.(\trill sol''16 la'') |
sib''4. la''8 sol''4 |
fa'' fa''4.\trill mib''8 |
re''4.\trill mib''8 fa''4 |
do'' re''2 |
la'4\trill fa' sib'~ |
sib'8 do'' do''4.\trill sib'8 |
sib'2. |
sib' |
fa''4. sol''8 la''4 |
sib'' sol''4.\trill fa''8 |
mi''4.\trill re''8 do''4 |
fa'' la'4.\trill sib'8 |
do''4 sol' la' |
sib' sol'4.\trill fa'8 |
fa'2 la'4 |
la'4. si'8 do''4 |
si'4 mi''2 |
dod''4. si'?8 la'4 |
la''4. sol''8 fa'' mi'' |
re'' dod'' re''4. mi''8 |
fa''4 dod'' re''~ |
re''8 mi'' mi''4.\trill re''8 |
re''2 re''4 |
si'4. do''8 re''4 |
mib''4. re''8 do'' sib' |
la'4 fa''4. fa''8 |
fa''4 re'' sib'' |
sol'' la''4. sib''8 |
fad''4.\trill mi''8 re''4 |
la'4 sib' sol' |
fad'4.\trill sol'8 la'4 |
sib' la' do''~ |
do''4 sib'\trill la' |
re'' sib' mib'' |
la' la'4.\trill sol'8 |
sol'2. |
sol'2 re''4 |
si'4. do''8 re''4 |
mib''4. re''8 do'' sib' |
la'4 fa''4. fa''8 |
fa''4 re'' sib'' |
sol'' la''4. sib''8 |
fad''4.\trill mi''8 re''4 |
la'4 sib' sol' |
fad'4.\trill sol'8 la'4 |
sib' la' do''~ |
do''4 sib'\trill la' |
re'' sib' mib'' |
la' la'4.\trill sol'8 |
sol'2. |
