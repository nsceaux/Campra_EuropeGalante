\clef "haute-contre" sol'2 sol'4 |
re' re'2 |
re' re'4 |
fa' sol'2 |
re''4 la' sib'8 do'' |
re''4 re''2 |
re''2 re''4 |
do''4.\trill sib'8 la'4 |
sib'2 sib'4 |
la'\trill fa'2 |
fa' fa'4 |
sib' la'4.\trill sib'8 |
sib'2. |
sib' |
re''4. re''8 do''4 |
re'' re''2 |
do''4 sol'2 |
la'4. sol'8 fa'4 |
sol'2 fa'4 |
fa' mi'4.\trill fa'8 |
fa'2 fa'4 |
re'4. sol'8 fad'4 |
sol'4 sol'2 |
la' la'4 |
la'2 la'4 |
sib'4 sib'4. sib'8 |
la'2 la'4 |
re''4 dod''4.\trill re''8 |
re''2 fad'4 |
sol'4. la'8 si'4 |
do''4 sol'2 |
fa'4 la'4. la'8 |
sib'2 re''4 |
sib' do''4. sib'8 |
la'2 sib'4 |
re'4 re' do' |
re'2 re'4 |
re' fad' sol' |
la' sol' fad' |
sol'2 sol'4 |
sol' fad'4.\trill sol'8 |
sol'2. |
sol'2 fad'4 |
sol'4. la'8 si'4 |
do''4 sol'2 |
fa'4 la'4. la'8 |
sib'2 re''4 |
sib' do''4. sib'8 |
la'2 sib'4 |
re'4 re' do' |
re'2 re'4 |
re' fad' sol' |
la' sol' fad' |
sol'2 sol'4 |
sol' fad'4.\trill sol'8 |
sol'2. |
