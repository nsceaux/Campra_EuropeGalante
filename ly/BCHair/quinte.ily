\clef "quinte" re'2 sol4 |
la la2 |
sol4. la8 sib4~ |
sib sol do' |
la2\trill re4 |
re' do'2\trill |
sib sib4 |
do' la do' |
re'2 fa8 sol |
la4 sib2 |
do'4 la sib |
sol do' fa |
fa2. |
fa |
sib4. sib8 do'4 |
sib8 do' re'4. re'8 |
mi'4 do'2 |
do'4. sib8 la4 |
sol do' do' |
sib sib4. do'8 |
la2\trill la4 |
la4. sol8 la4 |
sol4 si2 |
la2 la4 |
la2 re'4 |
re'2 re4 |
la2 la4 |
la4 la4. la8 |
la2 la4 |
sol2 sol4 |
sol2 sol4 |
la4. sol8 fa4 |
sib2 sib4 |
mib'2 mib'4 |
re'2 sol4 |
la sol sol |
re'2 la4 |
sol la sol |
re' sib do'\trill |
re' sol sol |
do' do' la |
sib2. |
sib2 la4 |
sol2 sol4 |
sol2 sol4 |
la4. sol8 fa4 |
sib2 sib4 |
mib'2 mib'4 |
re'2 sol4 |
la sol sol |
re'2 la4 |
sol la sol |
re' sib do'\trill |
re' sol sol |
do' do' la |
sib2. |
