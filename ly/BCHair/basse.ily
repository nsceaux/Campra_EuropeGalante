\clef "basse" sol2 sol4 |
sol fad2 |
sol2. |
fa4 mib2 |
re4. do8 sib, la, |
sol,4 fad,2 |
sol,2 sol4 |
la2 fa4 |
sib2 re8 mib |
fa4 sib,2 |
mib2 re4 |
mib fa fa, |
sib, sib8 do' sib la |
sib,2. |
sib4. sib8 la4 |
sol8 la sib4 sol |
do'4. sib8 la sol |
fa2 fa4 |
mi2 fa4 |
sib, do do, |
fa,2 fa4 |
fad4. mi8 re4 |
sol4 mi2 |
la4. sol8 fa mi |
re2 re8 do |
sib,4. la,8 sol,4 |
la,4. sol,8 fa, mi, |
re,4 la,2 |
re, re4 |
sol2 sol4 |
do4. re8 mib4 |
fa4. mib8 re do |
sib,4 sib sol |
mib'4. re'8 do'4 |
re'4. do'8 sib4 |
fad sol mib |
re4. mi8 fad4 |
sol re mi |
fad sol la |
sib sib, do |
do re re, |
sol, sol8 fa sol la |
sol,4 sol fad |
sol2 sol4 |
do4. re8 mib4 |
fa4. mib8 re do |
sib,4 sib sol |
mib'4. re'8 do'4 |
re'4. do'8 sib4 |
fad sol mib |
re4. mi8 fad4 |
sol re mi |
fad sol la |
sib sib, do |
do re re, |
sol,2. |
