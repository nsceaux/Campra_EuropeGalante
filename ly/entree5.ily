\newBookPart#'()
\act Cinquième Entrée
\markup\fill-line\fontsize#4 { LA TURQUIE }
\markup\vspace#1
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente les Jardins du Serail du Grand Seigneur,
  & dans le fond, l’Appartement des Sultanes.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Zaïde.
}
%% 5-1
\pieceToc\markup\wordwrap {
  Zaïde : \italic { Mes yeux ne pourrez-vous jamais }
}
\includeScore "EAAzaide"
\newBookPart#'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Zuliman, Roxane.
}
%% 5-2
\pieceToc\markup\wordwrap {
  Roxane, Zuliman :
  \italic { Quoy pour d’autres appas vôtre ame est enflamée }
}
\includeScore "EBAzulimanRoxane"
\newBookPart#'(full-rehearsal)

\scene "Scene III" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Zuliman, Roxane, Zayde & les autres Sultanes.
}
%% 5-3
\pieceToc "Passacaille"
\includeScore "ECApassacaille"
\newBookPart#'(full-rehearsal)
%% 5-4
\pieceToc\markup\wordwrap {
  Zayde, les Sultanes :
  \italic { Que l’Amour dans nos cœurs fasse naistre }
}
\includeScore "ECBair"
\newBookPart#'(full-rehearsal)
%% 5-5
\pieceToc\markup\wordwrap {
  Zumiman, Zayde, Roxane :
  \italic { Vous brillez seule en ces retraites }
}
\includeScore "ECCrecit"
\newBookPart#'(full-rehearsal)

\scene "Scene IV" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Zuliman, Zayde, & les autres Sultanes.
}
%% 5-6
\pieceToc\markup\wordwrap {
  Zayde, Zumiman :
  \italic { Au nom de nos tendres ardeurs }
}
\includeScore "EDAzaideZuliman"
\newBookPart#'(full-rehearsal)

\scene "Scene V" "Scene V"
\sceneDescription\markup\center-column {
  \wordwrap-center { Zuliman, Zayde, & les autres Sultanes. }
  \wordwrap-center {
    Les Bostangis ou Jardinier du Grand Seigneur,
    forment plusieurs Jeux, suivant leur caractere.
  }
}
%% 5-7
\pieceToc "Marche des Bostangis"
\includeScore "EEAmarche"
%% 5-8
\pieceToc\markup\wordwrap {
  Le chef des Bostangis, chœur :
  \italic { Vivir, vivir, gran Sultana }
}
\includeScore "EEBchoeur"
\includeScore "EECchoeur"
\newBookPart#'(full-rehearsal)
\markup\fill-line {
  \line { Le Bostangi Bacchi repete \italic Vivr, & le Chœur aussi. }
} \noPageBreak
\includeScore "EEDchoeur"
\newBookPart#'(full-rehearsal)
\markup\fill-line { \line { Ensuite on reprend la Marche. } } \noPageBreak
\reIncludeScore "EEAmarche" "EEEmarche"
\newBookPart#'(full-rehearsal)
%% 5-9
\pieceToc\markup\wordwrap {
  Le Bostangi, chœur :
  \italic { Bello como star un flor }
}
\includeScore "EEFchoeur"
%% 5-10
\pieceToc "Premier air, pour les Bostangis"
\includeScore "EEGair"
\newBookPart#'(full-rehearsal)
%% 5-11
\pieceToc "Deuxième air, pour les mesmes"
\includeScore "EEHair"
%% 5-12
\pieceToc\markup\wordwrap {
  Le Bostangi, chœur : \italic { Star contento }
}
\includeScore "EEIchoeur"
\markup\fill-line { \line { On reprend le second Air. } } \noPageBreak
\reIncludeScore "EEHair" "EEJair"
\markup\fill-line {
  \line {
    Le Bostangi recommence \italic { Star Contento }
  }
} \noPageBreak
\includeScore "EEKchoeur"
\newBookPart#'(full-rehearsal)

\scene "Scene Sixième, et Derniere" "Scene VI"
\sceneDescription\markup\wordwrap-center {
  Venus, La Discorde, & les Acteurs de la Scene précedente.
}
\pieceToc\markup\wordwrap {
  La Discorde, Venus :
  \italic { C’en est trop, Déesse inhumaine }
}
\includeScore "EFArecit"
\actEnd "FIN DE LA CINQUIÈME ET DERNIERE ENTRÉE"
