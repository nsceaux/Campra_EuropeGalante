\newBookPart#'()
\act Deuxième Entrée
\markup\fill-line\fontsize#4 { LA FRANCE }
\markup\vspace#1
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente un Boccage, & dans le fonds un Hameau.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Silvandre & Philene.
}
%% 2-1
\pieceToc\markup\wordwrap {
  Philene, Silvandre : \italic {
    Quoy ? pour l'objet de vôtre ardeur
  }
}
\includeScore "BAAphileneSilvandre"
\newBookPart #'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Cephise.
}
%% 2-2
\pieceToc\markup\wordwrap {
  Cephise : \italic {
    Paisibles lieux, agréables retraites
  }
}
\includeScore "BBAcephise"
\newBookPart #'(full-rehearsal)

\scene "Scene III" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Cephise, Troupe de Bergers, de Bergeres, & de Pastres
  portant des fleurs & des fruits à Cephise,
  qui l’interrompent par leurs danses.
}
%% 2-3
\pieceToc "Marche"
\includeScore "BCAmarche"
\newBookPart #'(full-rehearsal)
%% 2-4
\pieceToc\markup\wordwrap {
  Cephise : \italic { Que voy-je ? quel spectacle ! }
}
\includeScore "BCBrecit"
%% 2-5
\pieceToc\markup\wordwrap {
  Chœur : \italic { Aimez, aimez, belle Bergere }
}
\includeScore "BCCchoeur"
\newBookPart #'(full-rehearsal)
%% 2-6
\pieceToc\markup\wordwrap {
  Une Bergere : \italic { Soûpirez, jeunes cœurs }
}
\includeScore "BCDair"
\markup\fill-line {
  \line {
    On reprend le chœur, \italic { Aimez belle Bergere, }
    jusqu’à la premiere cadence.
  }
}\noPageBreak
\includeScore "BCEchoeur"
%% 2-7
\pieceToc\markup\wordwrap {
  Une Bergere : \italic { Aimons dans la jeune saison }
}
\includeScore "BCFair"
\markup\fill-line {
  \line {
    On reprend le chœur, \italic { Aimez belle Bergere, }
    tout du long.
  }
}\noPageBreak
\includeScore "BCGchoeur"
\newBookPart #'(full-rehearsal)
%% 2-8
\pieceToc "Premier Air"
\includeScore "BCHair"
%% 2-9
\pieceToc "Deuxième Air"
\includeScore "BCIair"
\newBookPart #'(full-rehearsal)
%% 2-10
\pieceToc\markup\wordwrap {
  Un Berger : \italic { Soupirons tous }
}
\includeScore "BCJair"
%% 2-11
\pieceToc "Rigaudons"
\includeScore "BCKrigaudon"
\newBookPart #'(full-rehearsal)
\includeScore "BCLrigaudon"
%% 2-12
\pieceToc "Passepieds"
\includeScore "BCMpassepied"
\includeScore "BCNpassepied"
\newBookPart #'(full-rehearsal)
%% 2-13
\pieceToc\markup\wordwrap {
  Cephise : \italic { Que je sçache du moins d’où me vient cet hommage }
}
\includeScore "BCOrecit"

\scene "Scene IV" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Silvandre, Cephise.
}
%% 2-14
\pieceToc\markup\wordwrap {
  Silvandre, Cephise :
  \italic { Voyez à vos genoux cet Amant empressé }
}
\includeScore "BDArecit"
%\newBookPart #'(full-rehearsal)

\scene "Scene V" "Scene V"
\sceneDescription\markup\wordwrap-center { Doris }
%% 2-15
\pieceToc\markup\wordwrap {
  Doris : \italic { Quel funeste coup pour mon ame }
}
\includeScore "BEAdoris"
\newBookPart #'(full-rehearsal)
%% 2-16
\pieceToc "Marche des Bergers"
\reIncludeScore "BCAmarche" "BEBmarche"
\actEnd "FIN DE LA DEUXIÈME ENTRÉE"
