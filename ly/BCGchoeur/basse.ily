\clef "basse" sol4 |
do'2. |
si4 do' la |
sol2 sol4 |
sol4. la8 si4 |
fad sol2 |
re' re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol, sol4 |
do2 do4 |
sol2 sol4 |
do8 si, do re do4 |
sol,2 \clef "alto" <>_"B.C." sol'4 |
sol'2 mi'4 |
la'2. |
fad'4 mi' re' |
dod'2 dod'4 |
re'4. mi'8 fad'4 |
sol' la'2 |
re'2 \clef "bass" re'4 |
sol4. la8 si4 |
do'4 do' la |
si2 si4 |
sol4. fad8 mi4 |
la,4 si,2 |
mi mi4^"Bassons" |
la,2 la,4 |
mi2 mi4 |
la8 sold la si la4 |
mi2 <>^"B.C." mi4 |
la4. si8 do'4 |
re' mi' fa' |
mi'2 mi4 |
do4. si,8 la,4 |
re,4 mi,2 |
la,2 la4 |
re'2 \clef "alto" re'4 |
sol'2. |
sol'4 fad' sol' |
re'2 re'4 |
\clef "bass" sol4. la8 si4 |
fad sol2 |
re' re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol, <>^"Bassons" sol4 |
do'2 do'4 |
sol2 sol4 |
do8 si, do re do4 |
sol,2 <>^"B.C." sol4 |
sol4. sol8 fad4 |
mi la2 |
re' re4 |
si,4. la,8 sol,4 |
fad, sol,2 |
re, re4 |
la4. sol8 fad4 |
sol4 fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol,
