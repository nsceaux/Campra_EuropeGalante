\clef "dessus" re''4 |
mi''2. |
re''4 mi'' do'' |
re''4. do''8( si'4) |
si'4. do''8 re''4 |
do'' si'2\trill |
la'\trill la'4 |
do''4. re''8 do''4 |
si' la' sol' |
la'4. sol'8( fad'4) |
sol'4. la'8 si'4 |
do'' la'2\trill |
sol' \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    mi''4. re''8 do''4 |
    re''8 do'' re'' mi'' re''4 |
    mi'' mi''2\trill |
    re''2 re''4 |
    re''2 sol''4 |
    mi''2.\trill |
    fad''4 sol'' fad'' |
    mi''2\trill mi''4 |
    fad''4. sol''8 fad''4 |
    mi'' mi''2\trill |
    re'' }
  { si'4 |
    do''4. si'8 la'4 |
    si'8 la' si' do'' si'4 |
    do'' do''2\trill |
    si'2\trill si'4 |
    si'2 mi''4 |
    dod''2.\trill |
    re''4 dod'' re'' |
    la'2 la'4 |
    la' la' re'' |
    re'' dod''2\trill |
    re'' }
  { s4^"Hautbois" s2.*3 s2 s4^"Tous" }
>> re''4 |
si'4.\trill do''8 re''4 |
mi'' mi'' fad'' |
red''2\trill si'4 |
mi''4. red''8 mi''4 |
fad''4 red''2\trill |
mi''2 <>^"Hautbois" \twoVoices #'(dessus1 dessus2 dessus) <<
  { si'4 |
    do''4. si'8 la'4 |
    si'8 la' si' do'' si'4 |
    do'' do''2\trill |
    si'2\trill }
  { sold'4 |
    la'4. sold'8 fad'4 |
    sold'8 fad' sold' la' sold'4 |
    la' la'2\trill |
    sold'2 }
>> <>^"Tous" si'4 |
do''4. re''8 mi''4 |
fa'' mi'' la'' |
sold''4.\trill fad''8( mi''4) |
la'4. si'8 do''4 |
re''4 si'4.\trill la'8 |
la'2 do''4 |
la'2\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    re''2. |
    re''4 do'' si' |
    la'2\trill la'4 | }
  { la'4 |
    si'2. |
    si'4 la' sol' |
    fad'2\trill fad'4 | }
>>
si'4. do''8 re''4 |
do''4 si'2\trill |
la'2\trill la'4 |
do''4. re''8 do''4 |
si' la' sol' |
la'4. sol'8( fad'4) |
sol'4. la'8 si'4 |
do'' la'2\trill |
sol' <>^"Hautbois" \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    mi''4. re''8 do''4 |
    re''8 do'' re'' mi'' re''4 |
    mi'' mi''2\trill |
    re'' }
  { si'4 |
    do''4. si'8 la'4 |
    si'8 la' si' do'' si'4 |
    do'' do''2\trill |
    si' }
>> <>^"Tous" re''4 |
re''4. mi''8 fad''4 |
sol''4 mi'' la'' |
fad''4.\trill mi''8( re''4) |
sol'4. la'8 si'4 |
do'' si'2\trill |
la'2\trill la'4 |
do''4. re''8 do''4 |
si'4 la' sol' |
la'4. sol'8( fad'4) |
sol'4. la'8 si'4 |
do''4 la'2\trill |
sol'2
