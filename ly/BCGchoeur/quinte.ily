\clef "quinte" re'4 |
do'2. |
re'4 do' do' |
si2\trill si4 |
si4. la8 sol4 |
re re'2 |
re'2 re'4 |
do'4. sol8 la4 |
si re' mi' |
la2 la4 |
sol4. fad8 si4 |
la la2\trill |
si r4 |
R2.*3 |
r4 r sol' |
sol'2 mi'4 |
la'2. |
fad'4 mi' re' |
dod'2\trill dod'4 |
re'4. mi'8 fad'4 |
sol' la'2 |
re'2 re'4 |
re'4. re'8 re'4 |
do' do' do' |
si2 si4 |
si4. fad'8 si4 |
la4 fad si |
si2 r4 |
R2.*3 |
r4 r mi' |
do'4. si8 mi'4 |
re' do' la |
mi'2 si4 |
do'4. re'8 mi'4 |
re'4 re' si |
do'2 do'4 |
re'2 re'4 |
sol'2. |
sol'4 fad' sol' |
re'2 re'4 |
si4. la8 sol4 |
re re'2 |
re' re'4 |
do'4. sol8 la4 |
si re' mi' |
la2 la4 |
sol4. fad8 si4 |
la la2\trill |
si2 r4 |
R2.*3 |
r4 r re' |
re'2 re'4 |
mi' mi'2 |
re' re'4 |
re'4. do'8 si4 |
la re2 |
re re'4 |
do'4. sol8 la4 |
si4 re' mi' |
la2 la4 |
sol4. fad8 si4 |
la la2\trill |
si2
