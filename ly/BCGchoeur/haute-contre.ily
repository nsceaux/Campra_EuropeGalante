\clef "haute-contre" si'4 |
do''2. |
sol'4 sol' la' |
si'4. la'8( sol'4) |
sol'4. fad'8 sol'4 |
la' sol'2 |
fad'\trill la'4 |
la'4. si'8 la'4 |
sol' re' sol' |
fad'2\trill fad'4 |
mi'4. mi'8 re'4 |
sol' fad'2\trill |
sol'2 r4 |
R2.*3 |
r4 r sol' |
sol'2 mi'4 |
la'2. |
fad'4 mi' re' |
dod'2\trill dod'4 |
re'4. mi'8 fad'4 |
sol' la'2 |
re' la'4 |
sol'4. sol'8 sol'4 |
sol' sol' la' |
fad'2\trill si'4 |
si'4. la'8 si'4 |
do'' si'2 |
sold'2\trill r4 |
R2.*3 |
r4 r sold' |
la'4. re''8 do''4 |
la' mi'' re''\trill |
mi''2 sold'4 |
la'4. sold'8 la'4 |
la'4 sold'2\trill |
la'2 la'4 |
fad'2\trill re'4 |
sol'2. |
sol'4 fad' sol' |
re'2 re'4 |
sol'4. fad'8 sol'4 |
la' sol'2 |
fad'2\trill la'4 |
la'4. si'8 la'4 |
sol' re' sol' |
fad'2\trill fad'4 |
mi'4. mi'8 re'4 |
sol'4 fad'2\trill |
sol' r4 |
R2.*3 |
r4 r si' |
si'4. dod''8 re''4 |
re'' re'' dod'' |
re''2 fad'4 |
sol'4. fad'8 sol'4 |
la' sol'2 |
fad'2\trill la'4 |
la'4. si'8 la'4 |
sol'4 re' sol' |
fad'2\trill fad'4 |
mi'4. mi'8 re'4 |
sol' fad'2\trill |
sol'
