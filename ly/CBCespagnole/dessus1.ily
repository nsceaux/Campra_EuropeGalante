\clef "dessus" R1. |
fa''2 sol'' lab'' |
sol''1.\trill |
sol''2 la'' sib'' |
la''1\trill sib''2~ |
sib'' la''2. la''4 |
la''2 sol''2. sol''4 |
sol''2 fa'' fa'' |
fa'' fa'' mi''\trill |
fa''1. |
fa''2 sol'' lab'' |
sol''1.\trill |
mib''2 fa'' sol'' |
do''1 re''2~ |
re'' do''2.\trill sib'4 |
sib'1 r2 |
R1.*5 |
sib'2 do'' re'' |
sol'1. |
fa' |
fa''2 sol'' lab'' |
sib''1 sib''2 |
sib'' r fa'' |
sol'' fa''2.\trill mib''4 |
re''1.\trill |
fa''2 sol'' lab'' |
sol''1.\trill |
sol''2 la'' sib'' |
la''1\trill sib''2~ |
sib'' la''2.\trill sib''4 |
sib''2 r r |
R1.*3 | \allowPageTurn
r2 sol'' sol'' |
do'''1 r2 |
r2 fa'' fa'' |
sib''1 r2 |
sol'' la'' sib'' |
do'''1 sib''2 |
la''2.\trill sol''4 fa''2~ |
fa'' fa'' mi'' |
fa''1. |
R1. |
r2 r la'' |
mi'' fad'' sol'' |
fad''\trill re'' fad'' |
fad'' mi'' fad'' |
sol'' la'' sib'' |
re''1 re''2 |
re''1 mib''2 |
lab'' sol''2. sol''4 |
sol''1 sol''2 |
la''1 la''2 |
la'' sib'' do''' |
fa''1 mib''2 |
do'' sib'2. sib'4 |
sib'1 r2 |
r2 fa'' sol'' |
lab''1 lab''2 |
sol'' fa'' fa'' |
fa'' r r |
r mib'' fa'' |
sib' do'' re'' |
do''2\trill do''2. fa''4 |
re''1.\trill |
R1.*5 | \allowPageTurn
sib'2 do'' re'' |
sol'1. |
fa' |
fa''2 sol'' lab'' |
sib''1 sib''2 |
sib'' r fa'' |
sol'' fa''2.\trill mib''4 |
re''1.\trill |
fa''2 sol'' lab'' |
sol''1.\trill |
sol''2 la'' sib'' |
la''1\trill sib''2~ |
sib'' la''2.\trill sib''4 |
sib''1. |
