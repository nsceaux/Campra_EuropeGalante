\score {
  <<
    \new GrandStaff \with { instrumentName = "Flutes" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = #-1 \includeLyrics "paroles" }
  >>
  \layout { indent = \largeindent }
}
