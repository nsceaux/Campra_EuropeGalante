\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { instrumentName = "Flutes" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Une Espagnole
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basse-Continue" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*10\break s1.*11\break s1.*10\pageBreak
        s1.*11\break s1.*10\break s1.*10\pageBreak
        s1.*9\break s1.*10\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
