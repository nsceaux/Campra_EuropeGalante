\clef "vbas-dessus" R1.*15 |
re''2 mib'' fa'' |
sib'1 r2 |
do'' re'' mib'' |
la' sib' do'' |
fad'1 sol'2~ |
sol' sol' fad' |
sol'1. |
mib''2 fa'' sol'' |
\appoggiatura { fa''16[ mib'' re''] } do''1. |
re''2 mib'' fa'' |
sib' lab'( sol'4) sol' |
sol'2 r sib'~ |
sib' sib' la' |
sib'1. |
R1.*5 |
re''2 sol' sol' |
mi'1\trill r2 |
do''2. sib'4 la'2 |
la' sol'\trill fa' |
do''1 do''2 |
r \afterGrace do''2( fa'8) fa'2 |
fa'1. |
r2 \afterGrace re''2( sol'8) sol'2 |
sol'1. |
do''2 re'' mi'' |
fa''1 la'2\( |
la'\) la'2.\trill sol'8[ fa'] |
fa'1. |
fa''2 mi'' re'' |
dod''( la') re''\( |
re''\) re'' dod'' |
re''1. |
R1. |
r2 r re'' |
la' si' do'' |
si' sol' do'' |
do'' do'' si' |
do'' do'' r |
r r fa'' |
do'' re'' mib'' |
re'' sib' mib'' |
mib'' mib'' re'' |
mib'' mib'' r |
r re'' mib'' |
fa'' fa' sib' |
sib' sib' la' sib' sib' r |
R1.*3 |
re''2 mib'' fa'' |
sib'1 r2 |
do'' re'' mib'' |
la' sib' do'' |
fa'1\trill sol'2~ |
sol' sol' fad' |
sol'1. |
mib''2 fa'' sol'' |
\appoggiatura { fa''16[ mib'' re''] } mib''1. |
re''2 mib'' fa'' |
sib' lab'( sol'4) sol' |
sol'2 r sib'~ |
sib' sib' la' |
sib'1. |
R1.*6 |
