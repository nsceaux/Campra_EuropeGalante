\clef "dessus" re''2 mib'' fa'' |
sib'1. |
mib''2 fa'' sol'' |
do''1. |
do''1 re''2~ |
re'' do'' fa'' |
re''1\trill re''2 |
do''1 do''2 |
sib'1 sib'2 |
la'1.\trill |
re''2 mib'' fa'' |
sib'1. |
sol''2 la'' sib'' |
la''1\trill sib''2~ |
sib'' la''2.\trill sib''4 |
sib''1 r2 |
R1.*6 |
do''2 re'' mib'' |
la' sib' do'' |
fa'1. |
re''2 mib'' fa'' |
sib' r re'' |
do'' do''2.\trill sib'4 |
sib'1. |
re''2 mib'' fa'' |
sib'1. |
mib''2 fa'' sol'' |
do''1 re''2~ |
re'' do''2.\trill sib'4 |
sib'2 r r |
R1.*3 |
r2 mi'' mi'' |
la''1 r2 |
r re'' re'' |
sol''1 r2 |
mi''2 fa'' sol'' |
la''1 sol''2 |
fa'' do''1 |
re''2 do''2.\trill sib'4 |
la'1.\trill |
R1.*7 |
r2 r sol'' |
re'' mi'' fa'' |
mi''\trill do'' mi'' |
fa''1 fa''2 |
fa''1. |
r2 r sib'' |
fa'' sol'' lab'' |
sol''1\trill r2 |
r2 sib' sib' |
sib'1 sib'2 |
do''2 do''2.\trill do''4 |
re''2 fa'' sol'' |
lab''1. |
sol''2 la''! sib'' |
sib'' la''2.\trill sib''4 |
sib''1. |
R1.*6 |
do''2 re'' mib'' |
la' sib' do'' |
fa'1. |
re''2 mib'' fa'' |
sib' r re'' |
do'' do''2.\trill sib'4 |
sib'1. |
re''2 mib'' fa'' |
sib'1. |
mib''2 fa'' sol'' |
do''1 re''2~ |
re'' do''2.\trill sib'4 |
sib'1. |
