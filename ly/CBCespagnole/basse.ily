\clef "basse" sib1. |
re |
mib |
mi! |
fa1 mib2~ |
mib fa1 |
sib,2 sib1 |
la1. |
sol |
fa2 sol la |
re1. |
mib2 fa sol |
do1. |
fa1 sib,2~ |
sib, fa,1 |
sib lab2 |
sol la sib |
do1. |
re1 mib2 |
re1 sib,2 |
do re1 |
sol2 la sib |
mib1. |
fa2 sol la |
sib1 lab2 |
sol re1 |
mib2 r re |
mib fa fa, |
sib do' re' |
re1. |
mib2 fa sol |
do1 do2 |
fa1 mib2~ |
mib fa fa, |
sib, si,1 |
do2 do' sib |
la2. sol4 fa2 |
fa mi re |
do do sib, |
la,1. |
sib,2 sib, la, |
sol,1. |
do2 do' sib |
la sib do' |
re' la1 |
sib2 do' do |
fa fa mi |
re do sib, |
la, sol, fa, |
sol, la,1 |
re re'2 |
la sib do' |
sib la sol |
fad1. |
sol1 mib2 |
fa sol1 |
do2 do' sib |
la1.~ |
la2 sol fa |
sib lab sol |
lab sib sib, |
mib sol la |
sib1. |
re |
mib2 fa fa, |
sib, re mib |
fa do re |
mib1 re2 |
mib fa fa, |
sib1 lab2 |
sol la sib |
do1. |
re1 mib2 |
re1 sib,2 |
do re1 |
sol2 la sib |
mib1. |
fa2 sol la |
sib1 lab2 |
sol re1 |
mib2 r re |
mib fa fa, |
sib do' re' |
re1. |
mib2 fa sol |
do1 do2 |
fa1 mib2~ |
mib fa fa, |
sib,1. |
