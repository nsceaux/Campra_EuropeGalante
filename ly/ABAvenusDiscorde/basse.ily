\clef "basse" re,2 re4. re8 |
sold,2 la,4. la,8 |
do2~ do4.*13/12 do32 si, la, |
si,2~ si,8 si, si, si, |
dod2~ dod8 la la la |
re'4.*13/12 mi'32 re' do' si4.*13/12 la32 sol fad |
mi2 la4. la8 |
la2 sold |
la4. la,8 re4. re8 |
mi8.*5/6 fad32 mi re dod8.*5/6 re32 dod si, la,8.*5/6 si,32 la, sol, fad,8.*5/6 sol,32 fad, mi, |
re,2~ re,8 re si, si, |
mi2 red4. red8 |
mi2~ mi8 mi mi mi |
dod4. dod8 la, la, la, la, |
fad,4.*13/12( fad32 fad fad) fad4 fad |
red2~ red8 si, si, si, |
mi4.*13/12 fad32 mi re dod8.*5/6 re32 dod si, la,4 |
re8.*5/6 mi32 re dod si,8.*5/6 si,32 la, sol, fad,2 |
sol,8 sol, sol, sol, sold, sold, sold, sold, |
la, la, la, la, sib, sib, sib, sib, |
sold, sold, sold, sold, la, la, la, la, |
re,2 re8^\fortSug re re re |
re4 fad, la,8 la, la, la, |
la8.*5/6 si32 la sol fad8.*5/6 sol32 fad mi re4.^\doux dod8 |
si,4. sol,8 la,2 |
re,2~ re,4. re8 |
sol8. mi16 la8. re16 la,2 |
la4 sol8 fad mi4 mi, |
si,1 |
sold,2 la, |
mi mi,4 |
la,2 la4 |
fad2. |
lad,2.~ |
lad,4. si,8 fad,2 |
si,2 si8 la |
sol4 fad8. sol16 mi4 |
la8. sol16 fad8. mi16 re8 dod |
si, sol, la,2 |
re,2. |
re8.*5/6 re32 re re re8. re16 dod8.*5/6 dod32 dod dod dod8. dod16 |
si,8.*5/6 si,32 si, si, si,8. si,16 la,8.*5/6 la,32 la, la, la,8. la,16 |
re,8.*5/6\douxSug re32 re re re8. re16 dod8.*5/6 dod32 dod dod si,8.*5/6 si,32 si, si, |
sold,2. |
la,4 red2 |
mi dod |
re1 |
sol4 fad8. mi16 re2 |
fad,4 sol, la, |
fad,1 |
sol,2 sol4. mi8 |
si4. dod'8 re' re |
la2 la,8 si, dod la, |
re2 re4 |
sol fad mi |
re2 re8 mi |
fad2 fad,8 sol, |
la,2 la,4 |
sold,8 fad, mi, fad, sold, mi, |
la, sold, la, si, dod re |
mi4 mi,2 |
la, la4 |
fad4. fad8 fad mi |
re2 re'8 dod' |
si4 lad si |
fad4. mi8 red4 |
mi8 re dod4\trill si, |
fad sold lad |
si re mi |
fad fad,2 |
si, si,4 |
mi4. fad8 sol mi |
la4 fad sol |
mi2. |
re2 re8 mi |
fad sol fad mi re4 |
sol fad sol |
la re sol, |
la,2. |
re,2 re8 mi re dod |
si,1~ |
si,2 lad, |
si,4. la,8 sol,4 |
fad,1 |
si,2. |
red4 mi2 |
sol,8 la, si,4 mi,2 |
mi dod |
re2. |
sol2 la |
fad4 si,4. sol,8 |
la,4. sol,8 fad, mi,16 re, la,4 |
re, re do2 |
si, la, |
sol, sol4 |
mi do re re, |
sol,2. |
sol4 fad2 |
mi1~ |
mi8. re16 dod8. si,16 la,2 |
fad,4 mi, re,2 |
re4 sol8. fad16 mi8. re16 |
la,4~ la,8.*5/6 si,32 la, sol, fad,8.*5/6 sol,32 fad, mi, |
re,4 re si, |
do2 si,4 |
sold,8 la, mi4 mi, |
la,4\fortSug~ la,16 re' dod' si la si la sol fad sol fad mi |
<< re2 { s4. s8\douxSug } >> si,4 mi, |
fad,8^\fortSug fad re4 mi fad8 fad, |
<< si,2 { s4 s8 s^\douxSug } >> sold,2 |
la,4\fortSug~ la,16 re dod si, la,4~ la,16 si, la, sol, |
<< fad,2. { s4 s^\douxSug } >> |
sol,2 la, |
re,1 |
