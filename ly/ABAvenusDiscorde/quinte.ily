\clef "quinte" re2 re4. re8 |
si,2 mi4. mi8 |
fad2~ fad8 fad fad fad |
sol2~ sol8 sol sol sol |
mi2~ mi8 mi' mi' mi' |
fad'2~ fad'8 si si si |
si2 la4 dod' |
mi'2~ mi'8 mi' mi' mi' |
mi'8.*5/6 fad'32 mi' re' dod'8.*5/6 re'32 dod' si la4 si |
si8 mi mi mi la4. la8 |
la8.*5/6 si32 la sol fad8.*5/6 sol32 fad mi re4 fad |
mi sold fa4. fa8 |
mi2 mi4. mi8 |
mi2. r8 la |
dod'2~ dod'8 fad fad fad |
fad2~ fad8 si si si |
si2 la4.*13/12 si32 la sol |
fad8.*5/6 sol32 fad mi re4~ re8 re re re |
re2 mi4. mi8 |
mi2 re~ |
re sol4. sol8 |
fad4 r fad8\fortSug fad fad fad |
fad4 r la8 la la la |
la2 la4\douxSug sol8 fad |
fad4. mi8 mi4.\trill re8 |
re2 r8 la la8. la16 |
si8. si16 la8. la16 la2~ |
la mi'4. mi'8 |
re'2. si4 |
si mi mi2 |
mi4 mi4. mi8 |
mi2 dod'4 |
dod'2 fad4 |
fad2.~ |
fad2 fad4. fad8 |
fad2 re'8 do' |
si4 la8. sol16 si4 |
dod'8. si16 la2 |
re'8 re la4. la8 |
la2. |
r8 r16 fad32 fad fad8. fad16 mi8.*5/6 mi32 mi mi mi8. mi16 |
re8.*5/6 re32 re re re8. re16 la4 la8. la16 |
la4\douxSug fad8. fad16 mi4 si8. si16 |
si2 mi4 |
mi8 la fad4 si |
si2 mi4. mi8 |
fad1 |
si4 re8. sol16 fad2 |
fad4 re la |
la2. la4 |
si2 si4. si8 |
si4. la8 re' la |
dod'2 r |
r4 r la |
si fad sol |
re'2 re4 |
la4 la la |
la2 la4 |
mi2. |
mi2 mi4 |
mi2 mi4 |
mi2 mi'4 |
la4. la8 la mi' |
la4 re'4. lad8 |
si4 dod' re' |
fad'2 fad4 |
si lad fad |
fad si fad |
si si si |
fad2. |
fad2 fad4 |
sol2 si4 |
dod' re' re' |
sol2. |
la2 re4 |
la2 la4 |
sol re' re' |
la la sol~ |
sol mi4. mi8 |
fad2 r |
r re'4. re'8 |
re'4 si dod'2 |
si2 si4 |
dod' lad fad2 |
fad4 si2 |
si4 sol2 |
mi8. fad16 fad8.\trill mi16 mi2 |
mi~ mi |
fad la4 |
sol4. si8 dod'2 |
la4 si4. si8 |
la2 la8 mi'16 la la8. la16 |
fad2~ fad4 la |
sol2 la |
re2 sol4 |
sol2 re4. re8 |
re2. |
sol4 la fad |
sold1~ |
sold4 la8. re16 la2 |
la8 fad sol mi fad2 |
la4 si mi8. fad16 |
la4 r r |
r fad sol |
sol4. do'8 sold4 |
mi mi'4. mi'8 |
dod'4\fortSug la la4. la8 |
<< la2 { s4. s8\douxSug } >> si4 si, |
fad4.\fortSug si8 si8. dod'16 dod'8.\trill si16 |
<< si2. { s4 s8 s\douxSug } >> mi4 |
mi la dod'4~ dod'16 re' dod' si |
<< la2 { s4 s\douxSug } >> la4 |
sol mi mi la |
la1 |
