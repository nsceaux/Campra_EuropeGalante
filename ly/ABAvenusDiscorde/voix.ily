\clef "vbas-dessus" R1*21 |
\ffclef "vbas-dessus" <>^\markup\character Venus
la'8 la'16 la' re''8. re''16 fad'4 r |
fad'8 fad'16 re' la'8. re''16 dod''4\trill r |
mi'' r la'8 la' si' dod'' |
re''4 fad'8 si' mi'4\trill fad'8 sol' |
fad'2\trill
\ffclef "vbas-dessus" <>^\markup\character La Discorde
r8 la'16 la' re''8 re''16 re'' |
si'8\trill si'16 mi'' dod''8\trill dod''16 re'' \appoggiatura re''8 mi'' mi'' r la' |
mi'\trill mi' mi' fad' sol' sol'16 la' si'8 si'16 dod'' |
re''4 r8 re''16 re'' fad'8 fad'16 fad' si'8. si'16 |
mi'8 mi' r mi'' mi''4 dod''8 re''16 mi'' |
sold'4.\trill mi'16 mi' si'8 dod''16 re'' |
dod''4\trill r8 mi'' dod''16\trill dod'' dod'' dod'' |
fad''8 fad'' lad'4\trill lad'8 lad'16 dod'' |
fad'8 fad' r fad' fad' fad' |
dod''4 dod''8 re'' lad'4.\trill lad'16 si' |
si'4 fad'8 fad'16 fad' sol'8 la' |
si' si'16 dod'' re''8 re''16 si' mi''4 |
dod''\trill r8 la' si' dod'' |
re''4. re''8 re'' dod'' |
re''2. |
R1*2 |
r8 fad'16 fad' fad'8 fad'16 re' la'8 la'16 la' si'8 si'16 si' |
mi'8 mi' r16 si' si' si' mi''8. mi''16 |
dod''8\trill r16 la' la'4 la'8 la'16 sold' |
sold'4 r8 mi'16 mi' la'8. sol'?16 sol'8 sol'16 fad' |
fad'4\trill r8 la' la' la' re'' re'' |
si'\trill si'16 dod'' re''8 re''16 mi'' fad''8. fad''16 r8 la' |
la'8. si'16 sol'8.\trill fad'16 mi'8 la' |
re'4
\ffclef "vbas-dessus" <>^\markup\character Venus
r16 re'' re'' re'' la'8 la'16 la' do''8 do''16 si' |
si'8\trill si' r sol' si' si' si' dod'' |
re'' re'' re'' mi'' fad''8. sol''16 |
mi''4.\trill mi''8 r2 |
r4 r re''8 dod'' |
si'4 la' sol' |
fad'2\trill fad'8 sol' |
la'2 la'8 si' |
mi'2\trill mi''4 |
mi''2.~ |
mi''2 re''8 dod'' |
si'2\trill si'8 mi'' |
dod''4\trill la' dod'' |
re''4. re''8 re'' mi'' |
fad''2 fad''8 mi'' |
re''4 dod'' si' |
lad'2 fad'8 fad' |
sold'4 lad' si' |
lad'\trill si' dod'' |
fad' si'4. dod''8 |
re''4( dod''2)\trill |
si'2 si'4 |
sol''4. fad''8 mi'' re'' |
dod''4\trill la' si' |
sol'4.\trill fad'8 sol'4 |
fad'2\trill fad'8 sol' |
la'4 la' re'' |
si'\trill la' si' |
mi' fad'4. sol'8 |
fad'4( mi'2)\trill |
re'2 r |
r r8 fad'16 fad' si'8 si'16 dod'' |
re''8. re''16 re'' re'' mi'' fad'' dod''8.\trill dod''16 lad' lad' lad' dod'' |
fad'4 fad'8 fad'16 fad' si'8. dod''16 |
lad'8 lad'
\ffclef "vbas-dessus" <>^\markup\character La Discorde
r16 fad' fad' fad' dod''8 r16 dod'' dod'' dod'' re'' mi'' |
red''8 red'' r si' si'16 si' si' si' |
fad'8\trill fad'16 fad' sol'8 sol'16 mi' si'8 si' |
mi''8 mi'' mi'' red'' mi'' mi'' r4 |
\ffclef "vbas-dessus" <>^\markup\character Venus
sold'8 sold'16 sold' sold'8. la'16 la'8. la'16 la' sol' sol' fad' |
fad'4\trill r8 la'16 la' re''8 re''16 re'' |
si'8\trill re''16 re'' sol''8 sol''16 sol'' mi''8\trill mi'' r la'16 la' |
re''8 re''16 fad' sol'8 sol'16 si' sol'8\trill sol'16 sol' |
mi'4\trill la'16 la' si' dod'' re''8 mi''16 fad'' mi''4\trill |
re'' r8 la' fad'\trill fad' fad' fad' |
sol'4 sol'8 sol' do''4 do''8 si' |
si'4\trill r8 re''16 re'' sol''8 sol''16 si' |
do''4. do''16 si' la'4\trill la'8 si' |
sol'4 r8 sol'16 la' si'8 si'16 sol' |
re''8 re''16 re'' red''4\trill red''8. mi''16 |
mi''4 mi''8 r
\ffclef "vbas-dessus" <>^\markup\character La Discorde
mi''4 sold'16 sold' sold' si' |
mi'8 mi'16 mi' la'8 la'16 si' \appoggiatura si'16 dod''4
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 la' |
re'' re''16 re'' mi''8 fad''16 sol'' fad''8\trill fad'' r re'' |
la'16 la' la' re'' si'\trill si' dod'' re'' dod''8.\trill re''16 |
mi''4 r r |
\ffclef "vbas-dessus" <>^\markup\character La Discorde
r la'16 la' la' re'' sol'8 sol'16 sol' |
mi'8.\trill mi'16 r mi' mi'32[ fad' sol' la']( si'16) si' si' si' |
mi''8 r16 mi'' sold'8\trill sold' sold' la' |
la' la' r4 r2 |
r8 la' re''16 re'' re'' fad'' si'8 si'16 si' mi''8 mi''16 mi'' |
lad'4\trill r r2 |
r8 fad'' re''16 re'' re'' fad'' si'8 si'16 si' mi''8 mi''16 si' |
\appoggiatura si'16 dod''8 dod'' r4 r2 |
r16 la' la' la' re''8. do''16 do''8. si'16 |
si'8\trill si' r16 sol' sol' si' mi'8.\trill mi'16 mi'8. fad'16 |
re'1 |
