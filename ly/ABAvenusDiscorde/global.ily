\key re \major \beginMark "Prélude pour la Discorde"
\digitTime\time 2/2 \midiTempo#120 s1*21
\time 4/4 \midiTempo#80 s1*9
\digitTime\time 3/4 s2.*4
\time 4/4 s1
\digitTime\time 3/4 s2.*5
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#160 s2 \beginMark "Air" s4 s2.*25
\time 4/4 \midiTempo#80 s1 s2 \beginMark "Recitatif" s2 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.*5
\time 4/4 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1*2 \bar "|."
