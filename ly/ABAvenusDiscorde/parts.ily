%{
\piecePartSpecs
#`((dessus #:score-template "score-basse-voix")
   (haute-contre #:score-template "score-basse-voix")
   (taille #:score-template "score-basse-voix")
   (quinte #:score-template "score-basse-voix")
   (basse #:score-template "score-basse-voix")
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#114 #}))
%}
\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#114 #}))
