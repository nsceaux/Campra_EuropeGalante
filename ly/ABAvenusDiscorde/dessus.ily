\clef "dessus" re'2~ re'4.*13/12 re'32 mi' fad' |
mi'2~ mi'4.*13/12 mi'32 fad' sol' |
fad'2~ fad'8 re' re' re' |
sol'2~ sol'4.*13/12 sol'32 la' si' |
la'4. sol'8 sol'4.\trill fad'8 |
fad'4 re' re''2~ |
re''4.*13/12 re''32 dod'' si' dod''4.*13/12 dod''32 si' la' |
si'4. si'8 si'4 dod''8 re'' |
dod''8.\trill mi''16 la''8.*5/6 sol''32 fad'' mi'' fad''8.*5/6 sol''32 fad'' mi'' re''8.*5/6 mi''32 re'' dod'' |
si'8 mi'' mi'' mi'' mi''2~ |
mi''8 la' la' la' re''4. re''8 |
re''4. dod''16 si' do''4. do''8 |
do''4. si'8 si'4.\trill la'8 |
la'2. r8 la' |
lad'2\trill r8 lad' lad' lad' |
si'4. la'8 la'4. si'8 |
sold'4\trill mi' la'4. la'8 |
la'4 re' re''4.*13/12 dod''32 si' la' |
si'8.*5/6 do''32 si' la' sol'8.*5/6 la'32 sol' fad' mi'4. mi'8 |
mi'2~ mi'8 mi' mi' mi' |
mi'4. mi'8 mi'4.\trill re'8 |
re'4 r la'8.*5/6\fort si'32 la' sol' fad'8.*5/6 sol'32 fad' mi' |
re'4 r mi''8.*5/6 fad''32 mi'' re'' dod''8.*5/6 re''32 dod'' si' |
la'4 la'8.*5/6 si'32 la' sol' fad'8\doux fad' sol' la' |
re'4 re'8 re' re'4 dod'\trill |
re'2 r8 fad'' fad''8. fad''16 |
re''8 re''16 sol'' mi''8\trill mi''16 fad'' dod''2\trill~ |
dod''4 dod''8 red'' mi''8. fad''16 sol''8 mi'' |
fad''2. fad''4 |
si''4. si''8 mi''2~ |
mi''4 sold'4. la'8 |
la'2 la'4 |
lad' dod''4. dod''8 |
dod''2. |
fad'4 fad''4. fad''8 dod''8.\trill dod''16 |
re''4. re''8 mi'' fad'' |
sol'' mi'' fad'' fad''16 re'' sol''4 |
mi''8.\trill mi''16 la''8. sol''16 fad''8\trill mi'' |
re'' sol'' mi''4.\trill re''8 |
re''2. |
r8 r16 fad'32 fad' fad'8. fad'32 re' la'8. dod''32 dod'' dod''8. dod''32 la' |
re''8.*5/6 re''32 mi'' fad'' sol''8.*11/12 fad''32*1/2 mi'' re'' dod'' si' dod''8.*5/6 re''32 dod'' si' la'8.*5/6 si'32 la' sol' |
fad'8\doux la' la'8. la'16 dod''8. dod''16 fad'8. fad'16 |
si'4 mi'4. mi'8 |
mi'4 fad'2 |
mi'4 si' dod''4. dod''8 |
la'2 fad'4. fad'8 |
re'8. mi'16 fad'8. sol'16 la'2 |
re''4 re'' dod''8.\trill re''16 |
re''2. re''4 |
re''4 si' sol''4. sol''8 |
fad''4. mi''8 re''8. mi''16 |
dod''2\trill r |
r4 r fad''8 mi'' |
re''4 re'' dod''\trill |
re''2 re''8 dod'' |
re''4 la' re'' |
dod''8 re'' dod'' si' dod'' la' |
si' la' sold' la' si' sold' |
la'4 mi' la' |
la' sold'4. la'8 |
la'2 mi''4 |
fad''4. fad''8 fad'' dod'' |
re''2 re''8 mi'' |
fad''4 mi'' re'' |
dod''2\trill fad''4 |
si' mi'' re'' |
dod''\trill mi''4. dod''8 |
re''4 fad' si' |
si'( lad'4.) si'8 |
si'2 si''4 |
si''4. la''8 sol'' si'' |
mi''4\trill fad'' re'' |
re'' dod''4.\trill re''8 |
re''2 la''8 sol'' |
fad''4 fad''4. fad''8 |
re''4 re''4. mi''8 |
dod''4\trill re'' re'' |
re''( dod''4.)\trill re''8 |
re''2 r |
r2 re''4. si'8 |
fad''2. fad''4 |
re''2 re''8 mi'' |
fad''2. fad''4 |
fad''2.~ |
fad''4 si'4. si'8 |
si'8 do'' fad'8.\trill fad'16 sol'4. mi'8 |
si'2 dod''4. dod''8 |
la'4 fad'4.\trill fad'8 |
sol'8 si' mi''4 dod''2\trill |
re''4 re''4. mi''8 |
dod''4\trill dod''16 dod'' re'' mi'' fad''8 sol''16 la'' dod''8.\trill re''16 |
re''2 la'4. la'8 |
si'4 sol'2 fad'4\trill |
sol' si'4. si'8 |
mi''4 sol''~ sol'' fad''8.\trill sol''16 |
sol''2. |
si'4 la'4. si'8 |
sold'4. mi'8 si'2~ |
si'4 dod''8.\trill re''16 mi''2 |
fad''4 dod''8.\trill( si'32 dod'') re''2~ |
re''4 re''8 mi''16 fad'' sol''8. la''16 |
dod''4\trill r r |
r re''4. re''8 |
sol'2 sold'4 |
si'8 mi' mi'4. mi'8 |
mi'16 la''\fort sol'' fad'' mi'' fad'' mi'' re'' dod'' re'' dod'' si' la' si' la' sol' |
fad'4. fad'8\doux sol'2 |
fad'16 fad''\fort sol'' la'' si'' la'' sol'' fad'' sol'' fad'' mi'' re'' dod''8 fad''16 fad'' |
fad''4. fad'8\doux mi'4. mi'8 |
mi'16 la''\fort sol'' fad'' mi'' fad'' mi'' re'' dod'' la' si' dod'' re'' mi'' fad'' sol'' |
la''4 la'4.\doux la'8 |
re'4 re'8 re' re'4 dod'8.\trill re'16 |
re'1 |
