\clef "taille" fad2 fad4. fad8 |
si2 la4. la8 |
la2~ la8 la la la |
si2~ si8 re' re' re' |
dod'2~ dod'8 mi' mi' mi' |
re'2~ re'8 re' re' re' |
mi'4.*13/12 fad'32 mi' re' dod'4. dod'8 |
si2~ si8 si si si |
dod'8 la' la' la' la' fad' fad' fad' |
mi'4.*13/12 fad'32 mi' re' dod'4. dod'8 |
re'2~ re'8 re' re' re' |
si4 re' do' la |
mi'2~ mi'8 mi' mi' mi' |
mi'2. r8 dod' |
dod'2~ dod'8 dod' dod' dod' |
si2~ si8 red' red' red' |
mi'2~ mi'8.*5/6 fad'32 mi' re' dod'8.*5/6 re'32 dod' si |
la4 si la8 re' re' re' |
re'4 si si4. si8 |
la2 sol |
sib2 la4. la8 |
la4 r la8\fortSug la la la |
la4 r dod'8 dod' dod' dod' |
dod'4 la la4.\douxSug la8 |
re'4 re la4. la8 |
la2 r8 re' re'8. re'16 |
re'8 mi' mi' la la2~ |
la4. la8 sol4. si8 |
si2. si4 |
si2 la |
si4 si4. si8 |
la2. |
dod'4 dod'4. dod'8 |
dod'2.~ |
dod'4. si8 dod'2 |
si2 si8 do' |
re' mi' re'4 mi' |
mi'4 fad'8. dod'16 re'8 mi' |
fad' mi' mi'2\trill |
fad'2. |
r8 r16 la32 la la8. la16 dod'8.*5/6 la32 la la la8. la16 |
si8.*5/6 si32 si si si8 re' dod'8. dod'16 dod'8. dod'16 |
re'4\douxSug la8. la16 la4 re'8 si |
si2 si4 |
la2 fad4 |
sold4. si8 la4. la8 |
la2. la4 |
si la8. mi'16 la2 |
la4 mi4. mi8 |
fad2. fad'4 |
re'2 re'4 si |
si4. sol'8 fad' re' |
mi'2 r |
r4 r re' |
re' re' sol |
la2 fad'8 mi' |
re'4 re' la |
la2 dod'4 |
si2 si4 |
dod' dod' la |
mi' si4. si8 |
dod'2 dod'4 |
re'4. fad'8 fad' sol' |
la'4 fad'4. dod'8 |
re'4 mi' fad' |
fad'2 si4 |
si dod' re' |
fad' mi'4. mi'8 |
re'4 re' si |
fad'2 fad'4 |
red'2 red'4 |
mi'2 mi'4 |
mi' re' re' |
mi'2. |
fad'2 fad'8 mi' |
re'2 re'4 |
re' re' si |
dod' dod' re' |
la2. |
la2 r |
r fad'4. fad'8 |
fad'2. dod'4 |
re'4 si2 |
fad' lad |
si4 red'2~ |
red'4 si2~ |
si8 mi si8. si16 si2~ |
si2 la |
la4 re'2~ |
re'4 mi' mi'2 |
fad'4 re' si |
dod' mi' re'8 dod'16 re' mi'8 la |
la2~ la4. la8 |
re'2 do'4 la |
si4 si4. re'8 |
do'2. do'8. si16 |
si2.\trill |
si4 fad la |
si1 |
si4 la8. sold16 la2~ |
la4 mi' la2 |
re'4 re' mi'8. la16 |
la4 r r |
r la si |
sol4. mi8 si4 |
si8 dod' si4.\trill la8 |
la2\fortSug dod'4 re'8 mi' |
la4. re'8\douxSug re'4 dod' |
dod'\fortSug fad' mi'8 sol' fad'8. fad'16 |
fad'4 << si4 { s8 s\douxSug } >> si4. si8 |
mi2\fortSug mi'4~ mi'16 dod' re' mi' |
<< fad'2 { s4 s\douxSug } >> la4 |
si4. sol8 sol4. la8 |
fad1 |
