\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*5 s2 \bar "" \break s2 s1*3\pageBreak
        s1*4\break s1*3\pageBreak
        s1*2 s2.\break s2.*3 s1\pageBreak
        s2.*5\break s1*2 s2 \bar "" \pageBreak
        s2 s2.*2\break s1*3\pageBreak
        s2. s1 s2 \bar "" \break s2 s2. s1 s2.*2\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*8\break s1*3\pageBreak
        s2. s1 s2.\break s2. s1 s2 \bar "" \pageBreak
        s2 s2. s1\break s2. s1 s2 \bar "" \pageBreak
        s2 s1 s2. s1\break s2.*2 s1\pageBreak
        s1*2 s4 \bar "" \break s2 s2.*3\pageBreak
        s2. s1*2\break s1*2 s2 \bar "" \pageBreak
      }
      \modVersion { s1*21\break }
    >>
  >>
  \layout { }
  \midi { }
}
