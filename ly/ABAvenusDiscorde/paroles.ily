Quel -- le sou -- daine hor -- reur ! & quels ter -- ri -- bles bruits !
Ciel ! qui peut a -- me -- ner la Dis -- corde où je suis ?

C’est en vain qu’à tes loix tu pré -- tens qu’on ré -- pon -- de.
Dé -- es -- se, fai ces -- ser d’i -- nu -- ti -- les tra -- vaux ;
A quel coin re -- cu -- lé du mon -- de,
l’A -- mour veut- il ten -- ter des tri -- om -- phes nou -- veaux ?
%
Pour qui des -- ti -- ne -- t’il les traits qu’on luy pre -- pa -- re ?
De tous cô -- tez je le fais dé -- dai -- gner ;
Lors -- que de tous les cœurs la Dis -- cor -- de s’em -- pa -- re,
sur qui veut- il en -- cor re -- gner ?
Tout res -- sent la fu -- reur dont je suis a -- ni -- mé -- e,
a mes san -- glants Au -- tels tout vient sa -- cri -- fi -- er,
et ton fils se voit ou -- bli -- er ;
Je l’ay du moins ban -- ny de l'Eu -- rope al -- lar -- mé -- e,
S’il ne l’est pas du monde en -- tier.
%{
De quels traits im -- puis -- sans me -- na -- ce- t’il la ter -- re ?
Quoy dé -- ja son pou -- voir veut suc -- cé -- der au mien ?
A peine a- t’on é -- teint le flam -- beau de la Guer -- re,
Qu’il pré -- tend ral -- lu -- mer le sien.
Non, non, j’ay pour toû -- jours trom -- pé son es -- pe -- ran -- ce,
J’ay dé -- truit, j’ay bri -- sé les Au -- tels & les fers :
J’ay du moins ar -- ra -- ché l’Eu -- rope à sa puis -- san -- ce,
Si ce n’est pas tout l’U -- ni -- vers.
%}
Tu t’a -- plau -- dis d’u -- ne faus -- se vic -- toi -- re,
l’A -- mour a dans l’Eu -- rope u -- ne nou -- vel -- le gloi -- re.
Il re -- cueil -- le le fruit de tes noi -- res fu -- reurs,
il regne __ au mi -- lieu de la guer -- re,
Mal -- gré tes vains ef -- forts il ras -- sem -- ble deux cœurs
qui fe -- ront quel -- que jour le des -- tin de la ter -- re.
Mal -- gré tes vains ef -- forts, il ras -- sem -- ble deux cœurs
qui fe -- ront quel -- que jour le des -- tin de la ter -- re.
Le Hé -- ros qui les joint, com -- menca à dé -- noü -- er
ce nœud que tu for -- mas a -- vec un soin fu -- nes -- te.

C’en est as -- sez ; é -- par -- gne- moy le res -- te ;
Et ne me for -- ce pas à t’en -- ten -- dre loü -- er
un Roy qui me dé -- tes -- te.

Je te fe -- ray souf -- frir de plus cru -- els tour -- mens ;
Tu mé -- pri -- ses l’A -- mour, tu ver -- ras sa vic -- toi -- re :
Et je veux que ces lieux par di -- vers chan -- ge -- mens,
ser -- vent de thé -- atre à sa gloi -- re :
L’Eu -- ro -- pe que tu crois at -- ten -- tive à ta voix,
Va chan -- ter à tes yeux la dou -- ceur de ses loix ;
Tu vas voir que des cœurs, l’A -- mour seul est le maî -- tre.

Ah ! ne te flat -- te pas de m’en ren -- dre té -- moin.

Je veux te con -- train -- dre de l’ê -- tre,
Tu prends, pour t’en def -- fendre, un i -- nu -- ti -- le soin.

Puis -- que dans ces lieux on m’ar -- rê -- te,
Fu -- reurs, se -- con -- dez- moy, trou -- blons au moins la Fê -- te ;
Fai -- sons des in -- cons -- tans, des Ja -- loux o -- di -- eux !
Jet -- tons dans tous les cœurs, les soup -- çons & les crain -- tes,
Qu’on re -- con -- noisse à mil -- le plain -- tes,
que la Dis -- corde est dans ces lieux.
