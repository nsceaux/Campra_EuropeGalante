\clef "haute-contre" la2 la4. la8 |
si2 dod'4. dod'8 |
re'2~ re'8 re' re' re' |
re'2~ re'8 re' re' re' |
mi'2~ mi'8 mi' mi' mi' |
fad'2 si'4.*13/12 dod''32 si' la' |
sold'2 la'4. la'8 |
mi'2~ mi'8 mi' mi' mi' |
mi' dod'' dod'' dod'' re''8.*5/6 mi''32 re'' dod'' si'8.*5/6 dod''32 si' la' |
sold'4. sold'8 la'4.*13/12 si'32 la' sold' |
fad'2~ fad'8 fad' fad' fad' |
sold'2 la'4. la'8 |
la'4. si'8 sold'4.\trill la'8 |
la'2. r8 mi' |
fad'2~ fad'8 fad' fad' fad' |
fad'2~ fad'8 fad' fad' fad' |
mi'2~ mi'8 mi' mi' mi' |
fad'2~ fad'8 la' la' la' |
sol'4 re' re'4. re'8 |
dod'2 re'~ |
re' dod'4.\trill re'8 |
re'4 r re'8\fortSug re' re' re' |
re'4 r la'8 la' la' la' |
la'4 re' re'4.\douxSug la8 |
si4. si8 la4. la8 |
la2 r8 la' la'8. la'16 |
sol'8 si' la'8. la'16 la'2~ |
la'4. la'8 si' si'16 la' sol'4 |
fad'2. re'4~ |
re' si dod' mi' |
mi' mi'4. mi'8 |
mi'2. |
fad'4 fad'4. fad'8 |
fad'2.~ |
fad'4 fad' fad'4. fad'8 |
fad'4. re'8 sol' fad' |
si' sol' la'8. si'16 si'4 |
la'2 la'4 |
si' la'4. la'8 |
la'2. |
r8 r16 re'32 re' re'8. re'16 mi'8.*5/6 mi'32 mi' mi' la'8. la'16 |
la'4 sol'8 si' mi'8. mi'16 mi'8.\trill re'16 |
re'4\douxSug fad'8. fad'16 mi'4 re'8. re'16 |
re'2 si4 |
dod' si4. si8 |
si4. mi'8 mi'4. mi'8 |
re'2. re'4 |
re'4 re'8. dod'16 re'2 |
re'4 si la |
la2. la'4 |
sol'2 sol'4 re'8 mi' |
fad'4. la'8 la'8. la'16 |
la'2 r |
r4 r fad' |
sol' re' mi' |
la'2 la'8 sol' |
fad'2 fad'8 re' |
la'2 mi'4 |
mi'2.~ |
mi'2 mi'4 |
mi'2 mi'4 |
mi'2 la'4 |
la'4. la'8 la' sol' |
fad'4 la' si'8 dod'' |
fad'4 fad' fad' |
fad'2 fad'4 |
mi' mi' fad' |
fad' si fad' |
fad' fad' sol' |
fad'2 fad'4 |
fad'2 si'4 |
si'2 si'4 |
la' la' sol'~ |
sol' mi'2 |
la' re''8 dod'' |
re''4 re'' fad' |
sol' la' re' |
la' la' si' |
la'2~ la'8 la' |
fad'2\trill r |
r si'4. si'8 |
fad'4 fad' fad'4. fad'8 |
fad'2 fad'8 mi' |
dod'2. fad'4 |
fad'2.~ |
fad'4 mi'2 |
mi'8 do' si8. si16 si2~ |
si4 mi' mi'2 |
re'2.~ |
re'4 si' la'2 |
re'2 re'4 |
la'4. la'8 la' sol'16 fad' la'8. la'16 |
la'4 fad' re'4. re'8 |
re'2 mi'4 do' |
re' sol'4. sol'8 |
sol'4 mi' re'4. re'8 |
re'2. |
re'4 fad'2 |
si2. mi'4 |
mi'4 mi'8. re'16 dod'2\trill |
la'4 sol' la'2~ |
la'4 sol' sol'8. fad'16 |
mi'4\trill r r |
r re'4. re'8 |
do'2 re'4 |
mi'4 mi'4. mi'8 |
mi'4.\fortSug mi'8 mi'4 fad'8 dod' |
<< re'2 { s4. s8\douxSug } >> re'4 mi' |
dod'8\fortSug dod'' re''4 si'8 si' si' lad' |
si'4. fad'8\douxSug si4. si8 |
la4.\fortSug la'8 la'4. la'8 |
la'4 fad'\douxSug re' |
re' si la4. la8 |
la1 |
