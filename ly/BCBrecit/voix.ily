\clef "vbas-dessus" <>^\markup\character Cephise
r4 r8 re'' si'8\trill si' r si'16 si' |
mi''8 mi'' mi'' mi'' fad'' sol'' |
fad''4\trill r16 re'' re'' mi'' do''4\trill do''8 do''16 si' |
si'1\trill |
