\clef "dessus" re''2 la'4 la' |
re'' mi'' fad'' sol'' |
la'' sol''8 fad'' mi''4 la'' |
fad''4.\trill mi''8 re''4 re'' |
mi''2 si'4 mi'' |
la' si' dod'' re'' |
mi'' re''8 dod'' si'4 mi'' |
dod''4.\trill si'8 la'4 la' |
dod''4.\trill si'8 la'4 la' |
la''2 mi''4 mi'' |
la'' fad'' sol'' la'' |
si'' la''8 sol'' fad''4 mi'' |
red''4.\trill dod''8 si'4 si' |
si''2 fad''4 fad'' |
si'' la'' sol'' fad'' |
mi'' fad''8 sol'' fad''4 si'' |
sold''4.\trill fad''8 mi''4 mi'' |
la''2 mi''4 la'' |
fad''4.\trill mi''8 re''4 fad'' |
mi'' dod'' re'' si' |
mi''4. re''8 dod'' si' la'4 |
do''2 do''4 re'' |
si'4 mi'' dod''\trill la'' |
fad''4.\trill mi''8 mi''4.\trill re''8 |
re''1 |
re'' |
