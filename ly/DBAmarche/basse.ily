\clef "basse" re2 re4 dod |
si, dod re mi |
fad sol la la, |
re4. mi8 fad4 re |
la2 sold4 mi |
fad sold la si |
dod' re' mi' mi |
la,2 la4 la |
la,1 |
la2 la4 la |
fad re mi fad |
sol la8 si do'4 la |
si4. la8 sol fad mi4 |
red si, si la |
sol fad mi re |
do la, si, si |
mi'2 mi'4 mi' |
dod'4.\trill si8 dod'4 la |
re'2 re'4 re' |
dod' dod' si si |
la2 la4 la |
fad4.\trill mi8 fad4 re |
sol mi la dod |
re sol, la,2 |
re, re'4 re' |
re,1 |
