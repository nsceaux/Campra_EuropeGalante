\clef "haute-contre" la'2 fad'4 la' |
si' la' la' sol' |
la' re'' re'' dod'' |
re''2 la'4 la' |
la'2 si'4 sold' |
fad' mi' mi' sold' |
la' si' si' si' |
la'2 la'4 la' |
la'2 la'4 la' |
dod''2 dod''4 dod'' |
re'' re'' do'' do'' |
si' do''8 re'' do''4 mi' |
si'2 si'4 si' |
si'2 red''4 red'' |
mi'' fad'' si' re'' |
mi'' do'' si' si' |
si'2 si'4 si' |
dod''4. re''8 dod''4 dod'' |
re''2 re''4 re'' |
la' la' si' si' |
dod''4. si'8 la'4 la' |
la'2 la'4 la' |
sol' si' la' la' |
la' si' la'4. la'8 |
fad'1\trill |
fad' |
