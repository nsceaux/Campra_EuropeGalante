\clef "quinte" la2 la4 dod' |
re' dod' la dod' |
re' si la mi' |
re'2 re'4 re' |
dod'2 si4 si |
la mi' mi' re' |
dod' fad' mi' mi' |
mi'4. re'8 dod' si la4 |
mi'4. re'8 dod' si la4 |
la2 la4 mi' |
re' fad' mi' re' |
re' do' do' do' |
si2 mi'4 mi' |
si2 si4 si |
si fad' sol' si |
do' mi' mi' red' |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
fad'4.\trill sol'8 la'4 la' |
la' la re' re' |
dod'2\trill dod'4 dod' |
re'2 re'4 la |
si si dod' mi' |
re' re la4. la8 |
la1 |
la |
