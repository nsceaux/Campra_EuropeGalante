\clef "taille" fad'2 fad'4 mi' |
re' sol' fad' mi' |
re' re' la' la' |
la'4. sol'8 fad'4 fad' |
mi'2 mi'4 mi' |
re' re' dod' fad' |
sol' fad' la' sold' |
la'2 mi'4 mi' |
la'2 mi'4 mi' |
mi'2 mi'4 mi' |
fad' la' sol' fad' |
re' si' la' la'8 sol' |
fad'2\trill sol'4 sol' |
fad'2 fad'4 fad' |
sol' red' mi' fad' |
la' fad' fad' fad' |
mi'4. fad'8 sold'?4 sold' |
la'2 la'4 la' |
la'4. sol'8 fad' mi' re'4 |
mi' la' la' sold' |
la'2 mi'4 mi' |
fad'4. sol'8 la' sol' fad' mi' |
re'4 mi' mi' mi' |
mi' re' dod'4.\trill re'8 |
re'1 |
re' |
