\score {
  \new StaffGroup <<
    \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*9\pageBreak s1*10\break }
    >>
  >>
  \layout { }
  \midi { }
}
