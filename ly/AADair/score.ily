\score {
  \new StaffGroup <<
    \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8\pageBreak
        s1*7\break s1*6\pageBreak
        s1*6\break s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
