\clef "dessus" re''2 la'4.\trill la'8 |
sib'4. la'8 sol'4 sol'' |
fad''\trill re'' sib''4. sib''8 |
sol''4 do''' la''\trill fa'' |
sib''2 fa''4.\trill fa''8 |
sol''4 mib'' do''4.\trill do''8 |
do''4 fa'' la'4.\trill la'8 |
sib'4. sib'8 do'' do'' re'' mib'' |
re''8\trill re'' mib'' fa'' sol''4. sol''8 |
do''4 re'' do''4.\trill sib'8 |
sib'1 |
sib' |
fa''4. fa''8 fad''4 la'' |
re''4. re''8 do''4.\trill sib'16 la' |
sib'4. sib'8 si'4.\trill si'8 |
do''4 re''8 mib'' re''4.\trill re''8 |
mib''4 fa''8 sol'' si'4.\trill do''8 |
do'' do'' re'' mi'' fa''4. fa''8 |
re'' re'' mi'' fa'' sol''4. sol''8 |
dod''4 la' mi''8 mi'' fa'' mi'' |
re'' dod'' re'' mi'' mi''4.\trill re''8 |
re''4 la''8. la''16 la''2~ |
la''8 la'' fad'' re'' sol''4. sol''8 |
mi''8\trill do'' do'' do'' fa''4. fa''8 |
re''8\trill re'' mib'' re'' do''\trill do'' re'' la' |
sib' re'' re'' mi'' fa'' re'' sol'' fa'' |
mi'' mi'' fa'' sol'' la''4. la''8 |
la'' re'' sol'' la'' sib'' la'' sol'' fa'' mib'' re'' do'' sib' la'4.\trill sol'8 |
sol'1 |
sol'8 re'' re'' mi'' fa'' re'' sol'' fa'' |
mi'' mi'' fa'' sol'' la''4. la''8 |
la'' re'' sol'' la'' sib'' la'' sol'' fa'' |
mib'' re'' do'' sib' la'4.\trill sol'8 |
sol'1 |
