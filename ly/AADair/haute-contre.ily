\clef "haute-contre" re'2 re'4. re'8 |
re'2 mib'4 mib' |
re'2 re''4. re''8 |
do''4 do'' do''2 |
re''2 re''4. re''8 |
sib'4 sol' sol'4. sol'8 |
la'4 fa' fa'4. fa'8 |
fa'4. sol'8 la' la' sib' do'' |
sib'4.\trill sib'8 sib' do'' sib' do'' |
la'4 sib' la'4.\trill sib'8 |
sib'1 |
sib' |
re''4. re''8 do''4. la'8 |
sib'2 la'4. la'8 |
sol'2. sol'4 |
sol'2 sol'4. sol'8 |
sol'4 lab' sol'4. sol'8 |
sol'4. sol'8 la' la' sib' do'' |
sib'2 si'4. si'8 |
la'2 la'4. la'8 |
la'4 sib' la'4. la'8 |
la'2. re''8. re''16 |
re''2~ re''8 re'' re'' re'' |
do''4. do''8 do'' la' la' la' |
sib'4 sib' la' la' |
sol'8 sib' sib' do'' re''4. re''8 |
do'' sol' la' sib' do''4. do''8 |
la' la' sib' do'' re'' sol' la' si' |
do'' sol' fad' sol' fad'4.\trill sol'8 |
sol'1 |
sol'8 sib' sib' do'' re''4. re''8 |
la' sol' la' sib' do''4. do''8 |
la' la' sib' do'' re'' sol' la' si' |
do'' sol' fad' sol' fad'4.\trill sol'8 |
sol'1 |
