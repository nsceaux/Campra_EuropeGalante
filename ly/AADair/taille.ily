\clef "taille" sib2 do'4. do'8 |
sib4 sib sib do' |
la re' re' sol' |
mi'4.\trill mi'8 fa'4 fa' |
fa'2 fa'4. fa'8 |
mib'4 do' do'4. do'8 |
do'2 do'4. do'8 |
re'4. re'8 fa'4. fa'8 |
fa'4. fa'8 mib' mib' fa' sol' |
fa'4. fa'8 fa'4. fa'8 |
fa'1 |
fa' |
sib'4. fa'8 la'4 fad' |
sol'2 re'4. re'8 |
re'2 re'4. re'8 |
do'2 re'4. re'8 |
do'4. re'8 re'4.\trill do'8 |
do'4. do'8 do'4. fa'8 |
fa'2 mi'4. mi'8 |
mi'2~ mi'8 mi' re' dod' |
re'4 re' dod'4.\trill re'8 |
re'2. la'8. la'16 |
la'2 sib'4. sib'8 |
sol'8 mi' mi' mi' la'4. la'8 |
fa'4 sol' fad' re' |
re' sol' fa'8 mi' re'4 |
mi'2 mi'4 la' |
fad'8 fad' sol' fad' sol' re' mib' fa'? |
sol' sol' la' re' re'4. re'8 |
re'1 |
re'4. sol'8 fa'8 mi' re'4 |
mi'2 mi'4 la' |
fad'8 fad' sol' fad' sol' re' mib' fa'? |
sol' sol' la' re' re'4. re'8 |
re'1 |
