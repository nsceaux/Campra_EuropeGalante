\clef "basse" sol2 fad4. fad8 |
sol4. fa8 mib re do4 |
re re' sol4. sol8 |
do'4 do fa8 fa mib fa |
re2~ re8 re do sib, |
mib2 mi4. mi8 |
fa4. fa8 do4 mib |
re do8 sib, la,4 fa, |
sib,8 sib, do re mib do re mib |
fa4 sib, fa,2 |
sib,4 sib8 la sib do' sib la |
sib,1 |
sib2 la4. la8 |
sol2 fad4. fad8 |
sol4. sol8 fa4.\trill mib16 re |
mib4 re8 do si,4. si,8 |
do4 fa, sol,2 |
do8 do' sib do' la fa sol la |
sib4. sib8 mi mi fa sol |
la2 dod8 dod re mi |
fa,4 sol, la,2 |
re,2. re'8. re'16 |
re'2~ re'8 re' si sol |
do'4. do'8 la fa fa fa |
sib4 sol la fad |
sol8 sol fa mi re do si,4 |
do4. do'8 la la sib do' |
re' do' sib la sol fa mib re |
do sib, la, sol, re4 re, |
sol,8 re mi fad sol fad sol la |
sol,8 sol fa mi re do si,4 |
do4. do'8 la la sib do' |
re' do' sib la sol fa mib re |
do sib, la, sol, re4 re, |
sol,1 |
