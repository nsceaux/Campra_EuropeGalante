\clef "quinte" sol2 la4. la8 |
sol2 sol4 sol |
re' fad' sol'4. sol'8 |
sol'4 do' do'4. do'8 |
sib2 sib4. sib8 |
sol2 sol4. sol8 |
fa2 fa4. fa8 |
fa4. re'8 do' do' sib la |
sib sib la sib sol4 mib' |
mib' re' fa' do' |
re'1 |
re' |
re'4 sib do'4. do'8 |
sib4 sol la re' |
re' sol sol4. sol8 |
do'2 sol4 sol' |
mib' do' sol4. sol8 |
mi4\trill do do'4. do'8 |
re' do' sib la sol4 sol' |
mi'8 re' dod' si la2 |
re'4 re la4. la8 |
fad2. fad'8. fad'16 |
fad'2 re'4. re'8 |
mi'4. mi'8 do'4. do'8 |
re'4 sib do' la |
re' sol la re' |
sol do' do'8 do' re' mib' |
re' re' re' do' sib sib do' re' |
mib' sib do' re' do'4 la |
sib1 |
sib4 sol la re' |
sol do' do'8 do' re' mib' |
re' re' re' do' sib sib do' re' |
mib' sib do' re' do'4 la |
sib1 |
