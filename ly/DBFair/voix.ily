\clef "vbas-dessus"
re''4 mi''8 fa''4 mi''8 r4 r8 |
r4 r8 re''4 mi''8 fa''4 mi''8 fa'' mi'' re'' |
mi''4 la'8 sib'4 la'8 sol'4 fa'8 mi'4 re'8 |
la'4. r4 r8 r2*3/2 |
r4 r8 sib'4 la'8 sol'4 fa'8 mi'4\trill re'8 |
la'4.\melisma la'8[ si' do''] si'4. si'8[ dod'' re''] |
dod''[ re'' mi''] la'[ si' dod''] re''4.\melismaEnd fa''4 fa''8 |
fa''[ mi''] mi'' mi''4 mi''8 mi''4 si'8 dod''4 re''8 |
re''4. r4 r8 r2*3/2 |
R1.*2 |
r4 r8 fa''4 mi''8 re'' do'' si' mi''4 la'8 |
sold' fad'[ mi'] r4 r8 r4 r8 mi'4 mi'8 |
fad'4 fad'8 sold'4\trill sold'8 la' si' do'' si'4\trill la'8 |
mi''1.\trill\melisma |
mi''8[ re'' do''] si'[ do'' la'] sold'4\trill\melismaEnd mi''8 do''\trill si'[ la'] |
la'4 r8 r4 r8 r2*3/2 |
r4 r8 la'4 sib'8 do'' re'' do'' do''4 do''8 |
do'' sib'[ la'] r4 r8 r4 r8 do''4 do''8 |
do''([ sib']) sib' sib'4 sib'8 sib'[ la'] la' do''4 do''8 |
do'' sib' la' sol'4\trill fa'8 do''4. r8 r do'' |
re''4. mi''\trill fa''4 la'8 la' sol'[ fa'] |
fa'[\melisma la' fa'] do''[ mi'' do''] fa''4\melismaEnd la'8 la' sol'[ fa'] |
fa'4 r8 r4 r8 r2*3/2 |
r4 r8 re''4 mi''8 fa''4 mi''8 r4 r8 |
