\clef "basse" r4 r8 r4 r8 re'4 dod'8 |
re'4 dod'8 re'4 dod'8 re'4 dod'8 re'[ do'?] sib |
la sol fad sol4 fa8 mi4 re8 do4 sib,8 |
la, la sol fa4 mi8 re4 do8 sib, la, sol, |
la,4 la8 sol4 fa8 mi4 re8 do4 sib,8 |
la, la sol fad4. sol8 sol fa? mi4. |
la4. la4 sol8 fa mi re re' do' re' |
sol4 sol8 do' sib? do' fa4 sol8 la4 la,8 |
re4.~ re4 re8 sol4.~ sol8 fa mi |
la la, sol, fa, mi, re, la,4 la8 re' do' re' |
sol4 sol8 do' sib do' fa4 sol8 la4 la,8 |
re4. re4 mi8 fa mi re do4 re8 |
mi4 re8 do si, la, mi4. do |
re mi8 mi' re' do' si la sol4 fa8 |
mi4. la4 sold8 la4 sold8 la sol? fa |
mi4 la8 sol4 fa8 mi do re mi4 mi,8 |
la,4 la8 sol4 fa8 mi do re mi4 mi,8 |
la,4. r4 r8 fa,4. r4 r8 |
fa,4. do'4 do'8 do' sib la la,4. |
sib, do re la, |
sib, do4 re8 do4 do'8 la sol la |
sib la sib do' sib do' re' la sib do'4 do8 |
fa4. mi re8 la, sib, do4 do8 |
fa,4 la8 sib4 do'8 re' la sib do'4 do8 |
fa,4. r4 r8 r4 r8 re'4 dod'8 |
