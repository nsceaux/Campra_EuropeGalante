\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4.*3 s1.*2\break s1.*4\break s1.*4\break s1.*3\pageBreak
        s1.*3 s2. \bar "" \break s2. s1.*2 s2. \bar "" \break
        s2. s1.*2 s2. \bar "" \break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
