\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre #:music , #{ s2 s2.*15 s4\break #})
   (taille #:music , #{ s2 s2.*15 s4\break #})
   (quinte #:music , #{ s2 s2.*15 s4\break #})
   (basse #:music , #{ s2 s2.*15 s4\break s2 s2.*15 s4\break #})
   (basse-continue
    #:system-count 6
    #:music , #{ s2 s2.*15 s4\break s2 s2.*15 s4\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#80 #}))
