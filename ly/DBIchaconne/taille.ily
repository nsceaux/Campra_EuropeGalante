\clef "taille" la'4 la' |
la' sol' la' |
sol' la' la' |
sib' re' re' |
la' la' la' |
la' sol' la' |
sol' la' la' |
sib' re' re' |
la' la' mi' |
fa' sol' la' |
sol' mi' fa' |
re' la' mi' |
fa' la' mi' |
fa' sol' la' |
sol' mi' fa' |
re' la' mi' |
fa'
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
