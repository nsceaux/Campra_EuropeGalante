\clef "basse" re' la |
re mi fa |
sol re' do' |
sib2. |
la4 re' la |
re mi fa |
sol re' do' |
sib2. |
la4 la sol |
fa mi re |
dod la, fa, |
sol, la,2 |
re4 la sol |
fa mi re |
dod la, fa, |
sol, la,2 |
re,4
%%
<>_\markup\whiteout Basse continue
re4 re |
sol sol fa |
mi mi fa |
re si, do |
sol mi fa |
sol do si, |
do re mi |
fa sol sol, |
do do' la |
re' re' si |
mi' do' si |
la sold la |
mi' re' mi' |
do' si la |
sold mi do |
re mi mi, |
la,
%%
<>_\markup\whiteout Basse continue
re' dod' |
re' sib la |
sol fad sol |
mib mib4.\trill re8 |
re4 do re |
sib la sol |
fad re sib, |
do re re, |
sol, sol sol |
do' do' la |
sib sol fa |
sib, sol, fa, |
do do' sib8 la |
sol4 re' do'8 sib |
la4 sib sol |
fa do do, |
fa,
