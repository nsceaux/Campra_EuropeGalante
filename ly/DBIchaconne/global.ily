\key la \minor \beginMark "Rondeau" \midiTempo#160
\digitTime\time 3/4 \partial 2 s2 s2.*15 s8. \fineMark s16 \bar "|."
\beginMark "Premier Couplet" s2 s2.*15 s8.
\endMark "On reprend le Rondeau" s16 \bar "|."
\beginMark "Deuxième Couplet" s2 s2.*15 s4
\endMark "On reprend le Rondeau" \bar "|."
