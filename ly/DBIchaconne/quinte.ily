\clef "quinte" fa'4 mi' |
re' sol' fa' |
re' re' mi' |
fa' fa' sol' |
mi' fa' mi' |
re' sol' fa' |
re' re' mi' |
fa' fa' sol' |
mi' mi' mi' |
la mi' la |
la la la |
sib la4. la8 |
la4 la mi' |
la mi' la |
la la la |
sib la4. la8 |
la4
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
