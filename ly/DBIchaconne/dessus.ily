\clef "dessus" re''4 mi'' |
fa'' mi''\trill re'' |
sib'' la'' la'' |
la''4. sib''8 sol''4\trill |
la'' re'' mi'' |
fa'' mi''\trill re'' |
sib'' la'' la'' |
la''4. sib''8 sol''4\trill |
la'' mi'' dod'' |
re'' mi'' fa'' |
mi'' dod'' re''~ |
re''8 mi'' mi''4.\trill re''8 |
re''4 mi'' dod'' |
re'' mi'' fa'' |
mi'' dod'' re''~ |
re''8 mi'' mi''4.\trill re''8 |
re''4
%
<>^\markup\whiteout Flutes
\twoVoices #'(dessus1 dessus2 dessus) <<
  { fa''4 mi'' |
    re'' mi'' fa'' |
    sol'' mi'' do'' |
    fa'' fa'' mi'' |
    re''\trill sol'' fa''8\trill mi'' |
    re''4 mi'' fa'' |
    mi'' fa'' sol'' |
    la'' re''4.\trill do''8 |
    do''4 mi'' la'' |
    fad'' fad'' si'' |
    sold'' la'' si'' |
    do''' si'' la'' |
    sold'' si'' la''8 sold'' |
    la''4 si'' do''' |
    si'' sold'' la''~ |
    la''8 si'' sold''4.\trill la''8 |
    la''4 }
  { re''4 do'' |
    si' do'' re'' |
    mi'' do'' la' |
    re'' re'' do'' |
    si'\trill mi'' re''8\trill do'' |
    si'4\trill do'' re'' |
    do'' si'\trill do'' |
    do'' si'4.\trill do''8 |
    do''4 do'' do'' |
    la'\trill la' re'' |
    si'\trill do'' re'' |
    mi'' re'' do'' |
    si'\trill re'' do''8 si' |
    mi''4 re'' do'' |
    mi'' mi'' mi'' |
    fa'' si' mi'' |
    dod''\trill }
>>
%
<>^\markup\whiteout Flutes
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 sol'' |
    fad'' sol'' la'' |
    sib'' la'' sib'' |
    sol'' sol''4. la''8 |
    fad''4 la'' sol''8 fad'' |
    sol''4 la'' sib'' |
    la'' fad'' sol''~ |
    sol''8 la'' la''4.\trill sol''8 |
    sol''4 sib'' la'' |
    sol''\trill sol'' la'' |
    fa'' sol'' la'' |
    re'' mi'' fa'' |
    mi''\trill sol'' sol''8 la'' |
    sib''4 la'' sib'' |
    do''' fa'' sib'' |
    la''4 sol''4.\trill fa''8 |
    fa''4 }
  { fa''4 mi'' |
    re'' mi'' fad'' |
    sol'' re'' re'' |
    re''4. mib''8 do''4\trill |
    re'' mib'' re'' |
    re'' do''\trill sib' |
    re'' re'' re'' |
    sol'' fad''4.\trill sol''8 |
    sol''4 sol'' fa'' |
    mi''\trill mi'' fa'' |
    re'' sib' la' |
    sib' sib' la' |
    sol'\trill mi'' mi''8 fa'' |
    sol''4 fa'' sol''\trill |
    la'' re'' mi'' |
    fa'' mi''4.\trill fa''8 |
    fa''4 }
>>

