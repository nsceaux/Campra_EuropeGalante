\clef "haute-contre" re''4 dod'' |
re'' dod'' re'' |
re'' fa'' mi'' |
re''2. |
dod''4 re'' dod'' |
re'' dod'' re'' |
re'' fa'' mi'' |
re''2. |
dod''4 la' la' |
re'' dod'' re'' |
la' la' re'' |
re'' dod''4.\trill re''8 |
re''4 la' la' |
re'' dod'' re'' |
la' la' re'' |
re'' dod''4.\trill re''8 |
re''4
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
