\score {
  \new StaffGroup \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff <<
        \global \keepWithTag #'dessus1 \includeNotes "dessus"
      >>
      \new Staff <<
        { \startHaraKiri s2 s2.*15 s4 \stopHaraKiri }
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s2.*10\break s2.*5 s4\pageBreak
        s2 s2.*11\break s2.*4 s4\break
        s2 s2.*10\break
      }
      \modVersion {
        s2 s2.*15 s4\break
        s2 s2.*15 s4\break
      }
    >>
  >>
  \layout { system-count = 6 }
  \midi { }
}
