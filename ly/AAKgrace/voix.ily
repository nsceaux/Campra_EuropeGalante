\clef "vbas-dessus" <>^\markup\character Une Grace
sib'4 la'8 sol' |
fad'4 sol' la' sib' |
do'' la' sib' do''8 re'' |
mib''4 re'' do''4.\trill sib'8 |
la'2\trill sib'4 la'8 sol' |
fad'4 sol' la'4.*5/6 sib'16([ la' sib']) |
do''8. sib'16( la'4) sib' do''8 re'' |
mib'' re''[ do'' re''] mib''[ re''] do''[ sib'] |
la'2\trill re''4 mib''8 re'' |
do''4.\trill sib'8 la'4.\trill sol'8 |
re''4 re'' mi'' fad'' |
sol''2 fad''4.\trill sol''8 |
sol''2 re''4 mib''8 re'' |
do''4.*5/6\trill sib'16[ la' sib'] do''16([ sib'8.]) la'16([ sol'8.]) |
re''4 re'' mi''4.*5/6 fad''16([ mi'' fad'']) |
sol''2 fad''4.\trill sol''8 |
sol''2
