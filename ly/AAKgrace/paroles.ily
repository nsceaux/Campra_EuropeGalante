C’est dans u -- ne ten -- dresse ex -- trê -- me,
qu’on trou -- ve des plai -- sirs par -- faits.
On n’est con -- tent que quand on ai -- me,
les au -- tres biens sont sans at -- traits ;
Pour être heu -- reux, l’A -- mour lui- mê -- me
s’est bles -- sé de ses traits.
Pour être heu -- reux, l’A -- mour lui- mê -- me
s’est bles -- sé de ses traits.
