\clef "basse" sol4 fa8 mib |
re4 do8 sib, la,4 sol, |
fad,2 sol,4 la,8 sib, |
do4 sib, la,4. sol,8 |
re mi fad re sol4 fa?8 mib |
re4 do8 sib, la,4 sol, |
fad,2 sol,4 la,8 sib, |
do4 sib, la,4. sol,8 |
re4 re'8 do' sib4 do'8 sib |
la4. sol8 fad4. mi8 |
re4 sib, do re |
mib do re re, |
sol, sol8 la sib4 do'8 sib |
la4. sol8 fad4. mi8 |
re4 sib, do re |
mib do re re, |
sol,2
