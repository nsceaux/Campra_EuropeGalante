\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*4 s2 \bar "" \break s2 s1*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { system-count = 5 }
  \midi { }
}
