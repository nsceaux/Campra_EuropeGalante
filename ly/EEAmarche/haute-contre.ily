\clef "haute-contre" R1*2 |
mi'2 mi' |
mi' mi'4 red' |
mi'2. mi'4 |
fad' red' red' fad' |
sold' mi' mi' sold' |
sold'2. sold'4 |
la'2 mi' |
mi'4. re'8 dod' re' mi' fad' |
red'2. red'4 |
mi'2 mi' |
mi'1 |
R1*2 |
si'2 si' |
si' si'4 la' |
la'2. la'4 |
sol'2 sol' |
sol'?2. sol'4 |
fad'2. fad'4 |
fad' dod' dod' fad' |
fad'2. fad'4 |
mi'2. mi'4 |
la'2 la' |
la'2. la'4 |
fad' re' re' fad' |
fad'2 fad' |
mi' mi'4 mi' |
mi'2 mi' |
dod'1 |
