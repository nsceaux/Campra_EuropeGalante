\clef "dessus" la'2 la' |
la' sold'4\trill fad' |
mi'2. la'4 |
la' mi' mi' la' |
sold'4.\trill fad'8 mi' fad' sold' la' |
si'2 si' |
si'2. mi''4 |
mi'' si' si' mi'' |
dod''4.\trill re''8 dod'' si' la' sold' |
fad'2 fad' |
fad'2. si'4 |
si' la' sold' fad' |
mi'1 |
si'2 si' |
si' si'4 dod'' |
re''2. re''4 |
re'' dod'' re'' mi'' |
fad''4. sol''8 fad'' mi'' re'' dod'' |
si'2. si'4 |
mi''4. fad''8 mi'' re'' dod'' si' |
lad'4. sold'8 fad'4 si' |
si'2 lad' |
si'2. si'4 |
sold' mi' mi' mi'' |
mi''2. mi''4 |
dod'' la' la' fad'' |
fad''2 si' |
si'4. dod''8 re'' dod'' si' la' |
sold'4.\trill fad'8 mi'4 la' |
la'2 sold'\trill |
la'1 |
