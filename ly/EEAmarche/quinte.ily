\clef "quinte" R1*2 |
la2 la |
la mi4 fad |
sold2. sold4 |
fad2. fad4 |
mi sold sold si |
si2. si4 |
dod' la la mi |
fad2 fad |
fad2. fad4 |
sold fad sold la |
si1 |
R1*2 |
re'2 re' |
re' re'4 sold' |
fad'2. fad'4 |
sol' re' re' si |
si2 si |
fad' si |
fad'2. fad'4 |
fad' red' red' si |
si2. mi'4 |
mi' mi' mi' la |
la2 dod' |
re' fad |
fad si |
si4 sold la mi |
mi2 mi |
mi1 |
