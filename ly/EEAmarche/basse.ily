\clef "basse" R1*2 |
la,2 la, |
la, sold,4 fad, |
mi,2. mi4 |
red4. dod8 si, dod red si, |
mi2 mi |
mi4. fad8 mi re dod si, |
la,2. la,4 |
lad,4 fad, fad, lad, |
si,4. dod8 si, la, sold, fad, |
mi,2 mi, |
mi,1 |
R1*2 |
si,2 si, |
si, si,4 dod |
re2. re4 |
sol4. la8 si la sol fad |
mi2. mi4 |
fad4. mi8 re4 si, |
fad,2. fad4 |
red si, si, red |
mi2. mi4 |
dod la, la, la |
la4. si8 la sol fad mi |
re2 re |
re4. dod8 si,4 si, |
mi4. re8 dod4 la, |
mi,2 mi, |
la,1 |
