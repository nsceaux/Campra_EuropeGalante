\clef "taille" R1*2 |
dod'2 dod' |
dod' si4 la |
si2. si4 |
si si red' red' |
mi'2 mi' |
mi'2. mi'4 |
mi'4 dod' la dod' |
dod'2. dod'4 |
si2 si |
si4 dod' si la |
sold1\trill |
R1*2 |
fad'2 fad' |
fad' fad'4 mi' |
re'2. re'4 |
re' si si re' |
mi'4. re'8 mi' fad' mi' re' |
dod'2 re' |
dod' dod' |
si2. si4 |
si sold sold si |
dod'2. mi'4 |
mi' mi' la la |
re'2 re' |
re'4. mi'8 fad' mi' re' dod' |
si2 dod' |
si si |
la1 |
