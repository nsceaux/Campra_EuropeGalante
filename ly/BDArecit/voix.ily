\clef "vbasse" <>^\markup\character Silvandre
r8 la sol4\trill sol8 la16 mi |
fa8 fa16 fa sol8 la16 sib la4\trill r8 la16 la |
re'8 re'16 re' si4.\trill mi'8 |
sold4\trill sold8 sold16 la la4 la8 r |
mi4 r8 mi16 mi fa8 fa |
re4\trill re8 re16 re mi8 fa |
mi4\trill sol sol8 la |
sib sib sib8. la16 la2\trill |
la8 la sib do' mi4\trill fa16[ mi] fa8 |
fa4.( mi8) fa2 |
la4 la8 la sib sib la\trill sol |
fad2\trill sol8 sol \appoggiatura fa?8 mi mi |
dod4 re8 mi fa4( mi)\trill |
re
\ffclef "vbas-dessus" <>^\markup\character Cephise
r8 fa'' re''\trill re'' r re''16 re'' |
la'4 r16 la' la' la' re''8. la'16 |
sib'8 r16 sib' sib' sib' sib' re'' sib'8.\trill sib'16 sib'8. la'16 |
la'4\trill
\ffclef "vbasse" <>^\markup\character Silvandre
do'4 r8 do' la\trill la16 la |
fa4 fa8 fa16 fa fa8. mi16 |
mi8\trill mi sol sol16 sol sol8 la |
sib4 la8 la16 la re'8. mi'16 |
dod'2 mi'4 |
la r8 la la la |
sib sib sib la la4( sol8)[\trill fad16] sol |
fad2 re'8 re' |
mib'4 la8 sib sib4( la)\trill |
sol4
\ffclef "vbas-dessus" <>^\markup\character Cephise
r8 sol''16 sol'' re''8\trill re''16 re'' mi''8 fa'' |
mi''4\trill mi''8 sol'' do'' do'' do''\trill do''16 si' |
do''8 do'' r16 do'' do'' do'' fa''8. la'16 |
\appoggiatura la'8 sib'4 sib'8 la' sol'4\trill la'8 sib' |
la'8.\trill sol'16( fa'8) r la'4 |
r8 la'16 la' la'8. re''16 si'8\trill si'16 mi'' dod''8\trill dod''16 re'' |
\appoggiatura re''16 mi''2 r |
\ffclef "vbasse" <>^\markup\character Silvandre
re'2 dod'4.\trill re'8 |
si4. si8 la4 sol |
fad2\trill fad4 fad |
sol la si dod' |
re' si sold\trill la mi4. mi8 |
la1 |
la2 la4 la |
mi4. mi8 do'2 do'4 la |
si2 si4 si8 la sol4 fad8 mi |
red4.\trill red8 mi4 la, si,4. si,8 |
mi2 mi4. mi8 |
la4 fad si2 si4 sol |
re'2 re'4 re8 mi fad4\trill fad8 sol |
la4. si8 dod'4 re' la4. la8 |
re1 |
\ffclef "vbas-dessus" <>^\markup\character Cephise
r8 fad' sol' |
la' si' dod'' |
re'' re'' dod'' |
si' dod'' re'' |
dod''4\trill dod''8 |
r re'' dod'' |
si' la' sol' |
fad'\trill fad' sold' |
la' la' sold' |
la'4. |
la'4 mi''8 |
dod''4.\trill |
re''8 mi'' fad'' |
si' mi'' fad'' |
sol'' fad''8.\trill fad''16 |
sol''8. fad''16( mi''8) |
si' si'16 si' si' dod'' |
re''8 la' si' |
sol' mi' la' |
fad'\trill fad' sol' |
la'4. |
si'8 dod'' re'' |
dod''\trill re'' mi'' |
fad'' mi''8.\trill re''16 |
re''4
\ffclef "vbasse" <>^\markup\character Silvandre
r8 re' la4 r8 fa16 fa |
sib8 sib16 la la8\trill la r4 |
r2 la4 re' |
dod' re'8 mi' la4 sol8 fa |
mi4 fa8 sol fa4\trill re8 la |
re'4 do' si la |
sold\trill si8 do' la4 la8 sold |
la2 la4 re' |
la2 la4 la8 sib |
do'4 sib8 la sib4 la8 sol |
la4 fa8 do' re'4 sib |
\appoggiatura la16 sol4 la sol4.\trill fa8 |
fa2 la4 si8 do' |
si4\trill si8 do' re'4 re'8 mi' |
dod'4\trill \appoggiatura si8 la fa sib4 \appoggiatura la8 sol4 |
\appoggiatura fa8 mi4 fa fa( mi8.)\trill re16 |
re4
\ffclef "vbas-dessus" <>^\markup\character Cephise
r8 re''16 re'' la'8 r16 la' re''8 re''16 mi'' |
fa''8 fa'' r la'16 la' sib'8 sib'16 sib' |
sol'8 sol'16 sol' sol'8 la'16 sib' la'8\trill
\ffclef "vbasse" <>^\markup\character Silvandre
r16 la la si do' la |
re'4
\ffclef "vbas-dessus" <>^\markup\character-text Doris qui survient
r8 re'' mi'' mi'' r mi''16 mi'' |
la'8\trill la' fa''4 r8 re''16 re'' sol''8 mi'' |
dod'' la'16 la' re''4 re''8 dod'' |
re''2. |
