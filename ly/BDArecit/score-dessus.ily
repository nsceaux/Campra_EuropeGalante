\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        \global \includeNotes "dessus2"
      >>
    >>
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = #-2 \includeLyrics "paroles" }
  >>
  \layout { }
}
