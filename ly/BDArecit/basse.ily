\clef "basse" la,4\repeatTie dod2 |
re4 mi fa2 |
re4 mi2 |
mi, la,~ |
la,2. |
sib,4 si,2 |
do2. |
sol,2 re~ |
re4. do8 sib,4 la,8 fa, |
do4 do, fa,2 |
fa sib,4 do |
re4. do8 si,4 sib, |
la, sib,8 sol, la,2 |
re1~ |
re2. |
sib,2 do |
fa1 |
re2. |
do2 sib,8 la, |
sol,4 re8 do sib,4 |
la,2 la4 |
fad2. |
sol4. fa8 mib2 |
re sib,4 |
do2 re4 re, |
sol,1 | \allowPageTurn
do2 la,8 fa, sol,4 |
do2 la,4 |
sol,4. fa,8 do4 do, |
fa,2 fa4 |
fad2 sol8. mi16 la8. re16 |
la,2 la8 si dod' la |
re'2 dod'4. re'8 |
si4. si8 la4 sol |
fad2\trill fad4 fad |
sol la si dod' |
re' si sold la mi4. mi8 |
la1 |
la2 la4 la |
mi4. mi8 do'2 do'4 la |
si2 si4 si8 la sol4 fad8 mi |
red4. red8 mi4 la, si,4. si,8 |
mi2 mi4. mi8 |
la4 fad si2 si4 sol |
re'2 re'4 re8 mi fad4 fad8 sol |
la4. si8 dod'4 re' la la, |
re1 |
re'4. |
dod' |
si |
mi'4 mi8 |
la la sol |
fad4. |
sol8 fad mi |
re dod si, |
la, mi mi, |
la,4 la8 |
la,4. |
la8 la sol |
fad mi re |
sol sol fad |
mi si si, |
mi mi fad |
sol4. |
fad8 fad sol |
mi la la, |
re re mi |
fad mi re |
sol mi re |
la, fad, mi, |
re, la,4 |
re,1 | \allowPageTurn
re2. |
re,2 re8 mi fa re |
la4. sol8 fa4 mi8 re |
dod4 la, re4. do?8 |
si,4 la, sold, la, |
mi4. do8 re4 mi |
la,8 si, dod la, re mi fa re |
la, sib, la, sol, fa,4 fa |
mi fa sib, do |
fa la, sib,8 do re sib, |
do4 fa, do,2 |
fa, fa4 sol8 la |
sol4 sol8 la sib4 sol |
la fa8 re sol fa mi re |
dod la, re sol, la,2 |
re, re~ |
re sib,4 |
do2 fa, |
fa mi |
re sib, |
la,4 fa,8 re, la,4 |
re,2. |
