\clef "dessus" r4 r2 R1 R2. R1 R2.*3 R1*7 R2. R1*2 R2.*5 R1 R2.
R1*3 R2. R1 R2. R1*2 |
re''2 la'4. la'8 |
si'4. dod''8 re''4 mi'' |
fad''2 re''4 fad'' |
si' la' sol'4. la'8 |
fad'4.\trill fad'8 si'4 la' sold'4. la'8 |
la'4. si'8 dod'' re'' mi'' dod'' |
la'2 dod''4. re''8 |
mi''4. mi''8 mi''2 mi''4 fad'' |
red''2\trill red''4. red''8 mi''4 red''8 mi'' |
si'4. si'8 si'4 mi'' red''4. mi''8 |
mi''2 mi''4. mi''8 |
dod''4\trill dod'' re''2 re''4 mi'' |
fad''2 fad''4 fad''8 mi'' re''4 mi''8 re'' |
dod''4.\trill re''8 mi''4 re'' dod''4.\trill re''8 |
re''1 |
R4.*24 R1 R2. R1*15 R2. R1*3 R2.*2
