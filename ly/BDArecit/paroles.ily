Voy -- ez à vos ge -- noux cet A -- mant em -- pres -- sé :
Je dé -- couvre en trem -- blant l’ar -- deur qui me pos -- se -- de :
Mais, par -- don -- nez aux maux dont je me sens pres -- sé :
C’est dans les yeux qui m’ont bles -- sé,
que j’en viens cher -- cher le re -- me -- de.
C’est dans les yeux qui m’ont bles -- sé,
que j’en viens cher -- cher le re -- me -- de.

Qu’en -- tends- je ? quels dis -- cours ? vous se -- riez- vous mé -- pris ?
Vous me pre -- nez, peut- ê -- tre, pour Do -- ris ?

Non, Ce -- phi -- se, c’est vous à qui je viens ap -- pren -- dre
le vi -- o -- lent a -- mour dont je res -- sens les coups.
He -- las ! Do -- ris a- t’elle au -- tant d’at -- traits que vous,
et peut- on s’y mé -- pren -- dre ?

Ce n’est donc que de -- puis deux jours,
que vos yeux la trou -- vent moins bel -- le ;
Vous luy ju -- riez a -- lors une flâ -- me é -- ter -- nel -- le ;
Quoy ! pou -- vez- vous si- tôt dé -- men -- tir vos dis -- cours ?

Lors -- que Do -- ris me pa -- rut bel -- le,
je ne con -- nois -- sois pas en -- co -- re vos at -- traits.
- traits ;
Il fau -- droit pour ê -- tre fi -- del -- le,
vous a -- voir toû -- jours vûë, ou ne vous voir ja -- mais.
Il fau -- droit pour ê -- tre fi -- del -- le,
vous a -- voir toû -- jours vûë, ou ne vous voir ja -- mais.

Que n’a -- dres -- sez- vous mieux un lan -- ga -- ge si ten -- dre,
de quel -- qu’au -- tre Ber -- gere il sur -- pren -- droit la foy :
foy :
Pour moy, je fuis l’a -- mour, & je veux m’en def -- fen -- dre ;
Mais s’il me con -- trai -- gnoit quel -- que jour à me ren -- dre,
du moins vou -- drois-je un cœur que n’eût ai -- mé que moy.

Eh bien, vous se -- rez sa -- tis -- fai -- te !
J’ay sen -- ty pour vous seule u -- ne flâ -- me par -- fai -- te,
je n’ay ja -- mais ai -- mé com -- me j’aime en ce jour :
J’ay sen -  jour :
Do -- ris es -- toit ma der -- niere a -- mou -- ret -- te,
vous es -- tes mon pre -- mier a -- mour.
Do -- ris es -- toit ma der -- niere a -- mou -- ret -- te,
vous es -- tes mon pre -- mier a -- mour.

Lais -- sez- moy, c’est trop vous en -- ten -- dre ;
Re -- don -- nez vô -- tre cœur à l’ai -- ma -- ble Do -- ris.

Je vous sui -- vray par tout.

Sil -- van -- dre, cher Sil -- van -- dre,
Ah ! je l’ap -- pelle en vain, il est sourd à mes cris.
