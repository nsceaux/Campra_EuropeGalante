\clef "dessus" r4 r2 R1 R2. R1 R2.*3 R1*7 R2. R1*2 R2.*5 R1 R2.
R1*3 R2. R1 R2. R1*2 | \allowPageTurn
fad''2 mi''4.\trill fad''8 |
re''4. mi''8 fad''4 sol'' |
la''2 fad''4 la'' |
re'' dod'' re'' mi'' |
la' re'' re'' dod'' si'4.\trill mi''8 |
dod''4. re''8 mi'' fad'' sol'' mi'' |
dod''2\trill mi''4. fad''8 |
sol''4. sol''8 sol''2 sol''4 la'' |
fad''2\trill fad''4. fad''8 si''4 la''8 sol'' |
fad''4. fad''8 sol''4. fad''8 fad''4.\trill mi''8 |
mi''2 sol''4. sol''8 |
mi''4 la'' fad''2\trill fad''4 sol'' |
la''2 la''4 fad''8 sol'' la''4 la''8 si'' |
mi''4 la'' sol'' fad'' mi''4.\trill re''8 |
re''1 | \allowPageTurn
R4.*24 R1 R2. R1*15 R2. R1*3 R2.*2
