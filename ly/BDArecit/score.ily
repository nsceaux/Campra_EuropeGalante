\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        \global \includeNotes "dessus2"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1 s2.\break s1 s2.*3\break s1*4\break
        s1*3 s2.\break s1*2 s2.\pageBreak
        s2.*4 s2 \bar "" \break s2 s2. s1*2\break
        s1 s2. s1\break s2. s1*2\break s1*4 s1.\pageBreak
        s1*2 s1.*3\break s1 s1.*3 s1\break s4.*8\pageBreak
        s4.*8\break s4.*8\break s1 s2. s1*2\break
        s1*4 s2 \bar "" \break s2 s1*4 s2 \bar "" \break
        s2 s1*3\pageBreak
        s2. s1\break
      }
      \modVersion {
        s2. s1 s2. s1 s2.*3 s1*7 s2. s1*2 s2.*5 s1 s2.
        s1*3 s2. s1 s2. s1*2\break
        s1*4 s1. s1 s1 s1.*3 s1 s1.*3 s1\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
