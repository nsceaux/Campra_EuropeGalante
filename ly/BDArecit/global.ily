\key la \minor \midiTempo#80
\time 4/4 \measure 3/4 s2. \measure 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1*7
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 \grace s8 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\beginMark "Air" \bar ".!:" \key re \major
\time 2/2 \midiTempo#160 s1*4
\time 3/2 s1.
\time 2/2 \alternatives s1 s1
\time 3/2 s1.*3
\time 2/2 s1
\time 3/2 s1.*3
\time 2/2 s1
\beginMark "Air gayement" \bar "||.!:"
\time 3/8 \midiTempo#120 s4.*9 \alternatives s4. s4. s4.*13
\key la \minor \time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s2 \beginMark "Air" s2
\bar ".!:" s1*4 \alternatives s1 s1 s1*7
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2 \bar "|."
