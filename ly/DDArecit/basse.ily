\clef "basse" re2 la4~ |
la2. fad4 |
sol2.~ |
sol4 fad mi2~ |
mi2. |
dod2~ dod4. re8 |
fad,2 sol, |
la, re, |
re1 |
si,2. |
la,4 la fad |
red2. |
mi~ |
mi |
la4 fad2 |
re4 fad,2 |
sol, sol8 fad |
mi re la4 la, |
re,2.~ |
re,4. la,8 re4~ |
re8. dod16 si,2 |
la,4. la8 fad4. fad8 |
lad,2 si, |
mi, mi4 |
re dod2 |
si,~ si,4.*5/6 do16 si, la, |
sol,1 | \allowPageTurn
sol2 mi4 do |
re2 si,8 do |
re4 re, sol,2 |
sol fad4 mi |
re2. |
si,2 la,4 la |
dod4. re8 la,2 |
re, re'8 dod' |
re'4 re' dod' |
si8 do' si la sol fad |
mi re la4. re8 |
dod4 re8 dod si, la, |
sold,2 la,4 |
mi fad sold |
la mi mi, |
la, la re'8 dod' |
la,2. |
la4 la fad |
sol2 sol,4 |
la, si,2 |
mi8 re mi fad sol mi |
la2 sol4 |
fad8 sol fad mi re4 |
fad8 sol la4 la, |
re4. dod8 re mi |
fad2. |
sol8 fad sol la si sol |
la re la,2 |
re,2. | \allowPageTurn
re2 sol4. mi8 |
la4. re8 la,2 |
fad,2. |
sol, |
la,2 si, |
do si, |
si4 sol8 fad16 mi si,4 |
mi,1~ |
mi,2 mi |
la8. sol16 fad8 mi re2~ |
re4 dod2 |
re mi4 |
la,1 |
fad,2 sol,4 sol |
mi2 fad |
red2. |
mi |
fad4 sol8 mi fad4 fad, |
si,1 |
si2 fad |
mi re~ |
re do |
si,4. sol,8 la,4 |
re,2. |
