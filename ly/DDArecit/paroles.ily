Mais, que vois-je ? ô Ciel !
Cru -- el, quel -- le ra -- ge vous gui -- de ?
De quels af -- freux trans -- ports é -- tein -- cel -- lent vos yeux ?

Ge -- my, pleure à ton tour, per -- fi -- de ;
Va, cours de ton a -- mant re -- ce -- voir les a -- dieux ;
Il ex -- pi -- re près de ces lieux.

Ciel !

Eh bien, mal -- heu -- reux ! en dou -- te -- ray- je en -- core ?
Sa dou -- leur m’en dit plus que je n’en veux sça -- voir :
Me voi -- là donc cer -- tain du feu qui la dé -- vo -- re ;
Ce -- pen -- dant je n’ay pû van -- ger mon dé -- ses -- poir
sur ce -- luy que son cœur a -- do -- re.
En vain je l’ay sui -- vy ce trop heu -- reux A -- mant :
Fa -- ta -- le Fê -- te, nuit trop som -- bre,
c’est vous, dont le tu -- multe & l’om -- bre
ont dé -- ro -- bé ses jours à mon res -- sen -- ti -- ment.

Tu re -- prens tes es -- prits, cru -- elle, à ce lan -- ga -- ge !
Je suis le seul qui souffre i -- cy.
De tous ses mou -- ve -- ments je sens croî -- tre ma ra -- ge ;
Je vou -- lois luy sur -- prendre un se -- cret qui m’ou -- tra -- ge ;
Je n’ay que trop bien ré -- üs -- si.
\tag #'basse {
  Vous voy -- ez mon ar -- deur, il n’est plus temps de fein -- dre,
  mon se -- cret se dé -- couvre à vos soup -- çons ja -- loux :
  Vous voy -  - loux :
  C’est à l’A -- mour qu’il faut vous plain -- dre,
  je l’au -- rois é -- coû -- té, s’il m’eût par -- lé pour vous.
  Je l’au -- rois é -- coû -- té, s’il m’eût par -- lé pour vous.

  Quoy, per -- fi -- de, mes feux, le de -- voir, ma ten -- dres -- se,
  mes pleurs n’ont pû vous at -- ten -- drir ?
  Ah ! je veux dé -- sor -- mais re -- pa -- rer ma foi -- bles -- se,
  je met -- tray tous mes soins à vous fai -- re souf -- frir :
  Puis -- que vous brû -- lez pour un au -- tre,
  mon Ri -- val en per -- dra le jour,
  ma fu -- reur dans son sang é -- tein -- dra son a -- mour
  et pu -- ni -- ra le vô -- tre.

  Cru -- el, ces -- sez de m’al -- lar -- mer,
  n’e -- coû -- tez point une in -- jus -- te co -- le -- re ;
  C’é -- toit à moy de vous ai -- mer ;
  mais, c’é -- toit à vous de me plai -- re.

  In -- gra -- te, ce dis -- cours vient en -- cor a -- ni -- mer
  mon de -- ses -- poir & ma van -- gean -- ce.

  Pour vous ai -- der à les cal -- mer,
  il faut fuïr de vô -- tre pré -- sen -- ce.
}
