\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1 s2.\break s1 s2. s2 \bar "" \break
        s2 s1*2\break s1 s2.*2\break s2.*4\pageBreak
        s2.*5\break s2. s1*2\break \grace s16 s2.*2 s1*2\break
        s1 s2. s1 s2 \bar "" \pageBreak
        s2 s2. s1\break s1 s2 \bar "" \break
        s4 s2.*6\break s2.*8\break s2.*6 \pageBreak
        s2. s1*2 s2.\break s2. s1*2 s4 \bar "" \break
        s2 s1*2 s4 \bar "" \break s2. s2.*2 \break s1*3\break
        s2.*2 s1*2\pageBreak
        s1*2 s2 \bar "" \break \grace s8
      }
      \modVersion {
        s2. s1 s2. s1 s2. s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
