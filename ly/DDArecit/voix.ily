\clef "vbas-dessus" <>^\markup\character Olimpia
la'4 r8 la' dod'' la' |
mi''4 r8 mi'' dod''\trill la'16 la' re''8 re''16 fad'' |
si'8 si' r sol' sol'16 la' si' sol' |
re''8 re''16 re'' red''8\trill red''16 mi'' mi''4
\ffclef "vhaute-contre" <>^\markup\character Octavio
r8 mi' |
mi'4 si8 si16 si mi'8 mi' |
la4 la8 r mi' r la'16 sol' fad' mi' |
re'8 re'16 re' la8 la16 re' si4\trill sol'8 sol' |
dod'8\trill dod' dod' dod'16 re' re'4
\ffclef "vbas-dessus" <>^\markup\character Olimpia
fad''4 |
\ffclef "vhaute-contre"
<>^\markup\character-text Octavio Olimpia évanouie
r4 r8 la8 la4 la8 la |
re'4 sold8 sold16 sold sold8 la |
la la r la16 la lad8\trill lad16 si |
si4 si8 si16 la la8 sold |
sold4\trill r8 mi'16 mi' si8 si16 si |
sold8\trill si mi'4 si8 dod'16 re' |
dod'8\trill dod' r la16 la re'8 re'16 re' |
fad'8 la' re'4 re'8 re'16 fad' |
si4 sol'8 sol' si8 si16 si |
dod'8 re' re'4( dod') |
re'2 r8 la |
re' re' re' mi' fad'4 |
fad'8 fad'16 la' re'4( dod'8) re' |
dod'4\trill r16 mi' mi' mi' lad8\trill lad r lad16 dod' |
fad8 fad r fad' re'16 re' re' fad' si8. fad16 |
\appoggiatura fad16 sol8 sol mi' mi'16 mi' dod'8.\trill dod'16 |
fad'4 lad8 lad16 lad lad8. si16 |
si2 r |
r4 r8 <>^\markup\italic à Olimpia si16 si si4\trill si8 do' |
re'4. sol'8 do' do' do' do' |
la\trill la r re' re' mi' |
do'\trill do' do'8. si16 si4\trill r8 <>^\markup\italic à part sol |
sol la si dod' re' re'16 re' mi'8 fad'16 sol' |
fad'8\trill fad' r fad'16 fad' fad'8 fad'16 la' |
re'8 re'16 re' si8\trill si16 si mi'4 mi'8 r16 sol' |
sol'4 sol'8 fad' mi'4\trill mi'8 fad' |
re'2
\tag #'basse {
  \ffclef "vbas-dessus" <>^\markup\character Olimpia
  la'8 la' |
  la'4 si' dod'' |
  re''2 r8 si' |
  dod'' re'' dod''4.\trill re''8 |
  mi''4 la' re''8 dod'' |
  si'4\trill si' dod'' |
  sold'\trill la' si' |
  dod'' si'4.\trill la'8 |
  la'2 la'8 la' |
  la'2. |
  dod''4 dod'' re'' |
  si'4.\trill si'8 si'4 |
  mi'' mi''( red'') |
  mi''2 mi''8 re'' |
  dod''4\trill dod'' mi'' |
  la'2 la'4 |
  la'8 si' \appoggiatura la'8 sol'4( fad'8) sol' |
  fad'2\trill fad''8 mi'' |
  re''4 re'' la' |
  si'2 si'4 |
  dod''8 re'' re''4( dod''8.)\trill re''16 |
  re''4
  \ffclef "vhaute-contre" <>^\markup\character Octavio
  re'4. re'8 |
  la8 la r re' si4\trill r8 si16 mi' |
  dod'4\trill r8 dod'16 re' \appoggiatura re'8 mi' mi' r la' |
  re' re' re' do' do' si |
  si4\trill sol' r8 sol' |
  mi'4\trill mi'8 fad' red'4\trill si8 si |
  si4 si8 la \appoggiatura la16 si8 si r fad'16 fad' |
  red'8\trill red'16 si mi'8 fad'16 sol' red'8\trill red'16 mi' |
  mi'2 si8 si si si |
  do'4 do'8 do' sold\trill sold r si16 mi' |
  dod'8\trill dod'16 dod' re'8 mi' \appoggiatura mi' fad'4 r8 fad'16 la' |
  re'8 re'16 re' la8 la16 sol sol8 sol16 fad |
  fad4\trill re'8 re'16 fad' si8.\trill dod'16 |
  la8 la
  \ffclef "vbas-dessus" <>^\markup\character Olimpia
  r8 mi'' mi''4 r8 dod'' |
  re'' re'' re'' fad'' si'4 r16 si' si' si' |
  mi''8 mi''16 mi'' dod''8\trill dod''16 dod'' lad'8\trill lad' r fad' |
  fad' sold' la' la' la'[ sold'16] la' |
  sold'4\trill mi''8 r16 mi'' dod''8 mi'' |
  lad'4\trill si'8 dod'' re''4( dod'')\trill |
  si'4
  \ffclef "vhaute-contre" <>^\markup\character Octavio
  r8 fad' si si r si16 dod' |
  re'8 re'16 mi' fad'8\trill fad'16 sol' la'4 re'8 re'16 re' |
  sol'4 sol'8 mi'16 la' fad'8\trill fad'
  \ffclef "vbas-dessus" <>^\markup\character Olimpia
  r16 la' la' la' |
  fad'8\trill fad' fad' sol' \appoggiatura sol'8 la'4 r8 la'16 la' |
  si'8. dod''16 re''4 re''8 dod'' |
  re''4 re'' r |
}
