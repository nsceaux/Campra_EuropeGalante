\key re \major
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\time 4/4 s1
\digitTime\time 3/4 s2.*12
\time 4/4 s1*2
\digitTime\time 3/4 \grace s16 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2
\midiTempo#120 \beginMark "Air" s4
\bar ".!:" s2.*7 \alternatives s2. s2. s2.*12
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5
\digitTime\time 3/4 s2.*2 \bar "|."
