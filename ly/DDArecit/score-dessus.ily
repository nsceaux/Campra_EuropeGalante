\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff \haraKiri } \withLyrics <<
      \global \keepWithTag #'dessus \includeNotes "voix"
      { s2. s1 s2. s1 s2. s1*3
        s1 s2.*12 s1*2 s2.*2 s1*3 s2. s1*2 s2. s1*2 s2 r4\break }
    >> { \set fontSize = #-3
      \keepWithTag #'dessus \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
      { s2. s1 s2. s1 s2. s1*3\break \noHaraKiri }
    >>
  >>
  \layout { }
}
