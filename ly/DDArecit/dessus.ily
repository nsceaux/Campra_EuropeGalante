\clef "dessus" R2. R1 R2. R1 R2. R1*3 |
r4 fad''\doux ^"Violons" fad''4. fad''8 |
fad''4 re''4. dod''8 |
dod''2\trill dod''4 |
fad''4 fad''4. fad''8 |
si'2 si'4 |
mi'' sold'4. sold'8 |
la'2~ la'8 la' |
re''8 fad'' la''4. la''8 |
re''2 re''8 re'' |
sol'' fad'' mi''4.\trill re''8 |
re''4 r8 la' re'' mi'' |
fad''4. dod''8 re''4 |
la'' la'' sold'' |
la'' r8 dod'' dod''4 r8 dod'' |
dod''4 r8 dod'' fad'4. fad'8 |
mi'4 r lad' |
si' dod''4. dod''8 |
fad'2 r |
r4 re'' sol''4. sol''8 |
sol''2 sol''4. la''8 |
fad''2\trill sol''4~ |
sol''4 fad''8.\trill sol''16 sol''2 |
si'4. si'8 la'8 re'' re'' dod'' |
re''2 la'4~ |
la' sold'\trill la' r8 la' |
mi''4 mi''8 re'' re''4( dod''8.)\trill re''16 |
re''2 r4 |
R2.*21 R1*2 R2.*2 R1*2 R2. R1*3 R2.*2 R1*3 R2.*2 R1*5 R2.*2
