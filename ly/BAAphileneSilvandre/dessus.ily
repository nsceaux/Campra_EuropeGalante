\clef "dessus" R2.*49 R1*2 R2. R1*2 R2.*3 R1*3 R2. R1*3 R2.*2 R1*2 |
r4 r r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''8 |
    sol''4 fa'' sol'' |
    mi'' fad''4.\trill sol''8 |
    re''4 re'' sol'' |
    fad''\trill sol'' sol'' |
    sol''( fad''4.)\trill sol''8 |
    sol''2 sol''8 sol'' |
    mi''2\trill mi''4 |
    fad''4 fad'' sol'' |
    red''2 red''4 |
    mi'' fad''\trill sol'' |
    si'2 si'4 |
    mi'' red''4.\trill mi''8 |
    mi''2 do''8 do'' |
    do''2 re''4 |
    si'4 dod'' re'' |
    dod''2 dod''4 |
    re'' mi'' fad'' |
    sol''2 fad''4 |
    sol'' mi''4.\trill re''8 |
    re''2 r8 fad'' |
    sol''4 fa'' sol'' |
    mi'' fad''4.\trill sol''8 |
    re''4 re'' sol'' |
    fad''\trill sol'' sol'' |
    sol''( fad''4.)\trill sol''8 |
    sol''4 }
  { la'8 |
    si'4 do'' re'' |
    sol' do''4. si'8 |
    la'4\trill si'8 do'' re'' mi'' |
    la'4\trill do''4. si'8 |
    si'4( la'4.)\trill sol'8 |
    sol'2 si'8 si' |
    sol'2 la'4 |
    la' la' sol' |
    fad'2\trill fad'4 |
    sol' la' si' |
    la'2\trill sol'4 |
    la' fad'4.\trill( mi'16 fad') |
    sol'2 sol''8 sol'' |
    la''2 fad''4 |
    re'' sol'' fad'' |
    mi''2\trill mi''4 |
    re'' dod'' re'' |
    mi''2 la'4 |
    re'' dod''4.\trill re''8 |
    re''2 r8 la' |
    si'4 do'' re'' |
    sol' do''4. si'8 |
    la'4\trill si'8 do'' re'' mi'' |
    la'4 do''4. si'8 |
    si'4( la'4.)\trill sol'8 |
    sol'4 }
>> r4 r2 |
R1 R2. R1*4 |

