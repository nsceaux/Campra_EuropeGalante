\clef "vhaute-contre" <>^\markup\character Philene
sol'2 re'8 sol' |
fad'4.\trill fad'8 sol' la' |
re' re' re' do' sib\trill la |
sib2 do'8 re' |
mib'2 mib'8 fa' |
re'4.\trill do'8( sib4) |
re'4 re'8 re' mib' fa' |
la2\trill do'8 fa' |
sib2 sib8 la |
\appoggiatura la16 sib2 fa'4 |
re'4.\trill re'8 re' sib |
fa' fa' fa' la' re' la |
\appoggiatura la16 sib4. la8([ sol]) r |
sol4 sol8 la sib do' |
re' re' re' mi' fad' sol' |
fad'2\trill re'4 |
mi'4. mi'8 mi' mi' |
fad' sol' sol'4( fad') |
sol'2
\ffclef "vbasse" <>^\markup\character Silvandre
re8 re |
sol2 la8 sib |
la2\trill la8 re' |
sol2 sol8. la16 |
fad4\trill
\ffclef "vhaute-contre" <>^\markup\character Philene
la'4 r8 fad' |
re'4 re'
\ffclef "vbasse" <>^\markup\character Silvandre
la8 re' |
sib4\trill sib8 sib do' re' |
sol4 sol8 sol do' re' |
mib'2 la8 sib |
sib4( la2)\trill |
sol2 sol4 |
re'4. do'8 sib la |
sib4 sib la |
sol\trill sol fa |
mi4.\trill la8 sib la |
sol fa mi4\trill la |
fad8.\trill mi16( re4) sol |
fad8.\trill mi16( re4) la8 la |
sib4 sib sib |
sol2\trill sol8 do' |
la4\trill la do' |
fa sib8 sib do' re' |
sol4 la sib |
sib2( la4) |
sib2 sib8 la |
sol4 fa sol |
mi2\trill mi8 la |
fad4 fad la |
re sib8 sib do' re' |
\appoggiatura sol8 fad4 fad sol |
sol2( fad4) |
sol2
\ffclef "vhaute-contre" <>^\markup\character Philene
r8 sol'16 sol' re'8\trill re'16 mib' |
fa'4 fa'8 fa' mib'4 mib'8 re' |
do'4\trill
\ffclef "vbasse" <>^\markup\character Silvandre
la8 la la do' |
fa4 sol8 la sib sib r16 sib sib re' |
sib8.\trill sib16 sib8 sib sol8.\trill sib16 sib la sol fa |
mib8. re16 do4\trill do8 do16 fa |
sib,8 sib,
\ffclef "vhaute-contre" <>^\markup\character Philene
r8 re' re' sib |
fa' fa' fad'8.\trill fad'16 fad'8. sol'16 |
sol'4 re'8 re'16 mi' fa'8 fa'16 fa' fa'8 mib'16 re' |
mib'8 mib'
\ffclef "vbasse" <>^\markup\character Silvandre
r8 do' sol4 r16 sol sol sol |
mib8. mib16 mib mib mib sol do4 r8 lab |
lab4 sol sol8 do' |
si4\trill do'8 lab \appoggiatura sol8 fa4 \appoggiatura mib16 re8 mib! |
mib4( re)\trill do r8 do'16 do' |
la4\trill r8 fa do r do fa |
re4\trill fa8 fa16 fa sib8 sib |
sol8\trill sol sol sol sol la |
fad\trill fad
\ffclef "vhaute-contre" <>^\markup\character Philene
r8 re'16 re' sol'4 sol'8 re' |
mib'4 mib'8 re' do'4\trill do'8 sib |
la4\trill la
\ffclef "vbasse" <>^\markup\character Silvandre
r8 re |
sol4 la si |
do' la4.\trill sol8 |
fad4\trill sol8 la si do' |
re'4 mi' do' |
re'( re2) |
sol sol8 sol |
do'2 la4 |
fad4\trill fad mi |
si2 si4 |
sol fad mi |
red2\trill mi4 |
la, si,2 |
mi mi8 mi |
fad2\trill fad4 |
sol mi\trill re |
la2 la4 |
fad mi re |
dod2 re4 |
sol, la,2 |
re r8 re |
sol4 la si |
do' la4.\trill sol8 |
fad4\trill sol8 la si do' |
re'4 mi' do' |
re'( re2) |
sol4 r8 re' si\trill si16 si sol8 sol16 si |
mi4
\ffclef "vhaute-contre" <>^\markup\character Philene
mi'8 mi'16 sol' do'8 do' do' re' |
\appoggiatura re' mi' mi'
\ffclef "vbasse" <>^\markup\character Silvandre
r8 do' la r16 la |
fad8\trill fad16 la fad8\trill fad16 fad re8 re r re |
sol sol la si mi16 mi fad sol fad8.\trill sol16 |
sol1 |
R1 |
