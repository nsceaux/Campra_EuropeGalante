Quoy ? pour l’ob -- jet de vôtre ar -- deur,
vous pré -- pa -- rez en -- cor u -- ne Fê -- te nou -- vel -- le ?
Tant de fi -- de -- li -- té doit flé -- chir sa ri -- gueur ;
En vain Do -- ris af -- fecte u -- ne fier -- té cru -- el -- le,
el -- le se las -- se -- ra de re -- fu -- ser son cœur
aux soins que vous pre -- nez pour el -- le.

Ce n’est plus de Do -- ris que j’at -- tens mon bon -- heur.

Ciel ! qu’en -- ten- je ?

L’A -- mour m’offre un nou -- veau vain -- queur,
et me for -- ce d’estre in -- fi -- del -- le.
Je romps mes pre -- miers nœuds pour des nœuds plus char -- mans,
mon in -- fi -- de -- li -- té m’est che -- re :
Je
che -- re :
Et j’ay plus de plai -- sir à tra -- hir mes ser -- mens,
que je n’en sen -- tis à les fai -- re.
Et j’ay plus de plai -- sir à tra -- hir mes ser -- mens,
que je n’en sen -- tis à les fai -- re.

A qui donc of -- frez- vous vôtre hom -- ma -- ge nou -- veau ?

A l’in -- dif -- fe -- ren -- te Ce -- phi -- se ;
Que mon tri -- om -- phe se -- roit beau,
si je la soû -- met -- tois au Dieu qu’el -- le mé -- pri -- se !

Vous de -- si -- riez a -- vec la même ar -- deur,
qu’un jour Do -- ris par -- ta -- geât vô -- tre flâ -- me.

Eh bien, je vous ap -- prens que j’ay soû -- mis son cœur.
Les feux dont je bru -- lois, ont pas -- sé dans son a -- me.
Mes ser -- mens, mes pleurs, mes soû -- pirs,
m’ont ob -- te -- nu l’a -- veu que je de -- man -- dois d’el -- le.

Pour -- quoy donc brû -- lez- vous d’u -- ne fla -- me nou -- vel -- le ?

L’A -- mour en com -- blant nos de -- sirs,
à de nou -- veaux nœuds nous ap -- pel -- le :
Plus de fois on est in -- fi -- del -- le,
et plus on goû -- te de plai -- sirs.
Plus de fois on est in -- fi -- del -- le,
et plus on goû -- te de plai -- sirs.
L’A -- mour en com -- blant nos de -- sirs,
à de nou -- veaux nœuds nous ap -- pel -- le.
Ce -- phi -- se se plaît en ces lieux.

C’est el -- le- mê -- me qui s’a -- van -- ce.

Al -- lons, Phi -- lene, é -- vi -- tons sa pré -- sen -- ce ;
la Fête en ma fa -- veur doit pré -- ve -- nir ses yeux.
