\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \keepWithTag #'dessus1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = #-2 \includeLyrics "paroles" }
  >>
  \layout { }
}
