\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \keepWithTag #'dessus1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*6\break s2.*7\break s2.*6\break s2.*5 s2 \bar "" \pageBreak
        s4 s2.*5 s4 \bar "" \break s2 s2.*5\break s2.*8\break
        s2.*5 s1\break s1 s2. s1\break s1 s2.*2\pageBreak
        s2. s1*2\break s1 s2. s1\break s1*2 s2.*2\break
        s1*2 s2 \bar "" \break s4 s2.*7\pageBreak
        s2.*11\break s2.*7 s4 \bar "" \break s2. s1 s2.\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
