\clef "basse" sol,2 sol4 |
re'4. do'8 sib la |
sib4 fad2 |
sol2. |
la |
sib~ |
sib4 re do8 sib, |
fa2 fa8 re |
sol mib fa4 fa, |
sib,2. |
sib |
la2 fad4 |
sol2. |
sol,4. re8 sol la |
sib4. sib8 la sol |
re'4 re8 do si, sol, |
do re do sib, la,4 |
re8 sol, re,2 |
sol,2.~ |
sol, |
fa, |
mib, |
re,~ |
re,2 re4 |
sol2~ sol8 fa |
mib2~ mib8 re |
do2 re8 sol, |
re,2. | \allowPageTurn
sol,2 sol,4 |
fad,2. |
sol,4 sol fa |
mi dod re |
la,4. fa,8 sol, la, |
sib, sol, la,2 |
re8 do sib, la, sol,4 |
re,2 re4 |
sol4 sol re |
mib8 fa mib re do4 |
fa2 mib4 |
re4. re8 do sib, |
mib8 re do4 sib, |
fa fa,2 |
sib, sol,8 la, |
sib,4 si,2 |
do8 re do sib, la,4 |
re2 do4 |
sib,4. sib,8 la, sol, |
re2 mib8 do |
re4 re,2 |
sol,2. sol4 |
re2 do4. sib,8 |
fa2.~ |
fa4 mib re2~ |
re mib |
mib,4 fa,2 |
sib,2. |
sib4 la2 |
sol si, |
do1~ |
do | \allowPageTurn
sib,2 lab,4 |
sol, mib, re,4. do,8 |
sol,2 do |
fa la, |
sib, sol,4 |
mib2. |
re2 si, |
do1 |
re2~ re8 re |
sol4 la si |
do'4 la4.\trill sol8 |
fad4\trill sol8 la si do' |
re'4 mi' do' |
re' re2 |
sol sol8 sol |
do'2 la4 |
fad4 fad mi |
si2 si4 |
sol fad mi |
red2 mi4 |
la, si,2 |
mi mi8 mi |
fad2\trill fad4 |
sol mi\trill re |
la2 la4 |
fad mi re |
dod2 re4 |
sol, la,2 |
re r8 re |
sol4 la si |
do' la4.\trill sol8 |
fad4\trill sol8 la si do' |
re'4 mi' do' |
re'( re2) |
sol,4~ sol,~ sol,2 |
do1~ |
do2. |
re1 |
si,4 la,8 sol, do4 re8 re, |
sol,2~ sol,8 la, sol, fad, |
mi,1 |
