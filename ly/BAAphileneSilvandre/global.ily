\key re \minor
\digitTime\time 3/4 \midiTempo#160 s2.*28 s2
\beginMark "Air" s4 \bar ".!:" s2.*5 \alternatives s2. s2. s2.*13
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*3
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2 \midiTempo#160 \bar "||"
\key sol \major \beginMark "Air" s4 s2.*25
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \grace s8 s2.
\time 4/4 s1*4 \bar "|."
