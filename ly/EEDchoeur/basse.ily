\clef "basse" la8\douxSug |
la2. r8 fad |
fad2 si4 si |
mi2 mi4 r8 la |
la2. r8 re |
re2 mi4 mi |
la,2 la,4 r8 la |
la2. r8 fad |
fad2 si4 si |
mi2 mi4 r8 la |
la2. r8 re |
re2 mi4 mi |
la,2 la,4
