<<
  \tag #'vdessus { \clef "vdessus" r8 R1*5 r2 r4 }
  \tag #'vhaute-contre { \clef "vhaute-contre" r8 R1*5 r2 r4 }
  \tag #'vtaille { \clef "vtaille" r8 R1*5 r2 r4 }
  \tag #'vbasse {
    \clef "vbasse" <>^\markup\character Le Bostangi
    la8 |
    la2. r8 fad |
    fad2 si4 si |
    mi2 mi4 r8 la |
    la2. r8 re |
    re2 mi4 mi |
    la,2 la,4
  }
>>
<<
  \tag #'vdessus {
    <>^\markup\character Chœur r8 mi'' |
    mi''2. r8 mi'' |
    mi''2 red''4.\trill red''8 |
    mi''2 mi''4 r8 dod'' |
    dod''2. r8 si' |
    si'2 si'4 mi'' |
    dod''2\trill dod''4
  }
  \tag #'vhaute-contre {
    r8 la' |
    la'2. r8 la' |
    la'2 fad'4. fad'8 |
    sold'2 sold'4 r8 mi' |
    mi'2. r8 fad' |
    fad'2 mi'4 mi' |
    mi'2 mi'4
  }
  \tag #'vtaille {
    r8 dod' |
    dod'2. r8 dod' |
    dod'2 si4 si |
    si2 si4 r8 la |
    la2. r8 la |
    la2 sold4.\trill sold8 |
    la2 la4
  }
  \tag #'vbasse {
    r8 la |
    la2. r8 fad |
    fad2 si4 si |
    mi2 mi4 r8 la |
    la2. r8 re |
    re2 mi4 mi |
    la,2 la,4
  }
>>