\clef "taille" dod'8\douxSug |
dod'2. r8 dod' |
dod'2 si4. si8 |
si2. r8 dod' |
dod'2. r8 re' |
re'2 re'4. re'8 |
dod'2. r8 la' |
la'2. r8 la' |
la'2 fad'4. fad'8 |
sold'2. r8 mi' |
mi'2. r8 fad' |
fad'2 mi'4. mi'8 |
mi'2.
