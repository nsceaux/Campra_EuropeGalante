\clef "quinte" la8\douxSug |
la2. r8 la |
la2 si4. si8 |
sold2. r8 la |
la2. r8 si |
si2 si4. si8 |
la2. r8 mi' |
mi'2. r8 fad' |
fad'2 fad'4. fad'8 |
mi'2. r8 mi' |
mi'2. r8 re' |
re'2 si4.\trill si8 |
la2.
