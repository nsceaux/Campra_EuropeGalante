\clef "haute-contre" mi'8\douxSug |
mi'2. r8 mi' |
mi'2 red'4. red'8 |
mi'2. r8 mi' |
mi'2. r8 fad' |
fad'2 mi'4. mi'8 |
mi'2. r8 dod'' |
dod''2. r8 dod'' |
dod''2 si'4. si'8 |
si'2. r8 la' |
la'2. r8 la' |
la'2 sold'4.\trill la'8 |
la'2.
