\clef "dessus" r4 r8 r4 r8 re''4\forte mi''8 |
fa''4 mi''8 r4 r8 r2*3/2 |
R1. |
r4 r8 la''4\forte sol''8 fa''4 mi''8 re''4 mi''8 |
dod'' si' la' r4 r8 r2*3/2 |
r8 r mi''\dolce la''4. r8 r re'' sol''4. |
r2*3/2 r4 r8 la''4 la''8 |
sib'' la'' sib'' sol''4\trill sol''8 la'' sol'' fa'' mi''4\trill re''8 |
re''4 la'8\forte re''4.~ re''8 do'' si' mi''4.~ |
mi''8 re'' dod'' re'' mi'' fa'' mi''4\trill la''8 fa'' mi'' re'' |
sib'' la'' sib'' sol''4\trill sol''8 la'' sol'' fa'' mi''4 re''8 |
re''4. r4 r8 r2*3/2 |
r4 r8 mi''4\forte la''8 sold'' fa'' mi'' mi''4 mi''8\dolce |
mi''4 re''8 re'' do'' si' do'' re'' mi'' mi''4 la''8 |
sold''4\trill si'8 do''4 si'8 do''4 si'8 do'' si' la' |
sold'4\trill mi''8 mi''4 re''8 mi''4 la'8 la'4 sold'8 |
la'4 do''8\forte si'4\trill la'8 mi'' re'' do'' si'4\trill la'8 |
la'4. fa'4\dolce sol'8 la' sib' la' la'4 la'8 |
la' sol'( fa') r4 r8 r4 r8 fa''4 do''8 |
re'' do'' re'' mi''\trill re'' mi'' fa''4. r8 r do'' |
re'' mi'' fa'' mi''4\trill fa''8 sol''4 do''8 do'''4 do'''8 |
do'''4 sib''8 sib''4 la''8 la'' sol'' fa'' fa''4 mi''8 |
fa''4. sol''\trill la''8 sol'' fa'' fa''4 mi''8\trill |
fa''4 do''8\forte re''4 mi''8 fa'' do'' fa'' fa''4 mi''8\trill |
fa''4. r4 r8 r4 r8 re''4\forte mi''8 |
