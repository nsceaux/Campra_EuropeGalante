\key la \minor \midiTempo#132
\time 12/8 \partial 4.*3
s4.*3 \segnoMark \bar "||" s1.*10 s4 \endMark "Fine" s8 \bar "|."
s4.*3 s1.*13 \bar "|." \endMark "Ca Capo, al fine"