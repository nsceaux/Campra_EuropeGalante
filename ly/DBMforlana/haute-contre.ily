\clef "haute-contre" la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
sol'2 si'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
sol'2 si'4 la'2 la'4 |
la'2 la'4 la'2 re''4 |
re''2 re''4 re''2 dod''4\trill |
re''2 la'4 la'2 re''4 |
re''2 re''4 re''2 dod''4\trill |
re''2 la'4 la'2 la'4 |
sol'2 fad'4 fad'2 la'4 |
sol'2 fad'4 fad'2 la'4 |
sol'2 la'4 sol'2 sol'4 |
fad'2\trill fad'4 fad'2 r4 |
R1.*3 |
r2 r4 r2 fad'4 |
mi'2 la'4 si'2 si'4 |
dod''2 dod''4 dod''2 fad'4 |
mi'2 la'4 si'2 si'4 |
dod''2 dod''4 dod''2 dod''4 |
re''4. mi''8 fad''4 dod''2 re''4 |
re''2 re''4 re''2 dod''4 |
re''4. mi''8 fad''4 dod''2\trill re''4 |
re''2 re''4 re''2 r4 |
R1.*3 |
r2*3/2 r4 r8 re'' mi''4 |
re''2 dod''4 dod''4. re''8 mi''4 |
re''2 dod''4 dod''4. re''8 mi''4 |
re''2 mi''4 re''2 si'4 |
dod''2 dod''4 dod''2 r4 |
R1.*3 |
r2 r4 r2 la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
sol'2 si'4 la'2 la'4 |
la'2 la'4 la'2 la'4 |
sol'2 si'4 la'2 la'4 |
la'2 la'4 la'2
