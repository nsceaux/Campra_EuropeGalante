\clef "quinte" re'4 |
re'2 la4 la2 re'4 |
dod'2\trill dod'4 dod'2 re'4 |
re'2 la4 la2 re'4 |
dod'2\trill dod'4 dod'2 re'4 |
re'2 re'4 la2 la4 |
la2 la4 la2 re'4 |
re'2 re'4 la2 la4 |
la2 la4 la2 re'4 |
re'2 si4 re'2 sol4 |
la2 la4 la2 re'4 |
re'2 si4 re'2 sol4 |
la2 la4 la2 la4 |
si2 re'4 re'2 la4 |
si2 re'4 re'2 la4 |
si2 re'4 sol2 la4 |
la2 la4 la2 r4 |
R1.*3 |
r2 r4 r2 la4 |
dod'2 la4 la2 sold4 |
la2 la4 la2 la4 |
dod'2 la4 la2 sold4 |
la2 la4 la2 la4 |
re'4. dod'8 re'4 mi'2 la4 |
la2 la4 la2 la4 |
re'4. dod'8 re'4 mi'2 la4 |
la2 la4 la2 r4 |
R1.*3 |
r2*3/2 r4 r8 fad' mi'4 |
fad'2 la'4 la'4. fad'8 mi'4 |
fad'2 la'4 la'4. fad'8 mi'4 |
fad'2 dod'4 re'2 mi'4 |
mi'2 mi'4 mi'2 r4 |
R1.*3 |
r2 r4 r2 re'4 |
re'2 la4 la2 re'4 |
dod'2\trill dod'4 dod'2 re'4 |
re'2 la4 la2 re'4 |
dod'2\trill dod'4 dod'2 re'4 |
re'2 re'4 la2 la4 |
la2 la4 la2 re'4 |
re'2 re'4 la2 la4 |
la2 la4 la2
