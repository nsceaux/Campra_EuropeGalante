\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1.*7\pageBreak
        s1.*8 s2. \bar "" \break s2. s1.*8\pageBreak
        s1.*8\break s1.*7 s2. s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
