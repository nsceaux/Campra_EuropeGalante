\clef "dessus" <>^\markup\whiteout Tous la''4 |
re''2 re''4 re''4. mi''8 fad''4 |
mi''2 la'4 la'2 la''4 |
re''2 re''4 re''4. mi''8 fad''4 |
mi''2 la'4 la'2 la'4 |
si'4. dod''8 re''4 re''4. mi''8 dod''4 |
re''2 re''4 re''2 la'4 |
si'4. dod''8 re''4 re''4. mi''8 dod''4 |
re''2 re''4 re''2 la''4 |
fad''2 si''4 la''2 sol''4 |
fad''2\trill re''4 re''2 la''4 |
fad''2 si''4 la''2 sol''4 |
fad''2\trill re''4 re''4. re''8 dod''4 |
si'2\trill la'4 la'4. re''8 dod''4 |
si'2\trill la'4 la'4. re''8 dod''4 |
si'2\trill la'4 si'2 dod''4 |
re''2 la'4 la'2 <>^\markup\whiteout "Hautbois"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 |
    la''2.~ la''2 si''4 |
    la''2.~ la''2 la''4 |
    la''4. sol''8 la''4 si''4. la''8 sol''4 |
    la''2 la''4 la''2 }
  { fad''4 |
    fad''2.~ fad''2 sol''4 |
    fad''2.~ fad''2 fad''4 |
    fad''4. mi''8 fad''4 sol''4. fad''8 mi''4 |
    fad''2 fad''4 fad''2 }
>> <>^\markup\whiteout Tous la'4 |
la'4. si'8 dod''4 re''4. dod''8 si'4 |
mi''2 mi''4 mi''2 la'4 |
la'4. si'8 dod''4 re''4. dod''8 si'4 |
mi''2 mi''4 mi''2 la''4 |
la''4. sol''8 fad''4 mi''2\trill re''4 |
re''2 re''4 re''2 la''4 |
la''4. sol''8 fad''4 mi''2\trill re''4 |
re''2 re''4 re''2 <>^\markup\whiteout "Hautbois"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    re''4. dod''8 re''4 mi''4. re''8 mi''4 |
    fad''2 re''4 re''2 re''4 |
    re''4. dod''8 re''4 mi''4. re''8 mi''4 |
    fad''2 re''4 re''4. }
  { la'4 |
    si'4. la'8 si'4 dod''4. si'8 dod''4 |
    re''2 la'4 la'2 la'4 |
    si'4. la'8 si'4 dod''4. si'8 dod''4 |
    re''2 la'4 la'4. }
>> <>^\markup\whiteout Tous la''8 sol''4 |
fad''2\trill mi''4 mi''4. la''8 sol''4 |
fad''2\trill mi''4 mi''4. la''8 sol''4 |
fad''2\trill mi''4 fad''2 sold''4\trill |
la''2 mi''4 mi''2 <>^\markup\whiteout "Hautbois"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 |
    mi''2.~ mi''2 fad''4 |
    mi''2.~ mi''2 mi''4 |
    mi''4. re''8 mi''4 fad''4. mi''8 re''4 |
    mi''2 mi''4 mi''2 }
  { dod''4 |
    dod''2.~ dod''2 re''4 |
    dod''2.~ dod''2 dod''4 |
    dod''4. si'8 dod''4 re''4. dod''8 si'4 |
    dod''2 dod''4 dod''2 }
>> <>^\markup\whiteout Tous la''4 |
re''2 re''4 re''4. mi''8 fad''4 |
mi''2 la'4 la'2 la''4 |
re''2 re''4 re''4. mi''8 fad''4 |
mi''2 la'4 la'2 la'4 |
si'4. dod''8 re''4 re''4. mi''8 dod''4 |
re''2 re''4 re''2 la'4 |
si'4. dod''8 re''4 re''4. mi''8 dod''4 |
re''2 re''4 re''2
