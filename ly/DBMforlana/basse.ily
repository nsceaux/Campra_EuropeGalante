\clef "basse" re'4 |
fad2 fad4 fad4. mi8 re4 |
la2 la4 la2 re'4 |
fad2 fad4 fad4. mi8 re4 |
la2 la4 la2 fad4 |
sol2 sol4 la2 la4 |
re2 re4 re2 fad,4 |
sol,2 sol,4 la,2 la,4 |
re2 re4 re2 re'4 |
re'2 sol4 fad2 mi4 |
re2 re,4 re,2 re'4 |
re'2 sol4 fad2 mi4 |
re2 re4 re4. mi8 fad4 |
sol2 re4 re4. mi8 fad4 |
sol2 re4 re4. mi8 fad4 |
sol2 fad4 mi2 la4 |
re2 re,4 re,2 <>^\markup\whiteout Bassons re'4 |
re'4. dod'8 re'4 re'2 sol4 |
re'4. dod'8 re'4 re'2 re4 |
re2 re4 sol,2 sol,4 |
re2 re,4 re,2 <>^\markup\whiteout Tous re4 |
dod2 dod4 si,2 si,4 |
la,2 la,4 la,2 re4 |
dod2 dod4 si,2 si,4 |
la,2 la,4 la,2 la4 |
fad4. mi8 re4 la,2 la,4 |
re2 re4 re2 fad4 |
fad4. mi8 re4 la,2 la,4 |
re,2 re,4 re,2 <>^\markup\whiteout Bassons re4 |
si,2 si,4 la,2 la,4 |
re2 re4 re2 re'4 |
si2 si4 la2 la4 |
re'2 re4 re4. <>^\markup\whiteout Tous re'8 dod'4 |
re'2 la4 la4. si8 dod'4 |
re'2 la4 la4. si8 dod'4 |
re'2 dod'4 si2 mi'4 |
la2 la,4 la,2 <>^\markup\whiteout Bassons la4 |
la4. sold8 la4 la2 re4 |
la4. sold8 la4 la2 la,4 |
la,2 la,4 re2 re4 |
la,2 la,4 la,2 <>^\markup\whiteout Tous re'4 |
fad2 fad4 fad4. mi8 re4 |
la2 la4 la2 re'4 |
fad2 fad4 fad4. mi8 re4 |
la2 la4 la2 fad4 |
sol2 sol4 la2 la4 |
re2 re4 re2 fad,4 |
sol,2 sol,4 la,2 la,4 |
re,2 re,4 re,2
