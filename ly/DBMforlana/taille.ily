\clef "taille" fad'4 |
fad'2 re'4 re'2 re'4 |
mi'2 mi'4 mi'2 fad'4 |
fad'2 re'4 re'2 re'4 |
mi'2 mi'4 mi'2 fad'4 |
re'2 sol'4 mi'2\trill mi'4 |
fad'2 fad'4 fad'2 fad'4 |
re'2 sol'4 mi'2\trill mi'4 |
fad'2 fad'4 fad'2 fad'4 |
fad'2 sol'4 re'2 mi'4 |
fad'2 fad'4 fad'2 fad'4 |
fad'2 sol'4 re'2 mi'4 |
fad'2 fad'4 fad'2 re'4 |
re'2 re'4 re'2 re'4 |
re'2 re'4 re'2 re'4 |
re'2 fad'4 sol'2 mi'4\trill |
re'2 re'4 re'2 r4 |
R1.*3 |
r2 r4 r2 re'4 |
mi'2 mi'4 re'2 re'4 |
dod'2 mi'4 mi'2 re'4 |
mi'2 mi'4 re'2 re'4 |
dod'2 mi'4 mi'2 mi'4 |
fad'4. sol'8 la'4 la'2 la'4 |
fad'2 fad'4 fad'2 mi'4 |
fad'4. sol'8 la'4 la'2 la'4 |
fad'2 fad'4 fad'2 r4 |
R1.*3 |
r2*3/2 r4 r8 la' la'4 |
la'2 la'4 la'4. la'8 la'4 |
la'2 la'4 la'4. la'8 la'4 |
la'2 la'4 si'2 si'4 |
la'2 la'4 la'2 r4 |
R1.*3 |
r2 r4 r2 fad'4 |
fad'2 re'4 re'2 re'4 |
mi'2 mi'4 mi'2 fad'4 |
fad'2 re'4 re'2 re'4 |
mi'2 mi'4 mi'2 fad'4 |
re'2 sol'4 mi'2\trill mi'4 |
fad'2 fad'4 fad'2 fad'4 |
re'2 sol'4 mi'2\trill mi'4 |
fad'2 fad'4 fad'2
