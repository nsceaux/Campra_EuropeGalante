\tag #'venus {
  Fai -- sons re -- gner l’A -- mour, fai -- sons bril -- ler ses char -- mes :
  Les doux plai -- sirs, les doux plai -- sirs sont ses plus for -- tes ar -- mes.
  Fai -- sons re -- gner l’A -- mour, fai -- sons bril -- ler ses char -- mes.
  Les doux plai -- sirs, les doux plai -- sirs sont ses plus for -- tes ar -- mes.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Fai -- sons re -- gner l’A -- mour, fai -- sons bril -- ler ses char -- mes ;
  Les doux plai -- sirs,
  \tag #'vdessus { les doux plai -- sirs }
  sont ses plus for -- tes ar -- mes.
  Les doux plai -- sirs, les doux plai -- sirs sont ses plus for -- tes ar -- mes.

  Fai -- sons re -- gner l’A -- mour,
  \tag #'(vdessus vhaute-contre) {
    fai -- sons bril -- ler ses char -- mes ;
  }
  Les doux plai -- sirs sont ses plus for -- tes ar -- mes.
  Les doux plai -- sirs,
  \tag #'vdessus { les doux plai -- sirs }
  sont ses plus for -- tes ar -- mes.
  Les doux plai -- sirs sont ses plus for -- tes ar -- mes.
}