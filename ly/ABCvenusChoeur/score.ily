\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new GrandStaff \with { instrumentName = "Flutes" } <<
      \new Staff <<
        \global \keepWithTag #'flute1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'flute2 \includeNotes "dessus"
      >>
    >>
    \new Staff\with {
      instrumentName = \markup\character Venus
    } \withLyrics <<
      \global \keepWithTag #'venus \includeNotes "voix"
    >> \keepWithTag #'venus \includeLyrics "paroles"
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \keepWithTag #'violon \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2.*34\break }
      \origLayout {
        s2.*9\pageBreak
        s2.*10\break s2.*11\break s2.*4\pageBreak
        s2.*9\pageBreak
        s2.*10\pageBreak
        s2.*11\pageBreak
        s2.*9\pageBreak
        s2.*11\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
