\clef "dessus"
<<
  \tag #'flute1 {
    la''4 la'' sib'' |
    sib''? sol'' do''' |
    do'''2 do'''4 |
    la'' la'' la'' |
    sib'' sol''4.\trill fa''8 |
    fa''2 r4 |
    R2. |
    r4 r la'' |
    la'' sol''2\trill |
    la''2 la''4 |
    sib'' la'' sol'' |
    fa'' mi''4.\trill re''8 |
    re''2 r4 |
    R2. |
    r4 r la'' |
    sol'' fa''2\trill |
    mi'' la''4 |
    sib'' la'' sol'' |
    fa'' mi''4.\trill re''8 |
    re''4 la'' la'' |
    si''2 do'''4 |
    si''2\trill do'''4 |
    do''' do''' do''' |
    si'' si''4.\trill la''8 |
    la''2 r4 |
    R2. |
    r4 r la'' |
    sol'' fa''2\trill |
    mi'' la''4 |
    la'' sol''2\trill |
    la''2 la''4 |
    sib'' la'' sol'' |
    fa'' mi''4.\trill re''8 |
    re''2. |
  }
  \tag #'flute2 {
    fa''4 fa'' fa'' |
    sol''2 sol''4 |
    la''2 mi''4 |
    fa'' fa'' fa'' |
    sol'' mi''4.\trill fa''8 |
    fa''2 la''4 |
    sol'' fa''2\trill |
    mi''\trill fa''4 |
    mi'' re''2 |
    dod''2 fa''4 |
    re'' dod'' re'' |
    re'' dod''4.\trill( si'16 dod'') |
    re''2 la''4 |
    sol'' fa''2\trill |
    mi'' fa''4 |
    mi'' re''2 |
    dod''2 fa''4 |
    re'' dod'' re'' |
    re'' dod''4.\trill re''8 |
    re''4 fad'' fad'' |
    sold''2 la''4 |
    sold''2 mi''4 |
    la'' la'' la'' |
    la'' sold''4.\trill la''8 |
    la''2 la''4 |
    sol'' fa''2\trill |
    mi'' r4 |
    R2. |
    r4 r fa'' |
    mi'' re''2 |
    dod'' fa''4 |
    re'' dod'' re'' |
    re'' dod''4. re''8 |
    re''2. |
  }
  \tag #'violon {
    re''4\doux re'' re'' |
    mi''2\trill mi''4 |
    fa''2 do''4 |
    re'' re'' re'' |
    sib' do''2 |
    fa' fa''4 |
    mi'' re''2 |
    dod'' re''4 |
    do'' sib'2\trill |
    la'2 fa'4 |
    sol' la' sib' |
    sol' la'2 |
    re'' fa''4 |
    mi'' re''2 |
    dod'' re''4 |
    dod'' re''2 |
    la' fa'4 |
    sol' la' sib' |
    sol' la'2 |
    re''4 re'' do'' |
    si'2\trill la'4 |
    mi''2 do''4 |
    fa'' fa'' fa'' |
    re'' mi''2 |
    la' fa''4 |
    mi'' re''2 |
    dod'' fa''4 |
    mi'' re''2 |
    dod'' re''4 |
    do'' sib'2\trill |
    la' fa'4 |
    sol' la' sib' |
    sol' la'2 |
    re'2. |
  }
>>
fa''4 fa'' fa'' |
re''2\trill sol''4 |
mi''2\trill mi''4 |
fa'' fa'' fa'' |
fa'' mi''4.\trill fa''8 |
fa''2 <<
  \tag #'(flute1 flute2) { la''4 | sol'' fa''2\trill | mi'' }
  \tag #'violon { fa''4 | mi'' re''2 | dod''\trill }
>> la'4 |
la' sol'2\trill |
la'2 la'4 |
re'' re'' mi'' |
fa'' mi''4.\trill mi''8 |
fa''4 fa'' fa'' |
re''2\trill re''4 |
sol''2 sol''4 |
mi''\trill mi'' mi'' |
fa'' re''4.\trill do''8 |
do''2 mi''4 |
re'' do''2 |
si'2\trill do''4 |
si' la'2 |
sold'\trill si'4 |
mi'' mi'' re''\trill |
do'' si'4.\trill la'8 |
la'2 <<
  \tag #'flute1 {
    r4 |
    R2. |
    r4 r la'' |
    sol'' fa''2\trill |
    mi'' la''4 |
    sib'' la'' sol'' |
    fa''4 mi''4.\trill re''8 |
  }
  \tag #'flute2 {
    la''4 |
    sol'' fa''2\trill |
    mi'' fa''4 |
    mi'' re''2 |
    dod''\trill fa''4 |
    re'' dod'' re'' |
    re''4 dod''4.\trill re''8 |
  }
  \tag #'violon {
    fa''4\doux |
    mi'' re''2 |
    dod''\trill re''4 |
    dod'' re''2 |
    la'2 fa'4 |
    sol' la' sib' |
    sol' la'2 |
  }
>>
re''4 la' la' |
re''2 la'4 |
sib'2 <<
  \tag #'(flute1 flute2) {
    re''4 |
    sol'' sol'' sol'' |
    sol'' fad''4.\trill sol''8 |
    sol''2
  }
  \tag #'violon {
    sib'4 |
    sib' sib' sib' |
    la' la'2\trill |
    sol'
  }
>> sib'4 |
sib' la'2\trill |
sol' do''4 |
la' la' la' |
sib' sol'2\trill |
fa' la''4 |
sol'' fa''2\trill |
mi'' mi''4 |
fa'' re'' la'' |
la'' sol''4.\trill la''8 |
la''2 <<
  \tag #'(flute1 flute2) {
    la''4 |
    sol'' fa''2\trill |
    mi''
  }
  \tag #'violon {
    fa''4 |
    mi'' re''2 |
    dod''\trill
  }
>> la'4 |
la' sol'2\trill |
la'2 la'4 |
re'' re'' mi'' |
fa'' fa''( mi'')\trill |
re''2 fa''4 |
mi'' re''2 |
dod''\trill la'4 |
re'' re'' mi'' |
fa'' mi''2\trill |
re''2. |
