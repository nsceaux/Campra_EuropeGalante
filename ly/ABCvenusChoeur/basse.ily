\clef "basse" R2.*34 |
re'4 re' la |
sib2 sol4 |
do'2 la4 |
re' re' re' |
sib do'2 |
fa r4 |
R2. |
r4 r re' |
do' sib2\trill |
la2 la4 |
fa fa mi |
re la,2 |
re4 re re |
sol2 sol4 |
mi2 mi4 |
la la la |
fa sol sol, |
do2 do'4 |
si la2 |
sold2 la4 |
sol fa2\trill |
mi mi'4 |
do' do' si\trill |
la mi2 |
la, r4 |
R2.*6 |
re'4 re' re' |
fad2 fad4 |
sol2 \clef "alto" sol'4 |
mib' mib' mib' |
do' re'2 |
sol \clef "basse" sol4 |
mi fa2 |
do' la4 |
re' re' re' |
sib do'2 |
fa fa4 |
mi re2 |
dod dod4 |
re re re |
do sib,2 |
la, r4 |
R2. |
r4 r re' |
do' sib2 |
la2 la4 |
fa fa mi |
re la,2 |
re re'4 |
do' sib2 |
la la4 |
fa fa mi |
re la,2 |
re2. |
