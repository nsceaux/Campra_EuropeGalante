\clef "taille" R2.*34 |
fa'4 fa' fa' |
fa'2 sol'4 |
sol'2 la'4 |
la' la' la' |
sib' sol'2\trill |
fa' r4 |
R2. |
r4 r fa' |
mi' re'2 |
dod'2 mi'4 |
fa' fa' sol' |
la' la'2 |
la'4 la' la' |
sol'2 sol'4 |
sol'2 sol'4 |
la' la' la' |
la' sol'2 |
sol'2 sol'4 |
si' mi'2 |
mi'2 mi'4 |
sol' do' re' |
si2\trill si4 |
do' do' re' |
mi' mi'2 |
dod' r4 |
R2.*6 |
re'4 re' re' |
re'2 re'4 |
re'2 sol'4 |
mib' mib' mib' |
do' re'2 |
sol2 re'4 |
do' do'2 |
do' do'4 |
re'4 re' re' |
re' do'2 |
la re'4 |
dod' re'2 |
mi' mi'4 |
re' re' re' |
la re'2 |
mi' r4 |
R2. |
r4 r fa' |
mi' re'2 |
dod'2 mi'4 |
fa' fa' sol' |
la' la' mi' |
fa'2 re'4 |
mi' fa' sol' |
mi'2\trill mi'4 |
fa' fa' sol' |
la' la' mi' |
fa'2. |
