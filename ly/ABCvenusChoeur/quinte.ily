\clef "quinte" R2.*34 |
re'4 re' do' |
re'2 re'4 |
mi'2 mi'4 |
re' re' re' |
fa' do'2 |
do' r4 |
R2. |
r4 r re' |
la re'2 |
mi'2 mi'4 |
re' re' dod' |
re' re'( dod') |
re' re' re' |
re'2 re'4 |
mi'2 mi'4 |
do' do' do' |
re' re'2\trill |
mi' do'4 |
re' do' la |
si2 la4 |
mi' la2 |
mi' sold4 |
la la si |
mi' mi'2 |
mi' r4 |
R2.*6 |
re'4 re' re' |
la2 do'4 |
sib2 sol'4 |
mib' mib' mib' |
do' re'2 |
sol sib4 |
sol la do' |
do'2 do'4 |
fa'4 fa' fa' |
sol' sol' do' |
do'2 fa4 |
sol la2 |
la la4 |
la la fa |
fa sol2 |
mi2\trill r4 |
R2. |
r4 r re' |
la re'2 |
mi'2 mi'4 |
re' re' dod' |
re' re'( dod') |
re'2 re'4 |
la re'2 |
la' dod'4 |
la la sol |
fa la2 |
la2. |
