<<
  \tag #'venus {
    \clef "vbas-dessus" re''4 re'' re'' |
    mi''2\trill mi''4 |
    fa''2 do''4 |
    re'' re'' re'' |
    sib' do''2 |
    fa' fa''4 |
    mi'' re''2 |
    dod'' re''4 |
    do'' sib'2\trill |
    la'2 fa'4 |
    sol' la' sib' |
    sol' la'2 |
    re' r4 |
    R2.*6 |
    re''4 re'' do'' |
    si'2\trill la'4 |
    mi''2 do''4 |
    fa'' fa'' fa'' |
    re'' mi''2 |
    la' r4 |
    R2. |
    r4 r fa'' |
    mi'' re''2 |
    dod'' re''4 |
    do'' sib'2\trill |
    la'2 fa'4 |
    sol' la' sib' |
    sol' la'2 |
    re'2. |
  }
  \tag #'vdessus { \clef "vdessus" R2.*34 }
  \tag #'vhaute-contre { \clef "vhaute-contre" R2.*34 }
  \tag #'vtaille { \clef "vtaille" R2.*34 }
  \tag #'vbasse { \clef "vbasse" R2.*34 }
>>
<<
  \tag #'vdessus {
    <>^\markup\character Les Chœurs
    fa''4 fa'' fa'' |
    re''2\trill sol''4 |
    mi''2\trill mi''4 |
    fa'' fa'' fa'' |
    fa'' fa''( mi'') |
    fa''2 fa''4 |
    mi'' re''2 |
    dod''2\trill la'4 |
    la' sol'2\trill |
    la'2 la'4 |
    re'' re'' mi'' |
    fa'' mi''2\trill |
    re''4 r r |
    R2.*4 |
    r4 r mi'' |
    re'' do''2 |
    si'2\trill do''4 |
    si' la'2 |
    sold'\trill si'4 |
    mi'' mi'' re''\trill |
    do'' si'2\trill |
    la' r4 |
    R2.*6 |
    la'4 la' la' |
    re''2 la'4 |
    sib'2 <<
      { \voiceOne re''4 |
        sol'' sol'' sol'' |
        sol'' sol''( fad'') |
        sol''2 \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'4 |
        sib' sib' sib' |
        do'' la'2\trill |
        sol'
      }
    >> sib'4 |
    sib' la'2\trill |
    sol' do''4 |
    la'4 la' la' |
    sib' sol'2\trill |
    fa' r4 |
    R2.*4 |
    r4 r fa'' |
    mi'' re''2 |
    dod''2\trill la'4 |
    la' sol'2\trill |
    la'2 la'4 |
    re'' re'' mi'' |
    fa'' fa''( mi'')\trill |
    re''2 fa''4 |
    mi'' re''2 |
    dod''2\trill la'4 |
    re'' re'' mi'' |
    fa'' mi''2\trill |
    re''2. |
  }
  \tag #'vhaute-contre {
    la'4 la' la' |
    fa'2 sib'4 |
    sol'2\trill la'4 |
    la' la' la' |
    sib' sol'2\trill |
    fa' r4 |
    R2. |
    r4 r re' |
    la re'2 |
    mi'2 sol'4 |
    fa' fa' sol' |
    la' la'2 |
    la'4 r r |
    R2.*4 |
    r4 r sold' |
    si' mi'2 |
    mi'2 mi'4 |
    mi' mi'( re') |
    mi'2 mi'4 |
    mi' fad' sold' |
    la' la'( sold') |
    la'2 r4 |
    R2.*6 |
    fa'4 fa' fa' |
    la'2 la'4 |
    sol'2 sol'4 |
    mib' mib' mib' |
    do' re'2 |
    sol sol'4 |
    sol' fa'2 |
    mi'\trill mi'4 |
    fa'4 fa' fa' |
    fa' fa'( mi') |
    fa'2 r4 |
    R2.*6 |
    r4 r re' |
    la re'2 |
    mi'2 mi'4 |
    fa' fa' sol' |
    la' la'( mi') |
    fa'2 la'4 |
    la' la'( sol') |
    la'2 mi'4 |
    fa' fa' sol' |
    la' la'( mi') |
    fa'2. |
  }
  \tag #'vtaille {
    re'4 re' do' |
    sib2 re'4 |
    do'2 do'4 |
    re' re' re' |
    re' do'2 |
    la\trill r4 |
    R2. |
    r4 r fa' |
    mi' re'2 |
    dod'2 la4 |
    la si dod' |
    re' re'( dod') |
    re' r r |
    R2.*4 |
    r4 r do' |
    sold la2 |
    si2\trill la4 |
    mi la2 |
    si si4 |
    do' do' re' |
    mi' mi'2 |
    dod'\trill r4 |
    R2.*6 |
    re'4 re' re' |
    re'2 re'4 |
    re'2 r4 |
    R2.*2 |
    r4 r re' |
    mi' do'2 |
    do' do'4 |
    re'4 re' re' |
    re' do'2 |
    la\trill r4 |
    R2.*6 |
    r4 r fa' |
    mi' re'2 |
    dod'2 la4 |
    la si dod' |
    re' re'( dod') |
    re'2 re'4 |
    la re'2 |
    mi' mi'4 |
    la si dod' |
    re' re'( dod') |
    re'2. |
  }
  \tag #'vbasse {
    re'4 re' la |
    sib2 sol4 |
    do'2 la4 |
    re' re' re' |
    sib do'2 |
    fa r4 |
    R2. |
    r4 r re' |
    do' sib2\trill |
    la2 la4 |
    fa fa mi |
    re la,2 |
    re4 r r |
    R2.*4 |
    r4 r do' |
    si la2 |
    sold2 la4 |
    sol fa2\trill |
    mi mi'4 |
    do' do' si\trill |
    la mi2 |
    la, r4 |
    R2.*6 |
    re'4 re' re' |
    fad2 fad4 |
    sol2 r4 |
    R2.*2 |
    r4 r sol |
    mi fa2 |
    do' la4 |
    re' re' re' |
    sib do'2 |
    fa r4 |
    R2.*6 |
    r4 r re' |
    do' sib2 |
    la2 la4 |
    fa fa mi |
    re la,2 |
    re re'4 |
    do' sib2\trill |
    la2 la4 |
    fa fa mi |
    re la,2 |
    re2. |
  }
>>
