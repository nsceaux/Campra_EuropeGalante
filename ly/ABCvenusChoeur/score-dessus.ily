\score {
  <<
    \new GrandStaff \with { instrumentName = "Flutes" } <<
      \new Staff <<
        \global \keepWithTag #'flute1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'flute2 \includeNotes "dessus"
      >>
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \keepWithTag #'violon \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
