\clef "haute-contre" R2.*34 |
la'4 la' la' |
fa'2 sib'4 |
do''2 do''4 |
re'' re'' re'' |
re'' do''2 |
la'\trill r4 |
R2. |
r4 r re' |
mi' fa' sol' |
mi'2 la'4 |
la' la' sol' |
fa' la'2 |
la'4 re'' re'' |
si'2\trill si'4 |
si'2 si'4 |
do'' do'' do'' |
do'' si'4. do''8 |
do''2 do''4 |
sold'4\trill la'2 |
mi'2 mi'4 |
mi' mi' re' |
mi'2 mi'4 |
mi' fad' sold' |
la' la'( sold') |
la'2 r4 |
R2.*6 |
fa'4 fa' fa' |
la'2 la'4 |
sol'2 sol'4 |
mib' mib' mib' |
do' re'2 |
sol'2 sol'4 |
sol' fa'2 |
mi'2\trill mi'4 |
fa'4 fa' fa' |
fa' fa'( mi') |
fa'2 do''4 |
mi'' la'2 |
la'2 la'4 |
la'2 fa''4 |
mi'' re''2 |
dod''\trill r4 |
R2. |
r4 r re' |
mi' fa' sol' |
mi'2\trill la'4 |
la' la' sol' |
fa' la'2 |
la' la'4 |
la' la'( sol')\trill |
la'2 la'4 |
la' si' dod'' |
re'' re''( dod'') |
re''2. |
