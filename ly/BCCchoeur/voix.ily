<<
  \tag #'vdessus {
    \clef "vdessus" <>^\markup\character Chœur re''4 |
    sol''2 re''4 |
    mi''2. |
    re''4 mi'' do'' |
    re''4. do''8( si'4) |
    si'4. do''8 re''4 |
    do''4 si'2\trill |
    la'2\trill la'4 |
    do''4. re''8 do''4 |
    si' la' sol' |
    la'4. sol'8( fad'4) |
    sol'4. la'8 si'4 |
    do'' la'2\trill |
    sol' r4 |
    R2.*3 |
    r4 r <>^\markup\character Petit chœur <<
      { \voiceOne re'' |
        re''2 sol''4 |
        mi''2.\trill |
        fad''4 sol'' fad'' |
        mi''2\trill mi''4 |
        fad''4. sol''8 fad''4 |
        mi'' mi''2\trill |
        re'' \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo si'4 |
        si'2 mi''4 |
        dod''2.\trill |
        re''4 dod'' re'' |
        la'2 la'4 |
        la' la' re'' |
        re'' dod''2\trill |
        re''2
      }
    >> re''4 |
    si'4.\trill do''8 re''4 |
    mi'' mi'' fad'' |
    red''2\trill si'4 |
    mi''4. red''8 mi''4 |
    fad''4 red''2 |
    mi'' r4 |
    R2.*9 |
    r4 r do'' |
    la'2\trill <<
      { \voiceOne re''4 |
        re''2. |
        re''4 do'' si' |
        la'2\trill la'4 | \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo la'4 |
        si'2. |
        si'4 la' sol' |
        fad'2\trill fad'4 |
      }
    >>
    si'4. do''8 re''4 |
    do''4 si'2\trill |
    la'2\trill la'4 |
    do''4. re''8 do''4 |
    si' la' sol' |
    la'4. sol'8( fad'4) |
    sol'4. la'8 si'4 |
    do'' la'2\trill |
    sol' r4 |
    R2.*9 |
    r4 r la' |
    do''4. re''8 do''4 |
    si'4 la' sol' |
    la'4. sol'8( fad'4) |
    sol'4. la'8 si'4 |
    do'' la'2\trill |
    sol'
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 |
    r r sol' |
    sol'2. |
    sol'4 sol' fad' |
    sol'2 sol'4 |
    sol'4. fad'8 sol'4 |
    la' sol'2 |
    fad'\trill fad'4 |
    mi'4. mi'8 fad'4 |
    re' re' mi' |
    fad'2 fad'4 |
    mi'4. re'8 re'4 |
    sol' fad'2\trill |
    sol' r4 |
    R2.*3 |
    r4 r sol' |
    sol'2 mi'4 |
    la'2. |
    fad'4 mi' re' |
    dod'2 dod'4 |
    re'4. mi'8 fad'4 |
    sol' la'2 |
    re'2 fad'4 |
    sol'4. sol'8 sol'4 |
    sol' sol' la' |
    fad'2\trill fad'4 |
    sol'4. la'8 sol'4 |
    fad'4 fad'2\trill |
    mi'2 r4 |
    R2.*9 |
    r4 r la' |
    fad'2\trill re'4 |
    sol'2. |
    sol'4 fad' sol' |
    re'2 re'4 |
    sol'4. fad'8 sol'4 |
    la' sol'2 |
    fad'\trill fad'4 |
    mi'4. mi'8 fad'4 |
    re' re' mi' |
    fad'2 fad'4 |
    mi'4. mi'8 re'4 |
    sol' fad'2\trill |
    sol' r4 |
    R2.*9 |
    r4 r fad' |
    mi'4. mi'8 fad'4 |
    re'4 re' mi' |
    fad'2 fad'4 |
    mi'4. mi'8 re'4 |
    sol' fad'2\trill |
    sol'2
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    r r si |
    do'2. |
    re'4 do' do' |
    si2\trill si4 |
    re'4. do'8 si4 |
    re' re'2 |
    re' re'4 |
    do'4. si8 la4 |
    si si dod' |
    re'2 re'4 |
    do'4. do'8 si4 |
    mi' re'2 |
    si\trill r4 |
    R2.*10 |
    r4 r re' |
    re'4. re'8 re'4 |
    do' do' do' |
    si2 si4 |
    si4. la8 si4 |
    do'4 si2 |
    sold2\trill r4 |
    R2.*9 |
    r4 r mi' |
    re'2 r4 |
    R2.*3 |
    re'4. do'8 si4 |
    re' re'2 |
    re' re'4 |
    do'4. si8 la4 |
    si re' dod' |
    re'2 re'4 |
    do'4. do'8 si4 |
    mi' re'2 |
    si\trill r4 |
    R2.*9 |
    r4 r re' |
    do'4. si8 la4 |
    si re' dod' |
    re'2 re'4 |
    do'4. do'8 si4 |
    mi' re'2 |
    si2\trill
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    r r sol |
    do'2. |
    si4 do' la |
    sol2 sol4 |
    sol4. la8 si4 |
    fad sol2 |
    re' re4 |
    la4. sol8 fad4 |
    sol fad mi |
    re2 re4 |
    mi4. fad8 sol4 |
    do re2 |
    sol, r4 |
    R2.*10 |
    r4 r re' |
    sol4. la8 si4 |
    do' do' la |
    si2 si4 |
    sol4. fad8 mi4 |
    la, si,2 |
    mi r4 |
    R2.*9 |
    r4 r la |
    re'2 r4 |
    R2.*3 |
    sol4. la8 si4 |
    fad sol2 |
    re' re4 |
    la4. sol8 fad4 |
    sol4 fad mi |
    re2 re4 |
    mi4. fad8 sol4 |
    do re2 |
    sol, r4 |
    R2.*9 |
    r4 r re |
    la4. sol8 fad4 |
    sol fad mi |
    re2 re4 |
    mi4. fad8 sol4 |
    do re2 |
    sol,
  }
>>
