\clef "taille" r4 |
r r sol' |
sol'2. |
sol'4 sol' fad' |
sol'2 sol'4 |
re'4. do'8 si4 |
re' re'2 |
re' fad'4 |
mi'4. mi'8 fad'4 |
re' re' dod' |
re'2 re'4 |
do'4. do'8 si4 |
mi' re'2 |
re' r4 |
R2.*3 |
r4 r sol' |
sol'2 mi'4 |
la'2. |
fad'4 mi' re' |
dod'2\trill dod'4 |
re'4. mi'8 fad'4 |
sol' la'2 |
re'2 fad'4 |
sol'4. sol'8 sol'4 |
mi' sol' fad' |
fad'2 fad'4 |
sol'4. la'8 sol'4 |
fad'4 fad'2\trill |
mi' r4 |
R2.*3 |
r4 r mi' |
mi'4. sol'8 sol'4 |
fa' sol' la' |
si'2 si'4 |
mi'4. re'8 do'4 |
fa'4 mi'2 |
mi'2 mi'4 |
re'2 re'4 |
sol'2. |
sol'4 fad' sol' |
re'2 re'4 |
re'4. do'8 si4 |
re'4 re'2 |
re' fad'4 |
mi'4. mi'8 fad'4 |
re' re' dod' |
re'2 re'4 |
do'4. do'8 si4 |
mi' re'2 |
re' r4 |
R2.*3 |
r4 r sol' |
sol'4. sol'8 la'4 |
sol' la'2 |
la'2 la4 |
si4. do'8 re'4 |
re' re'2 |
re' fad'4 |
mi'4. mi'8 fad'4 |
re'4 re' dod' |
re'2 re'4 |
do'4. do'8 si4 |
mi' re'2 |
re'
