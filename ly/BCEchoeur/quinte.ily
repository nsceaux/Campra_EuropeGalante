\clef "quinte" r4 |
r r re' |
do'2. |
re'4 do' do' |
si2\trill si4 |
si4. la8 sol4 |
re re'2 |
re'2 re'4 |
do'4. sol8 la4 |
si re' mi' |
la2 la4 |
sol4. fad8 si4 |
la la2\trill |
si
