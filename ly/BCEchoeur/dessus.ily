\clef "dessus" re''4 |
sol''2 re''4 |
mi''2. |
re''4 mi'' do'' |
re''4. do''8( si'4) |
si'4. do''8 re''4 |
do'' si'2\trill |
la'\trill la'4 |
do''4. re''8 do''4 |
si' la' sol' |
la'4. sol'8( fad'4) |
sol'4. la'8 si'4 |
do'' la'2\trill |
sol'
