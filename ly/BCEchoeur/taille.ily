\clef "taille" r4 |
r r sol' |
sol'2. |
sol'4 sol' fad' |
sol'2 sol'4 |
re'4. do'8 si4 |
re' re'2 |
re' fad'4 |
mi'4. mi'8 fad'4 |
re' re' dod' |
re'2 re'4 |
do'4. do'8 si4 |
mi' re'2 |
re'
