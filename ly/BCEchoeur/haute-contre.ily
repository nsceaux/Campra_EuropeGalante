\clef "haute-contre" r4 |
r r si' |
do''2. |
sol'4 sol' la' |
si'4. la'8( sol'4) |
sol'4. fad'8 sol'4 |
la' sol'2 |
fad'\trill la'4 |
la'4. si'8 la'4 |
sol' re' sol' |
fad'2\trill fad'4 |
mi'4. mi'8 re'4 |
sol' fad'2\trill |
sol'2
