\clef "basse" r4 |
r r sol |
do'2. |
si4 do' la |
sol2 sol4 |
sol4. la8 si4 |
fad sol2 |
re' re4 |
la4. sol8 fad4 |
sol fad mi |
re2 re4 |
mi4. fad8 sol4 |
do re2 |
sol,
