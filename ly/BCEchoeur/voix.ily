<<
  \tag #'vdessus {
    \clef "vdessus" re''4 |
    sol''2 re''4 |
    mi''2. |
    re''4 mi'' do'' |
    re''4. do''8( si'4) |
    si'4. do''8 re''4 |
    do''4 si'2\trill |
    la'2\trill la'4 |
    do''4. re''8 do''4 |
    si' la' sol' |
    la'4. sol'8( fad'4) |
    sol'4. la'8 si'4 |
    do'' la'2\trill |
    sol'
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 |
    r r sol' |
    sol'2. |
    sol'4 sol' fad' |
    sol'2 sol'4 |
    sol'4. fad'8 sol'4 |
    la' sol'2 |
    fad'\trill fad'4 |
    mi'4. mi'8 fad'4 |
    re' re' mi' |
    fad'2 fad'4 |
    mi'4. re'8 re'4 |
    sol' fad'2\trill |
    sol'
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    r r si |
    do'2. |
    re'4 do' do' |
    si2\trill si4 |
    re'4. do'8 si4 |
    re' re'2 |
    re' re'4 |
    do'4. si8 la4 |
    si si dod' |
    re'2 re'4 |
    do'4. do'8 si4 |
    mi' re'2 |
    si\trill
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    r r sol |
    do'2. |
    si4 do' la |
    sol2 sol4 |
    sol4. la8 si4 |
    fad sol2 |
    re' re4 |
    la4. sol8 fad4 |
    sol fad mi |
    re2 re4 |
    mi4. fad8 sol4 |
    do re2 |
    sol,
  }
>>
