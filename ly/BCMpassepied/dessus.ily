\clef "dessus" sol''8 |
sol'' fad'' mi'' |
re'' si'16 do'' re'' si' |
do''8 la'16 si' do'' la' |
si'8 si' sol'' |
sol'' fad'' mi'' |
re'' si'16 do'' re'' si' |
do''8 la'16 si' do'' la' |
si'4 re''8 |
re'' mi'' fad'' |
sol'' mi''16 fad'' sol'' mi'' |
la''8 sol'' fad'' |
mi'' dod'' re'' |
mi'' mi''16 fad'' sol'' mi'' |
fad''4 sol''8~ |
sol'' mi''4\trill |
re'' re''8 |
re'' do'' si' |
la' la'16 si' do'' la' |
si'8 la' sol' |
la' fad'\trill sol' |
la' la'16 si' do'' la' |
si'4 do''8~ |
do'' la'4\trill |
sol'
