\clef "basse" sol8 |
mi re do |
si, si, mi |
la, la, re |
sol,4 sol8 |
mi re do |
si, si, mi |
la, la, re |
sol,4 sol8 |
sol sol fad |
mi la sol |
fad mi re |
la la re' |
dod' dod' la |
re'4 sol8~ |
sol la4 |
re4 re8 |
si, la, sol, |
re fad re |
sol la si |
do' do' si |
la re' fad |
sol4 do8~ |
do re4 |
sol,
