\clef "haute-contre" si'8 |
do'' si' do'' |
sol' sol' sol' |
sol' sol' fad' |
sol' sol' si' |
do'' si' do'' |
sol' sol' sol' |
sol' sol' fad' |
sol'4 si'8 |
si' si' re'' |
mi'' dod'' dod'' |
re'' dod'' re'' |
dod'' la' la' |
la' la' la' |
la'4 re''8~ |
re'' dod''4\trill |
re''4 la'8 |
si' fad' sol' |
fad'\trill la' fad' |
sol' fad' sol' |
fad'\trill re' sol'~ |
sol' fad' la' |
sol'4 sol'8~ |
sol' fad'4\trill |
sol'4
