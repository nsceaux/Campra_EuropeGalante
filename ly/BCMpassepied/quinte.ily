\clef "quinte" sol8 |
sol si mi |
sol sol sol |
la la la |
sol sol sol |
sol si mi |
sol sol sol |
la la la |
sol4 re'8 |
re' si la |
si la la |
la mi' la |
la mi' re' |
la la dod' |
re'4 mi'8~ |
mi' mi'4\trill |
fad'4 re'8 |
sol la si |
do' la re' |
si do' sol |
do' la si |
do' re' re' |
si4 la8~ |
la la4\trill |
si4
