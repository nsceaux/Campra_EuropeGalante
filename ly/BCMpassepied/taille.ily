\clef "taille" re'8 |
do' re' sol |
si re' mi' |
mi' mi' re' |
re' re' re' |
do' re' sol |
si re' mi' |
mi' re' re' |
re'4 sol'8 |
sol' sol' la' |
sol' la' mi' |
fad' sol' la' |
la' la' fad' |
sol' mi' la' |
fad'4 si'8~ |
si' la'4 |
la' fad'8 |
sol' la' re' |
re' re' re' |
re' do' re' |
do' do' re' |
mi' re' re' |
re'4 mi'8~ |
mi' re'4 |
re'
