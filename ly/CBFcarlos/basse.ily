\clef "basse" sib,1 |
la, |
sib,2 la,4. sol,8 |
re2 mib8 do re re, |
sol,1~ |
sol,2 re4~ |
re2 mib~ |
mib re8 mib fa fa, |
sib,1~ |
sib,4 sib sib sib |
re2. re4 |
mib4. fa8 sol4. la8 |
sib2 sib4 sol |
do'4. sib8 la4.\trill sol8 |
fa4. mib8 re4 sib, do do, |
fa,2 fa8 mib re do |
fa4 fa fa fa |
\appoggiatura sol16 fad4. fad8 fad4 sol |
re4 re re' re'8 fad |
sol4 do re re, |
sol,4 sol sol sol |
\appoggiatura sol16 fa4. fa8 fa4 sol |
\appoggiatura fa16 mib4 mib re re8 mib! |
do4\trill sib, fa fa, |
sib,1 |
