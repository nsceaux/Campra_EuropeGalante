\clef "dessus" R1*5 R2. R1*3 |
r4 re''^"Violons" \doux re'' sib' |
fa''2. re''4 |
sol'4. la'8 sib'4. do''8 |
re''2 re''4 sol'' |
mi''4.\trill re''8 do''4. sib'8 |
la'2\trill la'4 sib' sol' do'' |
la'1\trill |
la'4\trill do'' do'' do'' |
do''2 do''4 sib' |
la'\trill la' re''4. re''8 |
re''4 mib'' la'4.\trill sol'8 |
sol'4 mib'' mib'' mib'' |
mib'' re'' re'' re'' |
re'' do'' do'' sib' |
mib'' re'' do''4.\trill sib'8 |
sib'1 |
