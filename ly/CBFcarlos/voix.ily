\clef "vbasse" <>^\markup\character Dom Carlos
r8 fa fa16 fa fa fa sib8. sib16 re\trill re re mib |
\appoggiatura mib8 fa fa r16 la la do' fa4 mib!8 mib16 re |
re8.\trill re16 r8 fa16 fa fad8\trill fad16 fad fad8 sol |
\appoggiatura sol8 la4 re'8 sib \appoggiatura la16 sol8 sol16 sol sol8 fad |
\appoggiatura fad8 sol4 sol8 r sib4 r8 sib |
re4 re8 sib, fa fa |
fa fa sol lab sol4\trill sol |
mib8 fa sol la sib4 sib8 la |
sib1 |
r4 sib sib sib |
re2. re4 |
mib4. fa8 sol4. la8 |
sib2 sib4 sol |
do'4. sib8 la4.\trill sol8 |
fa4. mib8 re4 sib, do do |
fa2 r |
fa4 fa fa fa |
\appoggiatura sol16 fad4. fad8 fad4 sol |
re re re' re'8 fad |
sol4 do re4. re8 |
sol,4 sol sol sol |
\appoggiatura sol16 fa4. fa8 fa4 sol |
\appoggiatura fa16 mib4 mib re re8 mib! |
do4\trill sib, fa4. fa8 |
sib,1 |
