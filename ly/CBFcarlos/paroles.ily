Vous ne pa -- rois -- sez point, in -- gra -- te Le -- o -- no -- re,
mé -- pri -- sez- vous qui vous a -- do -- re ?
Se peut- il que mon tendre a -- mour
ne flé -- chis -- se ja -- mais vôtre a -- me ?
Quoy, la nuit, si pro -- pice à l’a -- mou -- reu -- se flâ -- me,
ne me sert pas mieux que le jour ?
N’est- il pas temps qu’un sort heu -- reux ré -- pon -- de
aux soins trop é -- prou -- vez de ma sin -- cere ar -- deur ?
- deur ?
Le plus fi -- delle a -- mant du mon -- de
n’a- t’il pas droit sur vô -- tre cœur ?
Le plus fi -- delle a -- mant du mon -- de
n’a- t’il pas droit sur vô -- tre cœur ?
