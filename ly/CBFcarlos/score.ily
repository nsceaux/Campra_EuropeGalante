\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2 \bar "" \break
        s2 s1*2 s2.\break s1*3\break s1*5 s1.\break s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
