\key fa \major
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 2/2 \midiTempo#160 \beginMark "Air" \bar ".!:" s1*5
\time 3/2 s1.
\digitTime\time 2/2 \alternatives s1 s1 s1*8 \bar "|."
