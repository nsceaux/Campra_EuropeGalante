\clef "dessus" sol'4 |
sib'2. sol'4 |
sib'2. sib'4 |
sib' la' sol' fad' |
sol'8 fad' sol' la' sol'4 sol' |
sib'2. sol'4 |
sib'2. sib'4 |
sib' la' sol' fad' |
sol'2. sib'4 |
re''2. sib'4 |
re''2. re''4 |
re'' do'' sib' la' |
sib'8 la' sib' do'' sib'4 re'' |
re'' mib'' re'' do'' |
sib' re'' re'' do'' |
sib'2 la'\trill |
sib'2. sib'4 |
la'4.\trill sol'8 la'4 sib' |
la' sol' la' sib' |
la' sib' la' sol' |
fad'8 mi' fad' sol' fad'4 fad'' |
fad'' mi'' fad'' sol'' |
fad'' re'' mi'' fad'' |
sol''2 fad'' |
sol''2.
