\clef "basse" <>^\markup\whiteout Bassons sol4 |
sol8 fad sol la sol4 sol |
sol8 fad sol la sol4 sol,8 la, |
sib,4 do re re, |
sol,2. sol4 |
sol8 fad sol la sol4 sol |
sol8 fad sol la sol4 sol,8 la, |
sib,4 do re re, |
sol,2. sib4 |
sib8 la sib do' sib4 sib |
sib8 la sib do' sib4 sib,8 do |
re4 mib fa fa, |
sib,2. sib4 |
sib2. sib4 |
sib2. la4 |
sib4 mib fa fa, |
sib,2. sib,4 |
fa2. sib,4 |
fa2. sib,4 |
do re mib do |
re2. re'4 |
re'2. re'4 |
re' do' sib la |
sol2 re |
sol,2.
