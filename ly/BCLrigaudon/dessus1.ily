\clef "dessus" <>^"Hautbois" sib'4 |
re''2. sib'4 |
re''2. re''4 |
re''4 do'' sib'\trill la' |
sib'8 la' sib' do'' re''4 sib' |
re''2. sib'4 |
re''2. re''4 |
re'' do'' sib' la' |
sol'2. re''4 |
fa''2. re''4 |
fa''2. fa''4 |
fa'' mib'' re'' do'' |
re''8 do'' re'' mib'' re''4 fa'' |
fa'' sol'' fa'' mib'' |
re'' fa'' fa'' mib'' |
re''2 do''\trill |
sib'2. re''4 |
do''4.\trill sib'8 do''4 re'' |
do''\trill sib' do'' re'' |
mib'' re'' do'' sib' |
la'8\trill sol' la' sib' la'4 la'' |
la'' sol'' la'' sib'' |
la'' fad'' sol'' la'' |
sib''2 la''\trill |
sol''2.
