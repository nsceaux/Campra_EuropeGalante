\clef "taille" r2 |
r re'4 do' |
sib re' re' mib' |
re' sol' sol' fad' |
sol'2 r |
r re'4 do' |
sib re' re' mib' |
re' sib do' la |
sib2 r |
R1*6 |
r2 re'4 do' |
sib re' re' mib' |
re' sol' sol' fad' |
sol'2 r |
r re'4 do' |
sib re' re' mib' |
re' sib do' la |
sib2 r |
R1*6 |
r2 re'4 do' |
sib re' re' mib' |
re' sol' sol' fad' |
sol'2 r |
r re'4 do' |
sib re' re' mib' |
re' sib do' la |
sib2
