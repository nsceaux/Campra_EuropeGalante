\clef "basse" <>^"Tous" r2 |
r sol4 la |
sib sol sib do' |
re' do'8 sib la4 re' |
sol2 r |
r sol,4 la, |
sib, sol, sib, do |
re mib do re |
sol,2 <>^\markup\whiteout "Bassons" sol4 fa8 mib |
re4 mib re do |
sib, do re mib |
fa fa fa fa |
fa8 sol fa mib re4 sib, |
mib,2 fa, |
sib, r |
r <>^"Tous" sol4 la |
sib sol sib do' |
re' do'8 sib la4 re' |
sol2 r |
r sol,4 la, |
sib, sol, sib, do |
re mib do re |
sol,2 <>^\markup\whiteout "Bassons" sol4 la |
sib la sib sol |
la8 sib la sol fa4 sol |
la la la la |
la8 sib la sol fa4 re |
sol,2 la, |
re, r |
r <>^"Tous" sol4 la |
sib sol sib do' |
re' do'8 sib la4 re' |
sol2 r |
r sol,4 la, |
sib, sol, sib, do |
re mib do re |
sol,2
