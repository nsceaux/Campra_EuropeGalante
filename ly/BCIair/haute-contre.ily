\clef "haute-contre" r2 |
r sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' la' la' |
sol'2 r |
r sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' mib' re' |
re'2 r |
R1*6 |
r2 sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' la' la' |
sol'2 r |
r sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' mib' re' |
re'2 r |
R1*6 |
r2 sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' la' la' |
sol'2 r |
r sol'4 fa' |
fa' sib' sib' la'8 sol' |
fad'4 sol' mib' re' |
re'2
