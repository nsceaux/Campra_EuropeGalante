\clef "quinte" r2 |
r sib4 fa |
sib sib re' sol' |
re' re' do' re' |
re'2 r |
r sib4 fa |
sib sib re' sol re' sol la la |
sol2 r |
R1*6 |
r2 sib4 fa |
sib sib re' sol' |
re' re' do' re' |
re'2 r |
r sib4 fa sib sib re' sol |
re' sol la la |
sol2 r |
R1*6 |
r2 sib4 fa |
sib sib re' sol' |
re' re' do' re' |
re'2 r |
r sib4 fa |
sib sib re' sol |
re' sol la la |
sol2
