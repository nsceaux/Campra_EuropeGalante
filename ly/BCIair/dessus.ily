\clef "dessus" <>^"Tous" sol'4 la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sib' do'' la' |
sib' sol' sol' la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sol' sol' fad' |
sol'2 <>^"Hautbois" \twoVoices#'(dessus1 dessus2 dessus) <<
  { re''4 mib'' |
    fa'' fa'' fa'' fa'' |
    fa''2 fa''4 mib''8 re'' |
    do''4 re'' mib'' re'' |
    do''2\trill re''8 do'' re'' mib'' |
    re''2 do''\trill |
    sib' }
  { sib'4 do'' |
    re'' re'' re'' re'' |
    re''2 re''4 do''8 sib' |
    la'4 sib' do'' sib' |
    la'2\trill sib'8 la' sib' do'' |
    sib'2 la'\trill |
    sib' }
>> <>^"Tous" sol'4 la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sib' do'' la' |
sib' sol' sol' la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sol' sol' fad' |
sol'2 <>^"Hautbois" \twoVoices#'(dessus1 dessus2 dessus) <<
  { sib''4 la'' |
    sol'' la'' sol'' fa'' |
    mi''2\trill la''4 sol''8 fa'' |
    mi''4 fa'' sol'' fa'' |
    mi''2\trill fa''8 mi'' fa'' sol'' |
    fa''2 mi''\trill |
    re'' }
  { sol''4 fa'' |
    mi'' fa'' mi'' re'' |
    dod''2 fa''4 mi''8 re'' |
    dod''4 re'' mi'' re'' |
    dod''2 re''8 dod'' re'' mi'' |
    re''2 dod'' |
    re''2 }
>> <>^"Tous" sol'4 la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sib' do'' la' |
sib' sol' sol' la' |
sib' sol' sib' do'' |
re''2 re''4 do''8 sib' |
la'4 sol' sol' fad' |
sol'2
