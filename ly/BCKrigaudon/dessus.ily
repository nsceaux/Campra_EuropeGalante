\clef "dessus" re''4 |
re''2 sol'' |
re''4. do''8 sib'4 la' |
sol' la' sib' do'' |
re''8 do'' re'' mib'' re''4 re'' |
re''2 sol'' |
re''4. do''8 sib'4 la' |
sol' la' sib' do'' |
re''2. fa''4 |
fa''2 sib'' |
fa''4. mib''8 re''4 fa'' |
mib''4 re'' do''\trill sib' |
do''8 sib' do'' re'' do''4 fa' |
do''2 do'' |
do''4.\trill sib'8 do''4 re'' |
mib''2 do''\trill |
sib'2. re''4 |
re''2 re'' |
re''4. do''8 re''4 mib'' |
re'' do'' sib' la' |
sol'8 fad' sol' la' sol'4 re'' |
re''2 re'' |
re''4. do''8 re''4 mi'' |
fad'' sol'' la'' fad'' |
sol''2.
