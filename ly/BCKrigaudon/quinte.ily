\clef "quinte" sib4 |
sib2 sol |
la4 la sib re' |
sol sol fa mib |
re2 re4 sib |
sib2 sol |
la4 la sib re' |
sol sol fa mib |
re2. re'4 |
re'2 fa' |
fa'4 do' re' re' |
mib' fa' fa sib |
la2\trill la4 la |
la2 la |
la4. sib8 la4 re' |
do'2 do' |
re'2. sib4 |
sib2 sib |
sib4. fa8 sib4 mib' |
fa' mib' re' do' |
sib8 la sib do' sib4 sib |
sib2 sib |
sib4. fa8 sib4 sib |
la sol do' la |
sib2.
