\clef "basse" sol4 |
sol2 sol |
fad4 re sol fa? |
mib mib re do |
sib,2 sib,4 sol |
sol2 sol |
fad4 re sol fa? |
mib mib re do |
sib,2. sib4 |
sib2 sib |
la4 fa sib re |
do sib, la, sol, |
fa,2 fa,4 fa |
fa2 fa |
fa4. sol8 la4 sib |
mib2 fa |
sib,2. sib4 |
sib2 sib |
sib4. la8 sib4 do' |
sib do' re' re |
sol2 sol4 sol,8 la, |
sib,2 sib, |
sib,4. la,8 sib,4 do |
re mib do re |
sol,2.
