\clef "taille" sol'4 |
sol'2 re' |
re' re'4 fa' |
sib do' re' mib' |
fa'2 fa'4 sol' |
sol'2 re' |
re' re'4 fa' |
sib do' re' mib' |
fa'2. sib'4 |
sib'2 sib' |
do''4 la' sib' sib' |
do'' fa' fa' mi' |
fa'2 fa'4 do' |
fa'2 fa' |
fa'4. mi'8 fa'4 fa' |
sol'2 fa' |
fa'2. fa'4 |
fa'2 fa' |
fa'4. mib'8 re'4 do' |
re' sol' re' re' |
re'2 re'4 sol' |
fa'2 fa' |
fa' fa'4 mi' |
re' sol' mib' re' |
re'2.
