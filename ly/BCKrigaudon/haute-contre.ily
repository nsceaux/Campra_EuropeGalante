\clef "haute-contre" sib'4 |
sib'2 sib' |
la'4 fad' sol' re' |
mib' fa' sol' la' |
sib'8 la' sib' do'' sib'4 sib' |
sib'2 sib' |
la'4 fad' sol' re' |
mib' fa' sol' la' |
sib'2. re''4 |
re''2 re'' |
do''4 do'' sib' sib' |
la' sib' fa' sol' |
la'8 sol' la' sib' la'4 fa' |
la'2 la' |
la'4. sol'8 fa'4 sib' |
sib'2 la'\trill |
sib'2. sib'4 |
sib'2 sib' |
sib'4. do''8 sib'4 la' |
sib' sol' sol' fad' |
sol'2 sol'4 sib' |
sib'2 sib' |
sib'4. do''8 sib'4 sol' |
do''4 sib' la' la' |
sol'2.
