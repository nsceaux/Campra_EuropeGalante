\clef "vbas-dessus" <>^\markup\character Olimpia
R2.*4 |
r4 re''8 re''16 re'' la'8 la'16 la' mi'8\trill mi'16 fa' |
fa'8 fa' r4 fa'' la'16 la' la' sib' |
do''8 do'' sib' la' sol' fa' |
mi'4\trill r8 sol'16 sol' sol'8\trill sol'16 la' |
sib'8 sol' re'' re'' re'' mi'' |
\appoggiatura mi''8 fa'' fa'' r la'16 la' re''8 re''16 fa'' |
sib'4 la'8 sib' sol'4\trill la'8 sib' |
la'2\trill r |
r4 r fa'' |
\appoggiatura mi''8 re''4. re''8 re'' la' |
\appoggiatura la'16 sib'2 sol'4 |
sol'4. sol'8 fa' mi' |
fa'4 re'8 fa' sol' la' |
sib'4 la' do'' |
fa'2 fa'8*3/2 sol'8*1/2 |
\appoggiatura sol'16 la'2. |
r8 dod'' dod'' dod'' re'' mi'' |
\appoggiatura mi''16 fa''2 re''8 do'' |
\appoggiatura do''8 si'4. la'16[ sold'] la'8 la'( |
sold'4)\trill mi''8 mi'' sold'! si' |
mi'2 si'8 do'' |
do''4( si'2)\trill |
la'2. |
r8 la' la' la' fa'' fa'' |
re''2\trill sol''8 fa'' |
\appoggiatura fa''16 mi''4. re''16([ dod'']) re''8 re''( |
dod''4)\trill dod''8 dod'' dod'' mi'' |
la'2 la'8 sib' |
fa'4( mi'2)\trill |
re'2. |
re''4 r8 re'' la' la'16 la' la'8 la' |
fad'4 r8 la' la' la' si' do'' |
si'\trill sol' do'' do'' do'' si' |
do'' do'' r do'' sol'\trill sol' sol' la' |
sib'4 sib'8 sib'16 sib' sib'8. la'16 |
la'4\trill la'16 la' si' do'' re''8 mi''16 fa'' |
mi''8\trill mi'' r la'16 la' fad'4 r |
