\clef "dessus2" R2.*4 R1*2 R2.*4 R1*2 |
r4 r la'^"Violons" \doux |
la'2 la'4 |
re'2 re'4 |
re'4 dod'4.\trill dod'8 |
re'4. re'8 mi' fad' |
sol'4 fa' la' |
re'2 re'8. mi'16 |
dod'2.\trill |
r8 la' la' la' re'' dod'' |
re''2. |
fad'2 si'4 |
si'2 mi''8 sold' |
la'2 sold'8 la' |
la'4 sold'4.\trill la'8 |
la'2. |
r8 la' la'4. la'8 |
sib'2. |
si'!4 si'4. si'8 |
la'2 mi'4 |
dod' re' re' |
re'( dod'4.)\trill re'8 |
re'2. |
la'4. la'8 fad'4. fad'8 |
re'2 fad'4. fad'8 |
sol'4. sol'8 fa' sol' |
mi'2\trill~ mi'8 mi' mi' fad' |
sol'4 sol'4. sol'8 |
fa'2 la'4 |
dod'4. dod'8 re'4 r |
