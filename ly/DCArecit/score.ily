\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*4 s1\break s1 s2.*3\break \grace s8  s2. s1*2\break
        s2.*7\break \grace s16 s2.*7\pageBreak
        s2.*6\break s2.*2 s1*2 s2.\break s1 s2.*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
