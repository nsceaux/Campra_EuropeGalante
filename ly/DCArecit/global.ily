\key la \minor
\digitTime\time 3/4 \midiTempo#120 s2.*4
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*4
\time 4/4 s1*2
\beginMark "Air" \digitTime\time 3/4 \midiTempo#120 s2.*22
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
