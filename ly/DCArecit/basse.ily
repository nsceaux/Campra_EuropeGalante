\clef "basse" re'4. do'8 re'4 |
sib4. la8 sol4 |
la4. sol8 fa4 |
sib sol la |
re2. dod4 |
re1 |
la,4 sib,2 |
do2. |
sol2 fa8 mi |
re2. |
mi4 fa8 sib, do2 |
fa, fa8 sol fa mi |
re2 re4 |
fad,2. |
sol, |
la, |
re4. do8 sib, la, |
sol,4 re do |
sib,2. |
la, |
la4. sol8 fa mi |
re2. |
red |
mi2 re4 |
do2 si,8 la, |
mi4 mi,2 |
la,4. sol,8 fa, mi, |
re,2 re4 |
sib2. |
sold |
la2 sol4 |
fa fa,4. sol,8 |
la,2. |
re, |
re1~ |
re |
sol4 mi re |
do2. sib,8 la, |
sol,4 mi,2 |
fa, re,4 |
la,2 re, |
