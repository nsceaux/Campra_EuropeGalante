Qu’est de -- ve -- nu le ja -- loux qui m’ob -- se -- de ?
Ciel ! quel est le su -- jet de son é -- loi -- gne -- ment ?
Au -- roit- il re -- con -- nu l’ar -- deur qui me pos -- sè -- de ?
Mes re -- gards n’ont- ils pas dé -- cou -- vert mon a -- mant ?
Peut- ê -- tre de nos yeux la douce in -- tel -- li -- gen -- ce
n’a pû gar -- der le se -- cret de nos cœurs ;
Ces in -- dis -- crets té -- moins de nos ten -- dres lan -- gueurs
ont en -- fin rom -- pu le si -- len -- ce.
Ces in -- dis -- crets té -- moins de nos ten -- dres lan -- gueurs
ont en -- fin rom -- pu le si -- len -- ce.
Ah ! faut- il qu’une in -- jus -- te loy
des -- tine à ce ja -- loux le res -- te de ma vi -- e ;
Les soins que son Ri -- val a lais -- sé voir pour moy,
me font re -- dou -- ter sa fu -- ri -- e ;
Que je crains…
