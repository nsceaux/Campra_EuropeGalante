\clef "dessus" r2 do''2.( si'4) |
si'1\trill mi''2 |
mi'' re''2.\trill mi''4 |
mi''1 r4 sold'' |
sold''2 sold''2.\trill sold''4 |
la''2 mi'' fa'' |
re''\trill re''2. do''8( si') |
do''2 la' do''4 do'' |
fa''2 fa''2. sol''4 |
mi''2.\trill mi''4 mi''2 |
re''2 do''2.\trill( si'8) do'' |
si'1\trill r4 sol'' |
sol''4.( fa''8) fa''2. fa''4 |
fa''2 \appoggiatura mi''8 re''2 mi'' |
mi''( re''2.)\trill do''4 |
do''1 r4 mi'' |
la''2 fad''2.\trill fad''4 |
fad''2 si''1~ |
si''2 la''4 sol'' fad'' mi'' |
red''1. |
r2 red''2. red''4 |
mi''1 mi''2 |
mi''2 red''2.\trill mi''4 |
mi''1. |
r2 mi''2. fad''4 |
\appoggiatura fad''16 sold''2 sold'' la'' |
\appoggiatura la''16 si''2 si'2. do''4 |
do''2( si'2.)\trill la'4 |
\clef "dessus2" la'2\doux la'1 |
mi'1 mi'2 |
mi' re'2.\trill mi'4 |
mi'1 sold'2 |
la'1 mi'2 |
fa' la'1 |
la'2( sold'2.)\trill la'4 |
la'1 mi'2 |
fa'2. do'4 fa' sol' |
sol'2 re' mi' |
do'1.~ |
do'2 si2.\trill do'4 |
do'1. |
r2 la'1 |
mi'1 mi'2 |
mi' re'2.\trill mi'4 |
mi'1 sold'2 |
la'1 mi'2 |
fa' la'1 |
la'2( sold'2.) la'4 |
la'1. |
\origVersion\clef "dessus" r2 mi''2.\fort fad''4 |
sold''2 sold'' la'' |
\appoggiatura la''8 si''2 si'2. do''4 |
do''2( si'2.)\trill la'4 |
la'2\douxSug \origVersion\clef "dessus2" do'4. do'8 |
si4 mi'4. mi'8 |
mi'8 la' sol' do'' si'4.\trill si'8 |
si'4 mi'2 mi'4 |
red'2\trill fad'4 |
sol' red'8 mi' mi'4 red' |
mi'2 mi'4. mi'8 |
re'2 la'4 |
si'4. si'8 mi'4 la' |
sold'2. mi'4 |
do'4. do'8 do' si |
do'2 re'4. mi'8 |
mi'2 do'8 re' si8.\trill si16 |
do'4 \origVersion\clef "dessus" r8 mi''\fort mi''8. re''16 re''8.\trill do''16 |
si'8.\trill mi''16 mi''8 la'' la''4( sold''8.) la''16 |
la''4 \origVersion\clef "dessus2" la'4\doux fad'8. fad'16 sol'8 la' |
si'4 sol'4. sol'8 |
sol'4 sol' fad' |
sol'2 r8 si' |
si'2 r8 si' |
si'4 si' la' |
si' fad' r8 sol' |
mi'\trill mi' la' sol' fad' si' |
sold'2. |
r4 r r8 mi' |
mi'2 r8 mi' |
mi'4 mi' re' |
mi'2 r8 do'' |
la'\trill la' si' do'' sold'8.\trill la'16 |
la'2. la'4 |
la' si'2 |
mi'4 la'~ la'2~ |
la'4 la'8. la'16 la'4 sold'8.\trill la'16 |
la'1 |
