\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*10\pageBreak
        s1.*10\break s1.*8\pageBreak
        s1.*7\break s1.*7\pageBreak
        s1.*8\break s1.*3 s1 s2.\pageBreak
        s1*2 s2.\break s1*2 s2.\pageBreak
        s1*2 s2. s2 \bar "" \break s2 s1*3\pageBreak
        s1 s2.*2 s2 \bar "" \break s4 s2.*7\pageBreak
        s2.*3 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
