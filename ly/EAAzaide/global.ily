\key la \minor \midiTempo#240
\time 3/2 s1.*53
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 \grace s16 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.*2 \midiTempo#160 s2.*11 \bar "||"
\time 4/4 \key la \major \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3 \bar "|."
