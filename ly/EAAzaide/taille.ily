\clef "taille" r2 mi'1 |
sol' mi'2 |
fa' la1 |
mi' r4 mi' |
fa'2 fa'2. fa'4 |
mi'2 la1 |
mi'2 mi'2. mi'4 |
mi'1 la'2 |
sol'2 sol'2. sol'4 |
sol'1 sol'2~ |
sol' sol' fad' |
sol' re' mi' |
do'1 re'2 |
re' sol' do'~ |
do' sol'2. sol'4 |
mi'1\trill r4 mi' |
mi'2 fad'2. fad'4 |
fad'1 red'2 |
mi'1 la'2 |
fad'1. |
r2 fad'2. fad'4 |
fad'2 mi'2. fad'4 |
sol'2 fad'2.\trill mi'4 |
mi'1. |
r2 sol'2. la'4 |
si'2 si' mi' |
re' fa'1 |
mi'~ mi'4 mi' |
mi'2\douxSug do' la |
si1 sol2 |
la1. |
si1 si2 |
la1 mi'2~ |
mi' re'1 |
re'2 re' si |
do'1 la2 |
la2. la4 re' sol |
si1 sol2 |
fa1 fa2 |
re re2.\trill do4 |
do1. |
r2 do' la |
si1 sol2 |
la1. |
si1 si2 |
la1 mi'2~ |
mi' re'1~ |
re'2 re' si |
do'1. |
r2 sol'2.\fortSug la'4 |
si'2 si' mi' |
re' fa'1 |
mi'1. |
mi'2\douxSug mi4 la |
sol2 do'4 |
do'8 fa' mi' do' re'2 |
si4 la2 fad4 |
fad?2 si4 |
si la8 si si4. si8 |
si4 sold mi la |
la2 re'4 |
si2 do'4 la |
si2. mi4 |
la fa2 |
sol4 mi' re'4. do'8 |
sold4. mi'8 la4 mi'8. mi'16 |
mi'4 r8 mi'\fortSug fa'4. fa'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4 la2\douxSug la4 |
sol2 re'4 |
sol8. la16 la4.\trill sol8 |
sol2 r8 sol |
si2 r8 si |
mi2 mi4 |
si2 r8 mi' |
mi'4 red'8 si si8. si16 |
si2. |
r4 r r8 mi |
mi2 r8 mi |
la4 la la |
si2 r8 la |
la la sold la sold8. la16 |
la2. dod'4 |
dod' si2 |
dod'4. la8 la2 |
la4 si si4. si8 |
la1 |
