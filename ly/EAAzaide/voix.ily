\clef "vbas-dessus" <>^\markup\character Zaïde
R1.*28 |
r2 do''2.( si'4) |
si'2\trill r4 si' si' do'' |
\appoggiatura si'16 la'2.( sold'4) la'2 |
sold'1\trill si'2 |
do'' re'' mi'' |
la'1 si'4 do'' |
do''2( si'1)\trill |
la'1 r4 do'' |
la'\trill la' la' la' si'\trill do'' |
\appoggiatura do''16 re''2 re'' r4 sol' |
sol'1 sol'4 la' |
\appoggiatura sol'8 fa'1 fa'4 mi' |
mi'1.\trill |
r2 do''2.( si'4) |
si'2.\trill si'4 si' do'' |
\appoggiatura si'8 la'2.( sold'4) la'2 |
sold'1\trill si'2 |
do'' re'' mi'' |
la'1 si'4 do'' |
do''2( si'1)\trill |
la'1. |
R1.*4 |
r4 r8 la'16 la' mi'8\trill mi'16 mi' mi'8 fa' |
sol'8 sol' r sol' sol'16 sol' la' si' |
do''8 do''16 re'' mi''8 mi''16 fa'' re''4\trill r8 re''16 re'' |
re''8 mi''16 si' do''4 mi'8 mi'16 mi' la'8. la'16 |
fad'8\trill fad' r si'16 si' si'8\trill si'16 si' |
mi''4 la'8 sol' \appoggiatura sol'16 fad'4 sold'8 la' |
sold'4\trill r8 mi''16 mi'' la'8 la'16 la' la'8 mi' |
\appoggiatura mi'16 fa'8 fa' r la' la' la' |
re''4 re''8 mi'' do''4\trill do''8 si' |
si'4\trill mi'' r8 si'16 si' do''8 do'' |
la'4.\trill la'16 do'' fa'8 fa'16 mi' |
mi'4\trill mi'16 mi' mi' fad' sol'4 sold'8 la' |
\appoggiatura la'8 si' mi' mi'' mi''16 sold' la'8 la' la' sold' |
la'4 r r2 |
R1 |
r4 do''8 do'' la'\trill la'16 la' si'8\trill do'' |
re''8 re'' r re''16 re'' si'8\trill si'16 si' |
mi''8 do'' la'\trill la' si' do'' |
si'2\trill r8 sol'' |
fad''2\trill r8 sol'' |
mi''4\trill mi'' mi''8 fad'' |
red''4\trill red'' r8 si' |
dod'' dod'' red'' mi'' mi'' red'' |
\appoggiatura red'' mi''2. |
r4 r r8 do'' |
si'2\trill r8 do'' |
la'4\trill la' la'8 si' |
sold'4\trill sold' r8 mi'' |
fa'' mi'' re'' do'' si'8.\trill la'16 |
la'2 mi''4 r8 mi'' |
dod''8\trill dod''16 la' re''4 r8 re''16 mi'' |
dod''4\trill dod''8 re'' \appoggiatura re'' mi'' mi'' r dod'' |
dod'' dod'' fad'' fad'' si'\trill si'16 si' mi''8 mi''16 mi'' |
dod''1\trill |
