\clef "basse" la,2 la1 |
sol1. |
fa |
mi1 mi2 |
si1 si2 |
do' do re |
mi mi,1 |
la, la2 |
si1 sol2 |
do'2. do'4 do'2 |
si2 la1 |
sol2. sol4 mi2 |
fa2. fa4 re2 |
sol,1 fa,2~ |
fa, sol,1 |
do do'2 |
la lad1 |
si si,2 |
do1. |
si,2 si,2. si,4 |
fad1 fad2 |
sol sol, la, |
si,1. |
mi2. fa4 mi re |
do2 do'2. do'4 |
si2 si la |
re1 re2 |
mi mi,1 |
la,1.\douxSug |
sol, |
fa, |
mi,1 mi2 |
la1 sol2 |
fa re1 |
mi2 mi,1 |
la,1 la,2 |
fa2. mi4 re do |
sol,1 mi,2 |
fa,1. |
sol, |
do2. re4 do si, |
la,1. |
sol, |
fa, |
mi,1 mi2 |
la1 sol2 |
fa re1 |
mi2 mi,1 |
la,2. sol,4^\fortSug la, si, |
do2 do'2. do'4 |
si2 si la |
re1 re2 |
mi mi,1 |
la,1\doux |
mi2. |
la8. si16 do'8 do sol2 |
sold4 la la,2 |
si,2. |
sol,4 fad,8 mi, si,2 |
mi,4 mi dod2 |
re2. |
sold,2 la, |
mi1 |
fa2 re4 |
do2 si,4. la,8 |
mi2 fa8 re mi mi, |
la,4 la8.\fortSug la16 re4. re8 |
mi4 do8 la, mi,2 |
la,2 re4.^\douxSug do8 |
si,2. |
do4 re2 |
sol8 la sol fad mi4 |
red re2 |
dod4 do2 |
si,4. la,8 sol,4 |
la,8 sol, fad, mi, si,4 |
mi, mi8 fad sold mi |
la4 la,2 |
sold,4 sol,2 |
fad,4 fa,2 |
mi,4 mi8 re do4 |
re8 do si, la, mi,4 |
la,1 |
la2 sold4 |
la4. fad8 dod2~ |
dod4 re mi mi, |
la,1 |
