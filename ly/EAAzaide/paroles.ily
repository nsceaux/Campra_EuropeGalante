Mes yeux, ne pour -- rez- vous ja -- mais
for -- cer mon vain -- queur à se ren -- dre ?
Faut- il, a -- vec un cœur si ten -- dre,
a -- voir de si foi -- bles at -- traits ?
Mes yeux, ne pour -- rez- vous ja -- mais
for -- cer mon vain -- queur à se ren -- dre ?
Au mo -- ment de mon es -- cla -- va -- ge,
quand on me con -- dui -- sit dans ce ri -- che Pa -- lais,
il pa -- rut à mes yeux l’An -- tre le plus sau -- va -- ge,
je le fis re -- ten -- tir de mes tris -- tes re -- grets,
je me fis une i -- mage af -- freu -- se
du Sou -- ve -- rain que j’a -- dore au -- jour -- d’hui ;
Mais, sa pre -- sence en -- fin dis -- si -- pa mon en -- nuy,
et je me trou -- vay trop heu -- reu -- se
d’ê -- tre cap -- tive au -- près de luy.
Les beau -- tez dont il est le maî -- tre,
par son or -- dre bien- tôt s’as -- sem -- blent dans ces lieux ;
A -- mour, A -- mour, fay- luy con -- noî -- tre
le cœur qui le me -- ri -- te mieux.
A -- mour, A -- mour, fay- luy con -- noî -- tre
le cœur qui le me -- ri -- te mieux.
Mais, c’est luy que je vois, gar -- dons- nous de pa -- roî -- tre,
il n’est pas tems en -- cor de m’of -- frir à ses yeux.
