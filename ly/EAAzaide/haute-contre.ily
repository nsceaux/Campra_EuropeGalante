\clef "haute-contre" r2 la'1 |
mi' sol'2 |
la'1~ la'4. si'8 |
sold'1\trill r4 si' |
re''1 re''2 |
re'' do'' la' |
la' sold'2. sold'4 |
la'1 la'2 |
re''2 re''2. re''4 |
do''1 sol'2 |
sol'2 la'1 |
re'2 sol'2. sol'4 |
la'1~ la'4 la' |
si'1 do''2~ |
do'' si'2.\trill do''4 |
do''1 r4 do'' |
do''2 dod''2. dod''4 |
si'1 si'2 |
sol' la'2.\trill la'4 |
si'1. |
r2 la'2. la'4 |
la'2 sol' do'' |
si' si'2. si'4 |
si'1. |
r2 do''2. do''4 |
re''2 re'' do'' |
fa'' sold' la' |
la'( sold'2.) la'4 |
la'2\douxSug mi'1 |
sol' sol'2 |
do' re'1 |
si\trill mi'2 |
mi' fa' sol' |
do' fa'1 |
mi'1. |
mi'1 do'2 |
do'2. do'4 re' mi' |
re'1\trill do'2 |
la1 la2 |
sol sol2. sol4 |
sol1. |
r2 mi'1 |
sol' sol'2 |
do' re'1 |
si\trill mi'2 |
mi' fa' sol' |
do' fa'1 |
mi'1. |
mi' |
r2 do''2.\fortSug do''4 |
re''2 re'' do'' |
fa'' sold' la' |
la'( sold'2.) la'4 |
la'2\douxSug la4. la8 |
si2 re'4 |
do'4 do'8 sol' sol'2 |
mi' do'4. do'8 |
si2 red'4 |
mi'4 fad'8 si si4. si8 |
si2 la4. la8 |
la2 fa'4 |
mi'2. mi'4 |
mi'2. do'4 |
do'2 re'4 |
sol2 si4. do'8 |
si2 la8. si16 si8.\trill la16 |
la4 r8 do''\fortSug do''8. si'16 si'8.\trill la'16 |
sold'8 si' do''4 do''( si'8.)\trill la'16 |
la'4 mi'\douxSug re'2 |
sol' sol'4 |
mi' re'4. re'8 |
re'2 r8 mi' |
fad'2 r8 re' |
mi'4 mi' la' |
fad'2\trill r8 mi' |
dod'4 fad'8 si si8. si16 |
si2. |
r4 r r8 la |
si2 r8 sol |
la4 la re' |
si2\trill r8 mi' |
re' la re' mi' mi'8. mi'16 |
mi'2. mi'4 |
mi'2. |
mi'4. fad'8 mi'2 |
la'4 fad' mi'4. mi'8 |
mi'1 |
