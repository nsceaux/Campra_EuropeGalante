\clef "quinte" r2 la1 |
si do'2 |
do' re'1 |
si r4 si |
si2 si2. re'4 |
mi'1 do'2 |
si si2.\trill si4 |
la2 do'1 |
si2 re'1 |
mi' do'2 |
sol2 do' la |
si1 sib2 |
la1 re'2 |
si sol la~ |
la sol2. sol4 |
sol1 r4 sol' |
do'2 mi'1 |
red'1 si2 |
mi1. |
si |
r2 la2. la4 |
si1 la2~ |
la la2. si4 |
sold1. |
r2 mi'2. mi'4 |
re'2 si do' |
re'1 re'2~ |
re' re' si |
do'2\douxSug mi1 |
mi mi2 |
la la,1 |
mi1 mi'2 |
do'1 si2 |
la1 la2 |
mi1. |
mi1 mi'2 |
do'2. sol4 fa mi |
si1 do2 |
do1 do2 |
sol1 re2 |
mi1. |
r2 mi1 |
mi1 mi2 |
la la,1 |
mi mi'2 |
do'1 si2 |
la1 la2 |
mi1. |
mi |
r2 mi'2.\fortSug mi'4 |
re'2 si do' |
re'1 re'2~ |
re' re' si |
do'2.\douxSug do4 |
mi2 sol4 |
la8 do' do' sol sol4. si8 |
si4 mi' mi2 |
si fad4 |
mi la8 mi fad4. fad8 |
mi2. sol4 |
fa4 la2 |
mi2 mi4. mi8 |
mi2. mi4 |
la2 re4 |
mi2 si4. mi8 |
mi2 la8 fa mi8. mi16 |
mi4 r8 la\fortSug la4 si |
si la si4. si8 |
do'2 re'4\douxSug fad |
sol2.~ |
sol4 re'4. re'8 |
re'2 r8 si |
si2 r8 sol |
si4 si do'8 fad |
fad2 r8 si |
la mi la si fad8.\trill mi16 |
mi2. |
r4 r r8 do |
mi2 r8 do |
do4 do la, |
mi2 r8 la |
la mi si mi mi8. mi16 |
mi2. la4 |
fad mi2 |
la2 dod' |
mi'4 re'2 si4 |
la1 |
