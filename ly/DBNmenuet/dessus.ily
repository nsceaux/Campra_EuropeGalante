\clef "dessus" <>^\markup\whiteout Tous
 re''8 mi'' fad''4 sol'' |
fad'' re''8 mi'' fad'' re'' |
mi''4 la' dod''\trill |
re''8 dod'' re'' mi'' re''4 |
re''8 mi'' fad''4 sol'' |
fad'' re''8 mi'' fad'' re'' |
mi''4 la' dod''\trill |
re''2. |
<>^\markup\whiteout Hautbois \twoVoices #'(dessus1 dessus2 dessus) <<
  { dod''8 re'' mi''4 fad'' |
    mi'' dod''8 re'' mi'' fad'' |
    mi''4 fad''8 mi'' re'' dod'' |
    si'\trill la' si' dod'' si'4 | }
  { la'8 si' dod''4 re'' |
    dod'' la'8 si' dod'' re'' |
    dod''4 re''8 dod'' si' la' |
    sold' fad' sold' la' sold'4 | }
>>
<>^\markup\whiteout Tous
dod''8 re'' mi''4 fad'' |
mi'' re''8 dod'' si' dod'' |
la'4 si' sold' |
la'2. |
<>^\markup\whiteout Hautbois \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 fad'' la'' |
    la'' fad''8 sol'' la'' si'' |
    la''4 si''8 la'' sol'' fad'' |
    mi''\trill re'' mi'' fad'' mi''4 | }
  { fad''4 re'' fad'' |
    fad'' re''8 mi'' fad'' sol'' |
    fad''4 sol''8 fad'' mi'' re'' |
    dod'' si' dod'' re'' dod''4 | }
>>
<>^\markup\whiteout Tous
sol''4 fad''\trill mi'' |
re''8 dod'' re'' mi'' fad''4 |
sol'' mi''4.\trill re''8 |
re''2. |
