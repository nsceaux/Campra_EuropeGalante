\clef "taille" la'4 la' la' |
la' fad' fad' |
sol' sol' mi' |
fad'8 mi' fad' sol' fad'4 |
la' la' la' |
la' fad' fad' |
sol' sol' mi' |
fad'2. |
R2.*4 |
mi'4 la' la' |
la' fad' si |
fad' si si |
dod'2. |
R2.*4 |
sol'4 la' la' |
sol'2 fad'4 |
re' la'4. la'8 |
fad'2. |
