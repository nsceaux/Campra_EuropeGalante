\clef "basse" re'4 re' dod' |
re'2 re4 |
la la la, |
re2 re4 |
re' re' dod' |
re'2 re4 |
la la la, |
re2. |
<>^\markup\whiteout Bassons
la4 la re |
la la la |
la, re8 mi fad re |
mi2 mi4 |
<>^\markup\whiteout Tous
la4 la re |
la re mi |
fad re mi |
la,2. |
<>^\markup\whiteout Bassons
re'4 re' re' |
re re re |
re' sol8 la si sol |
la2 la4 |
<>^\markup\whiteout Tous
mi4 fad fad |
sol fad8 mi re4 |
sol, la,2 |
re,2. |
