\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*8\pageBreak s2.*8\break }
    >>
  >>
  \layout { }
  \midi { }
}
