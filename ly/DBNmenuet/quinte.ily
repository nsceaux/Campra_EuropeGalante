\clef "quinte" re'4 re' la |
re' re' re' |
dod' dod' mi' |
re'2 re'4 |
re' re' la |
re' re' re' |
dod' dod' mi' |
re'2. |
R2.*4 |
la4 la la |
la la mi' |
dod' re' si\trill |
la2. |
R2.*4 |
mi'4 la la |
si sol la |
si la4. la8 |
la2. |
