\clef "haute-contre" re''4 re'' mi'' |
re'' la' la' |
la'2 la'4 |
la'2 la'4 |
re'' re'' mi'' |
re'' la' la' |
la'2 la'4 |
la'2. |
R2.*4 |
la'8 si' dod''4 re'' |
dod'' la' sold' |
la' fad' mi' |
mi'2. |
R2.*4 |
mi''4 re'' dod'' |
si'8 la' si' dod'' re''4 |
re'' dod''4.\trill re''8 |
re''2. |
