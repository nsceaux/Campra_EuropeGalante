\clef "taille" r2*3/4 |
r fa'8. dod'16 re'8 |
mi'4 fa'8 mi'4 la'8 |
la'8. sol'16 fad'8 sol'4 sol'8 |
sol'8. fa'16 mi'8 sol'4 fa'8 |
mi'4 fa'8 sol'8. fa'16 mi'8 |
fa'4 fa'8 fa'8. fa'16 sol'8 |
do'4 do'8 do'4 do'8 |
do'4. fa'8. fa'16 sol'8 |
la'4 sol'8 sol'4 sol'8 |
sol'4 re'8 mi'8. fa'16 sol'8 |
sol'4 sol'8 sol'8. fa'16 sol'8 |
mi'8.\trill re'16 do'8 re'4 mi'8 |
mi'4 mi'8 mi'4 fa'8 |
si4\trill mi'8 mi'8. re'16 mi'8 |
mi'4 mi'8 mi'4 mi'8 |
mi'4. fa'4 fa'8 |
sol'4. la'4 la'8 |
la'8. sol'16 fa'8 re'4 re'8 |
sol'4 do'8 re'4 re'8 |
mi'4 mi'8 fa'8. sol'16 la'8 |
re'4 re'8 dod'8. re'16 mi'8 |
re'4.
