\clef "dessus" fa''8. mi''16 re''8 |
la''8. sol''16 la''8 re''8. mi''16 fa''8 |
dod''8.\trill si'16 la'8 la''4. |
fad''8.\trill mi''16 re''8 sol''4. |
mi''8.\trill re''16 do''8 do'''8. sib''16 la''8 |
sol''8. la''16 fa''8 mi''8.\trill fa''16 sol''8 |
do''4 fa''8 fa''8. mi''16 re''8 |
do''8. sib'16 la'8 sol'8. la'16 sib'8 |
la'8.\trill sol'16 fa'8 do''8. re''16 mi''8 |
fa''8. mi''16 re''8 sol''8. fa''16 mi''8 |
re''8.\trill do''16 si'8 do''8. re''16 mi''8 |
fa''8. sol''16 mi''8 re''4\trill do''8 |
do''8. re''16 mi''8 re''8. mi''16 do''8 |
si'8.\trill do''16 re''8 do''8.\trill si'16 la'8 |
mi''4. mi''8. fa''16 mi''8 |
re''8. mi''16 do''8 si'8. do''16 re''8 |
dod''8. si'16 la'8 la''8. sib''16 la''8 |
sol''8.\trill fa''16 mi''8 sol''8. la''16 sol''8 |
fa''8.\trill mi''16 re''8 sib'4 sib'8 |
sib'4 la'8 la'8. sib'16 sol'8 |
la'8. si'16 dod''8 re''8. mi''16 fa''8 |
sol''8. la''16 fa''8 mi''8.\trill re''16 dod''8 |
re''4.
