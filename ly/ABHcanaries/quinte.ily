\clef "quinte" r2*3/4 |
r la4 la8 |
la4. la4 la8 |
la4 re'8 re'4 re'8 |
do'4. do'4 do'8 |
do'4. do'4 do'8 |
fa'4 do'8 re'8. la16 sib8 |
fa8. sol16 la8 sib8. la16 sol8 |
la4. la8. la16 sol8 |
fa4 sol8 sol8. si16 do'8 |
re'4 sol8 sol4 sol8 |
sol4. sol4 sol8 |
sol4 mi'8 si4 do'8 |
re'8. do'16 si8 do'8. do'16 re'8 |
mi'4 si8 do'8. sold16 la8 |
si4 mi8 mi4 mi8 |
mi4. la4 do'8 |
do'4. la8. si16 dod'8 |
re'4. re'4 sib8 |
do'4 la8 sib4 sib8 |
mi8. la16 la8 la4 la8 |
sib4 re8 la4 la8 |
la4.
