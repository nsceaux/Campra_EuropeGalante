\clef "basse" r2*3/4 |
r fa8. mi16 re8 |
la8. sol16 la8 dod8. si,16 la,8 |
re4 re'8 si8. la16 sol8 |
do'4. mi4 fa8 |
do4 fa8 do'8. do'16 sib8 |
la8. sol16 fa8 re8. do16 sib,8 |
la,8. sol,16 fa,8 do4 do,8 |
fa,4. fa8. fa16 mi8 |
re4 sol8 mi8. re16 do8 |
sol,4 sol8 mi8. re16 do8 |
si,4 do8 sol,4 sol,8 |
do4 do'8 si4 la8 |
sold8. fad16 mi8 la8. sol?16 fa?8 |
mi8. re16 mi8 do8. si,16 la,8 |
sold,4 la,8 mi,4 mi,8 |
la,4. fa8. mi16 fa8 |
do'4. dod'8. si16 la8 |
re'4. re8. mi16 re8 |
do8. re16 do8 sib,4. |
la,8. la16 sol8 fa8. mi16 re8 |
sib,4 sol,8 la,4 la,8 |
re,4.
