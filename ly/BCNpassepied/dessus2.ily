\clef "dessus" si'8 |
si' do'' la' |
si' sol'16 la' si'8 |
si' la' sol' |
fad'8\trill fad'16 sol' la' fad' |
sol'8 sol' la' |
si' sol'16 la' si'8 |
si' la' sol' |
fad'4\trill fad''8 |
fad'' sol'' mi'' |
fad'' re''16 mi'' fad''8 |
fad'' mi'' re'' |
dod'' dod''16 re'' mi'' dod'' |
re''8 re'' mi'' |
fad''8 re''16 mi'' fad''8 |
re'' mi'' dod'' |
re''4 si'8 |
do'' si'\trill la' |
si' sol'16 la' si'8 |
si' la' sol' |
fad'\trill fad'16 sol' la'8 |
re' re' mi' |
fad' fad' sol' |
sol' fad'8.\trill sol'16 |
sol'4
