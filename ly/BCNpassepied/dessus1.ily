\clef "dessus" <>^"Hautbois"
re''8 |
re'' mi'' do'' |
re'' si'16 do'' re''8 |
re'' do'' si' |
la'8\trill la'16 si' do'' la' |
si'8 si' do'' |
re'' si'16 do'' re''8 |
re'' do'' si' |
la'4\trill la''8 |
la'' si'' sol'' |
la'' fad''16 sol'' la''8 |
la'' sol'' fad'' |
mi''8\trill mi''16 fad'' sol'' mi'' |
fad''8 fad'' sol'' |
la'' fad''16 sol'' la''8 |
fad''8 sol'' mi'' |
fad''4\trill re''8 |
mi'' re'' do'' |
re'' si'16 do'' re''8 |
re'' do'' si' |
la'8\trill la'16 si' do''8 |
si' la'\trill sol' |
la' re'' si' |
mi'' la'8.\trill sol'16 |
sol'4
