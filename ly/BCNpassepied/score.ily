\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout { s8 s4.*11\break }
    >>
  >>
  \layout { system-count = 3 }
  \midi { }
}
