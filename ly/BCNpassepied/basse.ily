\clef "basse" <>^\markup\whiteout Bassons sol8 |
sol do4 |
sol sol8 |
sol fad sol |
re4 re16 do |
si,8 si, la, |
sol,4 sol,8 |
sol, fad, sol, |
re,4 re'8 |
re' sol4 |
re'4 re'8 |
re' dod' re' |
la4 la16 sol |
fad8 fad mi |
re re' la |
si sol la |
re4 sol8 |
do4 do8 |
sol,4 sol8 |
sol fad sol |
re re' fad |
sol fad mi |
re si, mi |
do re re, |
sol,4
