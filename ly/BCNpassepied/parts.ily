\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#32 #}))
