\clef "haute-contre" re''4 do''2 |
si'4 la' sol' |
sol' sol' fad' |
sol'8 fad' sol' la' sol'4 |
re'' do''2 |
si'4 la' sol' |
sol' sol' fad' |
sol'2. |
si'4 dod'' re'' |
mi''8 re'' mi'' fad'' mi''4 |
dod'' re'' mi'' |
la'2 re''4 |
re'' re'' re'' |
do'' do'' si' |
do'' la'4.\trill sol'8 |
sol'2. |
