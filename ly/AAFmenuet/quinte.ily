\clef "quinte" si4 do' do' |
re'2 re'4 |
do'2 do'4 |
si2 si4 |
si do' do' |
re'2 re'4 |
do'2 do'4 |
si2. |
si4 si la |
si2 si4 |
la re' dod' |
re'2 la4 |
sol la sol |
sol la si |
la la4. si8 |
si2. |
