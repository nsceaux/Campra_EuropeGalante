\clef "dessus" sol''4 fad''\trill mi'' |
re''4. do''8 si'4 |
do'' si'\trill la' |
si'8 la' si' do'' si'4 |
sol'' fad''\trill mi'' |
re''4. do''8 si'4 |
do'' si'\trill la' |
si'2. |
re''4 mi'' fad'' |
sol''8 fad'' sol'' la'' sol''4 |
mi'' fad'' sol'' |
la''8 sol'' la'' si'' la''4 |
si'' la'' si'' |
mi'' fad'' sol'' |
la'' fad''4.\trill sol''8 |
sol''2. |
