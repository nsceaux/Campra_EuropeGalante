\clef "taille" si'4 la'2 |
sol'4 la' re' |
mi' re'2 |
re' re'4 |
si'4 la'2 |
sol'4 la' re' |
mi' re'2 |
re'2. |
sol'4 sol' la' |
sol'2 sol'4 |
la' la' mi' |
fad'8 mi' fad' sol' fad'4 |
re' re' re' |
mi' la' sol' |
mi' re'4. re'8 |
re'2. |
