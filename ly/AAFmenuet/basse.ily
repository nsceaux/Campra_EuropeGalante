\clef "basse" sol4 la2 |
si4 fad sol |
do re re, |
sol,2. |
sol4 la la |
si fad sol |
do re re, |
sol,2. |
sol4 sol fad |
mi2. |
la4 la sol |
fad2. |
sol4 fad sol |
do la, mi |
do re re, |
sol,2. |
