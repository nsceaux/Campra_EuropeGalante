\clef "dessus" dod''8\doux |
dod''4 dod''8 |
dod'' re'' mi'' |
fad''4 fad'8\fort |
fad'4 la''8\doux |
la''4 la''8 |
la''4 sold''8\trill |
la''4 la'8\fort |
la'4 la''8\doux |
la''4 la''8 |
la'' la'' sold'' |
fad''4 fad'8\fort |
fad'4 si''8\doux |
si''4 si''8 |
si'' si'' la'' |
sold''4\trill sold'8\fort |
sold' sold''[\doux la''] |
la''4 sold''8 |
fad''4\trill mi''8 |
mi''4 mi'8\fort |
mi'4 mi''8\doux |
mi''4 sold'8 |
sold' la' si' |
dod''4 dod'8\fort |
dod'4 mi'8\doux |
la'4 la''8 |
la''4 sold''8\trill |
la''4 la'8\fort |
la'4 mi''8\doux |
la''4 la''8 la''4 sold''8\trill |
la''4 la'8\fort |
la'4 %%
dod''8 |
dod''4 dod''8 |
dod'' re'' mi'' |
fad''4 fad'8 |
fad'4 fad''8 |
mi'' fad'' mi'' |
re'' mi'' re'' |
dod''4\trill dod'8 |
dod'4 fad''8 |
fad''4 fad''8 |
fad'' fad'' mi'' |
red''4\trill fad'8 |
fad'4 sold''8 |
sold''4 sold''8 |
sold'' sold'' fad'' |
mi''4 mi'8 |
mi'4 mi''8 |
mi''4 sold''8 |
fad''4\trill mi''8 |
mi''4 mi'8 |
mi'4 mi''8 |
mi''4 mi''8 |
mi'' mi'' re'' |
dod''4 dod'8 |
dod'4 fad''8 |
fad''4 fad''8 |
fad'' fad'' mi'' |
re''4 re'8 |
re' re''[ sol''] |
fad''4\trill sol''8 |
mi''4 la''8 |
fad''4\trill fad'8 |
fad'4 fad''8 |
mi'' fad'' mi'' |
re'' mi'' re'' |
dod''4\trill mi'8 |
mi'4 mi''8 |
mi''4 mi''8 |
mi'' mi'' red'' |
mi''4 mi'8 |
mi'4 mi''8 |
dod''4 re''8 |
si'4 mi''8 |
dod''4\trill dod'8 |
dod' mi''[ mi''] |
dod''4 re''8 |
si'4 mi''8 |
dod''4.\trill |
