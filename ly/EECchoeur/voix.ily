<<
  \tag #'vdessus { \clef "vdessus" r8 R4.*31 r8 r }
  \tag #'vhaute-contre { \clef "vhaute-contre" r8 R4.*31 r8 r }
  \tag #'vtaille { \clef "vtaille" r8 R4.*31 r8 r }
  \tag #'vbasse {
    \clef "vbasse" <>^\markup\character Le Bostangi
    la8 |
    la4 la8 |
    la si dod' |
    re'4 re'8 |
    r re' re' |
    dod'4 dod'8 |
    si4 si8 |
    la4 la8 |
    r r fad |
    fad4 fad8 |
    fad sold la |
    si4 si8 |
    r r sold |
    sold4 sold8 |
    sold la si |
    dod'4 dod'8 |
    r dod' sold |
    la4 la8 |
    si4 si8 |
    mi'4 mi'8 |
    r r mi |
    mi4 mi8 |
    mi fad sold |
    la4 la8 |
    r8 la mi |
    fad4 re8 |
    mi4 mi8 |
    la4 la8 |
    r la mi |
    fad4 re8 |
    mi4 mi8 |
    la4 la8 |
    r r
  }
>>
<<
  \tag #'vdessus {
    <>^\markup\character Chœur dod''8 |
    dod''4 dod''8 |
    dod'' re'' mi'' |
    fad''4 fad''8 |
    r fad'' fad'' |
    mi''4 mi''8 |
    re''4 re''8 |
    dod''4\trill dod''8 |
    r r fad'' |
    fad''4 fad''8 |
    fad'' fad'' mi'' |
    red''4\trill red''8 |
    r r sold'' |
    sold''4 sold''8 |
    sold'' sold'' fad'' |
    mi''4 mi''8 |
    r mi'' mi'' |
    mi''4 sold''8 |
    fad''4\trill mi''8 |
    mi''4 mi''8 |
    r r mi'' |
    mi''4 mi''8 |
    mi'' mi'' re'' |
    dod''4\trill dod''8 |
    r r fad'' |
    fad''4\trill fad''8 |
    fad'' fad'' mi'' |
    re''4 re''8 |
    r re'' sol'' |
    fad''4 sol''8 |
    mi''4 la''8 |
    fad''4\trill fad''8 |
    r r fad'' |
    mi''4 mi''8 |
    re'' mi'' re'' |
    dod''4\trill dod''8 |
    r r mi'' |
    mi''4 mi''8 |
    mi'' mi'' red'' |
    mi''4 mi''8 |
    r mi'' mi'' |
    dod''4 re''8 |
    si'4 mi''8 |
    dod''4\trill dod''8 |
    r mi'' mi'' |
    dod''4 re''8 |
    si'4 mi''8 |
    dod''4\trill dod''8 |
  }
  \tag #'vhaute-contre {
    la'8 |
    la'4 la'8 |
    la' la' la' |
    la'4 la'8 |
    r la' la' |
    la'4 la'8 |
    la'4 sold'8 |
    la'4 la'8 |
    r r la' |
    la'4 la'8 |
    la' la' la' |
    fad'4\trill fad'8 |
    r r si' |
    si'4 si'8 |
    si' si' la' |
    sold'4\trill sold'8 |
    r sold' si' |
    la'4 mi'8 |
    si'4 si'8 |
    sold'4\trill sold'8 |
    r r sold' |
    sold'4 sold'8 |
    sold' la' si' |
    mi'4 mi'8 |
    r r la' |
    la'4 fad'8 |
    fad' sol' la' |
    si'4 sol'8 |
    r re' mi' |
    fad'4 si'8 |
    la'4 la'8 |
    la'4 la'8 |
    r r la' |
    la'4 la'8 |
    la' la' sold' |
    la'4 la'8 |
    r r mi' |
    mi'4 mi'8 |
    fad' fad' fad' |
    si4 si8 |
    r mi' mi' |
    la'4 la'8 |
    la'4 sold'8 |
    la'4 la'8 |
    r la' sold' |
    la'4 la'8 |
    la'4 sold'8 |
    la'4 la'8 |
  }
  \tag #'vtaille {
    mi'8 |
    mi'4 mi'8 |
    mi' mi' mi' |
    re'4 re'8 |
    r re' re' |
    la4 la8 |
    si4 si8 |
    mi'4 mi'8 |
    r r dod' |
    dod'4 dod'8 |
    dod' dod' dod' |
    si4 si8 |
    r r mi' |
    mi'4 mi'8 |
    mi' mi' red' |
    mi'4 mi'8 |
    r mi' si |
    dod'4 mi'8 |
    mi'4 red'8 |
    mi'4 mi'8 |
    r r si |
    si4 si8 |
    si si si |
    dod'4 dod'8 |
    r r re' |
    re'4 re'8 |
    re' re' do' |
    si4 si8 |
    r si dod'! |
    re'4 re'8 |
    re'4 dod'8 |
    re'4 re'8 |
    r r re' |
    mi'4 mi'8 |
    re' re' re' |
    mi'4 mi'8 |
    r r dod' |
    si4 si8 |
    la si la |
    sold4\trill sold8 |
    r si dod' |
    la4 la8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    r dod' si |
    la4 la8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
  }
  \tag #'vbasse {
    la8 |
    la4 la8 |
    la si dod' |
    re'4 re'8 |
    r re' re' |
    dod'4 dod'8 |
    si4\trill si8 |
    la4 la8 |
    r r fad |
    fad4 fad8 |
    fad sold la |
    si4 si8 |
    r r sold |
    sold4 sold8 |
    sold la si |
    dod'4 dod'8 |
    r dod' sold |
    la4 la8 |
    si4 si8 |
    mi'4 mi'8 |
    r r mi |
    mi4 mi8 |
    mi fad sold |
    la4 la8 |
    r r re |
    re4 re8 |
    re mi fad |
    sol4 sol8 |
    r sol mi |
    si4 sol8 |
    la4 la8 |
    re'4 re'8 |
    r r re' |
    dod'4 dod'8 |
    si dod' si |
    la4 la8 |
    r r la |
    sold4 sold8 |
    fad sold fad |
    mi4 mi8 |
    r mi dod |
    fad4 re8 |
    mi4 mi8 |
    la4 la8 |
    r la mi |
    fad4 re8 |
    mi4 mi8 |
    la4 la8 |
  }
>>
