\clef "quinte" dod'8\douxSug |
dod'4 dod'8 |
dod' si mi' |
re'4 re8\fortSug |
re4 re'8\douxSug |
la4 la8 |
re'4 si8 |
dod'4 dod8\fortSug |
dod4 fad'8\douxSug |
fad'4 fad'8 |
fad'4 fad'8 |
fad'4 fad8\fortSug |
fad4 si8\douxSug |
si4 mi'8 |
mi'4 si8 |
mi'4 mi8\fortSug |
mi4 mi'8\douxSug |
dod'4 dod'8 |
si4 si8 |
sold4\trill mi8\fortSug |
mi4 si8\douxSug |
si4 si8 |
si4 si8 |
la4 la,8\fortSug |
la, la[\douxSug si] |
fad4 si8 |
si4 si8 |
la4 mi8\fortSug |
mi la[\douxSug sold] |
fad4 si8 |
si4 si8 |
la4 la,8\fortSug |
la,4 %%
la8 |
la4 la8 |
la4 la8 |
re'4 re8 |
re4 re'8 |
la4 la8 |
si4 si8 |
dod'4 dod8 |
dod4 dod'8 |
dod'4 dod'8 |
dod'4 dod'8 |
red'4 red8 |
red4 si8 |
si4 si8 |
si4 si8 |
mi'4 sold8 |
sold mi'[ re'] |
dod'4 dod'8 |
si4 si8 |
si4 mi8 |
mi4 sold8 |
sold4 sold8 |
sold fad mi |
la4 dod8 |
dod4 re'8 |
re'4 re'8 |
re' re' do' |
si4 si,8 |
si,4 si8 |
si4 mi'8 |
mi'4 mi'8 |
re'4 re8 |
re4 re'8 |
mi'4 dod'8 |
re'4 si8 |
dod'4 dod8 |
dod4 mi'8 |
mi'4 sold8 |
la4 fad8 |
sold4 mi8 |
mi4 mi'8 |
mi'4 re'8 |
si4\trill si8 |
la4 la,8 |
la,4 mi'8 |
mi'4 re'8 |
re'4 si8 |
dod'4. |
