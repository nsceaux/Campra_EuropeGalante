\clef "haute-contre" la'8\douxSug |
la'4 la'8 |
la' la' la' |
la'4 re'8\fortSug |
re'4 fad''8\douxSug |
mi'' fad'' mi'' |
re'' mi'' re'' |
dod''4\trill dod'8\fortSug |
dod'4 dod''8\douxSug |
dod''4 fad''8 |
fad'' fad'' mi'' |
red''4 red'8\fortSug |
red'4 mi''8\douxSug |
mi''4 mi''8 |
mi'' mi'' red'' |
mi''4 mi'8\fortSug |
mi'4 mi''8\douxSug |
mi''4 mi''8 |
mi''4 red''8 |
mi''4 mi'8\fortSug |
mi'4 sold'8\douxSug |
sold'4 sold'8 |
sold' fad' mi' |
mi'4 la8\fortSug |
la4 dod'8\douxSug |
dod'4 fad'8 |
mi'4 mi'8 |
mi'4 dod'8\fortSug |
dod'4 dod''8\douxSug |
dod''4 re''8 |
si'4 mi''8 |
dod''4\trill dod'8\fortSug |
dod'4 %%
la'8 |
la'4 la'8 |
la' la' la' |
la'4 re'8 |
re'4 la'8 |
la'4 la'8 |
la' la' sold' |
la'4 la8 |
la4 dod''8 |
dod''4 dod''8 |
dod'' dod'' dod'' |
si'4 si8 |
si4 si'8 |
si'4 si'8 |
si' si' la' |
sold'4 dod'8 |
dod' dod''[ si'] |
dod''4 mi''8 |
mi''4 red''8 |
mi''4 si8 |
si4 si'8 |
si'4 si'8 |
si' si' si' |
la'4 la8 |
la4 la'8 |
la'4 la'8 |
la' la' la' |
si'4 si8 |
si4 si'8 |
si'4 re''8 |
re''4 dod''8 |
re''4 re'8 |
re'4 la'8 |
la'4 la'8 |
si'4 si'8 |
mi'4 dod'8 |
dod'4 dod''8 |
si'8 dod'' si' |
la' si' la' |
sold'4\trill si8 |
si4 sold'8 |
la'4 la'8 |
la'4 sold'8 |
la'4 la8 |
la la'[ sold'] |
la'4 la'8 |
la'4 sold'8 |
la'4. |
