\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s8 s4.*11\break s4.*12\pageBreak
        s4.*8 s4 \bar "" \pageBreak
        s8 s4.*12\pageBreak
        s4.*12\pageBreak
        s4.*12\pageBreak
      }
      \modVersion {
        s8 s4.*12\break
        s4.*12\break\noPageBreak s4.*10\pageBreak
        s4.*11\break\noPageBreak s4.*11\pageBreak
        s4.*12\break\noPageBreak
      }
    >>
  >>
  \layout {
    indent = \noindent
    system-count = 7
  }
  \midi { }
}
