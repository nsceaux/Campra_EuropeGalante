\clef "basse" la8\douxSug |
la4 la8 |
la si dod' |
re'4 re8\fortSug |
re4 re'8^\douxSug |
dod' re' dod' |
si dod' si |
la4 la,8\fortSug |
la,4 fad8\douxSug |
fad4 fad8 |
fad sold la |
si4 si,8\fortSug |
si,4 sold8^\douxSug |
sold4 sold8 |
sold la si |
dod'4 dod8\fortSug |
dod dod'[ sold]^\douxSug |
la4 la8 |
si4 si8 |
mi'4 mi8\fortSug |
mi4 mi8\douxSug |
mi4 mi8 |
mi fad sold |
la4 la,8\fortSug |
la, la[\douxSug mi] |
fad4 re8 |
mi4 mi8 |
la4 la,8\fortSug |
la, la[\douxSug mi] |
fad4 re8 |
mi4 mi8 |
la4 la,8\fortSug |
la,4 %%
la8 |
la4 la8 |
la si dod' |
re'4 re8 |
re4 re'8 |
dod' re' dod' |
si dod' si |
la4 la,8 |
la,4 fad8 |
fad4 fad8 |
fad sold la |
si4 si,8 |
si,4 sold8 |
sold4 sold8 |
sold la si |
dod'4 dod8 |
dod dod'[ sold] |
la4 la8 |
si4 si8 |
mi'4 mi8 |
mi4 mi,8 |
mi,4 mi8 |
mi fad sold |
la4 la,8 |
la,4 re8 |
re4 re8 |
re mi fad |
sol4 sol,8 |
sol, sol[ mi] |
si4 sol8 |
la4 la8 |
re'4 re8 |
re4 re'8 |
dod' re' dod' |
si dod' si |
la4 la,8 |
la,4 la8 |
sold la sold |
fad sold fad |
mi4 mi,8 |
mi, mi[ dod] |
fad4 re8 |
mi4 mi8 |
la4 la,8 |
la, la[ mi] |
fad4 re8 |
mi4 mi8 |
la,4. |
