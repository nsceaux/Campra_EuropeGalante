\clef "taille" mi'8\douxSug |
mi'4 mi'8 |
mi' re' dod' |
fad'4 fad8\fortSug |
fad4 fad'8\douxSug |
la'4 la'8 |
si'4 si'8 |
mi'4 mi8\fortSug |
mi4 la'8\douxSug |
la'4 la'8 |
la' si' dod'' |
si'4 si8\fortSug |
si4 sold'8\douxSug |
sold'4 sold'8 |
sold'4 fad'8 |
mi'4 mi8\fortSug |
mi4 mi'8\douxSug |
mi'4 mi'8 |
si'4 si'8 |
si'4 si8\fortSug |
si4 mi'8\douxSug |
mi'4 mi'8 |
mi'4 re'8 |
dod'4 dod8\fortSug |
dod4 sold'8\douxSug |
fad'4 fad'8 |
si4 mi'8 |
dod'4 dod8\fortSug |
dod4 mi'8\douxSug |
dod'4 fad'8 |
mi'4 mi'8 |
mi'4 mi8\fortSug |
mi4
%%
mi'8 |
mi'4 mi'8 |
mi' mi' mi' |
re'4 la8 |
la4 re'8 |
mi'4 dod'8 |
re'4 re'8 |
mi'4 mi8 |
mi4 la'8 |
la'4 la'8 |
la' la' la' |
fad'4 fad8 |
fad4 mi'8 |
mi'4 mi'8 |
mi' mi' red' |
mi'4 mi8 |
mi4 mi'8 |
mi'4 mi'8 |
si'4 si'8 |
sold'4 sold8 |
sold4 sold'8 |
sold'4 sold'8 |
sold' la' si' |
mi'4 mi8 |
mi4 fad'8 |
fad'4 re'8 |
re'4 re'8 |
re'4 sol8 |
sol re'[ mi'] |
fad'4 si'8 |
la'4 la'8 |
la'4 la8 |
la4 la'8 |
dod''4 la'8 |
la' la' sold' |
la'4 la8 |
la4 la'8 |
mi'4 mi'8 |
fad'4 fad'8 |
si4 sold8 |
sold4 mi'8 |
la4 fad'8 |
mi'4 mi'8 |
mi'4 mi8 |
mi4 mi'8 |
la4 fad'8 |
mi'4 mi'8 |
mi'4. |
