\piecePartSpecs
#`((dessus #:indent 0)
   (haute-contre #:indent 0)
   (taille #:indent 0)
   (quinte #:indent 0)
   (basse #:indent 0)
   (basse-continue #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet#79 #}))
