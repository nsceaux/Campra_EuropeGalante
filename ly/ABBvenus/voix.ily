\clef "vbas-dessus" <>^\markup\character Venus
r8 la'16 la' re''8 re''16 fa'' re''8\trill re''16 re'' sib'8 sib'16 la' |
la'8\trill la' r la'16 si' do''8 do''16 la' re''8 re'' |
mi'' mi'' mi'' mi'' dod''2\trill |
