\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix"
                   #:system-count 1
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet#3 #}))
