\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*6\break s1*6\break s1*6\break s1*6\pageBreak
        s1*4\break s2.*2 s1 s2.\break s2.*2 s1\break
        s2. s4 \bar "" \break s2. s1*5 s4 \bar "" \pageBreak
        s2. s1*5 s2 \bar "" \break s2 s1*2\break
        s1*2\break s2. s1 s2. s2 \bar "" \pageBreak
        s2 s1*3\break s2.*2 s1*2\break
        s2.*7\break s2.*8\pageBreak
        s2.*3 s1 s2 \bar "" \break s2 s2.*2 s2 \bar "" \break
        s2 s1*2 s2 \bar "" \break s2 s1 s2. s2 \bar "" \break
        s2 s1*2\pageBreak
        s2.*2 s1\break
      }
      \modVersion { s1*24\break }
    >>
  >>
  \layout { }
  \midi { }
}