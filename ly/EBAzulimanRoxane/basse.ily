\clef "basse" R1*2 |
r4 mi' dod'\trill re'8 mi' |
la4 la fad sold8 la |
mi4 mi dod re8 mi |
la,4 la,8 sold, fad,4 fad, |
si, si sold\trill la8 si |
mi4 mi dod\trill re8 mi |
la,2 si, |
mi r4 la |
fad sol8 la re4 mi8 fad |
sol4 mi fad fad |
re si, fad fad, |
si, si, mi mi |
dod re8 mi la,4 la, |
re mi8 fad si,4 dod8 re |
mi4 fad8 sold la,4 si,8 dod |
re mi fad re mi4 mi, |
la,2 sol, |
fad, dod |
re4 mi8 fad si,4 dod8 re |
mi4 mi' la si8 dod' |
re2 mi4 mi, |
la,1 |
la2 sold4. mi8 |
fad2. dod8 re |
mi2 dod |
lad,2 si,8 sold, fad, si, |
mi,2 mi4 |
mid2. |
fad4 si, dod2 |
fad2. |
sol |
fad2 mi4 |
re2 mi |
fad8 re mi4 mi, |
la, la8 la la4 fad |
dod'2 dod'4 la |
mi'4 mi' sold\trill sold8 sold |
la2 si4 si, |
mi2 mi8 re dod si, |
la,4 la8 la la4 fad |
mi4 mi la fad |
sol2 mi4 la |
re re sol sol8 sol |
mi2 fad4. fad8 |
si,4 si mi' dod' |
sold2\trill sold4 la |
mi mi fad fad8 fad |
re2 mi4. mi8 |
la,1 | \allowPageTurn
la4 sold fad4. re8 |
mi2 la, |
si,2. |
dod2 mid |
fad8 si, dod2 |
fad, fad |
dod4 red mi2 |
re dod |
si,1 |
fad4 dod2 |
re2. |
red2 mi4 dod |
re8 dod si, la, mi4 mi, |
la,4 la la8 la |
re'4. dod'8 si la |
sold2\trill sold8 sold |
la4 la fad |
red red mi |
si2 si8 si, |
mi4 mi mi'8 re' |
dod'2\trill dod'4 |
re' si2\trill |
la4 la la8 la |
si4. si8 dod'4 |
fad dod2 |
fad si8 la |
sold2\trill sold4 |
la fad2\trill |
mi4 sol sol8 sol |
re4. mi8 fad4 |
re4 mi2 |
la,2 lad, |
si, dod4 fad, |
dod2. |
re4 si, dod |
fad,2 fad |
sold la |
fad sol |
red4 mi mi8 re? dod si, |
fad1 |
sol8 mi fad4 fad, |
si,2 mi |
sold, la,~ |
la, re |
red2. |
mi |
dod2 re4 mi |
la,2 la4 fad |
red2. |
mi2. mi,4 |
la,1 |
