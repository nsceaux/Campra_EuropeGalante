\clef "dessus" <>^"Violons" r4 la'' sold''\trill la''8 si'' |
mi''4 fad''8 sold'' la''4 si''8 la'' |
sold''4\trill sold'' la'' sold''\trill |
la'' mi'' mi'' re''8 dod'' |
si'4\trill si' mi'' si' |
dod'' dod'' fad'' fad''8 mi'' |
red''4 fad'' si'' si'' |
sold'' la''8 si'' mi''4. mi''8 |
mi''4. fad''8 red''4.\trill mi''8 |
mi''4 mi'' dod'' re''8 mi'' |
la'4 dod'' re'' re''8 dod'' |
si'4 dod'' lad' si'8 dod'' |
fad'4 si' lad'4.\trill si'8 |
si'4 si'' sold'' la''8 si'' |
mi''4 fad''8 sold'' la''4 la''8 sol'' |
fad''4 fad'' re'' mi''8 fad'' |
si'4 mi'' dod''\trill re''8 mi'' |
la'4. si'8 si'4.\trill la'8 |
la'2 r |
r4 la'' mi'' fad''8 sol'' |
fad''2~ fad''8 sold'' la'' si'' |
sold''2\trill la''4. la''8 |
la''4. si''8 sold''4.\trill la''8 |
la''1 |
R1*4 R2.*2 R1 R2.*3 R1 R2. |
r4 mi''8 mi'' mi''4 la'' |
la''2 la''4 la'' |
sold''\trill sold'' si'' sold'' |
mi'' la'' fad'' si'' |
sold''1\trill |
r4 mi''8 mi'' mi''4 la'' |
sold''4\trill si'' mi'' la'' |
re''2 sol''4 mi'' |
fad'' fad''8 mi'' re''4 re'' |
mi''8 fad'' mi'' re'' dod''4.\trill dod''8 |
re'' mi'' re'' dod'' si'4 dod'' |
re''2 re''4 dod'' |
si'\trill mi'' dod'' dod''8 dod'' |
fad''4 re'' si' mi'' |
dod''1\trill |
R1*2 R2. R1 R2. R1*4 R2.*2 R1*2 |
r4 <>^"Violons" mi'' mi''8 la'' |
fad''4.\trill fad''8 sold'' la'' |
mi''2 mi''8 mi'' |
mi''4 mi'' la'' |
la'' la'' sold'' |
fad''2\trill fad''8 si'' |
sold''4\trill sold'' sold''8 sold'' |
la''2 la''4 |
la'' sold''4.\trill la''8 |
la''4 dod'' dod''8 dod'' |
red''4. red''8 mid''4 |
fad'' mid''4.\trill fad''8 |
fad''2 fad''8 fad'' |
fad''2 mi''4 |
mi'' red''4.\trill mi''8 |
mi''4\trill re'' re''8 mi'' |
fad''4. mi''16 re'' dod''4 |
re'' si'4.\trill la'8 |
la'2 r |
R1 R2.*2 R1*5 R2. R1*3 R2.*2 R1*2 R2. R1*2 |
