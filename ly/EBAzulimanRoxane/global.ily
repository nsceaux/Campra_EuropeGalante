\key la \major \tempo "Vivement" \midiTempo#160
\time 2/2 s1*24 \bar "||"
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.*2
\time 4/4 \grace s8 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 s4
\beginMark "Air" s2. \bar ".!:" s1*3 \alternatives s1*2 s1 s1*8
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 3/4 s2.*2
\time 4/4 \grace s8 s1*2
\digitTime\time 3/4 \midiTempo#120 s2.*18
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2 \bar "|."
