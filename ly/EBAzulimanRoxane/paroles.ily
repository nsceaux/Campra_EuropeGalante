Quoy ! pour d’au -- tres ap -- pas, vôtre ame est en -- fla -- mé -- e ?
Mes soû -- pirs dé -- sor -- mais vont ê -- tre su -- per -- flus :
Ah ! pour -- quoy m’a -- vez- vous ai -- mé -- e ?
Ou pour -- quoy ne m’ai -- mez vous plus ?
Ah ! pour -- quoy m’a -- vez- vous ai -- mé -- e,
ou pour -- quoy ne m’ai -- mez vous plus ?
Ou pour -- quoy ne m’ai -- mez vous plus ?

Je ne rom -- prois pas nô -- tre chaî -- ne,
si vous sça -- viez m’y re -- te -- nir :
Je ne rom -- prois   - nir :
Mon cœur s’ac -- cor -- de sans pei -- ne,
à qui sçait mieux l’ob -- te -- nir ;
Mon cœur s’ac -- cor -- de sans pei -- ne,
à qui sçait mieux l’ob -- te -- nir.

Que vôtre in -- cons -- tance est cru -- el -- le !
He -- las ! vous m’ô -- tez vô -- tre cœur,
et mal -- gré tou -- te ma dou -- leur,
je n’o -- se vous trai -- ter d’in -- grat & d’in -- fi -- del -- le ;
Je vois a -- vec hor -- reur mé -- pri -- ser mes ap -- pas,
je sens les plus vi -- ves al -- lar -- mes ;
Mais, le res -- pect me force à mur -- mu -- rer tout bas,
et me fait dé -- vo -- rer mes sou -- pirs & mes lar -- mes.

Vous me -- ri -- tez un sort plus doux,
et mon cœur à re -- gret se dé -- ta -- che du vô -- tre :
La pi -- tié parle en -- cor pour vous ;
mais l’A -- mour par -- le pour un au -- tre.
La pi -- tié parle en -- cor pour vous,
mais l’A -- mour par -- le pour un au -- tre.

C’en est donc fait, Sei -- gneur, mes beaux jours sont pas -- sez.

Je n’ou -- blie -- rai ja -- mais que vous me fû -- tes che -- re.

Vous ne m’ai -- mez plus, c’est as -- sez,
tout le res -- te me de -- ses -- pe -- re ;
Que ne puis-je ou -- bli -- er que je vous ay sçu plai -- re,
je ne sen -- ti -- rois pas que vous me tra -- his -- sez.

On ap -- pro -- che, ces -- sez u -- ne plain -- te trop vai -- ne.
Cel -- les qu’i -- cy mon ordre a -- mei -- ne,
vont par leurs jeux ré -- pondre à mes de -- sirs.
Dis -- si -- mu -- lez vô -- tre pei -- ne,
et res -- pec -- tez mes plai -- sirs.

Voy -- ons du moins l’ob -- jet de ses nou -- veaux de -- sirs ;
Sça -- chons à qui je dois ma hai -- ne.
