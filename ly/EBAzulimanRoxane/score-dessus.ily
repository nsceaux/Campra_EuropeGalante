\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff <<
        \global \includeNotes "dessus2"
        { s1*24\break }
      >>
    >>
  >>
  \layout { }
}
