\clef "vbas-dessus" R1*24 |
<>^\markup\character Roxane
dod''4 r8 re'' si'4\trill si'8 mi'' |
la'4. re''8 re'' re'' mi'' fad'' |
si' si' r mi''16 mi'' mid''4\trill mid''8 mid'' |
fad''4. dod''8 red'' mi'' mi'' red'' |
mi''4 sold' r8 sold' |
dod''4 dod''8 si' la'\trill sold' |
\appoggiatura sold'8 la' la' re'' si' sold'4\trill sold'16 sold' lad' si' |
lad'4\trill fad'' r8 fad'' |
fad''8.[ mi''16] mi''8 mi'' mi'' mi'' |
mi''8.[ re''16] re''8 r dod'' dod'' |
fad''4 si'16 si' dod'' re'' sold'4\trill mi''8 sold' |
la' si'16 dod'' dod''4( si'8.)\trill la'16 |
la'4
\ffclef "vbasse" <>^\markup\character Zuliman
la8 la la4 fad |
dod'2 dod'4 la |
mi' mi' sold\trill sold8 sold |
la2 si4 si8 si |
mi2 r |
r4 la8 la la4 fad |
mi4 mi la fad |
sol2 mi4 la |
re re sol sol8 sol |
mi2 fad4. fad8 |
si,4 si mi' dod' |
sold2\trill sold4 la |
mi mi fad fad8 fad |
re2 mi4. mi8 |
la,1 |
\ffclef "vbas-dessus" <>^\markup\character Roxane
dod''16 dod'' dod'' re'' mi''8 mi''16 sold' la'8 la' r re'' |
si'\trill si'16 si' mi''8 mi''16 mi'' dod''4\trill la'8 la' |
la'4 sold'8 sold' sold' si' |
mid'4 r8 sold' dod'' dod'' dod'' sold' |
la' re'' sold'4 sold'8 lad'16 si' |
lad'8. sold'16( fad'8) r16 fad'' dod''8 dod'' dod'' la' |
mi'' la'16 dod'' la'8\trill la'16 sold' sold'4\trill r8 mi' |
si'4 si'8 re'' lad'4\trill lad'8. si'16 |
si'8 si'16 r re''4 r8 fad'16 fad' fad'8 sold' |
la'8 la' la' sol' sol' fad' |
fad'2\trill fad''8 re'' |
\appoggiatura dod''8 si'4 si'8. dod''16 sold'4\trill mi''8 la' |
\appoggiatura sold'8 fad'4 sold'8 la' la'4( sold') |
la'
\ffclef "vbasse" <>^\markup\character Zuliman
la4 la8 la |
re'4. dod'8 si la |
sold2\trill sold8 sold |
la4 la fad |
red red mi |
si2 si8 si, |
mi4 mi mi'8 re' |
dod'2\trill dod'4 |
re' si4.\trill la8 |
la4 la la8 la |
si4. si8 dod'4 |
fad dod2 |
fad si8 la |
sold2\trill sold4 |
la fad4.\trill mi8 |
mi4 sol sol8 sol |
re4. mi8 fad4 |
re mi2 |
la,4
\ffclef "vbas-dessus" <>^\markup\character Roxane
r8 mi'' mi'' mi'' dod''8.\trill dod''16 |
\appoggiatura dod''16 re''4 si'8. si'16 si'4 la'8. sold'16 |
sold'4\trill
\ffclef "vbasse" <>^\markup\character Zuliman
dod'8 dod'16 dod' dod'8. mid16 |
fad4 fad8 fad16 fad fad8 mid |
fad4 fad8 r
\ffclef "vbas-dessus" <>^\markup\character Roxane
dod''8 dod'' dod'' re'' |
si'4\trill r8 si'16 mi'' dod''4\trill r8 la'16 la' |
re''8 re'' re'' re''16 fad'' si'8\trill si' r si'16 si' |
fad'8\trill fad'16 fad' sol'4 sold'8 sold'16 sold' lad'8 si' |
lad'4\trill lad'8 r dod'' dod'' dod'' dod''16 red'' |
mi''8. sol''16 dod''8\trill dod'' dod'' re'' |
si'4
\ffclef "vbasse" <>^\markup\character Zuliman
r8 si16 si sold8\trill sold r mi |
si8 si16 si si8 dod'16 re' dod'8\trill dod'16 r la8 la16 la |
mi8 mi fad sol fad\trill fad16 r re'8 re'16 re' |
si8\trill si si la la sold |
sold4\trill r16 mi' mi' mi' sold8 sold16 si |
mi8 mi16 r la8 la16 dod re4 mi16[ re] mi8 |
la,4
\ffclef "vbas-dessus" <>^\markup\character-text Roxane à part
r8 dod'' dod'' mi'' la' la' |
fad'\trill fad' fad' fad' sold' la' |
sold'\trill r16 mi' si'4 si'8 dod''16 re'' sold'8.\trill la'16 |
la'4. la'8 r2 |
