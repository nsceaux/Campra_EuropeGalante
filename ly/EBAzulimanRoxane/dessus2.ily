\clef "dessus" r2 r4 mi'' |
dod''\trill re''8 mi'' la'4 re''8 dod'' |
si'4\trill si' mi'' si' |
dod'' dod'' dod'' si'8 la' |
sold'4\trill la'8 si' mi'4 sold'\trill |
la' mi'' la'' la''8 sold'' |
fad''4\trill red'' mi'' mi''8 red'' |
mi''4 si' dod'' sold' |
la' si'8 dod'' fad'4 si' |
sold'2\trill r |
r4 la'' fad'' sol''8 la'' |
re''4 dod'' dod''2\trill |
re''4 mi''8 re'' dod''4 fad'' |
red''4\trill mi''8 fad'' si'2 |
r4 mi'' dod'' re''8 mi'' |
la'4 la' si' si' |
sold'4\trill la'8 si' mi'4 sol' |
fad'4. sold'!8 sold'4.\trill la'8 |
la'4 mi'' dod''\trill re''8 mi'' |
la'2~ la'4. la'8 |
la'2 re''~ |
re''8 si' dod'' re'' dod''4.\trill dod''8 |
dod''4. si'8 si'4.\trill la'8 |
la'1 |
R1*4 R2.*2 R1 R2.*3 R1 R2. |
r4 dod''8 dod'' dod''4 re'' |
mi''2 mi''4 re''8 dod'' |
si'4\trill si' re'' re''8 mi'' |
dod''4 fad'' red''4.(\trill dod''16 red'') |
mi''1 |
r4 dod''8 dod'' dod''4 re'' |
mi''4 mi'' dod'' fad'' |
si'2 mi''4 dod'' |
re'' re''8 dod'' si'4 si' |
si'2 lad'4.\trill lad'8 |
si' dod'' si' la' sold'4 la' |
si'2 si'4 la' |
sold'\trill sold' la' la'8 la' la'2 la'4 sold' |
la'1 |
R1*2 R2. R1 R2. R1*4 R2.*2 R1*2 |
r4 dod'' dod''8 dod'' |
re''4. la'8 re'' dod'' |
si'2\trill si'8 si' |
dod''4 dod'' dod'' |
fad'' fad'' mi'' |
mi'' red''4.\trill red''8 |
mi''4 mi'' si'8 si' |
mi''2 mi''4 |
fad'' re''4.(\trill dod''16 re'') |
dod''4\trill la'' la''8 la'' |
la''4 sold'' sold'' |
la'' sold''4.\trill fad''8 |
fad''2 re''8 dod'' |
si'2\trill si'4 |
dod'' la'4.(\trill sold'16 la') |
sold'4\trill sol'? si'8 dod'' |
re''4. dod''16 si' la'4 |
si' sold'4.\trill la'8 |
la'2 r |
R1 R2.*2 R1*5 R2. R1*3 R2.*2 R1*2 R2. R1*2 |
