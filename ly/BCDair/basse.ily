\clef "basse" sol8 la |
si4 si sol |
do'2 la4 |
re' re do |
si, la, sol, |
re re,2 |
sol,4 sol sol |
sold4. fad8 mi4 |
la4. sol8 fad4 |
dod2 re4 |
sol, la,2 |
re4. do8 si, la, |
sol,2 sol8 la |
si4 si sol |
do'8 re' do' si la4 |
mi'4 si do' |
re' re2 |
sol, sol8 la |
si4 si sol |
do'8 re' do' si la4 |
mi' si do' |
re' re2 |
sol,
