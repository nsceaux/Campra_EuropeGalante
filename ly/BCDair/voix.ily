\clef "vbas-dessus" <>^\markup\character Une Bergere
si'8 do'' |
re''4 re''4.\trill re''8 |
mi''2 do''4 |
la'\trill la' \appoggiatura sol'8 fad'4 |
sol' la' si' |
si'( la'2)\trill |
sol'4 si' si' |
mi''4. re''8 mi''4 |
dod'' la' la' |
la'2 \appoggiatura sol'16 fad'4 |
sol' mi' la' |
fad'4.\trill\melisma mi'8\melismaEnd re'4 |
r r si'8 do'' |
re''4 re''4.\trill re''8 |
mi''2 mi''8 fad'' |
sol''4 sol' la' |
si'( la'2)\trill |
sol' si'8 do'' |
re''4 re''4.\trill re''8 |
mi''2 mi''8 fad'' |
sol''4 sol' la' |
si'( la'2)\trill |
sol'2
