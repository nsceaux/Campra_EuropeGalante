\newBookPart#'()
\partBlankPageBreak#'(dessus)
\act Troisième Entrée
\markup\fill-line\fontsize#4 { L’ESPAGNE }
\markup\vspace#1
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente une Place publique,
  que l’on discerne à peine, parce que l’action
  se passe dans la nuit.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Dom Pedro, Chevalier Espagnol, sous le balcon de sa Maîtresse.
}
%% 3-1
\pieceToc\markup\wordwrap {
  Dom Pedro : \italic {
    Sommeil, qui chaque nuit joüissez de ma belle
  }
}
\includeScore "CAApedro"
\newBookPart #'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Dom Carlos, suivi d’une Troupe de Musiciens & de Danseurs,
  pour donner une Serenade.
}
%% 3-2
\pieceToc\markup\wordwrap {
  Dom Carlos : \italic {
    La nuit rameine en vain le repos dans le monde
  }
}
\includeScore "CBAcarlos"
\newBookPart #'(full-rehearsal)
%% 3-3
\pieceToc\markup\wordwrap {
  Premier air, pour les Espagnols
}
\includeScore "CBBair"
\newBookPart #'(full-rehearsal)
%% 3-4
\pieceToc\markup\wordwrap {
  Une Espagnole : \italic { El esperar en amor es merecer }
}
\includeScore "CBCespagnole"
\newBookPart #'(full-rehearsal)
%% 3-5
\pieceToc "Second air, rondeau"
\includeScore "CBDrondeau"
\newBookPart #'(full-rehearsal)
%% 3-6
\pieceToc\markup\wordwrap {
  Un Espagnol : \italic { Nuit soyez fidelle }
}
\includeScore "CBEairChoeur"
\newBookPart #'(full-rehearsal)
\markup\fill-line { \line { On reprend le second air. } }
\reIncludeScore "CBDrondeau" "CBFrondeau"
\newBookPart #'(full-rehearsal)
%% 3-7
\pieceToc\markup\wordwrap {
  Dom Carlos : \italic { Vous ne paroissez point, ingrate Leonore }
}
\includeScore "CBFcarlos"
\newBookPart #'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Dom Carlos, suivi d’une Troupe de Musiciens & de Danseurs,
  pour donner une Serenade.
}
%% 3-8
\pieceToc\markup\wordwrap {
  Dom Pedro, Dom Carlos : \italic {
    Moderez le transport que vous faites paroître
  }
}
\includeScore "CCArecit"
\newBookPart #'(full-rehearsal)
%% 3-9
\pieceToc\markup\wordwrap {
  Chœur : \italic { Chantons de si belles ardeurs }
}
\includeScore "CCBchoeur"
\newBookPart #'(full-rehearsal)
%% 3-10
\pieceToc "Sarabande"
\includeScore "CCCsarabande"
%% 3-11
\pieceToc\markup\wordwrap {
  Une Espagnole : \italic { Soyez constans dans vos amour }
}
\includeScore "CCDespagnole"
%% 3-12
\pieceToc "Sarabande"
\reIncludeScore "CCCsarabande" "CCEsarabande"
\newBookPart #'(full-rehearsal)
%% 3-13
\pieceToc\markup\wordwrap {
  Chœur : \italic { Chantons de si belles ardeurs }
}
\reIncludeScore "CCBchoeur" "CCFchoeur"
\newBookPart #'(full-rehearsal)
%% 3-14
\pieceToc "Rondeau"
\reIncludeScore "CBDrondeau" "CCGrondeau"
\actEnd "FIN DE LA TROISIÈME ENTRÉE"
