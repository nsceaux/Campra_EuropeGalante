\clef "dessus" la''4 la''8 |
sold''4\trill mi''8 mi''4 la''8 |
sold''4\trill mi''8 mi''8. fad''16 sold''8 |
la''4 la'8 la'8. si'16 dod''8 |
si'4\trill la'8 dod''4 fad''8 |
red''4\trill si'8 si'4 mi''8 |
red''4\trill fad''8 fad''8. sold''16 la''8 |
si''4 mi''8 mi''8. fad''16 sold''8 |
fad''4\trill mi''8 dod''4 fad''8 |
red''4\trill si'8 si'4 mi''8 |
red''4\trill fad''8 fad''8. sold''16 la''8 |
si''4 mi''8 mi''8. fad''16 sold''8 |
fad''4\trill mi''8 sold'4 si'8 |
re''4. la'4 dod''8 |
mi''4. mi''8. re''16 dod''8 |
mi''8. re''16 dod''8 mi''8. re''16 dod''8 |
si'4\trill la'8 la''4 la''8 |
sold''4\trill mi''8 mi''4 la''8 |
sold''4\trill mi''8 mi''8. fad''16 sold''8 |
la''4 la'8 la'8. si'16 dod''8 |
si'4\trill la'8 la''4 la''8 |
sold''4\trill mi''8 mi''4 la''8 |
sold''4\trill mi''8 mi''8. fad''16 sold''8 |
la''4 la'8 la'8. si'16 dod''8 |
si'4\trill la'8
