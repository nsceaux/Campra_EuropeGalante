\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4. s2.*6\break s2.*6 s4. \bar "" \pageBreak
        s4. s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
