\clef "haute-contre" dod''4 dod''8 |
si'4 si'8 dod''4 dod''8 |
si'4 dod''8 dod''8. re''16 mi''8 |
la'4 la'8 la'8. sold'16 la'8 |
sold'4\trill la'8 la'4 dod''8 |
si'4 si'8 si'4 si'8 |
si'4 si'8 si'4 si'8 |
mi''4 mi''8 mi''8. red''16 mi''8 |
red''4 mi''8 la'4 dod''8 |
si'4 si'8 si'4 si'8 |
si'4 si'8 si'4 si'8 |
mi''4 mi''8 mi''8. red''16 mi''8 |
red''4 mi''8 mi'4 mi'8 |
re'4. la'4 la'8 |
la'4. dod''8. si'16 la'8 |
dod''8. si'16 la'8 dod''8. sold'16 la'8 |
sold'4\trill la'8 dod''4 dod''8 |
si'4 si'8 dod''4 dod''8 |
si'4 dod''8 dod''8. re''16 mi''8 |
la'4 la'8 la'8. sold'16 la'8 |
sold'4\trill la'8 dod''4 dod''8 |
si'4 si'8 dod''4 dod''8 |
si'4 dod''8 dod''8. re''16 mi''8 |
la'4 la'8 la'8. sold'16 la'8 |
sold'4\trill la'8
