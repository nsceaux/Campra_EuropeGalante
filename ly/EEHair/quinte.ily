\clef "quinte" dod'4 la8 |
si4 si8 la4 la8 |
si4 la8 dod'8. dod'16 si8 |
fad'4 fad'8 mi'8. re'16 dod'8 |
mi'4 mi'8 dod'4 dod'8 |
red'4 red'8 mi'4 mi'8 |
fad'4 red'8 red'8. mi'16 fad'8 |
si4 si8 si4 si8 |
si4 si8 dod'4 dod'8 |
red'4 red'8 mi'4 mi'8 |
fad'4 red'8 red'8. mi'16 fad'8 |
si4 si8 si4 si8 |
si4 si8 si4 si8 |
si4. re'4 dod'8 |
dod'4. mi' |
mi' mi'8. si16 dod'8 |
mi'4 dod'8 dod'4 la8 |
si4 si8 la4 la8 |
si4 la8 la4 mi'8 |
fad'4 fad'8 mi'8. re'16 dod'8 |
mi'4 mi'8 dod'4 la8 |
si4 si8 la4 la8 |
si4 la8 la4 mi'8 |
fad'4 fad'8 mi'8. re'16 dod'8 |
mi'4 mi'8
