\clef "basse" la la8 |
mi4 mi8 la4 la8 |
mi4 la8 la8. la16 sold8 |
fad8. mi16 re8 dod8. si,16 la,8 |
mi4 la,8 la4 la8 |
si4 si8 sold4 mi8 |
si,4 si8 si8. si16 la8 |
sold4 sold8 sold8. fad16 mi8 |
si4 mi8 la4 la8 |
si4 si8 sold4 mi8 |
si,4 si8 si8. si16 la8 |
sold4 sold8 sold8. fad16 mi8 |
si,4 mi,8 mi4 sold8 |
si4. fad4 la8 |
dod'4. dod' |
dod' dod'8. si16 la8 |
mi4 la,8 la4 la8 |
mi4 mi8 la4 la8 |
mi4 la8 la8. la16 sold8 |
fad8. mi16 re8 dod8. si,16 la,8 |
mi4 la,8 la4 la8 |
mi4 mi8 la4 la8 |
mi4 la8 la8. la16 sold8 |
fad8. mi16 re8 dod8. si,16 la,8 |
mi4 la,8
