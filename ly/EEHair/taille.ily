\clef "taille" mi'4 mi'8 |
mi'4 sold'8 la'4 mi'8 |
mi'4 mi'8 mi'4 mi'8 |
re'8. dod'16 re'8 mi'4 mi'8 |
mi'4 dod'8 la'4 la'8 |
fad'4 fad'8 sold'4 sold'8 |
fad'4 fad'8 fad'4 fad'8 |
sold'4 sold'8 sold'8. la'16 si'8 |
si'4 sold'8 mi'4 la'8 |
fad'4 fad'8 sold'4 sold'8 |
fad'4 fad'8 fad'4 fad'8 |
sold'4 sold'8 sold'8. la'16 si'8 |
si'4 sold'8 sold'4 sold'8 |
fad'4. fad'4 fad'8 |
mi'4. la' |
la' la'8. si'16 mi'8 |
mi'4 mi'8 mi'4 mi'8 |
mi'4 sold'8 la'4 mi'8 |
mi'4 mi'8 mi'4 mi'8 |
re'8. dod'16 re'8 mi'4 mi'8 |
mi'4 dod'8 mi'4 mi'8 |
mi'4 sold'8 la'4 mi'8 |
mi'4 mi'8 mi'4 mi'8 |
re'8. dod'16 re'8 mi'4 mi'8 |
mi'4 dod'8
