\clef "vbas-dessus" <>^\markup\character-text Venus à la Discorde
r8 la' re''16 re'' re'' mi'' fa''8. fa''16 fa'' fa'' fa'' mi'' |
mi''8\trill mi'' r mi'' la' la' r la'16 la' |
re''8 mi''16 fa'' mi''4.\trill mi''16 fa'' |
re''8 re'' r4 r2\fermata |
