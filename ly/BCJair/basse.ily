\clef "basse" sol4 |
sib2 sol |
re'4. do'8 sib4 la |
sol fad sol sol, |
re2. sol4 |
sib2 sol |
re'4. do'8 sib4 la |
sol do re re, |
sol,2. sol4 |
sib2 sol |
re'4. do'8 sib4 re |
do sib, la, sol, |
fa,2 fa,4 fa |
re mib re do |
sib, sib sib la |
sib2 fa |
sib,2. sib4 |
sib2 mib |
fa2. sib,4 |
do re mib do |
re2. re'4 |
re'2. re'4 |
re' do' sib la |
sol2 re |
sol,2.
