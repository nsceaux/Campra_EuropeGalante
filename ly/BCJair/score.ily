\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s4 s1*6\break s1*6\break s1*6\break }
    >>
  >>
  \layout { system-count = 5 }
  \midi { }
}
