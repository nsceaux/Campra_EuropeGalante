\clef "vhaute-contre" <>^\markup\character Un Berger
re'4 |
re'2 sol' |
fad'4.\trill fad'8 sol'4 la' |
sib' la' sib' sol' |
la'2 re'4 re' |
re'2 sol' |
fad'4. fad'8 sol'4 la' |
re' do' sib\trill la |
sib2 sol4 re' |
re'2 sib |
fa'4. mib'8 re'4 fa' |
mib' re' do'\trill sib |
do'2 do'4 fa' |
fa' sol' fa' mib' |
re' fa' fa' mib' |
re'2 do'\trill |
sib2. fa'4 |
fa'2 mib'4 re' |
do'4.\trill sib8 do'4 re' |
mib' re' do' sib |
la2\trill la4 la' |
la' sol' la' sib' |
la' fad' sol' la' |
sib'2 la'\trill |
sol'2.
