Sou -- pi -- rons tous,
sui -- vons l’A -- mour sans nous con -- train -- dre ;
Il est plus doux
de le sen -- tir que de le crain -- dre.
Qui sent ses coups,
les ché -- rit au lieu de s’en plain -- dre ;
L’A -- mour rend les a -- mants
ja -- loux de leurs tour -- mens.
Ses feux sont char -- mans,
gar -- dons- nous bien de les é -- tein -- dre,
C’est des ten -- dres soû -- pirs
que nais -- sent les plai -- sirs.
