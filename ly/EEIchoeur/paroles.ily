\tag #'(vbasse basse) {
  Star con -- ten -- to,
  star po -- ten -- to,
  del mon -- do star l’a -- mor, ô lo spa -- ven -- to.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Star con -- ten -- to,
  star po -- ten -- to,
  del mon -- do star l’a -- mor, ò lò spa -- ven -- to.
}
\tag #'(vbasse basse) {
  En re -- gnar,
  en a -- mar,
  far tri -- bu -- tir
  l’oc -- ci -- den -- to, l’O -- rien -- to.
  En re -- gnar,
  en a -- mar,
  sem -- pre sen -- tir
  pla -- zer sen -- sa tor -- men -- to,
  dir è far,
  o di -- sfar
  su -- bi -- to, su -- bi -- to
  sù lò mo -- men -- to.
}
