\piecePartSpecs
#`((dessus #:score-template "score-basse-voix")
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#20 #}))
