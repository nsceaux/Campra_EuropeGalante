\clef "taille" r4 r8 |
R2.*3 |
r4 r8 mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
re'8. mi'16 fad'8 mi'8. si16 dod'8 |
mi'4 mi'8 r4 r8 |
R2.*11 |
r4 r8
