\clef "haute-contre" r4 r8 |
R2.*3 |
r4 r8 la'8. la'16 la'8 |
sold'8.\trill sold'16 sold'8 la'8. la'16 la'8 |
sold'8. sold'16 sold'8 la'8. la'16 la'8 |
la'8. la'16 si'8 la'8. sold'16 la'8 |
sold'4\trill la'8 r4 r8 |
R2.*11 |
r4 r8
