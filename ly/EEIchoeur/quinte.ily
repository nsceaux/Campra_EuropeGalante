\clef "quinte" r4 r8 |
R2.*3 |
r4 r8 dod'8. si16 la8 |
si8. si16 si8 la8. la16 la8 |
si8. si16 si8 la8. la16 la8 |
re'8. re'16 re'8 dod'8. re'16 mi'8 |
mi'4 dod'8 r4 r8 |
R2.*11 |
r4 r8
