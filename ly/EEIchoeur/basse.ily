\clef "basse"
<<
  \tag #'basse-continue {
    la4 la8 |
    mi4 mi8 la4 la8 |
    mi4 mi8 la8. si16 dod'8 |
    re'8. dod'16 si8 dod'8. si16 la8 |
    mi4 la,8
  }
  \tag #'basse { r4 r8 R2.*3 r4 r8 }
>>
la4 la8 |
mi4 mi8 la4 la8 |
mi4 mi8 la8. si16 dod'8 |
re'8. dod'16 si8 dod'8. si16 la8 |
mi4 la,8
<<
  \tag #'basse-continue {
    la4 la8 |
    si4. si,4 si,8 |
    mi4. mi8. fad16 sold8 |
    la8. sold16 fad8 sold8. fad16 mi8 |
    si4 mi8 la4 la8 |
    si4. si,4 si,8 |
    mi4. mi8. fad16 sold8 |
    la8. sold16 fad8 sold8. fad16 mi8 |
    si4 mi8 mi4 sold8 |
    si4. fad4 la8 |
    dod'4. dod' |
    dod' dod'8. si16 la8 |
    mi4 la,8
  }
  \tag #'basse { r4 r8 R2.*11 r4 r8 }
>>
