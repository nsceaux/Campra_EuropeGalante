\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre)
   (taille)
   (quinte #:notes "taille")
   (basse #:tag-notes basse)
   (basse-continue #:tag-notes basse-continue
                   #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#164 #}))
