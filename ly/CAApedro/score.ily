\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff <<
        \new Staff <<
          \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff <<
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'pedro \includeNotes "voix"
    >> \keepWithTag #'pedro \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*11\pageBreak
        s2.*9 s4 \bar "" \break s2 s2.*6 s4 \bar "" \pageBreak
        s2 s2.*6 s4 \bar "" \break s2 s2.*5 s4 \bar "" \pageBreak
        s2 s2.*6\break s2.*7\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*8\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*10\pageBreak
        s2.*8\break s2.*10\pageBreak
        s2.*5\break s2.*6\pageBreak
        s2.*8\break s2.*7 s1*2\break
      }
      \modVersion { s2.*158 s1\break }
    >>
  >>
  \layout { }
  \midi { }
}
