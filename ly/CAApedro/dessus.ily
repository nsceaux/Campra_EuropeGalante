\clef "dessus"
<>^\markup\whiteout Flutes <<
  \tag #'dessus1 {
    r4 r sol'' |
    sol''2. |
    do''4 do''4. re''8 |
    si'2\trill si'8 si' |
    do''( re'') mib''( fa'') sol''( mib'') |
    fa''( mib'') fa''( sol'') lab''( fa'') |
    sol''2~ sol''8 sol'' |
    fa''4 re''4.\trill re''8 |
    mib''2
  }
  \tag #'dessus2 {
    \stopHaraKiri do''2. |
    sib' |
    lab' |
    sol' |
    do''4 sol'4. la'8 |
    sib'2 sib'4 |
    r sol' do'' |
    do'' si' re'' |
    sol'2 \startHaraKiri
  }
>> <>^\markup\whiteout Violons & Flutes sol'8\fortSug sol' |
sol'2.~ |
sol'2 sol'8 fa' |
sol'2. |
sol''4 mib'' do'' |
lab'' fa''4. sib''8 |
sib''4 sol'' mib''~ |
mib''8 re'' re''4.\trill do''8 |
do''4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 {
    sol''8( fa'') sol''( mib'') |
    fa''( mib'') fa''( mib'') fa''( sol'') |
    lab''( sol'') fa''( mib'') re''( do'') |
    sol''4 sol''4. sol''8 |
    sol''4
  }
  \tag #'dessus2 {
    \stopHaraKiri mib''8( re'') mib''( do'') |
    re''( do'') re''( do'') re''( mib'') |
    fa''( mib'') re''( do'') sib'( la') |
    sib'4 si'4.\trill si'8 |
    do''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous sol'4 do'' |
do'' sib'4.\trill lab'8 |
sol'4 mib''2 |
r4 re''8 mib'' fa'' re'' |
mib'' re'' mib'' fa'' sol'' la'' |
sib''4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 { sol''8( la'') sib''( sol'') | do'''4 }
  \tag #'dessus2 {
    \stopHaraKiri re''8( do'') re''( mib'') |
    do''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous do''8( si') do''( re'') |
si'4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 { re''8( mib'') fa''( re'') | mib''4 }
  \tag #'dessus2 {
    \stopHaraKiri si'8( do'') re''( si') |
    do''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous sol'4 do'' |
do'' sib'4.\trill lab'8 |
sol'4 mib''2~ |
mib''8 re'' re''4.\trill do''8 |
do''4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 { sol''8( fa'') sol''( mib'') | fa''4 }
  \tag #'dessus2 {
    \stopHaraKiri mib''8( re'') mib''( do'') |
    re''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous re''4. sol''8 |
do''4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 { mib''8( re'') mib''( fa'') | re''2\trill }
  \tag #'dessus2 {
    \stopHaraKiri do''8( si') do''( re'') |
    si'2\trill \startHaraKiri
  }
>> <>^\markup\whiteout Tous sol''8( fa'') |
mib''( re'') do''( sib') lab'( sol') |
lab'4 lab'4. sib'8 |
sol'4.\trill sol'8 do''4~ |
do''8 re'' si'4.\trill do''8 |
do''4 mib''8(\doux re'') mib''( do'') |
re''4 re'' sol''~ |
sol'' sol'' fa'' |
sol'' re''8( mib'') fa''( re'') |
mib''( re'') mib''( fa'') sol''( mib'') |
fa''( mib'') fa''( sol'') lab''( fa'') |
sol''2~ sol''8 sol'' |
fa''4 re''4.\trill re''8 |
mib''4 mib''8( re'') mib''( do'') |
re''4 sol''8( fa'') mib''( re'') |
do''( si') do''( re'') do''( re'') |
si'( do'') re''( mib'') fa''( re'') |
mib''( re'') mib''( fa'') sol''( mib'') |
sib''4 sib'' sib'' |
sib''8( lab'') sol''( fa'') mib''( re'') |
mib''4 re''4.\trill re''8 |
mib''4 <>^\markup\whiteout Flutes <<
  \tag #'dessus1 {
    sol''8( fa'') sol''( mib'') |
    fa''( mib'') fa''( mib'') fa''( sol'') |
    lab''( sol'') fa''( mib'') re''( do'') |
    sol''2.~ |
    sol''4
  }
  \tag #'dessus2 {
    \stopHaraKiri mib''8( re'') mib''( do'') |
    re''( do'') re''( do'') re''( mib'') |
    fa''( mib'') re''( do'') sib'( la') |
    sib'4 si'4.\trill si'8 |
    do''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous mib''2\doux~ |
mib''4 re'' re'' |
re'' do'' do'' |
do'' si'4. si'8 |
do''4 lab''2~ |
lab''4 sol'' mib'' |
\appoggiatura re''8 do''2~ do''8 do'' |
re''2 r4 |
r4 mib''2~ |
mib''4 re'' re'' |
re'' do'' do'' |
do'' si'4.(\trill la'16 si') |
do''8( re'') mib''( fa'') sol''( la'') |
sib''4 re''4. mib''8 |
do''2 do''4~ |
do''4 si'\trill <>^\markup\whiteout Tous sol''8(\fort fa'') |
mib''( re'') do''( sib') lab'( sol') |
lab'4 lab'4. sib'8 |
sol'4.\trill sol'8 do''4~ |
do''8 re'' si'4.\trill do''8 |
do''4 mib''4.\doux mib''8 |
re''2 sol''4~ |
sol'' fa''2~ |
fa''4 fa'' mib''8\trill re'' |
mib''2 r8 mib'' |
sib'4 sib'4. sib'8 |
sib'4 sol' mib'' |
re''8 do'' si'4.\trill si'8 |
do''2 sol'4 |
sol'2~ sol'8 sol' |
lab'4 fa'4.\trill fa'8 |
sol'4 sol''8 fa'' mib''\trill re'' |
mib''2~ mib''8 mib'' |
fa''2~ fa''8 fa'' |
sol''2~ sol''8 sol'' |
lab''4 fa'' re'' |
mib'' sol'4. la'8 |
sib'4 sol'2~ |
sol'4 fa'2~ |
fa'4 fa' re'\trill |
mib' <>^\markup\whiteout Flutes <<
  \tag #'dessus1 {
    lab''4 lab'' |
    lab'' lab''8 sol'' lab''4 |
    r sol''8 fa'' sol''4 |
    fa''8\trill mib'' re'' mib'' fa'' re'' |
    mib''4
  }
  \tag #'dessus2 {
    \stopHaraKiri mib''4. mib''8 |
    fa''4 fa''8 mib'' fa''4 |
    r mib''8 re'' mib''4 |
    re''8 do'' si' do'' re'' si' |
    do''4 \startHaraKiri
  }
>> <>^\markup\whiteout Tous sol''4. la''8 |
sib''4 mi''4.\trill mi''8 |
fa''4. mib''8 re'' do'' |
si'\trill la' sib'2~ |
sib'4 lab'8 sol' lab'4~ |
lab' fa' sib'~ |
sib' sol' mib''~ |
mib''8 re'' re''4.\trill do''8 |
do''2 la'4\doux |
fa'\trill la'4. la'8 |
sib'4 fad' sol' |
fad'2. |
la'2~ la'8 la' |
sib'2 sib'4 |
fad'8 sol' sol'4( fad'8.)\trill sol'16 |
sol'2. |
sib'4 sib'4. sib'8 |
sib'2. |
lab'4 sol'4.\trill sol'8 |
lab'2. |
r4 fa'' lab'' |
re''2 re''4 |
mib'' re''4.\trill do''8 |
do''2. |
r4 re''\fort fa'' |
si'2\trill si'4 |
do''2 do''8 si' |
do''4 sol'4.\doux sol'8 |
fa'2. |
mib' |
sol'2 fa'4 |
sol' sol'4. sol'8 |
sol'2 sol''4 |
mib'' lab''4. lab''8 |
fa''2\trill fa''8 fa'' |
sol''4 sol'' sib'' |
mib''2 mib''8 fa'' |
sol''4 fa''4.\trill mib''8 |
mib''2. |
sol''4 sol''4. sol''8 |
do''2 lab''8 sol'' |
fa''4 mib'' fa'' |
re''2\trill si'4 |
si'8 do'' do''4( si'8.) do''16 |
do''4 do''8 re'' mib'' fa'' |
sol''4 sol''4. sol''8 |
do''2 lab''8 sol'' |
fa''4 mib'' fa'' |
re''2 si'4 |
si'8 do'' do''4( si'8.) do''16 |
do''2. |
fa''4\fort sol'' lab'' |
si'2 re''4 |
re''8 mib'' mib''4( re''8.)\trill do''16 |
do''1 |
R1 R2. R1*3 |
