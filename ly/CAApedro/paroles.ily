\tag #'pedro {
  Som -- meil, qui cha -- que nuit joü -- is -- sez de ma bel -- le,
  ne ver -- sez point en -- cor vos pa -- vots __ sur ses yeux ;
  ne ver -- sez point en -- cor vos pa -- vots sur ses yeux.
At -- ten -- dez, at -- ten -- dez, pour re -- gner sur el -- le,
  qu’elle ait ap -- pris mes ten -- dres feux ;
  At -- ten -- dez, __ at -- ten -- dez, pour re -- gner sur el -- le,
  qu’elle ait ap -- pris mes ten -- dres feux.
  Je vais par -- ler, c’est as -- sez me con -- train -- dre,
  C’est trop ca -- cher les maux qu’el -- le me fait souf -- frir ;
  Du moins il est temps de m’en plain -- dre,
  lors -- que je suis prêt d’en mou -- rir.
Du moins il est temps de m’en plain -- dre,
  lors -- que je suis prêt d’en mou -- rir.
  Ah ! s’il plai -- soit aux beaux yeux que j’a -- do -- re,
  de sou -- la -- ger mon a -- mou -- reux tour -- ment,
  le sort fa -- tal que je dé -- plo -- re
  de -- vien -- droit un des -- tin char -- mant.
  Mais, ma mort est toû -- jours cer -- tai -- ne,
  quel -- que suc -- cés qu’A -- mour dai -- gne me pré -- pa -- rer ;
  Que Lu -- ci -- le soit in -- hu -- mai -- ne,
  ou sen -- sible à l’ar -- deur que je viens de -- cla -- rer,
  il fau -- dra toû -- jours ex -- pi -- rer
  de mon plai -- sir, ou de ma pei -- ne.
  Il fau -- dra toû -- jours ex -- pi -- rer
  de mon plai -- sir, ou de ma pei -- ne.
}
Quel -- le trou -- pe s’a -- vance, & qui l’a -- meine i -- ci ?
Res -- tons, j’en veux être é -- clair -- ci.
