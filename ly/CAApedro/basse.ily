\clef "basse" R2.*8 |
do2.\fortSug |
sib, |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do r r |
R2.*3 |
r4 do8(\fortSug re) mib( do) |
re( do) re( mib) fa( re) |
mib( re) mib( fa) sol( mib) |
fa4 sol sol, |
do2. |
sib,4 r r |
r lab,2^\fortSug |
sol,4 r r |
r4 do8(\fortSug re) mib( do) |
re( do) re( mib) fa( re) |
mib( re) mib( fa) sol( mib) |
fa4 sol sol, |
do r r |
r sib8(\fortSug lab) sib( sol) |
lab4 r r |
r sol8(^\fortSug fa) mib( re) |
do2. |
re |
mib |
fa4 sol sol, |
do do2\douxSug |
sib,2. |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do2. |
sib, |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do4 r r |
R2.*2 |
r4 <>^\markup\whiteout Tous sol8(\douxSug fa) mib( re) |
do( si,) do( re) mib( do) |
re( do) re( mib) fa( re) |
mib( re) mib( fa) sol( mib) |
fa4 sol sol, |
do, do2 |
sib,2. |
lab, |
sol,4 sol8( fa) mib( re) |
do8( sib,) do( re) mib( do) |
re( do) re( mib) fa( re) |
mib( re) mib( fa) sol( mib) |
fa4 sol sol, |
do2. |
sib, |
lab, |
sol, |
do\fortSug |
re |
mib |
fa4 sol sol, |
do2. |
sib,^\douxSug |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do2. |
sib, |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do2. |
sib, |
lab, |
sol, |
do,4 r r |
R2.*3 | \allowPageTurn
r4 do2 |
sib,2. |
lab, |
sol, |
do |
re |
mib |
fa4 sol sol, |
do2. |
re2^\douxSug re8 do |
sib,4 lab, sol, |
re2. |
do |
sib,2 sib,4 |
la,8 sol, re4 re, |
sol,2. |
sol4 sol4. sol8 |
do'2. |
fa4 do2 |
fa, fa8 sol |
lab2. |
sol |
do4 sol,2 |
do fa8^\fortSug sol |
lab2. |
sol2 sol4 |
do sol,2 |
do2.~ |
do^\douxSug~ |
do |
do4 sib, lab, |
sol, sol4. do8 |
sol,2 sol4 |
lab fa2 |
sib4 sib, lab, |
sol,2. |
lab, |
sol,8 mib, sib,2 |
mib,4 mib2 |
mi2. |
fa2~ fa8 sol |
lab4 sol fa |
sol2. |
fa4 sol sol, |
do2. |
mi, |
fa,2 fa,8 sol, |
lab,4 sol, fa, |
sol,2. |
fa,4 sol,2 |
do,2 do'8\fortSug sib |
lab4 sol fa |
sol2 sol,4 |
fa, sol,2 |
<<
  \tag #'basse { do1 | R1 R2. R1*3 }
  \tag #'basse-continue {
    do2~ do8 do sib, la, |
    \once\set Staff.whichBar = "|"
    sol,1 |
    sol,2.~ |
    sol,2 re |
    sib,4~ sib,8. do16 re4 re, |
    sol,1 |
  }
>>
