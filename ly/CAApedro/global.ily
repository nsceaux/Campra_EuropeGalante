\key sol \minor \midiTempo#120 \tempo "Lentement"
\digitTime\time 3/4 s2.*158
\digitTime\time 2/2 s1 \bar "|." s1
\key re \minor
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*3 \bar "|."
