\clef "haute-contre" R2.*4 |
do'2.\doux |
re' |
mib' |
fa'4 sol'2 |
do'2 mib'8\fort  mib' |
re'2 mib'4 |
mib'2 mib'8 fa' |
re'2.\trill |
mib'4 sol' mib' |
sib'2 sib'4 |
sol'4.\trill sol'8 do''4 |
do''4 si'4.\trill do''8 |
do''4 do''2\doux |
sib'2. |
lab' |
sol' |
do''4 mib'8(\fort fa') sol'( mib') |
fa'4 fa'2~ |
fa'4 mib' do''~ |
do'' si'8 do'' re'' si' |
do'' sib' do'' re'' mib'' do'' |
re''4 sib'2\doux |
lab'4 mib'8(\fort re') mib'( fa') |
sol'4 sol'2\doux |
do'8( re') mib'(\fort fa') sol'( mib') |
fa'( sol') fa'( mib') re'( fa') |
mib'( fa') sol'( lab') sib'( sol') |
lab'4 sol'4. sol'8 |
sol'4 do''4.\doux do''8 |
sib'4 sib'4.\fort sib'8 |
lab'4 lab'4.\doux fa'8 |
sol'2. |
sol'8(\fort fa') mib'( re') do'4 |
fa'4 fa'4. fa'8 |
fa'4 mib' sol' |
lab' sol' re' |
mib' r r |
r r sol'\doux |
lab'2 do'4 |
sol'2.~ |
sol'4 sol'8( fa') mib'4 |
lab' fa' sib'~ |
sib' sol' do'' |
lab' sol'4. sol'8 |
sol'2 sol'4 |
sib'2 sol'4 |
lab'2 lab'4 |
re' sol'4. sol'8 |
sol'4. fa'8 mib'4 |
fa'2 fa'4 |
sol'4 mib''8( re'') do''( sib') |
lab'8 do' sol'4. sol'8 |
sol'4 <>^\markup\whiteout Flutes do''2 |
sib'2. |
lab' |
sol' |
r4 <>^\markup\whiteout [Tous] do''\douxSug sol' |
sib'2 sib'4 |
sib' sol'8( lab') sib'( sol') |
lab'4 sol'4. sol'8 |
sol'4 do''8 sib' do'' lab' |
sib'( do'') sib'( lab') sib'( sol') |
lab'2 do'4 |
sol'2 r4 |
r4 do'' sol' |
sib'2 sib'4 |
sib' sol'8( lab') sib'( sol') |
lab'4 sol'4. sol'8 |
sol'4 do''8( re'') mib''( do'') |
re''4 sib'4. sib'8 |
mib'2 fa'4 |
sol' sol'4. sol'8\fort |
sol'8( fa') mib'4. mib'8 |
fa'2 fa'4 |
mib'2 sol'4 |
lab' sol'4. sol'8 |
sol'2 r4 |
r sib'4.\douxSug sol'8 |
lab'4. sol'8 fa'4 |
sol'2 sol'4 |
sol' mib'4. mib'8 |
fa'2 r4 |
sol'2 r4 |
fa'4. re'8 sol'8. sol'16 |
sol'2 mib'4 |
do'2.~ |
do'4 do' fa' |
re'2.\trill |
r4 sol' mib''~ |
mib'' re''4. re''8 |
sib'4 sol' do''~ |
do'' si'\trill sol'~ |
sol' mib'2 |
fa'4 sib2 |
do' fa'4 |
re'4 re'4.\trill do'8 |
do'4 <>^\markup\whiteout [Flutes] do'4. do'8 |
re'4 re' re' |
mib' mib'4. mib'8 |
fa'4 sol'2 |
do'4 <>^\markup\whiteout [Tous] mib''4. mib''8 |
re''4 sib' sol' |
lab'4. sol'8 fa'4 |
sol'2 sol'8 fa' |
mib'\trill re' mib'4. mib'8 |
fa'2 fa'4 |
sol'2 sol'4 |
do'' si'4.\trill do''8 |
do''2 r4 |
r re'4.\douxSug re'8 |
sol'4 do' re' |
re'2. |
r4 re'4. re'8 |
re'2 re'4 |
do'8 re' re'4. re'8 |
re'2. |
sol'4 sol'4. sol'8 |
sol'2. |
fa'4 sol'4. sol'8 |
fa'2. |
r4 fa' fa' |
fa'2. |
mib'4 sol'4. sol'8 |
sol'2. |
r4 fa'\fortSug fa' |
fa'2. |
mib'4 sol' re' |
mib'2 r4 |
r4 do'2\douxSug~ |
do'2. |
mib'2 fa'4 |
re' re'4. mib'8 |
re'2 sol'4 |
do'' do''4. do''8 |
re''2 re''8 re'' |
mib''4 sib'2 |
do''2. |
sib'4 lab'4. sib'8 |
sol'2.\trill |
do''4 do''4. do''8 |
lab'2 lab'8 sib' |
do''4 do'' re''~ |
re'' sol'2 |
lab'4 sol'4. sol'8 |
sol'2 do''4~ |
do'' do''4. do''8 |
lab'2 lab'8 sib' |
do''4 do'' re''~ |
re'' sol'2 |
lab'4 sol'4. sol'8 |
sol'2. |
do''4\fortSug sib' lab' |
sol'2 si'4 |
si'8 do'' do''4( si'8.) do''16 |
do''1 |
R1 R2. R1*3 |
