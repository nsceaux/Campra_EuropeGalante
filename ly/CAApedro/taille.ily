\clef "taille" R2.*4 |
do'2.\douxSug |
re' |
mib' |
fa'4 sol' sol |
do'2 do'8\fortSug sol |
sib2 sol4 |
do'2 do'8 re' |
si2. |
do'2 do'4 |
fa'2 fa'4 |
mib'2 sol'4 |
lab' sol' re' |
mib' do''2\doux |
sib'2. |
lab' |
sol' |
do'4 do'\fort sol |
re' re'4. re'8 |
sib8 lab sol lab sib sol |
lab4 sol4. sol8 |
sol4 sol'8( fa') mib'4 |
fa' sib'2\doux |
lab'4 lab'\fort do' |
re' sol'\doux sol |
do' do'\fort sol |
re'8( mib') re'( do') sib4 |
sib do'2~ |
do'4 si4.\trill do'8 |
do'4 do''4.\doux do''8 |
sib'4 fa'4.\fort mib'8 |
mib'4 lab'4.\doux fa'8 |
sol'2 sol4\fort |
do'4. re'8 mib'4~ |
mib' re'8 do' sib4 |
sib2 mib'4~ |
mib'8 re' re'4.\trill do'8 |
do'4 r r |
r r re'\doux |
mib'2 fa'4 |
re'2.\trill |
do'4. re'8 mib'4~ |
mib' re' fa'~ |
fa' mib'4. mib'8 |
re'4 re'4.\trill re'8 |
do'8( re') mib'2 |
re' mib'4~ |
mib' mib' do' |
sol' si8( do') re'( si) |
do'( si) do'( re') mib'4~ |
mib' re' fa' |
mib'4. fa'8 sol'4 |
fa'4. mib'8 re' fa' |
mib'4 <>^\markup\whiteout Flutes do''2 |
sib'2. |
lab' |
sol' |
r4 <>^\markup\whiteout [Tous] sol2\douxSug |
re' re'4 |
sol' do' do' |
re'2 re'4 |
do'2 do'4 |
fa' re' mib'~ |
mib' do' fa' |
re'2 r4 |
r4 sol2 |
re' re'4 |
sol' do' do' |
re'2 re'4 |
do' sol'8( fa') mib'4 |
fa'2 sib4 |
do'2 lab'4 |
fa' fa' re'\fortSug |
mib'2 mib'4~ |
mib' re'8 do' sib4~ |
sib sol mib'~ |
mib'8 re' re'4.\trill do'8 |
do'2 r4 |
r re'\douxSug mib'~ |
mib' fa'2~ |
fa'4 re'4. re'8 |
do'2 mib'4 |
mib' re'4. re'8 |
sib4 mib'2 |
lab'4 sol'4. fa'8 |
mib'( re') do'2 |
sol2. |
fa2 do4 |
sol2. |
r4 r sol'~ |
sol' fa'2~ |
fa'4 mib' do' |
re' re'4.\trill re'8 |
do'2 do'4 |
sib2 sol4 |
lab4. sol8 fa4 |
sol2 sol4 |
sol <>^\markup\whiteout [Flutes] do'4. do'8 |
re'4 re'4. re'8 |
mib'4 mib'4. mib'8 |
fa'4 sol' sol |
do'4 <>^\markup\whiteout [Tous] do'4. do'8 |
sol4 do'2 |
do' re'4 |
re'2. |
do'2 mib'4 |
mib' re' fa' |
mib'2 sol'4 |
fa' fa' re' |
mib'2 r4 |
r fad'\douxSug la' |
re' do' sib8 sol |
la2. |
r4 la4. la8 |
sol2 sol4 |
do'8 sib do'4. re'8 |
si2. |
sol4 re'4. re'8 |
do'2. |
do'4 do'4. do'8 |
do'2. |
r4 fa' re' |
re'2. |
do'4 re'4.\trill( do'16 re') |
mib'2. |
r4 fa'\fortSug lab' |
re'2 re'4 |
mib' re'4.\trill do'8 |
do'2 r4 |
r4 fa2\douxSug |
sol2. |
do'4 sol lab |
re sol4. sol8 |
sol2 sib4 |
lab4 do' fa' |
fa'2 fa'8 fa' |
mib'2 mib'4 |
mib'2 lab4 |
sib sib4. sib8 |
sib2. |
sol4 sol'4. sol'8 |
fa'2. |
do'2 fa'4~ |
fa' re'4. re'8 |
fa'4 fa' re' |
mib'8 re' mib' fa' sol'4~ |
sol' sol'4. sol'8 |
fa'2. |
do'4 do' fa'~ |
fa' re'4. re'8 |
fa'4 fa' re' |
mib'2. |
lab'4\fortSug mib' fa' |
re'2 re'4 |
fa' fa' re'\trill |
mib'1 |
R1 R2. R1*3 |
