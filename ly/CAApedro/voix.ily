\clef "vhaute-contre"
<<
  \tag #'basse { R2.*158 R1*2 }
  \tag #'pedro {
    <>^\markup\character Dom Pedro R2.*40 |
    r4 r sol' |
    sol'2. |
    do'4 do'4. re'8 |
    si2\trill si8 si |
    do'4 sol4. la8 |
    \appoggiatura la8 sib2 sib4 |
    r sol sol |
    do' si\trill re' |
    sol2 sol8 sol |
    sol2.~ |
    sol2 sol8 fa |
    sol2. |
    sol'4 \appoggiatura fa'8 mib'4 \appoggiatura re'8 do'4 |
    \appoggiatura sib8 lab4.( sol8) lab4 |
    sol2\trill sol8 sol |
    do'2 do'8 si |
    do'4 r r |
    R2.*2 |
    r4 sol' sol' |
    sol'2.~ |
    sol'4 fa' fa' |
    fa' mib' mib' |
    re'4.\trill( do'8) re'4 |
    \appoggiatura re'16 mib'4 mib'8 mib' fa' sol' |
    re'2\trill sol'4~ |
    sol' \appoggiatura fa'8 mib'4 \appoggiatura re'8 do'4 |
    si\trill sol' sol' |
    sol'2.~ |
    sol'4 fa' fa' |
    fa' mib' mib' |
    re'4.(\trill do'8) re'4 |
    \appoggiatura re'8 mib'2 mib'4 |
    r fa' fa'8 sol' |
    lab'2 re'4 |
    mib'( re'4.)\trill do'8 |
    do'4 r r |
    R2.*3 |
    r4 r8 do' sol' sol' |
    sol'2 sol8 sol |
    do'4 do'4. do'8 |
    do'4.( sib8) sib4 |
    r lab lab8 lab |
    lab4 lab4.( sol8) |
    sol2\trill do'4 |
    re'8 mib' re'4.\trill re'8 |
    mib'2 mib'4 |
    mi'2\trill mi'8 mi' |
    fa'4. sol'8 lab'4 |
    si2\trill si4 |
    r do'8 sib lab sol |
    lab4 lab4. sib8 |
    sol2\trill mib'4 |
    re'2\trill sol'8 si |
    do'4. re'8 mib'4 |
    re'2\trill re'4 |
    r fa'8 mib' re' do' |
    si2\trill si8 do' |
    do'4 r r |
    R2.*11 |
    mib'2 do'8 do' |
    la4\trill la re' |
    sol la sib |
    la2\trill la4 |
    r re' re'8 fad |
    sol2 sol4 |
    la8 sib sib4( la8.)\trill sol16 |
    sol2. |
    re'4 re'4. sol'8 |
    mi'2.\trill |
    fa'4 fa' mi'8 mi'( |
    fa'2) fa'4 |
    r re' fa' |
    si2 si4 |
    do' do' si |
    do'2. |
    R2.*3 |
    r4 mib' r8 mib' |
    lab4 lab8 lab lab sol |
    sol4\trill sol8 r sol4 |
    sol8 la sib sol do'4 |
    si si8 si si do' |
    \appoggiatura do'8 re'2 mib'8 mib' |
    do'\trill do' fa'4 do'8 fa' |
    re'4\trill re' sib8 sib |
    mib'8.([ reb'16]) reb'4( do'8) reb' |
    do'2\trill do'8 re'! |
    mib'2 mib'8 re' |
    \appoggiatura re'8 mib'4 sol'8 fa' mib' re' |
    do'2 do'8 sol |
    \appoggiatura sol8 lab2. |
    fa'4 sol' lab' |
    si2 re'4 |
    re'8 mib' mib'4( re')\trill |
    do' sol'8 fa' mib' re' |
    do'2 do'8 sol |
    \appoggiatura sol8 lab2. |
    fa'4 sol' lab' |
    si2\trill re'4 |
    re'8 mib' mib'4( re')\trill |
    do'2. |
    R2.*3 R1*2 |
  }
>>
r4 r8 sol'16 sol' re'8 re'16 re' |
sib8. sib16 sib sib sib la la4\trill r8 re' |
re'4 r8 re'16 mib' la4\trill la8 sib |
sol1 |
