\clef "dessus" R1*2 |
la'4.*13/12 sol'32 fa' mi' fa'4.*13/12 mi'32 re' dod' |
re'4.*11/12 re'32 mi' fa' sol' la' sib'4.*13/12 sib'32 la' sol' |
la'8.*5/6 la'32 sol' fa' mi'4\trill re'4.\doux re'8 |
re'2 re'4. re'8 |
do'2. |
mi'2 mi'4 |
fa'2 la'4~ |
la' sol'4.\trill sol'8 |
la'4 re'8 re' re'4 dod'8. re'16 |
re'2 r |
R1 |
r4 la'8\fort fa' do''4 do''8 do'' |
fa''4 fa'8 fa' fa'4 mi'\trill |
fa'2 r4 sol'\doux |
la'2 r4 sol' |
mi'2\trill r4 sol' |
fa'2 r4 la' |
sol'2 r4 mi' |
fa'2 r4 fa' |
sol'2 r4 do' |
fa'2 r4 sol' |
sol'4. fa'8 fa'4. sol'8 |
mi'2\trill r4 si' |
do''2 r4 la' |
sib'2 r4 mi' |
fa'8 fa'' fa'' mib'' re'' do'' sib' la' |
sol'4. sol'8 sol'4.\trill fa'8 |
fa'2 r8 fa''\fort fa'' mib'' |
re'' do'' sib' la' sol' sol'' sol'' fa'' |
mi'' re'' do'' sib' la'4.\trill la'8 |
la'4. sol'8 sol'4.\trill fa'8 |
fa'2 fa''8\doux mi'' |
fa''4 fa''4. fa''8 |
fa''4 re'' sol'' |
mi''4.\trill do''8 re'' mi'' |
fa''4 do'' fa''8 mib'' |
re'' do'' sib' la' sol' fa' |
mi'2\trill do''4~ |
do'' re''8 do'' sib' la' |
sol'4.\trill sol'8 la' sib' |
la'\trill sol' la' sib' do'' re'' |
mi'' re'' mi'' fa'' sol'' mi'' |
la''4. sol''8 fa'' mi'' |
re'' do'' si' do'' re'' si' |
do''4 si'4. do''8 |
do'' re'' mi'' fa'' sol'' mi'' |
fa''4. fa''8 sol'' la'' |
sib''4 sib''4. sib''8 |
la''4 fad'' sol'' |
sol'' fad''4.\trill sol''8 |
sol''8 la'' sib'' la'' sol'' fa'' |
mi'' re'' mi'' fa'' sol'' mi'' |
la''4 la''4. la''8 |
fa''4 fa'' sib'' |
sol''4.\trill mi''8 fa'' sol'' |
do''4 fa'' mi'' |
fa'' re'' sol'' |
mi''8\trill re'' mi'' fa'' sol'' mi'' |
fa''4. fa''8 sol'' la'' |
sib''4 sib''4. sib''8 |
sol''4\trill do'''8 sib'' la''\trill sol'' |
la'' sib'' sol''4.\trill fa''8 |
fa''\fortSug sol'' la'' sol'' fa'' mi'' |
re'' mib'' re'' do'' sib' la' |
sol'\trill fa' sol' la' sib' do'' |
la'\trill sol' la' sib' do'' re'' |
si'\trill sol' la' si' do'' re'' |
mi'' fa'' re'' mi'' fa'' sol'' |
mi''\trill sol'' do''' sib'' la'' sol'' |
fa'' mi'' fa'' sol'' la'' sib'' |
sol''\trill do'' re'' mi'' fa'' sol'' |
la'' sib'' sol''4.\trill fa''8 |
fa''1*3/4\fermata |
