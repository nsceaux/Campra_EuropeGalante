C’en est fait, Dé -- esse in -- hu -- mai -- ne,
lais -- se- moy fuïr de ce fa -- tal sé -- jour ;
Tu n’as que trop joü -- y de ma cru -- el -- le pei -- ne,
O Ciel ! tout e -- chape à ma hai -- ne,
et tout céde à l’A -- mour.
J’ex -- ci -- tois vai -- ne -- ment le Dé -- pit & la Ra -- ge,
la for -- ce de l’A -- mour en bril -- loit d’a -- van -- ta -- ge ;
Fuy -- ons, fuy -- ons de l’U -- ni -- vers,
fuy -- ons, fuy -- ons de l’U -- ni -- vers,
al -- lons du moins re -- gner __ dans les En -- fers.
Fuy -- ons, fuy -- ons de l’U -- ni -- vers,
fuy -- ons, fuy -- ons de l’U -- ni -- vers,
al -- lons du moins re -- gner __ dans les En -- fers.


La Dis -- corde à l’A -- mour, céde en -- fin la vic -- toi -- re.
Vous, Jeux cha -- mans, ten -- dres Plai -- sirs,
vo -- lez __ de tou -- tes parts, pour ser -- vir ses de -- sirs ;
Al -- lez ac -- croître en -- cor son Em -- pire & sa gloi -- re.
Vo -- lez __ de tou -- tes parts, pour ser -- vir ses de -- sirs ;
Al -- lez ac -- croître en -- cor son Em -- pire & sa gloi -- re.
