\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1 s2.*2\pageBreak
        s2.*2 s1*3\break s1*4 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*3 s2.*3\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
