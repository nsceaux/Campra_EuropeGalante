\clef "haute-contre"R1*2 |
fa'4.*13/12 mi'32 re' do' re'4 la |
re'4. re'8 re'4.*13/12 mi'32 fa' sol' |
fa'8 re' mi'4 la2\douxSug |
re'2. sib4 |
do'2.~ do' |
do'2 \once\tieDashed fa'4~ |
fa' sol'4. sol'8 |
mi'4\trill fa'8 re' la4. la8 |
la2 r |
R1 |
r4 fa'8\fortSug fa' sol'4 sol'8 sol' |
fa'4 re'8 re' do'4 do' |
do'2 r4 mi'\douxSug |
fa'2 r4 re' |
do'2 r4 mib' |
re'2 r4 fa' |
sol'2 r4 do' |
do'2 r4 re' |
re'2 r4 do' |
do'2 r4 do' |
do' do'8 do' do'4 si |
do'2 r4 sol' |
sol'2 r4 fa' |
fa'2 r4 do' |
do' fa' fa' re' |
re' re' do' do' |
do'2 r8 la'\fortSug la' la' |
sib'4 re' mi'8 mi' mi' fa' |
sol'4 la'8 mi' fa'4. fa'8 |
fa'4. sol'8 mi'4.\trill fa'8 |
fa'2 do''8\douxSug sib' |
la'4 la' do'' |
sib' re'' re'' |
do''4 do''4. do''8 |
do''4. fa'8 sol' la' |
sib'4 re' re' |
mi'8 re' mi' fa' sol' mi' |
fa'2 do'4 |
do'2 do'4 |
fa'2 fa'4 |
sol'2 do''4 |
re'' la' la' |
sol'2 sol'4 |
sol'8 la' sol' la' sol' fa' |
mi'4\trill do'' do'' |
do''4. do''8 re'' mib'' |
fa''4 re'' re'' |
re''2 sol'4 |
do'' do'' la' |
sib'4. do''8 re'' sib' |
do''2 do''4 |
do''8 re'' do'' sib' la' do'' |
sib'4 re'' re'' |
do''2 do'4 |
fa' do'' sib' |
la' fa' sol' |
sol' do'' do'' |
do''4. do''8 re'' mib'' |
fa''4 re'' sib' |
do'' do'' do'' |
re'' do''4. do''8 |
do''8\fortSug sib' la' sib' do'' la' |
sib' do'' sib' la' sol' fa' |
mi'\trill fa' mi' fa' sol' mi' |
fa'4 fa' fa' |
re'8 mi' do'4 fa' |
mi'8 la' sol'4. sol'8 |
sol'4 la'4. la'8 |
la'4 re''8 do'' do'' re'' |
do''4 sib'4. sib'8 |
la'8 re'' do''4. do''8 |
do''1*3/4\fermata |
