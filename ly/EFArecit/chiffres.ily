s1*2 s2 <"">4...\figExtOn <"">32\figExtOff <"">4...\figExtOn <"">32\figExtOff <_->4...\figExtOn <_->32\figExtOff <"">8..\figExtOn <"">32\figExtOff <6>2.
<6>2 <_-> s2. \new FiguredBass <7->
s2. <7>4 <6>2 <_+>4 <6>8. <_->16 <4>4 <3+> s1*2
s2 <6> <7>4 <6 5> <4> <3> s2 <6> s <7>4 <6> s2
<"">4.\figExtOn <"">8\figExtOff s2 <6> s <6> s1
<"">4.\figExtOn <"">8\figExtOff <"">4.\figExtOn <"">8\figExtOff s1 s2 <7>4 <6+> s2 <5>4 <_+> <"">4.\figExtOn <"">8\figExtOff
s2 s1 s4 <6>8 <6 _-> s2 <5/>4.\figExtOn <5/>8\figExtOff <5>2 s1
s2 <6>4. <6+>8 s1 <7> s2 s8 <6> <6>8*5\figExtOn <6>8\figExtOff s2 <6>4
<"">4.\figExtOn <"">8\figExtOff <6> <6> <"">8*5\figExtOn <"">8\figExtOff s2 <6>4 <"">8*5\figExtOn <"">8\figExtOff <"">8*5\figExtOn <"">8\figExtOff <"">8*5\figExtOn <"">8\figExtOff s2.
<6> s <_+>8*5\figExtOn <_+>8\figExtOff s4 <_+>2 s2. s2 <6>8 <6 _-> s2.
<_+>2. <_->4 <_+>2 <"">8*5\figExtOn <"">8\figExtOff <"">2\figExtOn <"">4\figExtOff <6>8*5\figExtOn <6>8\figExtOff s8*5 <6>8 s2 <6>8 <6>
s2 <6>4 <6> <6>2 <"">8*5\figExtOn <"">8\figExtOff <"">4.\figExtOn <"">8\figExtOff <6> <6 _-> s2.*2 s8 <6 5> <4>4 \new FiguredBass <3>
s2. s2 <6>4 s8 <6> <6>4 <5/> s2 s8 <6> <_+>2 <6>8 <5/> s <6 5> <_+>2
s2. s2 s8 <6 5> s2 <6>4 s8 <6 5>
