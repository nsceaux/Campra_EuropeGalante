\clef "basse" <>^"Prélude "la,2~ la,4.*5/6 sol,16 fa, mi, |
re,1 |
re,2 re4.*13/12 do32 sib, la, |
sib,4.*13/12 la,32 sol, fa, sol,4.*10/12 sol,32 la, sib, do re mi |
fa8.*5/6 fa32 mi re dod4 re2\douxSug |
fad,2 sol, |
do2. |
do, |
fa, |
sib, |
la,4 fa,8. sol,16 la,2 |
re,2. mi,4 |
fa,1 |
fa8\fort fa fa fa mi mi mi mi |
re re sib, sib, do do do do |
fa, fa\douxSug fa fa mi mi mi mi |
re re re re sib, sib, sib, sib, |
do do do re mib fa sol la |
sib sib sib sib la la la fa |
do' do' do' sib la la la sol |
fa mib re do sib, sib sib la |
sol fa mi re do sib, la, sol, |
fa,4. fa,8 mi, mi, mi, mi, |
mib, mib, mib, mib, re, re, re, re, |
do, do' do' do' si sol sol sol |
do' sib la sol fa mib re do |
sib, la, sol, fa, do, do do do |
fa mib re do sib, sib, sib, sib, |
si, si, si, si, do do do do |
fa, fa fa mib re re'\fortSug re' do' |
sib la sol fa mi mi mi re |
do sib, la, sol, fa, mib, re, do, |
sib,,2 do, |
fa, fa8^\douxSug sol |
la sib la sol la fa |
sib la sib do' re' sib |
do' re' do' sib la sol |
fa sol fa mib re do |
sib, la, sib, do re sib, |
do sib, do re mi do |
fa mi fa sol la sib |
do' re' do' sib la sol |
fa2. |
mi |
re8 do re mi fa re |
sol fad sol la si sol |
do' fa sol4 sol, |
do8 sib, do re mi do |
fa sol fa mib re do |
sib, do sib, la, sib, sol, |
re4 re' mib' |
do' re' re |
sol8 fa sol la sib sol |
do'4 do' sib |
la8 sol fa sol la fa |
sib la sib do' re' sib |
do' re' do' sib la sol |
fa4 fa, sol, |
la, sib, sol, |
do8 sib, do re mi do |
fa sol fa mib re do |
sib,4 sib sol |
do'8 sib la sol fa mib |
re sib, do4 do, |
fa8\fort mi fa sol la fa |
sib la sib do' re' sib |
do' la sol fa mi do |
fa mi fa sol la fa |
sol mi fa sol la si |
do' fa sol fa sol sol, |
do sib, la, sib, do la, |
re do re mi fa sib, |
do la, sib, do re mi |
fa sib, do4 do, |
fa,1*3/4\fermata |
