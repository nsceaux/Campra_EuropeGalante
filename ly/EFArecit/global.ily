\key la \minor
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 s2.*4
\time 4/4 s1*3
\digitTime\time 2/2 \key re \minor \tempo "Vite" \midiTempo#160 s1*20
\digitTime\time 3/4 s2.*42 \bar "|."
