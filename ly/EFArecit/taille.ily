\clef "taille" R1*2 |
re'2. fa4 |
fa2 sib |
la4. la8 la2\douxSug |
la2 sib |
sol2.~ |
sol |
la2. |
re' |
mi'4 la8. sol16 sol4 mi |
fa2 r |
R1 |
r4 do'8\fortSug do' mi'4 mi'8 mi' |
la4 sib8 sib sol4 do' |
la2 r4 sol\douxSug |
re'2 r4 sol |
sol2 r4 sib |
sib2 r4 do' |
do'2 r4 la |
la2 r4 sib |
sib2 r4 la |
la2 r4 sol |
sol do' la fa |
sol2 r4 re' |
mi'2 r4 do' |
re'2 r4 do' |
do'2 r4 fa |
sol4. sol8 sol4.\trill( fa16 sol) |
la2 r8 fa'\fortSug fa' fa' |
fa'4 sol' do' do'8 re' |
mi' mi' fa' sol' do'4 fa'8 mib' |
re'4 re' do'4. do'8 |
do'2 la'8\douxSug sol' |
fa'4 fa' fa' |
fa' fa' re' |
mi'4. re'8 do'4~ |
do' fa' fa' |
fa' la fa |
do'2 do'4 |
do'2.~ |
do'2 do'4 |
do'8 sib la sol fa4 |
do'2 sol4 |
re' re' re' |
re'2 sol'4 |
mi'8 do' sol'4. sol'8 |
sol' fa' mi' re' do' sol' |
fa'4. fa'8 fa' mib' |
re'4 sol' sol' |
la'2 sol'4 |
sol' re'4. re'8 |
re'4 sol' sol' |
sol' do'2~ |
do'4 fa'4. fa'8 |
fa'4 fa' re' |
sol'4. do'8 re' mi' |
fa'4 la' sol' |
fa'4. re'8 re' re' |
mi'2 sol'4 |
la'8 sib' la' sol' fa' mib' |
re'4 fa' sol' |
sol' fa' fa'~ |
fa'8 sol' sol'4.(\trill fa'16 sol') |
la'8\fortSug sol' fa'4 fa' |
fa' re' re' |
mi'8 do' do'4 do' |
do' do' re' |
re'8 sol' fa'4. re'8 |
sol' do' si4. do'8 |
do'8 mi' mi'4. mi'8 |
fa'4 fa'4. fa'8 |
mi'8 la' fa' mi' re' do' |
do' fa' mi'4.\trill fa'8 |
fa'1*3/4\fermata |
