\clef "vbas-dessus" <>^\markup\character La Discorde
R1*4 |
r4 r8 la'16 la' fa''8. fa''16 re''8\trill re''16 re'' |
la'8 la'16 r re''8 re''16 do'' sib'8.\trill sib'16 sib' la' sol' fa' |
mi'4\trill r8 sol' sol'16 sol' sol' la' |
sib'4 sib'8 sib'16 sib' sib'8. la'16 |
la'4\trill la'8 r r fa'' |
fa''4 r8 re''16 re'' sol''8 sol''16 sol'' |
dod''8\trill dod'' r la'16 sib' mi'4\trill mi'8. fa'16 |
re'2 r |
R1*3 |
r4 la'8 fa' do''4 do''8 do'' |
fa''4 la'8 la' re''4 sib'8 re'' |
sol'4\trill sol'8 sol' sol' la' sib' do'' |
re''4 re''8 mi'' fa''4 fa''8. sol''16 |
mi''4\trill mi'' r8 do'' do'' sib' |
la' sol' fa' mi' re' re'' re'' do'' |
sib' la' sol' fa' mi'4.\trill mi'8 |
fa' sol' la' sib' do''2~ |
do'' re''4 re''8 re'' |
sol'2 r8 sol'' sol'' fa'' |
mi'' re'' do'' sib' la' fa'' fa'' mib'' |
re'' do'' sib' la' sol'4.\trill do''8 |
la'\trill la' sib' do'' fa'2~ |
fa' mi'4\trill mi'8 fa' |
fa'2 r |
R1*3 |
r4 r \ffclef "vbas-dessus" <>^\markup\character Venus la'8 sib' |
do''4 do'' la' |
re'' re'' sib' |
sol'\trill sol' do'' |
la'4.\trill sol'8( fa'4) |
fa''4 re'' \appoggiatura do''8 sib'4 |
sol'2.\trill |
la'4 sib'16[ la'8.] sol'16[ fa'8.] |
mi'2\trill r8 mi' |
fa'[\melisma mi' fa' sol' la' sib'] |
do''[ sib' do'' re'' mi'' do'']( |
fa''4.)\melismaEnd mi''8 re'' do'' |
si'4\trill re'' re'' |
mi'' re''4.\trill do''8 |
do''2 r8 do'' |
la'4.\trill la'8 sib' do'' |
re''4 re'' sol'' |
fad''8.\trill mi''16( re''8.) do''16( sib'4) |
sib'( la'2)\trill |
sol' r8 sol' |
sol'[\melisma fa' sol' la' sib' sol'] |
do''[ sib' la' sib' do'' la'] |
re''[ do'' re'' mi'' fa'' sol'']( |
mi''4.)\trill\melismaEnd re''8 do'' sib' |
la'4\trill la' sib' |
do'' sib'4.\trill la'8 |
sol'2\trill r8 do'' |
la'4.\trill la'8 sib' do'' |
re''4 re'' sol'' |
mi''\trill do'' fa'' |
fa''2( mi''4) |
fa''4 r r |
R2.*9 |
R2.^\fermataMarkup |
