\clef "quinte" R1*2 |
la4.*13/12 sib32 la sol fa4. fa8 |
re2. sib4 |
la sol8 mi fa2 |
re2. sol4 |
sol2.~ |
sol4 mi2 |
do2 fa4 |
re2. |
la4 re'8 re la4. la8 |
la2 r |
R1 |
r4 la8\fortSug la sol4 do'8 do' |
re'4 sol8 sol sol4 sol |
fa2 r4 do'\douxSug |
fa2 r4 re |
mi2 r4 sib |
sib2 r4 la |
do'2 r4 fa |
fa2 r4 sib |
sib2 r4 do' |
do2. mi4 |
sol do fa re |
mi2 r4 re' |
do'2 r4 fa |
sib2 r4 sol |
la fa2 re4 |
re sol2 do4 |
do2 r8 re'\fortSug re' la |
re'2 sol'4. si8 |
do'4. sol8 la4. la8 |
sib4. sib8 sib4. do'8 |
la2\trill do'8\douxSug sol |
do'4 do' do' |
re' re' re' |
sol'4 do'4. do'8 |
do'2. |
re'4 re' re' |
do'2 do'8 sib |
la sol la sib do'4 |
do'2.~ |
do'4 do'8 sib la4 |
sol2 sol4 |
fa2. |
sol2 re'4 |
do' sol4. sol8 |
sol2 do'4 |
do'4. do'8 sib la |
re'4 re' re' |
re'2 sol'4 |
mib' re'4. do'8 |
sib la sol4 re' |
mi'2\trill mi'4 |
fa'8 sol' la' sol' fa' mi' |
re'2 re'4 |
mi' do' do' |
do'2 sol4 |
do' fa sib |
do'2 do'4 |
do'2 fa4 |
sib sib re' |
do'2 do'4 |
fa do'8 re' do' sib |
la4\fortSug do' do' |
sib re' re' |
do' sib8 la sol4 |
la2 la4 |
sol do'4. sol8 |
do'4 sol4. sol8 |
sol4 do do' |
la la8 sol fa4 |
do'8 fa' re' do' sib sol |
do' sib sib4. do'8 |
la1*3/4_\trill\fermata |
