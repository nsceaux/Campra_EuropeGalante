\clef "dessus" <>^\markup\whiteout Violons
\setMusic#'coupletI {
  la'4 la' |
  sold'\trill sold' la' |
  fad' sold'4. la'8 |
  mi' re' mi' fad' sold' mi' |
  la'4 la' la' |
  sold'\trill sold' la' |
  fad' sold'4. la'8 |
  mi' re' mi' fad' sold' mi' |
  la'4 la' la' |
  sold' sol'4. sol'8 |
  fad'4 fad' sold'! |
  la' mi'4. mi'8 |
  la'4 la' la' |
  sold' sol'4. sol'8 |
  fad'4 fad' sold'! |
  la'4 mi'4. mi'8 |
  la'4
}
<>\doux \keepWithTag#'() \coupletI
<>\fort \coupletI
\setMusic#'coupletII {
  la'4 la' |
  re''4. re''8 re''4 |
  dod'' re'' si'\trill |
  la'2 la'4 |
  la' la'8 si' dod'' la' |
  si'4 dod'' red'' |
  mi'' si'4. mi''8 |
  mi''4 mi' mi' |
  fad' fad' sold' |
  la' sold'\trill la' |
  mi'2 mi'4 |
  la' la'8 sold' la' si' |
  dod''4 sold'4.\trill sold'8 |
  la'4 mi'4. la'8 |
  la'4 mi' mi' |
}
<>\doux \keepWithTag#'() \coupletII la'4
<>\fort \coupletII la'2.
