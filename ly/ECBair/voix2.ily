\clef "vdessus" r4 r |
R2.*15 |
r4 la'4 la' |
sold'\trill sold' la' |
fad' sold'4. la'8 |
mi'2 mi'4 |
r la' la' |
sold'\trill sold' la' |
fad' sold'4. la'8 |
mi'2 mi'4 |
r la' la' |
sold' sol'4. sol'8 |
fad'4 fad' sold'! |
la' mi'4. mi'8 |
la'4 la' la' |
sold' sol'4. sol'8 |
fad'4 fad' sold'! |
la' mi'4. la'8 |
la'4 r4 r |
R2.*14 |
r4 la'4 la' |
re''4. re''8 re''4 |
dod'' re'' si' |
la'2 la'4 |
la' la'8 si' dod'' la' |
si'4 dod'' red'' |
mi'' si'4. mi''8 |
mi''4 mi' mi' |
fad' fad' sold' |
la' sold'\trill la' |
mi'2 mi'4 |
la' la'8 sold' la' si' |
dod''4 sold'4.\trill sold'8 |
la'4 mi'4. la'8 |
la'4 mi' mi' |
la'2. |
