\setMusic#'coupletI {
  dod''4 la' |
  mi'' si' dod'' |
  la' re''4. dod''8 |
  si'2\trill si'4 |
  r dod'' la' |
  mi'' si' dod'' |
  la' re''4. dod''8 |
  si'2\trill si'4 |
  r dod'' re'' |
  mi''8.[ si'16] si'4.\trill dod''8 |
  re''4. la'8 si'4 |
  dod'' si'4.\trill la'8 |
  la'4 dod'' re'' |
  mi''8.[ si'16] si'4.\trill dod''8 |
  re''4. la'8 si'4 |
  dod''4 si'4.\trill la'8 |
  la'4
}
\clef "vbas-dessus" <>^\markup\character-text Zayde \line {
  alternativement avec les autres Sultanes
}
\keepWithTag #'() \coupletI
\clef "vdessus" <>^\markup\character Les Sultanes
\coupletI
\setMusic#'coupletII {
  mi''4 dod'' |
  fad''4. fad''8 fad''4 |
  mi'' fad'' re'' |
  mi''4. re''8( dod''4) |
  dod'' dod''8 re'' mi'' fad'' |
  red''4\trill mi'' fad'' |
  sold'' fad''4.\trill mi''8 |
  mi''4 mi'' dod'' |
  re'' re'' si' |
  dod'' si'\trill la' |
  si'2 sold'4\trill |
  la' la'8 si' dod'' re'' |
  mi''4 si'4. si'8 |
  dod''4 si'4.\trill la'8 |
  la'4 mi'' dod'' |
}
\clef "vbas-dessus" <>^\markup\character Zayde
\keepWithTag#'() \coupletII la'4
\clef "vdessus" <>^\markup\character Les Sultanes
\coupletII la'2. |
