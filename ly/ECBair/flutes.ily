\clef "dessus" <>^\markup\whiteout Flutes
\setMusic#'coupletI {
  la''4 la'' |
  si'' sold'' mi'' |
  la'' si''4. la''8 |
  sold''\trill fad'' sold'' la'' si'' sold'' |
  la''4 la'' la'' |
  si'' sold'' mi'' |
  la'' si''4. la''8 |
  sold''\trill fad'' sold'' la'' si'' sold'' |
  la''4 mi'' fad'' |
  si'\trill mi''4. mi''8 |
  fad''4 la'' mi''~ |
  mi''8 la'' sold''4.\trill la''8 |
  la''4 mi'' fad'' |
  si'\trill mi''4. mi''8 |
  fad''4 la'' mi''~ |
  mi''8 la'' sold''4.\trill la''8 |
  la''4
}
\keepWithTag#'() \coupletI
\coupletI
la''4 la'' |
la''2 la''4 |
la'' la'' sold''\trill |
la''2 la''4 |
mi'' la''4. la''8 |
fad''4\trill mi'' si' |
mi'' red''4.\trill mi''8 |
mi''4 sold'' sold'' |
sold'' fad'' si'' |
mi'' mi''4. la''8 |
sold''\trill la'' sold'' fad'' mi'' re'' |
dod''4\trill la''4. la''8 |
sold''8\trill la'' si''4. si''8 |
la''4 sold''4.\trill la''8 |
la''4 sold'' sold'' |
la''4
la''4 la'' |
la''2 la''4 |
la'' la'' sold''\trill |
la''2 la''4 |
mi'' la''4. la''8 |
fad''4\trill mi'' si' |
mi'' red''4.\trill mi''8 |
mi''4 sold'' sold'' |
sold'' fad'' si'' |
mi'' mi''4. la''8 |
sold''\trill la'' sold'' fad'' mi'' re'' |
dod''4\trill mi'' la'' |
sold''8\trill la'' si''4. si''8 |
la''4 sold''4.\trill la''8 |
la''4 sold'' sold'' |
la''2.
