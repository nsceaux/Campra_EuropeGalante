\tag #'vdessus1 {
  Que l’A -- mour dans nos cœurs, fas -- se nais -- tre
  mille ar -- deurs pour nôtre au -- gus -- te Maî -- tre ;
  Que nos ten -- dres soû -- pirs
  pré -- vien -- nent ses de -- sirs.
  Que nos ten -- dres soû -- pirs
  pré -- vien -- nent ses de -- sirs.
}
Que l’A -- mour dans nos cœurs, fas -- se nais -- tre
mille ar -- deurs pour nôtre au -- gus -- te Maî -- tre ;
Que nos ten -- dres soû -- pirs
pré -- vien -- nent ses de -- sirs.
Que nos ten -- dres soû -- pirs
pré -- vien -- nent ses de -- sirs.
\tag #'vdessus1 {
  Dans ces lieux, tout doit le sa -- tis -- fai -- re ;
  Pour ce char -- mant Vain -- queur, lais -- sons- nous en -- flâ -- mer ;
  At -- ten -- dons le bon -- heur de luy plai -- re,
  en joü -- is -- sant toû -- jours du plai -- sir de l’ai -- mer.
  At -- ten -  - mer.
}
Dans ces lieux, tout doit le sa -- tis -- fai -- re ;
Pour ce char -- mant Vain -- queur, lais -- sons- nous en -- flâ -- mer ;
At -- ten -- dons le bon -- heur de luy plai -- re,
en joü -- is -- sant toû -- jours du plai -- sir de l’ai -- mer.
At -- ten -  - mer.
