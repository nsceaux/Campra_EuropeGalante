\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "flutes" >>
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> {
      \set fontSize = #-2
      \keepWithTag #'vdessus1 \includeLyrics "paroles"
    }
    \new Staff << \global \includeNotes "violons" >>
  >>
  \layout { }
}
