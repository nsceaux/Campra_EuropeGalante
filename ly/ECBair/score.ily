\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "flutes" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix2"
    >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "violons"
      \origLayout {
        s2 s2.*7\break s2.*8 s4 \bar "" \pageBreak
        s2 s2.*7\break s2.*8 s4 \bar "" \break s2 s2.*7\pageBreak
        s2.*7 s4 \bar "" \break s2 s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
