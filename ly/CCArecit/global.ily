\key re \minor
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 2/2 \midiTempo#160 s2
\beginMark "Air" s2 \bar ".!:" s1*4 \alternatives s1 s1 s1*8
\beginMark "Air" \time 2/2 \bar ".!:" s1*7 \alternatives s1 s1 s1*2
\time 3/2 \grace s8 s1.
\time 2/2 s1*4
\time 3/2 s1.
\time 2/2 s1*2 \bar "|."
%% Ensemble
\time 3/2 \midiTempo#320 s1.*41 \bar "|."
%% Récit
\time 4/4 \midiTempo#80 s1*5 \bar "|."
