\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff \haraKiri } \withLyrics <<
      \global \keepWithTag #'dessus \includeNotes "voix"
    >> { \set fontSize = #-2 \keepWithTag #'dessus \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
      { s1*2 s2.*2 s1*2 s2.*2 s1*17\break
        \noHaraKiri s1*11 s1. s1*4 s1. s1*2\break }
    >>
  >>
  \layout { ragged-last = ##t }
}
