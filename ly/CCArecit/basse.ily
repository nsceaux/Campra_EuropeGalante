\clef "basse" sib,4 sib2 la4 |
sib2 sib,8. la,16 sol,4 |
fa,4 fa mib8 re |
do2. |
re2 sib,8 sol, re,4 |
sol,1 |
do2. |
la,4 sol,2 |
fa,4 fa re2 |
mib re4 mib8 fa |
sib,2 sib4 la8 sib |
mib fa sol mib fa4 fa, |
sib,2 sib |
re mib4 mib8 re |
do re do sib, la,4 sib, |
fa,4 fa sib sib8 la |
fa,4 do8 re mib4 do |
sol8 fa mib re do sib, la, sol, |
re,4 re8 do sib, sib, do re |
mib4 do re re, |
sol,4 sol la2 |
sib8 la sol fa mib re do sib, |
fa,4 fa mib4. fa8 |
re4 sib, fa fa, |
sib,1 | \allowPageTurn
sib,4 sib sib fa |
sol4. sol8 fa4.\trill mib8 |
re4\trill re re re |
sol4. fa8 mib fa sol mib |
sib2 sib4 sol |
mi2.\trill do'4 |
la sib sol4.\trill fa8 |
fa2 fa8 mib re do |
fa1 |
do4. do8 do4 re |
mib2 mib4 fa |
sol2 sol4 sol sol do' |
si do' sol sol, |
do1 |
do'4. do'8 do'4 sib |
la2\trill la4 sib |
fa2 fa4. mib8 mib4 re8 mib |
re4 mib fa fa, |
sib,1 |
sib2 sol re |
mib1. |
re2 do\trill sib, |
fa fa,1 |
sib, sib2 |
sib sol\trill fa |
do' do1 |
fa fa4 re |
mib2 mib2. do4 |
re1 re2 |
mi1 mi2 |
fad1 fad2 |
sol re1 |
sol, sol4 fa |
mi1 mi2 |
fa1 fa4 mib |
re2\trill re2. re4 |
mib1 mib2 |
fa1 fa2 |
sol sol sol |
mib fa fa, |
sib,1. |
fa2 fa fa |
do1. |
lab2 lab fa |
sol sol sol |
la1 la2 |
si si2. si4 |
do'2 sol sol, |
do1 do'4 sib |
la2 la2. sib4 |
sol2 sol1\trill |
fa2. re4 re re |
mib1 do2 |
re re re |
mib fa fa, |
sib,1 sib2 |
sol1 sol2 |
fa1 fa2 |
mib fa fa, |
sib,1. |
sib,1 |
mib4. do8 fa4. sib,8 |
fa,2 re, |
mib,4 mib re8 mib fa fa, |
sib,1 |
