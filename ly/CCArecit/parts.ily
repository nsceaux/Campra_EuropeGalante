\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#101 #}))
