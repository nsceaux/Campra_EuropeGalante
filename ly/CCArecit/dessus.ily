\clef "dessus" R1*2 R2.*2 R1*2 R2.*2 R1*17 |
r4 <>^"Violons" re''\doux re'' fa'' |
sib'4. do''8 re''4.\trill mib''8 |
fa''4 fa'' re'' fa'' |
sib'2 mib''4 mib'' |
re''4.\trill mib''8 re'' do'' sib' la' |
sol'2.\trill sol'4 |
do'' re'' sib'4.(\trill la'16) sib' |
la'1\trill |
la'4.\trill do''8 do''4 re'' |
mib''2 mib''4 fa'' |
sol''2 sol''4 do'' |
si'2\trill si'4 re'' re'' mib'' |
fa'' mib'' re''4.\trill re''8 |
mib''4. sol''8 sol''4 fa'' |
mib''4. mib''8 mib''4 re'' |
do''2\trill do''4 sib' |
la'2\trill la'2. la'4 |
sib'2 la'4.\trill sib'8 |
sib'1 |
R1.*41 |
R1*5 |
