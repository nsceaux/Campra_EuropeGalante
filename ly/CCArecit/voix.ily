<<
  \tag #'voix0 {
    \clef "vhaute-contre"
    R1*2 R2.*2 R1*2 R2.*2 R1*28 R1. R1*4 R1. R1*2
  }
  \tag #'(voix1 basse dessus) {
    \clef "vhaute-contre" <>^\markup\character Dom Pedro
    r8 sib16 sib sib8 sib16 sib mib'8 mib'16 mib' mib'8 mib'16 re' |
    re'8\trill re' r re' re' re'16 fa' sib8 sib16 la |
    la4\trill r16 do' do' do' do'8 re' |
    \appoggiatura re' mib' mib' mib' re' do' sib |
    la4\trill r8 re' sol' sol'16 sol' sol'8\trill sol'16 fad' |
    sol'8. sol'16
    \ffclef "vbasse" <>^\markup\character Dom Carlos
    r8 re' sib sib16 re' sol8 sol16 sol |
    mi4\trill r8 sol16 sol do'8 do'16 mi |
    fa8 fa fa\trill fa fa mi |
    fa8 fa r fa16 fa sib8. re'16 sib8\trill sib16 sib |
    sol8\trill sol16 mib mib fa sol la sib8 sib mib fa |
    sib,2
    \ffclef "vhaute-contre" <>^\markup\character Dom Pedro
    fa'4 mib'8 re' |
    mib'4 mib'8 re' do'4\trill do'8[ sib16] do' |
    re'8. do'16( sib4) r re'8 mib' |
    fa'4 fa'8 re' sol'4 sol'8 fa' |
    mib'2\trill mib'4 re' |
    do'2\trill fa'4 mib'8 re' |
    do'4\trill r8 do' sol sol sol la |
    sib4. sib8 do' re' do' sib |
    la4\trill la r8 re' mi' fad' |
    sol'2 sol'4 sol'8 fad' |
    sol'4 r8 sib' la'\trill sol' fa' mib' |
    re'4.\trill re'8 mib' fa' la\trill sib |
    \appoggiatura sib8 do'4 do' r8 do' do' re' |
    \appoggiatura do' sib2 sib4 sib8 la |
    \appoggiatura la8 sib1 |
    \ffclef "vbasse" <>^\markup\character Dom Carlos
    r4 sib sib fa |
    sol4. sol8 fa4.\trill mib8 |
    re4\trill re re re |
    sol4.\melisma fa8[ mib fa sol mib] |
    sib2\melismaEnd sib4 sol |
    mi2.\trill do'4 |
    la sib sol4.\trill fa8 |
    fa1 |
    fa |
    do4. do8 do4 re |
    mib2 mib4 fa |
    \appoggiatura fa8 sol2 sol4 sol sol do' |
    si do' sol4. sol8 |
    do1 |
    do'4. do'8 do'4 sib |
    la2\trill la4 sib |
    fa2 fa4. mib8 mib4( re8) mib |
    re4\trill mib fa4. fa8 |
    sib,1 |
  }
>>
<<
  \tag #'(voix0 basse) {
    <>^\markup\character Dom Pedro
    re'2 mib' fa' |
    la1.\trill |
    sib2 do' re' |
    re'( do'1)\trill |
    sib1 re'2 |
    re' mi' fa' |
    fa' mi'2.\trill fa'4 |
    fa'1 do'4 re' |
    sib2 sib2. do'4 |
    la2.\trill re'4 re' fad |
    sol1 sol2 |
    la la la |
    sib sib( la)\trill |
    sol1. |
    do'2 do' sib |
    la1.\trill |
    sib2 lab( sol4) lab |
    sol2\trill sol mib' |
    do'1\trill fa'2 |
    re'\trill re'2. re'4 |
    mib'2 do'2.\trill fa'4 |
    re'1\trill re'4 re' |
    do'2\trill do'2. re'4 |
    mib'1 mib'4 mib' |
    mib'2 re'2. re'4 |
    si2. sol'4 sol' si |
    do'1 do'2 |
    re' re' re' |
    mib' mib'( re')\trill |
    do'1 mib'4 re' |
    do'2\trill do'2. re'4 |
    \appoggiatura do'16 sib2 sib( la4) sib |
    la2.\trill fa'4 fa' fa' |
    fa'2.( mib'4) mib'2 |
    mib'4.( re'8) re'2 re'~ |
    re'4 do' re'2( do')\trill |
    sib2. fa'4 fa' fa' |
    fa'2.( mib'4) mib'2 |
    mib'4.( re'8) re'2 re'\trill~ |
    re'4 do' re'2( do')\trill |
    sib1. |
  }
  \tag #'voix1 {
    <>^\markup\character Dom Carlos
    sib2 sol re |
    mib1. |
    re2 do\trill sib, |
    fa1. |
    sib,1 sib2 |
    sib sol\trill fa |
    do' do2. fa4 |
    fa1 fa4 re |
    mib2 mib2. do4 |
    re2. re4 re re |
    mi1 mi2 |
    fad2 fad fad |
    sol re1 |
    sol, sol4 fa |
    mi2\trill mi2. mi4 |
    fa1 fa4 mib |
    re2\trill re2. re4 |
    mib2. mib4 mib mib |
    fa1 fa2 |
    sol sol sol |
    mib fa1 |
    sib,1. |
    fa2 fa fa |
    do1. |
    lab2 lab fa |
    sol sol sol |
    la1 la2 |
    si si2. si4 |
    do'2 sol2. sol4 |
    do1 do'4 sib |
    la2 la2. sib4 |
    sol2 sol2.\trill fa4 |
    fa2. re4 re re |
    mib1 do2 |
    re re re |
    mib fa1 |
    sib,2. sib4 sib sib |
    \appoggiatura la8 sol1 sol2 |
    fa fa fa |
    mib fa1 |
    sib,1. |
  }
>>
<<
  \tag #'voix0 { R1*5 }
  \tag #'(voix1 basse) {
    <>^\markup\character-text Dom Carlos aux Musiciens
    fa4 r8 fa re4\trill r8 re16 re |
    sol4 sol8 do' la4\trill la8 sib |
    do'4 r8 la16 la re'4 r8 sib16 re' |
    sol4\trill sol8 la sib4 sib8. la16 |
    sib1 |
  }
>>
