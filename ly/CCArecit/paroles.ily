\tag #'(voix1 basse dessus) {
  Mo -- de -- rez le trans -- port que vous fai -- tes pa -- roî -- tre,
  il faut s’ex -- pli -- quer au -- tre -- ment ;
  N’u -- sur -- pez point le nom de plus fi -- delle a -- mant :
  c’est moy qui me pi -- que de l’ê -- tre.
  
  En vain l’a -- vez- vous pré -- ten -- du,
  on ne peut é -- ga -- ler mes feux & ma cons -- tan -- ce,
  ban -- nis -- sez l’in -- juste es -- pe -- ran -- ce
  de me ra -- vir un ti -- tre qui m’est dû.
  
  Puis -- que Lu -- cile est l’ob -- jet de ma flâ -- me,
  peut- il ê -- tre des feux plus ar -- dents que les miens ?
  Puis -- que Lu -  miens ?
  L’A -- mour par d’au -- tres yeux, peut- il bles -- ser une a -- me,
  si vi -- ve -- ment que par les siens ?
  L’A -- mour par d’au -- tres yeux, peut- il bles -- ser une a -- me,
  si vi -- ve -- ment que par les siens ?
  
  Lu -- cile est di -- gne qu’on l’a -- do -- re,
  elle en -- chaî -- ne les cœurs des plus ai -- ma -- bles nœuds :
  nœuds :
  Si je n’a -- vois veu Le -- o -- no -- re,
  nous brû -- le -- rions des mê -- mes feux.
  Si je n’a -- vois veu Le -- o -- no -- re,
  nous brû -- le -- rions des mê -- mes feux.
}
Que nôtre ar -- deur soit é -- ter -- nel -- le,
l’A -- mour nous pro -- met mille at -- traits ;
Dis -- pu -- tons à ja -- mais,
à qui se -- ra plus tendre & plus fi -- del -- le.
\tag #'(voix0 basse) {
  Que nôtre ar -- deur soit é -- ter -- nel -- le,
  l’A -- mour, l’A -- mour nous pro -- met mille at -- traits ;
  Dis -- pu -- tons à ja -- mais,
  dis -- pu -- tons à ja -- mais,
  à qui se -- ra plus tendre & plus fi -- del -- le.
}
\tag #'voix1 {
  Dis -- pu -- tons à ja -- mais,
  dis -- pu -- tons à ja -- mais,
  à qui se -- ra plus tendre & plus fi -- del -- le.
  Que nôtre ar -- deur soit é -- ter -- nel -- le,
  l’A -- mour, l’A -- mour nous pro -- met mille at -- traits ;
}
Dis -- pu -- tons à ja -- mais, à ja -- mais,
à qui se -- ra plus tendre & plus fi -- del -- le,
à qui se -- ra plus tendre & plus fi -- del -- le.
\tag #'(voix1 basse) {
  Vous, chan -- tez, ce -- le -- brez de si bel -- les ar -- deurs ;
  Que vos voix, que vos chants at -- ten -- dris -- sent les cœurs.
}
