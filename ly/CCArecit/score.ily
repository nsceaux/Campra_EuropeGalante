\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'voix0 \includeNotes "voix"
    >> \keepWithTag #'voix0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s2.*2 s1\break s1 s2.*2\break
        s1*2 s2 \bar "" \break s2 s1*4\pageBreak
        s1*5\break s1*5\break s1*6\break
        s1*5 s1.\break s1*4 s1. s1*2\pageBreak
        s1.*8\break s1.*8\break s1.*7\break s1.*7\pageBreak
        s1.*7\break \grace s8 s1.*4\break
        s1*3 s2 \bar "" \break
      }
      \modVersion {
        s1*2 s2.*2 s1*2 s2.*2 s1*17\break
        s1*11 s1. s1*4 s1. s1*2\break
        s1.*41\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
