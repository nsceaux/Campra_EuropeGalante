\clef "haute-contre" r4 r8 |
R2.*3 |
r4 r8 la'8. la'16 la'8 |
sold'8.\trill sold'16 sold'8 la'8. la'16 la'8 |
sold'8. sold'16 sold'8 la'8. la'16 la'8 |
la'8. la'16 si'8 la'8. sold'16 la'8 |
sold'4\trill la'8
%%
mi''4 mi''8 |
mi''4. red''8.\trill dod''16 red''8 |
mi''4. sold'8. la'16 si'8 |
la'4 la'8 sold'8. red'16 mi'8 |
red'4 mi'8 dod''4 dod''8 |
si'4. si'4 si'8 |
si'4. si'8. si'16 si'8 |
la'4 la'8 sold'8. red'16 mi'8 |
red'4\trill mi'8 mi'4 mi'8 |
re'4 re'8 fad'4 fad'8 |
mi'4. la'4 la'8 |
la'4. la'8. sold'16 la'8 |
sold'4\trill la'8
%%
la'8. la'16 la'8 |
sold'8.\trill sold'16 sold'8 la'8. la'16 la'8 |
sold'8. sold'16 sold'8 la'8. la'16 la'8 |
la'8. la'16 si'8 la'8. sold'16 la'8 |
sold'4\trill la'8
