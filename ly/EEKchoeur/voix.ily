<<
  \tag #'vdessus { \clef "vdessus" r4 r8 R2.*3 r4 r8 }
  \tag #'vhaute-contre { \clef "vhaute-contre" r4 r8 R2.*3 r4 r8 }
  \tag #'vtaille { \clef "vtaille" r4 r8 R2.*3 r4 r8 }
  \tag #'(vbasse basse) {
    \clef "vbasse" <>^\markup\character Le Bostangi
    la4 la8 |
    mi4 mi8 la4 la8 |
    mi4 mi8 la8. si16 dod'8 |
    re'8. dod'16 si8 dod'8. si16 la8 |
    mi4 la,8
  }
>>
<<
  \tag #'basse { r4 r8 R2.*3 r4 r8 }
  \tag #'vdessus {
    <>^\markup\character Chœur
    dod''4 dod''8 |
    si'4 si'8 dod''4 dod''8 |
    si'4 si'8 dod''8. re''16 mi''8 |
    fad''8. mi''16 re''8 mi''8. re''16 dod''8 |
    si'4\trill la'8
  }
  \tag #'vhaute-contre {
    la'4 la'8 |
    sold'4 sold'8 la'4 la'8 |
    sold'4 sold'8 la'8. la'16 sol'8 |
    fad'8. fad'16 fad'8 mi'8. mi'16 la'8 |
    sold'4\trill la'8
  }
  \tag #'vtaille {
    mi'4 mi'8 |
    mi'4 mi'8 mi'4 mi'8 |
    mi'4 mi'8 mi'8. mi'16 mi'8 |
    re'8. re'16 re'8 dod'8. re'16 mi'8 |
    mi'4 mi'8
  }
  \tag #'vbasse {
    la4 la8 |
    mi4 mi8 la4 la8 |
    mi4 mi8 la8. si16 dod'8 |
    re'8. dod'16 si8 dod'8. si16 la8 |
    mi4 la,8
  }
>>
<<
  \tag #'(vdessus vhaute-contre vtaille basse) { r4 r8 R2.*3 r4 r8 }
  \tag #'vbasse {
    <>^\markup\italic Toutes les Basses
    la4 la8 |
    si4. si,4 si,8 |
    mi4. mi8. fad16 sold8 |
    la8. sold16 fad8 sold8. fad16 mi8 |
    si4 mi8
  }
>>
<<
  \tag #'basse { r4 r8 R2.*3 r4 r8 }
  \tag #'vdessus {
    <>^\markup\character Chœur
    mi''4 mi''8 |
    mi''4. mi''4 red''8\trill |
    mi''4. mi''8. re''16 mi''8 |
    dod''8.\trill si'16 la'8 si'8. la'16 sold'8 |
    fad'4\trill mi'8
  }
  \tag #'vhaute-contre {
    la'4 la'8 |
    fad'4. fad'4 si'8 |
    sold'4.\trill sold'8. la'16 si'8 |
    mi'8. mi'16 fad'8 mi'8. red'16 mi'8 |
    red'4\trill mi'8
  }
  \tag #'vtaille {
    dod'4 dod'8 |
    si4. si4 si8 |
    si4. si8. si16 si8 |
    la8. la16 la8 sold8. la16 si8 |
    si4 sold8
  }
  \tag #'vbasse {
    la4 la8 |
    si4. si,4 si,8 |
    mi4. mi8. fad16 sold8 |
    la8. sold16 fad8 sold8. fad16 mi8 |
    si4 mi8
  }
>>
<<
  \tag #'(vdessus vhaute-contre vtaille basse) { r4 r8 R2.*3 r4 r8 }
  \tag #'vbasse {
    <>^\markup\character Le Bostangi
    mi4 sold8 |
    si4. fad4 la8 |
    dod'4. dod'8. si16 la8 |
    dod'8. si16 la8 dod'8. si16 la8 |
    mi4 la,8
  }
>>
<<
  \tag #'basse { r4 r8 R2.*3 r4 r8 }
  \tag #'vdessus {
    <>^\markup\character Chœur
    dod''4 dod''8 |
    si'4 si'8 dod''4 dod''8 |
    si'4 si'8 dod''8. re''16 mi''8 |
    fad''8. mi''16 re''8 mi''8. re''16 dod''8 |
    si'4\trill la'8
  }
  \tag #'vhaute-contre {
    la'4 la'8 |
    sold'4 sold'8 la'4 la'8 |
    sold'4 sold'8 la'8. la'16 sol'8 |
    fad'8. fad'16 fad'8 mi'8. mi'16 la'8 |
    sold'4\trill la'8
  }
  \tag #'vtaille {
    mi'4 mi'8 |
    mi'4 mi'8 mi'4 mi'8 |
    mi'4 mi'8 mi'8. mi'16 mi'8 |
    re'8. re'16 re'8 dod'8. re'16 mi'8 |
    mi'4 mi'8
  }
  \tag #'vbasse {
    la4 la8 |
    mi4 mi8 la4 la8 |
    mi4 mi8 la8. si16 dod'8 |
    re'8. dod'16 si8 dod'8. si16 la8 |
    mi4 la,8
  }
>>
