\clef "quinte" r4 r8 |
R2.*3 |
r4 r8 dod'8. si16 la8 |
si8. si16 si8 la8. la16 la8 |
si8. si16 si8 la8. la16 la8 |
re'8. re'16 re'8 dod'8. re'16 mi'8 |
mi'4 dod'8
%%
dod'4 fad'8 |
fad'4. fad'4 fad'8 |
mi'4. si4 si8 |
dod'4 red'8 si4 si8 |
si8. la16 sold8 la4 la'8 |
la'4. fad'4 fad'8 |
mi'4. si4 si8 |
dod'4 dod'8 si4 si8 |
si8. la16 sold8 mi4 si8 |
si4. la4 la8 |
la4. dod' |
dod' la4 la8 |
si4 dod'8
%%
dod'8. si16 la8 |
si8. si16 si8 la8. la16 la8 |
si8. si16 si8 la8. la16 la8 |
re'8. re'16 re'8 dod'8. re'16 mi'8 |
mi'4 dod'8
