\clef "taille" r4 r8 |
R2.*3 |
r4 r8 mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
re'8. mi'16 fad'8 mi'8. si16 dod'8 |
mi'4 mi'8
%%
la'4 dod''8 |
si'4. si'4 si'8 |
si'8. la'16 sold'8 mi'4 mi'8 |
mi'4 fad'8 mi'8. fad'16 si8 |
si4 si8 la4 la'8 |
fad'4. fad'4 fad'8 |
sold'4. mi'4 mi'8 |
mi'4 fad'8 mi'8. fad'16 si8 |
si4 si8 si4 si8 |
si4 re'8 re'4 dod'8 |
dod'4. mi' |
mi' dod'8. re'16 mi'8 |
mi'4 mi'8
%%
mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
mi'8. mi'16 mi'8 mi'8. mi'16 mi'8 |
re'8. mi'16 fad'8 mi'8. si16 dod'8 |
mi'4 mi'8
