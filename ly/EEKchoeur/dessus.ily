\clef "dessus" mi''4\doux la''8 |
sold''4\trill mi''8 mi''4 la''8 |
sold''4\trill mi''8 dod''8. re''16 mi''8 |
fad''8. mi''16 re''8 mi''8. re''16 dod''8 |
si'4\trill la'8
%%
dod''8. dod''16 dod''8 |
si'8. si'16 si'8 dod''8. dod''16 dod''8 |
si'8. si'16 si'8 dod''8. re''16 mi''8 |
fad''8. mi''16 re''8 mi''8. re''16 dod''8 |
si'4\trill la'8
%%
la''4 la''8 |
fad''4.\trill si''8. la''16 si''8 |
sold''8.\trill fad''16 mi''8 mi''8. re''16 mi''8 |
dod''8.\trill si'16 la'8 si'8. la'16 sold'8 |
fad'4\trill mi'8 mi''4 mi''8 |
mi''4. mi''4 red''8 |
mi''4. mi''8. re''16 mi''8 |
dod''8.\trill si'16 la'8 si'8. la'16 sold'8 |
fad'4\trill mi'8 sold'4 si'8 |
re''4 fad'8 la'4 dod''8 |
mi''4. mi'' |
mi'' mi''8. re''16 dod''8 |
si'4\trill la'8
%%
dod''8. dod''16 dod''8 |
si'8. si'16 si'8 dod''8. dod''16 dod''8 |
si'8. si'16 si'8 dod''8. re''16 mi''8 |
fad''8. mi''16 re''8 mi''8. re''16 dod''8 |
si'4\trill la'8
