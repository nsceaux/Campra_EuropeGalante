\clef "dessus" dod''8\doux |
dod''2. r8 la' |
la'2 fad'4.\trill fad'8 |
sold'2. r8 la' |
la'2. r8 la' |
la'2 sold'4.\trill la'8 |
la'2. r8 mi'' |
mi''2. r8 mi'' |
mi''2 red''4. red''8 |
mi''2. r8 dod'' |
dod''2. r8 si' |
si'2 si'4 mi'' |
dod''2\trill dod''4
