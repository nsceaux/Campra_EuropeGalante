\clef "haute-contre" R1.*2 |
si'2. dod''2 dod'4 |
mi'2. sold' |
si'2 si'4 la'2 la'4 |
la'2 la'4 si'2 si'4 |
si'2 sold'4 mi'2. |
mi'2 mi'4 mi'2 red'4 |
mi'2. mi' |
R1.*2 |
sold'2. la' |
la'2 sold'4 mi'2 mi'4 |
re'2. fad'4. fad'8 sold'4 |
la'4. mid'8 fad'4 fad'2 mid'4 |
fad'2. dod' |
re'2 la'4 la'2 mi'4 |
mi'2 la'4 la'2 dod''4 |
dod''2 si'4 mi'2 mi'4 |
mi'2 la'4 la'2 sold'4 |
la'2. mi' |
