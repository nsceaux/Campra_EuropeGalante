\clef "quinte" R1.*2 |
sold2. la |
si2 si4 mi'2 mi'4 |
re'2 re'4 re'2. |
mi'2 fad'4 fad'2 si4 |
si2. mi'2 mi4 |
mi2. si2 fad4 |
mi2. mi |
R1.*2 |
si2. dod' |
dod'2 si4 la2 la4 |
la2 re'4 re'4. la8 si4 |
dod'2 dod'4 dod'2 dod'4 |
dod'2. fad |
re'2 la4 la2. |
la la2 mi4 |
mi2 si4 si2 mi4 |
mi'4. re'8 dod'4 mi'2 mi'4 |
mi'2. mi |
