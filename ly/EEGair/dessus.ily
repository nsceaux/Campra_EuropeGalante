\clef "dessus" la'2. mi'' |
dod''4.\trill si'8 la'4 mi''2 la''4 |
sold''2\trill mi''4 mi''2 mi'4 |
mi'2. si' |
re'' fad''4. mi''8 re''4 |
dod''2 fad''4 red''2 si'4 |
mi''2 si'4 sold'2\trill mi'4 |
mi'4. fad'8 sold'4 fad'2 si'4 |
sold'2.\trill mi' |
si' mi' |
fad'4. sold'8 la'4 sold'2\trill mi'4 |
si'2 mi''4 dod''2\trill la'4 |
mi''2. la'4. si'8 dod''4 |
fad'2. si'4. dod''8 re''4 |
dod''4. si'8 la'4 sold'2 dod''4 |
lad'2.\trill fad' |
fad'' la'' |
la'2 dod''4 dod''2 mi''4 |
mi''2. mi' |
la'4. si'8 dod''4 si'2 mi''4 |
dod''2.\trill la' |
