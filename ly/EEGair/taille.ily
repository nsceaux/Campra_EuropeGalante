\clef "taille" R1.*2 |
mi'2. mi' |
si2 mi'4 mi'2. |
fad'2. fad'2 fad'4 |
mi'2 la'4 fad'2 sold'4 |
sold2. si2 dod'4 |
dod'2 dod'4 si2 si4 |
si2. sold |
R1.*2 |
mi'2. mi' |
mi'2 mi'4 mi'4. re'8 dod'4 |
re'2. re'2 re'4 |
mi'4. sold'8 dod'4 dod'2 dod'4 |
dod'2. la |
re' re'2 dod'4 |
dod'2 mi'4 mi'2 la'4 |
la'2 sold'4 sold'2 mi'4 |
mi'2 mi'4 mi'2 mi'4 |
mi'2. dod' |
