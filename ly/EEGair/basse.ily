\clef "basse" R1.*2 |
mi2. la |
sold4.\trill fad8 mi4 mi'2 mi'4 |
si2. re' |
la4. sold8 fad4 si2 sold4 |
mi2. mi2 dod4 |
la,2. si, |
mi mi, |
R1.*2 |
mi2. la, |
dod4. re8 mi4 dod2 la,4 |
re2 re'4 re'4. dod'8 si4 |
la4. sold8 fad4 dod2 dod4 |
fad,2. fad |
re2 fad4 fad2 la4 |
la2. la, |
dod2 mi4 mi2 mi'4 |
dod'4. si8 la4 mi2 mi4 |
la2. la, |
