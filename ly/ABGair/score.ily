\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*8\break s2.*8\break s2.*8\break s2.*8\break
      }
    >>
  >>
  \layout { system-count = 6 }
  \midi { }
}
