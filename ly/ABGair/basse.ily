\clef "basse" r4 |
R2. |
r4 r re' |
do' sib2 |
la4. sol8 fa4 |
sol la la, |
re2 fa4 |
mi re2 |
la4. sol8 fa4 |
mi fa do |
fa,4. sol,8 la,4 |
sib, do do, |
fa,2 fa4 |
fa mi2 |
re sol4 |
do' fa2 |
sol4. la8 si4 |
do' fa sol |
do4. re8 mi4 |
fa sol sol, |
do2 do'4 |
sold la2 |
mi4. fad8 sold4 |
la la,2 |
mi4. re8 do4 |
si, la, sold, |
la,4. si,8 do4 |
re mi mi, |
la,2 re'4 |
do' sib2 |
la4. si8 dod'4 |
re' re2 |
la4. sol8 fa4 |
mi re dod |
re2 re4 |
re sib,2 |
la,4. fa,8 mi,4 |
re, la,2 |
re4 la, sib, |
sol, la,2 |
re,
