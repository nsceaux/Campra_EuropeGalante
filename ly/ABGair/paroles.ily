Que tes fa -- veurs
vont char -- mer les cœurs,
A -- mour, que de cru -- el -- les
tu vas domp -- ter !
Et que d’A -- mants fi -- del -- les
vont en pro -- fi -- ter !
Tu vas flé -- chir tous les cœurs re -- bel -- les,
tu vas pour ja -- mais
les bles -- ser de tes traits :
Loin de les crain -- dre,
cher -- chons leurs coups ;
Que cœur peut se plain -- dre
d’un tour -- ment si doux ?
Au Dieu d’A -- mour cé -- dons la vic -- toi -- re ;
Quand il nous soû -- met à ses de -- sirs,
c’est moins pour sa gloi -- re,
que pour nos plai -- sirs.
