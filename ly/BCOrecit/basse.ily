\clef "vbasse" sol,2~ sol,8 sol, fa, mi, |
re,1 |
re~ |
re2 do4 |
sib,2. |
la,4*7/8~ \hideNotes la,32