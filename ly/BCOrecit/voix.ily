\clef "vbas-dessus" <>^\markup\character Cephise
R1*2 |
r8 re''16 re'' la'8 la'16 la' fa'8 fa'16 fa' sib'8 sib'16 la' |
la'8\trill la' r la'16 si' do''8 do''16 la' |
re''4 re''8 re''16 re'' re''8 mi'' |
dod''8 dod''
