\clef "vbas-dessus" R2.*19 |
<>^\markup\character Cephise r4 r8 mi'' fad'' sol'' |
si'2 si'8. do''16 |
la'4.\trill sol'8 fad'4 |
sol'4. fad'8( mi'4) |
r8 si' si' dod'' red''4 |
mi'' mi''( red''8.)\trill mi''16 |
mi''2 sol''4 |
si'8\trill si' si' do'' re''4 |
mi'' mi''8 re'' do'' si' |
la'2\trill re''8 re'' |
sol'2 sol'8 sol' |
do'' si' si'4( la')\trill |
sol' re''2 |
si'2\trill r8 mi'' |
fad'' sol'' red''4.\trill mi''8 |
\appoggiatura mi''8 fad''2 r4 |
fad''8 fad'' fad''4\trill fad''8 fad'' |
sol'' fad'' mi'' re'' dod'' re'' |
lad'4\trill fad'' fad''8 lad' |
si'4. si'8 dod''4 |
re'' re''( dod'') |
si'2. |
r4 r8 mi'' fad'' sol'' |
si'2 si'8. do''16 |
la'4.\trill sol'8 fad'4 |
sol'4. fad'8( mi'4) |
r8 si' si' dod'' red''4 |
mi'' mi''( red''8.)\trill mi''16 |
mi''2 r4 |
R2.*5 |
r2 si'8 si' mi'' sol'' |
do''4 do''8. si'16 si'4\trill si'8 si' |
mi''8 mi''16 fad'' sol''4 sol''8 fad'' |
sol''4 si' si'8 si' |
sold'8\trill sold'16 sold' sold'8 sold'16 si' mi'4 mi' |
do''8 do''16 la' mi''8 mi'' mi'' mi'' mi'' fad'' |
sol'' sol'' r si' mi'' re'' |
do'' si' la' sol' fad'4\trill r8 si'16 si' |
si'8 si'16 si' \appoggiatura si'16 dod''4 dod''8 dod''16 dod'' |
red''8 mi'' sol'4( fad')\trill |
mi'2. |
