Pai -- si -- bles lieux, a -- gré -- a -- bles re -- trai -- tes,
je n’ai -- me -- ray ja -- mais que vous.
En vain mil -- le Ber -- gers vien -- nent à mes ge -- noux
me ju -- rer des ar -- deurs par -- fai -- tes.
Beaux lieux, n’en soy -- ez point ja -- loux,
je mé -- pri -- se leur flâme, & je les quit -- te tous
pour le plai -- sir que vous me fai -- tes.
Pai -- si -- bles lieux, a -- gré -- a -- bles re -- trai -- tes,
je n’ai -- me -- ray ja -- mais que vous.
Pour for -- cer mon cœur à se ren -- dre,
on fait des ef -- forts cha -- que jour ;
Mais, quel -- ques pleurs que je fas -- se ré -- pan -- dre,
quel -- ques ser -- mens que l’on me fasse en -- ten -- dre,
ce sont les pie -- ges de l’A -- mour ;
je me gar -- de -- ray bien de m’y lais -- ser sur -- pren -- dre.
