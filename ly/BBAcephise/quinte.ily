\clef "quinte" r4 mi2 |
si sol4 |
la fad si |
sol2 sol4 |
r8 si si si la4 |
sol si4. si8 |
si2 sol4 |
la2 la4 |
si2 sol4 |
sol do'4. re'8 |
re'2 sol8 sol |
sol2 sol8 sol |
la si re'4. re'8 |
si2 si4 |
si si4. si8 |
do'2 re'4 |
do'2 do'4 |
fad la sol |
la la fad |
sol mi2\douxSug |
fad si8 mi |
mi4 si4. si8 |
sol2 sol4 |
r8 si si si fad'4 |
si si4. si8 |
si2. |
si4. la8 sol4~ |
sol mi4. mi8 |
fad2 sol4 |
sol2 sol8 sol |
la si re'4 re |
re2. |
sol2 r8 si |
fad' si si4. si8 |
si2. |
si4 si4. si8 |
si2. |
fad'2 fad4 |
si4. si8 mi4 |
si dod' fad |
fad2. |
r4 mi2 |
fad sol8 mi |
mi4 si4. si8 |
sol2 sol4 |
r8 si si si fad'4 |
si si4. si8 |
sol2. |
mi4\fortSug si4. si8 |
do'2 re'4 |
do' la2 |
si sol4 |
la la fad |
sol2 sol4. sol8 |
mi4 la sol si |
do' sol8. la16 la8.\trill sol16 |
sol2 sol4 |
si2. mi4 |
mi2 la4 si8 fad' |
si2 si4 |
do' fad fad2 |
si4 mi4. mi8 |
red si, si,4. si,8 |
si,2. |
