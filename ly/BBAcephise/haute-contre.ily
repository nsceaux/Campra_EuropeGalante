\clef "haute-contre" r4 r8 sol' la' si' |
fad'2\trill mi'4 |
mi' mi' red' |
mi'2 si4 |
r8 sol' sol' sol' la'4 |
si' si' fad' |
sol'2 si'4 |
la'2 la'4 |
sol'2 sol'4 |
sol' fad'4. sol'8 |
fad'2\trill si'8 si' |
mi'2 mi'8 mi' |
fad' sol' sol'4( fad'8.)\trill sol'16 |
sol'2 sol'4 |
si' si'4. si'8 |
la'2 sol'4 |
sol' mi' fad' |
fad'2 mi'4 |
mi' red'4. mi'8 |
mi'4 si2\douxSug |
si si4 |
mi' si4. si8 |
si2 mi'4 |
r8 re' re' mi' fad'4 |
sol' sol' fad'8.\trill mi'16 |
mi'2. |
sol'4 sol'8 fad' sol'4 |
mi' mi'4. mi'8 |
re'2 re'4 |
do'2 mi'8 mi' |
la' re' re'4. re'8 |
re'2.~ |
re'2 r8 sol' |
la' si' la'4.\trill sol'8 |
fad'2.\trill~ |
fad'4 fad'4. fad'8 |
mi'2 mi'4 |
dod'2\trill dod'4 |
re'4. re'8 mi'4 |
fad' fad'4. fad'8 |
fad'2. |
r4 si2 |
si si4 |
do' si4. si8 |
si2 sol'4 |
r8 re' re' mi' fad'4 |
sol' sol'( fad'8.)\trill mi'16 |
mi'2. |
si'4\fortSug si'4. si'8 |
la'2 sol'4 |
sol' mi' fad' |
fad'2 mi'4 |
mi'( red'4.) mi'8 |
mi'2 mi'4. mi'8 |
mi'4 re' re'4. sol'8 |
mi'4 re'4. re'8 |
re'2 re'4 |
si2 si4 re' |
do'2 mi'4 sol'8 la' |
sol'4 mi'4. mi'8 |
mi'4 do' si2 |
si4 la4. la8 |
la8 si si4. si8 |
si2. |
