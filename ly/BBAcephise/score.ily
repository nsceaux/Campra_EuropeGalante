\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << <>_"Violons" \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*9\break s2.*10\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*5\break s2.*7\pageBreak
        s2.*9\break s1*2 s2.*2\pageBreak
        s1*2 s2.\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
