\clef "taille" r4 si2 |
si si4 |
do' si4. si8 |
si2 si4 |
r8 re' re' mi' fad'4 |
sol' fad'4.\trill mi'8 |
mi'2 si4 |
re'2 re'4 |
re'2 sol'4 |
mi' la'4. re'8 |
re'2 re'8 re' |
do'2 mi'4 |
la'8 re' re'4. re'8 |
re'2 re'4 |
mi' mi'4. mi'8 |
mi'2 sol'4 |
mi'4. re'8 do'4 |
si2 si4 |
mi' si4. si8 |
si4 sol2\douxSug |
la2 sol4 |
do' fad si |
si2 si4 |
r8 si si si la4 |
sol si4. si8 |
si2. |
mi'4. la8 re'4 |
do' la4.\trill la8 |
la2 sol4 |
sol2 do'4 |
do'8 re' re'4. re'8 |
si2.\trill~ |
si2 r8 si |
red' mi' fad'4. mi'8 |
red'2.~ |
red'4 si4. si8 |
si2 dod'4 |
dod'2 dod'4 |
si4. si8 mi'4 |
re' fad'4. fad'8 |
red'2. |
r4 sol2 |
la sol4 |
la fad si |
si2 si4 |
r8 si si si la4 |
sol si4. si8 |
si2. |
si4\fortSug mi'4. mi'8 |
mi'2 re'4 |
mi'4. re'8 do'4 |
si2 si4 |
do' si4. si8 |
si2 si4. si8 |
do'4. la8 si4 re' |
do' re' re'8 la |
si2 si4 |
si4 sold si4. si8 |
la2 la'4 mi'8 red' |
mi'4 si4. si8 |
la4. mi8 si2 |
mi4 mi4. mi8 |
la8 sol si4 fad |
sol2. |
