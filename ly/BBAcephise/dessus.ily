\clef "dessus2" r4 r8 mi'' fad'' sol'' |
si'2 si'8. do''16 |
la'4.\trill sol'8 fad'4 |
sol'4. fad'8( mi'4) |
r8 si' si' dod'' red''4 |
mi'' red''4.\trill mi''8 |
mi''2 r8 sol'' |
fad''4.\trill mi''8 re'' do'' |
si'2\trill re''4 |
mi'' do''4.\trill si'8 |
la'2\trill re''8 re'' |
sol'2 sol'8 sol' |
do'' si' si'4( la'8.)\trill sol'16 |
sol'2 r8 si' |
re''4 re''4. mi''8 |
do''2\trill si'4 |
si' la'4.\trill la'8 |
la'4 fad' sol' |
sol'( fad'4.)\trill mi'8 |
mi'4 r8 sol'\doux la' si' |
fad'2\trill mi'4 |
mi' red'4. red'8 |
mi'2 si4 |
r8 sol' sol' sol' la'4 |
si' si' fad' |
\appoggiatura fad'8 sol'2.~ |
sol'4 sol'8 la' si'4 |
do'' do''8 si' la' sol' |
fad'2\trill si'8 si' |
mi'2 mi'8 mi' |
fad' sol' sol'4( fad'8.)\trill sol'16 |
sol'2.~ |
sol'2 r8 si' |
la' sol' fad'4\trill si' |
si'2.~ |
si'4 si'4. si'8 |
si'4 sol'4. sol'8 |
fad'2 fad'4 |
fad'4. sold'8 lad'4 |
si' si'( lad') |
si'2. |
r4 r8 sol' la' si' |
fad'2\trill mi'4 |
mi' red'4.\trill red'8 |
mi'2 si4 |
r8 sol' sol' sol' la'4 |
si' si' fad' |
sol'2 r8 si'\fort |
re''4 re''4. mi''8 |
do''2 si'4 |
si' la'4.\trill la'8 |
la'4 fad' fad' |
sol'( fad'4.)\trill mi'8 |
mi'2 sol'4. sol'8 |
sol'4 la' re' sol' |
sol'8. la'16 si'4 la'8 re'' |
si'4\trill re'2 |
mi'2 mi'4. mi'8 |
mi'2 do''4 si'8 la' |
si'4 sold'4. sold'8 |
la'8 sol' fad' mi' red'2 |
mi'4 mi'4. mi'8 |
fad'8 sol' mi'4( red'8.)\trill mi'16 |
mi'2. |
