\clef "basse" mi,4 mi2 |
red mi4 |
la, si,2 |
mi, mi8 fad |
sol2 fad4 |
mi si,2 |
mi, mi4 |
fad2. |
sol2 si,4 |
do la,4.\trill sol,8 |
re2 si,4 |
do2 do8 si, |
la, sol, re4 re, |
sol,2 sol4 |
sold2. |
la2 si4 |
do'4. si8 la4 |
red2 mi4 |
la, si,2 |
mi,4 mi2^\douxSug |
red mi4 |
la, si,2 |
mi, mi8 fad |
sol4. sol8 fad4 |
mi si,2 |
mi,2. |
mi4 re8 do si, sol, |
do2. |
re2 si,4 |
do2 do8 si, |
la, sol, re4 re, |
sol,2. |
sol |
fad8 mi si4. mi8 |
si,2 si4 |
red2. |
mi |
fad4 mi2 |
re4. re8 dod?4 |
si, fad fad, |
si,4. la,8 sol, fad, |
mi,4 mi2 |
red2 mi4 |
la, si,2 |
mi,2 mi8 fad |
sol4. sol8 fad4 |
mi si,2 |
mi,2. |
sold^\fortSug |
la2 si4 |
do'4. si8 la4 |
red2 mi4 |
la, si,2 |
mi,1 |
mi4 fad sol2 |
do4 si,8 do re re, |
sol,4 fa,2 |
mi,4 mi sold,2 |
la, la4 sol8 fad |
mi2. |
la,2 si, |
sold,4 la,4. sol,8 |
fad, mi, si,2 |
mi,2.~ |
mi,2 fad,4 |
sol,2
