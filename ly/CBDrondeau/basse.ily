\clef "basse" sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa re |
mib8 re do4 sib, |
fa,2 sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa sol |
mib fa fa, |
sib,2 sib4 |
do' re' re |
sol2 sol4 |
sol fa mib |
re4. do8 sib,4 |
do re re, |
sol,2.~ |
sol,2 sol4 |
do' do'4. sib8 |
la4.\trill sol8 fa4 |
sib, do re |
sib, do do, |
fa,2 sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa re |
mib8 re do4 sib, |
fa,2 sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa sol |
mib fa fa, |
sib,2 sib4 |
mib fa4. fa8 |
sol2 sol4 |
do' sib lab |
sol2 lab4 |
fa sol sol, |
do do4. sib,8 |
la,2 fa4 |
sib sib4. lab8 |
sol4. fa8 mib4 |
fa mib re |
do sib,2 |
fa, sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa re |
mib8 re do4 sib, |
fa,2 sib4 |
sib do'4. do'8 |
re'2 re4 |
mib fa4. fa8 |
sib4 fa sol |
mib fa fa, |
sib,2
