\clef "haute-contre" re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do'' do''4. do''8 |
sib'4 la' fa' |
mib' mib' fa' |
fa'2 re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do'' do''4. do''8 |
sib'4 la' sib' |
sib' la'4.\trill sib'8 |
sib'2 fa'4 |
la' sol'4.\trill fad'8 |
sol'2 re''4 |
re'' re'' do'' |
do'' la' sib'~ |
sib'8 la' la'4.\trill sol'8 |
sol'2.~ |
sol'2 si'4 |
do'' sol'4. sol'8 |
la'2 la'4 |
sib'4 sol' re'' |
re'' do''4. do''8 |
do''2 re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do'' do''4. do''8 |
sib'4 la' fa' |
mib' mib' fa' |
fa'2 re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do'' do''4. do''8 |
sib'4 la' sib' |
sib' la'4.\trill sib'8 |
sib'2 sib'4 |
sib' lab'4. lab'8 |
sol'2 re''4 |
mib'' re'' do'' |
si'2 do''4 |
do'' si'4.\trill do''8 |
do''2 do''4 |
do''4. sib'8 la'4 |
sib'2 sib'4 |
sib' sib'4. do''8 |
la'4 fa' sib' |
la' sib'4. do''8 |
la'2 re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do''4 do''4. do''8 |
sib'4 la' fa' |
mib' mib' fa' |
fa'2 re''4 |
re'' mib''4. mib''8 |
re''2 re''4 |
do''4 do''4. do''8 |
sib'4 la' sib' |
sib' la'4.\trill sib'8 |
sib'2
