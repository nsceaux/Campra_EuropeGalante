\clef "taille" fa'4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' fa' |
sib mib' re' |
do'2\trill sib4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' mib' |
sol' fa'4. fa'8 |
fa'2 fa'4 |
mib' re'4. re'8 |
re'2 sib'4 |
sib' re' sol' |
fad'4.\trill mi'8 re'4 |
mib' re'4. re'8 |
re'2.~ |
re'2 sol'4 |
sol' sol'4. sol'8 |
fa'4 fa'4. fa'8 |
fa'4 do'' la' |
sol' sol'4. fa'8 |
fa'2 fa'4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' fa' |
sib mib' re' |
do'2\trill sib4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' mib' |
sol' fa'4. fa'8 |
re'2 fa'4 |
sol' fa'4. mib'8 |
re'2 re'4 |
do' re' fa' |
re'2 re'4 |
do' sol'4. sol'8 |
mi'2 mi'4 |
la'4 la'4. sol'8 |
fa'2 fa'4 |
sol' sol'4. sol'8 |
fa'4 fa' fa' |
mib' fa'4. fa'8 |
fa'2 fa'4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' fa' |
sib mib' re' |
do'2\trill sib4 |
fa' mib'4. mib'8 |
sib'2 fa'4 |
mib' do' fa' |
fa' fa' mib' |
sol' fa'4. fa'8 |
re'2
