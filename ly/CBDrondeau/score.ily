\score {
  \new StaffGroup <<
    \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*10\break s2.*11\pageBreak
        s2.*11\break s2.*10\pageBreak
        s2.*10\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
