\clef "dessus" fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4\trill do'' re'' |
sol' la' sib' |
la'2\trill fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4\trill do'' mib'' |
re'' do''4.\trill sib'8 |
sib'2 re''4 |
do'' sib'4.\trill la'8 |
sib'4 sol' sol'' |
sib'' la''4.\trill sol''8 |
la''4 re'' sol''~ |
sol''8 la'' fad''4. sol''8 |
sol''2 re''4 |
sol'' sol''4.\trill fa''8 |
mi''4\trill do'' do'' |
fa''4. sol''8 la''4 |
re'' mi'' fa'' |
sol'' mi''4.\trill fa''8 |
fa''2 fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4\trill do'' re'' |
sol' la' sib' |
la'2\trill fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4\trill do'' mib'' |
re'' do''4.\trill sib'8 |
sib'2 re''4 |
mib'' re''4.\trill do''8 |
si'4\trill sol' sol'' |
lab'' sol''4.\trill fa''8 |
sol''4 re'' fa'' |
mib'' re''4.\trill do''8 |
do''2 do''4 |
fa''4 fa''4. mib''8 |
re''4\trill sib' sib' |
mib''4. fa''8 sol''4 |
do''4 la'\trill sib' |
do'' re''4. mib''8 |
do''2\trill fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4\trill do'' re'' |
sol' la' sib' |
la'2\trill fa''4 |
sib'' la''4.\trill sol''8 |
fa''4 re'' fa'' |
sol'' fa''4.\trill mib''8 |
re''4 do'' mib'' |
re'' do''4.\trill sib'8 |
sib'2
