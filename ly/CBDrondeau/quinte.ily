\clef "quinte" sib |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sib do' fa |
fa2 sib4 |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sol do' fa |
fa2 sib4 |
la re'4. do'8 |
sib2 re'4 |
re' re' mib' |
la re' re' |
do' do'4. re'8 |
si2.~ |
si2 re'4 |
mi' mi'4. mi'8 |
fa'2 do'4 |
re'4 do' fa |
sib sol do' |
la2\trill sib4 |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sib do' fa |
fa2 sib4 |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sol do' fa |
fa2 sib4 |
sol re'4. re'8 |
re'2 si4 |
do' sol do' |
sol' sol fa |
lab sol4. sol8 |
sol2 sol4 |
fa2 do'4 |
re' re'4. do'8 |
sib4 sol4. sol8 |
la4 do' re' |
mib' re' sib |
do'2 sib4 |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sib do' fa |
fa2 sib4 |
sib mib'4. mib'8 |
fa'2 sib4 |
sib sib la |
sib do' sib |
sol do' fa |
fa2
