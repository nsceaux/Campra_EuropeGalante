\clef "taille" fad'4 fad' |
re' sol' fad' |
mi' fad' sol' |
la' la' la' |
la' fad' fad' |
re' sol' fad' |
mi' fad' sol' |
la' la' la' |
la' la' la' |
la' sol' fad' |
mi' fad' sol' |
la' la'4. la'8 |
fad'4 la' la' |
la' sol' fad' |
mi' fad' sol' |
la' la'4. la'8 |
fad'4
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
