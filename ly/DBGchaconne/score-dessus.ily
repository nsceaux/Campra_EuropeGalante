\score {
  \new GrandStaff \with { \haraKiriFirst } <<
    \new Staff <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
    >>
    \new Staff <<
      { \startHaraKiri s2 s2.*15 s4 \stopHaraKiri\break
        s2 s2.*15 s4 \break }
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
    >>
  >>
  \layout { system-count = 6 }
}
