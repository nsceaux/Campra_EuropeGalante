\clef "dessus" re''4 la' |
si' dod'' re'' |
dod''\trill re'' mi'' |
fad'' sol'' fad'' |
mi''\trill re'' la' |
si' dod'' re'' |
dod''\trill re'' mi'' |
fad'' sol'' fad'' |
mi''\trill la'' sol'' |
fad'' sol'' la'' |
dod''\trill re'' mi'' |
fad'' mi''4.\trill re''8 |
re''4 la'' sol'' |
fad'' sol'' la'' |
dod''\trill re'' mi'' |
fad'' mi''4.\trill re''8 |
re''4
%
<>^\markup\whiteout Hautbois
\twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''4 mi'' |
    fad'' mi''\trill re'' |
    sol'' fad'' fad'' |
    fad''4. sol''8 mi''4\trill |
    fad''4 fad'' mi''8\trill re'' |
    dod''4 re'' mi'' |
    re'' mi'' fad'' |
    dod'' dod''4.\trill si'8 |
    si'4 re'' dod'' |
    si' dod'' re'' |
    mi'' fad'' sol'' |
    fad'' sold'' la'' |
    sold'' mi'' re''8\trill dod'' |
    si'4 dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' si'4.\trill la'8 |
    la'4 }
  { re''4 dod'' |
    re'' dod'' re'' |
    mi'' re'' dod'' |
    si' si'4. dod''8 |
    lad'4\trill re'' dod''8\trill si' |
    lad'4 si' dod'' |
    si' lad' si' |
    si' lad'4. si'8 |
    si'4 si' la' |
    sold' la' si' |
    dod'' re'' mi'' |
    la' re'' dod'' |
    si'\trill dod'' si'8\trill la' |
    sold'4 la' si' |
    la' sold' la' |
    la' sold'4.\trill la'8 |
    la'4 }
>>
%
<>^\markup\whiteout Hautbois
\twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''4 sol'' |
    mi'' fad'' sol'' |
    fad''\trill re'' sol''~ |
    sol''8 la'' fad''4.\trill sol''8 |
    sol''4 si'' la''8\trill sol'' |
    fad''4 sol'' la'' |
    sol'' la'' si'' |
    fad''4 fad''4.\trill mi''8 |
    mi''4 sol'' fad'' |
    mi'' fad'' sol'' |
    la'' fad'' re'' |
    sol'' sol'' fad'' |
    mi'' la'' sol''8\trill fad'' |
    mi''4 fad'' sol'' |
    fad''\trill sol'' la'' |
    si''4 mi''4.\trill re''8 |
    re''4 }
  { la'4 si' |
    sol' do'' si' |
    la'\trill si' si' |
    do'' la' re'' |
    si'\trill sol'' fad''8 mi'' |
    red''4 mi'' fad'' |
    mi'' red'' mi'' |
    mi'' red''4. mi''8 |
    mi''4 mi'' re'' |
    dod''\trill re'' mi'' |
    fad'' re'' si' |
    mi'' mi'' re'' |
    dod''\trill fad'' mi''8\trill re'' |
    dod''4 re'' mi'' |
    re'' dod'' re'' |
    re'' dod''4.\trill re''8 |
    re''4 }
>>

