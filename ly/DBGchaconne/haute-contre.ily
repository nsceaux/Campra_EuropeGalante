\clef "haute-contre" la'4 la' |
sol' sol' la' |
la' re'' dod'' |
re'' mi'' re'' |
dod''\trill la' la' |
sol' sol' la' |
la' re'' dod'' |
re'' mi'' re'' |
dod''\trill re'' mi'' |
re'' dod'' re'' |
la' re'' dod'' |
re'' dod''4.\trill re''8 |
re''4 re'' mi'' |
re'' dod'' re'' |
la' re'' dod'' |
re'' dod''4.\trill re''8 |
re''4
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
