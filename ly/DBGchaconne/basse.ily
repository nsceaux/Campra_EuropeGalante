\clef "basse" re'4 fad |
sol mi re |
la fad mi |
re dod re |
la re' fad |
sol mi re |
la fad mi |
re dod re |
la fad dod |
re mi fad |
sol fad mi |
re la la, |
re fad dod |
re mi fad |
sol fad mi |
re la la, |
re
%%
<>^\markup\whiteout Bassons
re'4 la |
re mi fad |
mi si la |
sol sol4.\trill fad8 |
fad4 re mi |
fad si, lad, |
si, dod re |
mi fad fad, |
si, si si |
mi' mi' re' |
dod' si la |
re' si\trill la |
mi dod re |
mi la, sold, |
la, si, dod |
re mi mi, |
la,
%%
<>^\markup\whiteout Bassons
re'4 si |
do' la sol |
re si, mi |
do re re, |
sol, sol la |
si mi red |
mi fad sol |
la si si, |
mi, mi mi |
la la sol |
fad fad sol |
mi dod re |
la fad sol |
la re dod |
re mi fad |
sol la la, |
re
