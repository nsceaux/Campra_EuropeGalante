\clef "quinte" re'4 re' |
si mi' la |
la la sol |
fad la la |
la re' re' |
si mi' la |
la la sol |
fad la la |
la la la |
la mi' re' |
mi' la sol |
fad la4. la8 |
la4 la la |
la mi' re' |
mi' la sol |
fad la4. la8 |
la4
%
r4 r | R2.*15 | r4
%
r4 r | R2.*15 | r4
