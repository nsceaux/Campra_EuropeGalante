\clef "haute-contre" sib'2 re''4. re''8 |
re''2 do''4 mib'' |
la' re'' re'' la' |
sib'2 la'4.\trill la'8 |
sol'2 do''4. do''8 |
do''2 sib'4. do''8 |
la'4. la'8 re''4 do'' |
re''4. re''8 mib''4 re'' |
mib'' sib' sib' sib' |
sib'2 la'4.\trill sib'8 |
sib'2 sib'4. sib'8 |
sib'2 la'4. la'8 |
la'4. sol'8 sol'4. la'8 |
fad'1\trill |
fad'\trill |
R1*2 |
r4 re'' la' sib'8 do'' |
sib'2~ sib'8 sib' la' sib' |
sol'4. sol'8 la'4. sib'8 |
fad'2. fad'8 fad' |
sol'4 sol' fa' re' |
mib' sol' sol' sol' |
sol'2. sol'8 sol' |
la'4 do'' do'' do'' |
do'' do'' do'' sib' |
la'2 la'4. la'8 |
la'2. dod''4 |
re'' re'' dod''4. re''8 |
re''2. re'8 re' |
re'2 re'4. re'8 |
re'4 sol' sol'4. sol'8 |
sol'2 sol'4. sol'8 |
sol'2. sol'4 |
lab'4 lab' sol'4. sol'8 |
sol'2. sol'8 sol' |
la'4 do'' do'' do'' |
do'' do'' sib' re'' |
do''2. do''4 |
do'' do'' do''4. do''8 |
la'4 la' la' la'8 si' |
do''4 do'' do'' sol'8 la' |
sib'4 sib' sib' la'8 sol' |
la'4 fa' fa' fa'8 sol' |
la'4. sib'8 do''4 la' |
sib' fa' mib' sol' |
fa'2. fa'8 fa' |
fa'4. sol'8 la' sib' do'' la' |
sib'4. do''8 la'4.\trill sib'8 |
sib'4 re' re' re' |
re'4. sol'8 sol'4. sol'8 |
la'4. la'8 sib'4. sib'8 |
sol'4.\trill sol'8 sol'4. la'8 |
fad'1 |
r4 sib' do'' |
sib' la' sol' |
la' sol' fad' |
sol' sol' re'' |
do'' sib' la' |
re'' re'' do'' |
re'' la' sol' |
re' sol' sol' |
sol' sol' fad' |
sol'2 sol'4 |
fad' sol' la' |
sol' sol'4. la'8 |
fad'4 re' re' |
mib' sol' la' |
sib' sib' sib' |
sib' la'4.\trill sib'8 |
sib'4 re'' mib'' |
re'' do'' sib' |
la' sib' la' |
sol'4. fad'8 sol'4 |
fad' la' la' |
sib' fad' sol' |
re' sol' fad' |
sol' fad' sol' |
fad'2 la'4 |
sib' do'' re'' |
re'' sib' sib' |
la' la'4.\trill sol'8 |
sol'2. |
