\clef "basse" sol,2 sol4. sol8 |
mib'4. mib'8 mib'4 do' |
re'4. re'8 re'4 do' |
sib la8 sol fad2 |
sol8 fa mib re do re do sib, |
la,2 sib, |
fa,4 fa re fa |
sib, sib sol sib |
mib4. fa8 sol4 mib |
fa2 fa, |
sib,4 sib sol sib |
mi4. mi8 fad4. fad8 |
sol2 sol,4. sol,8 |
re2 re,4. re,8 |
re,1 |
R1*6 |
r4 sol re mib8 fa |
mib4 re8 do si,4 sol, |
do do' sol la8 sib |
la4 sol8 fa mi4 do |
fa fa, fa, sol, |
la, la mi fa8 sol |
fa4 mi8 re dod4 la, |
re sol, la,2 |
re,4 re' la sib8 do' |
sib4 la8 sol fad4 re |
sol sol re mib8 fa |
mib4 re8 do si,4 sol, |
do do sol, lab,8 sib, |
lab,4 fa, sol,2 |
do,4 do' sol la8 sib |
la4 sol8 fa mi4 do |
fa la, sib, sol, |
do do sol, la,8 sib, |
la,4 fa, do do, |
fa, r r2 |
r4 do' do' sib8 la |
sol4 r r2 |
r4 re' re' do'8 sib |
la2. fa4 |
sib re mib do |
fa fa do re8 mib |
re4 do8 sib, la,4 fa, |
sib, mib, fa, fa |
sib la8 sol fad4 re |
sol2~ sol8 do re mib |
fa2~ fa8 sib, do re |
mib1\trill |
re |
R2.*9 |
r4 sol sib |
la sol fad |
sol sol,2 |
re4 sib, sol, |
mib do fa |
re sib re |
mib fa fa, |
sib, sib do' |
sib la sol |
fad sol fa |
mib2. |
re4 re mib |
re do sib, |
do sib, la, |
sib, la, sol, |
re re' do' |
sib la sol |
fad sol sib, |
do re re, |
sol,2. |
