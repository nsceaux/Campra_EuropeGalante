\clef "dessus" re''4. re''8 sib''4. sib''8 |
sib''2 la''4 do''' |
fad''4.\trill fad''8 fad''4 la'' |
re'' mib''8 re'' do'' sib' la' sib' |
sib'4\trill sol' mib''4. mib''8 |
mib''4. re''16 do'' re''4. mib''8 |
do''4.\trill do''8 fa''4. fa''8 |
fa''4 re'' sib''4. la''8 sol''4. fa''8 mib'' re'' do'' sib' |
re''2 do''4.\trill sib'8 |
sib'2 re''4. re''8 |
re''4 mib''8 re'' do''\trill sib' do'' la' |
sib'4. sib'8 sib'4.\trill la'8 |
la'1 |
la' |
r4 re'' la' sib'8 do'' |
sib'4 sol' sol''4. sol''8 |
sol''2 fad''8 fad'' sol'' la'' |
re''2 re''4. re''8 |
re''4. mib''8 do''4.\trill sib'8 |
la'2.\trill la'8 la' |
sib'4. sib'8 sib'4.\trill la'8 |
sol'4 sol'' re'' mi''8 fa'' |
mi''2.\trill mi''8 mi'' |
fa''4 sol''8 la'' sol''4 la''8 sib'' |
la''4 sib''8 la'' sol'' fa'' mi'' re'' |
fa''2 dod''4.\trill dod''8 |
re''4 mi''8 fa'' mi'' fa'' sol'' mi'' |
fa''4 sol''8 la'' mi''4.\trill re''8 |
re''2. fad'8 fad' |
sol'4. sol'8 la'4.\trill la'8 |
sib'2 si'4.\trill si'8 |
do''4. do''8 re''4. re''8 |
mib''2~ mib''8 mib'' fa'' sol'' |
do''4. re''8 si'4.\trill do''8 |
do''2~ do''8 do'' re'' mi'' |
fa''4 sol''8 la'' sol''4 do''' |
la'' fa'' re'' sol'' |
mi''2.\trill r8 mi'' |
fa''4. sol''8 mi''4.\trill fa''8 |
fa''4 do'' do'' do''8 re'' |
mib''4 mib'' mib'' re''8 do'' |
re''4 re'' re'' re''8 mib'' |
fa''4 la' la' la'8 sib' |
do''4. re''8 mib''4. fa''8 |
re''4\trill sib' sol' do'' |
la'2.\trill la'8 la' |
sib'4. sib'8 do'' re'' mib'' fa'' |
re''4.\trill do''8 do''4.\trill sib'8 |
sib'4 re'' la' sib'8 do'' |
sib'4 sol' mib''2~ |
mib''4. fa''8 re''4.\trill re''8 |
re''4. mib''8 do''4.\trill re''8 |
re''1 |
r4 re'' mib'' |
re'' do''\trill sib' |
do'' sib'\trill la' |
sib' sol' sol'' |
fad'' sol'' la'' |
sib'' re'' sol'' |
fad'' re'' do'' |
si' do'' re'' |
mib'' re''\trill do'' |
re''2 sol'4 |
la' sib' do'' |
sib' sib'4.\trill la'8 |
la'4 re'' sib' |
sol' mib'' do'' |
fa'' re'' sib''~ |
sib''8 re'' do''4.\trill sib'8 |
sib'4 sib'' la'' |
sol'' fad'' sol'' |
re''2 re''4 |
re'' do''4.\trill re''8 |
re''4 re'' do'' |
sib' la' sol' |
fad' sol' la' |
sol' la' sib' |
la'2\trill re''4 |
sol'' la'' sib'' |
la'' re'' sol''~ |
sol''8 la'' fad''4. sol''8 |
sol''2. |
