\key re \minor \midiTempo#160
\time 2/2 s1*13 \alternatives s1 s1 \bar "||"
\digitTime\time 2/2 \beginMarkSmall "Reprise" s1*39 \bar "||"
\digitTime\time 3/4 \tempo "Lentement" s2.*29 \bar "|."
