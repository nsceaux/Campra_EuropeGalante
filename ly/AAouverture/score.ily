\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*6\break s1*9\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*7\pageBreak
        s1*5\break s1*7\pageBreak
        s1*2 s2.*7\break s2.*10\pageBreak
      }
      \modVersion { s1*15\break }
    >>
  >>
  \layout { }
  \midi { }
}
