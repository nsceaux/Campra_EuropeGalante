\clef "quinte" re'2 re'4 re' |
mib'2 mib'4. mib'8 |
re'2 re'4. re'8 |
re'2 re'4. re'8 |
sib2 do'4 sol |
la2 fa4. fa8 |
fa2 fa'4. fa'8 |
re'4 sib sib4. sib8 |
sib4 sol sol sib |
mib'2 mib'4. fa'8 |
re'4 re' sib4. sib8 |
sol2 la4 re' |
sib2. sol4 |
la1 |
la |
R1*5 |
r4 re' la sib8 do' |
sib2~ sib4. sib8 |
sib4 do' re' re' |
mi'2. mi'8 mi' |
do'4 do' do' sol |
la la la re |
la2. la8 la |
re'4 la la la |
la sib la la8 sol |
fad2. re8 re |
sol4 la8 sib la4 fad |
sol2 sol4. sol8 |
sol4 sol sol4. sol8 |
sol2. mib'4 |
mib' re' re'4.\trill do'8 |
do'2. do'8 do' |
do'4 la do' do' |
do' do' re' re' |
mi'2. do'4 |
do' do' do'4. do'8 |
do'4 fa' fa' mib'8 re' |
do'4 do' do' do' |
sib sol' sol' fa'8 mib' |
re'4 re' re' re' |
do' do' la do' |
sib sib sib do' |
do' la la do' |
sib do'8 re' do'4 do' |
sib sol do'4. do'8 |
re'4 do'8 sib re'4 re' |
re' sib sol do' |
la2\trill re'4 sib |
sib sol sol4. sol8 |
re'1 |
R2.*6 |
r4 re' mib' |
re' do' si |
do' sib la |
sol2 sol4 |
do' sib la |
sib sib sol |
la sib sib |
sib mib fa |
fa re' re' |
sol do' fa |
fa re' do' |
re' la sib |
do' sib re' |
sol2 sol4 |
re' la do' |
re' la re' |
do' re' do' |
sib do' sib |
re'2 re'4 |
re' do' sib |
re' re' re' |
do' do' la |
sib2. |
