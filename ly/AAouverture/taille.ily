\clef "taille" sol'2 sol'4. sol'8 |
sol'2 la'4. la'8 |
la'2 la'4. la'8 |
sol'2 la'4 re' |
re' mib' mib' sol' |
fa'2 fa'4. fa'8 |
fa'4 la' sib' la' |
sib' fa' sol' fa' |
sol'2 sol'4. sol'8 |
fa'2 fa'4. fa'8 |
fa'2 sol'4. sol'8 |
sol'2 re'4. re'8 |
re'2 re'4. re'8 |
re'2 re'4. re'8 |
re'1 |
R1*3 |
r4 sol' re' mib'8 fa' |
mib'2~ mib'8 mib' re' do' |
re'2~ re'4. re'8 |
re'2 re'4. re'8 |
sib4 mib' re' si |
do'2. do'8 do' |
do'4 do'8 re' mi'4 mi' |
fa' la' la' sol' |
fa' fa' sol' fa'8 mi' |
re'4 re' mi' la' |
fa' mi' mi' la' |
la'2. re'8 re' |
re'4 do'8 sib re'4 re' |
re'2 re'4. re'8 |
mib'4. mib'8 re'4 si |
do'2. do'4 |
do'4 fa' re' mi'8 fa' |
mi'2\trill~ mi'8 mi' fa' sol' |
fa'4 fa' sol' sol' |
la' la' fa' sib' |
sol'2.\trill r8 sol' |
la'4. sib'8 sol'4.\trill fa'8 |
fa'4 fa' fa' fa' |
sol' sol' sol' sol'8 fad' |
sol'4 sol' sol' sol' |
fa' fa' fa' fa' |
fa'2. fa'4 |
fa'4 fa' sib mib' |
do'2.\trill do'8 do' |
re'4 fa' fa' fa' |
fa' sol' fa'4. fa'8 |
fa'4 sol' do' sib8 la |
sib4 sib do'4. do'8 |
do'2 re'4. re'8 |
sib4. sib8 do'4. do'8 |
la1\trill |
R2.*3 |
r4 sol' sib' |
la' sol' fad' |
sol' fa' mib' |
re' fa' sol' |
fa' mib' re' |
do' sol la |
sib2 sib4 |
do' re' re' |
re' re'4. re'8 |
re'4 sol sib |
sib do' do' |
re' fa' fa' |
sol' fa'4. fa'8 |
re'4 fa' mib' |
sol' la' re' |
re'2 re'4 |
mib'2 do'4 |
la la la |
sol la sib |
la sib do' |
re' do' re' |
re'2 fad'4 |
sol' fad' sol' |
la' sol' sol' |
mib' re'4. re'8 |
re'2. |
