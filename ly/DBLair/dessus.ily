\clef "dessus" <>^"Violini" r4 |
R2. |
r4 r re'' |
fad'' re''8 mi'' fad'' sol'' |
fad''4 re'' fad''\dolce |
la'' fad''8 sol'' la'' si'' |
la''4 fad'' r |
r r re'' |
mi''8 re'' mi'' fad'' mi''4 |
fad'' re'' la'' |
la'' r la'' |
la'' fad'' r |
R2. |
r4 r mi''\forte |
fad'' sol'' fad'' |
mi''2\trill dod''4\dolce |
re'' mi'' re'' |
dod''2\trill la'4 |
sold'8 fad' sold' la' sold'4 |
la' la' la' |
sold'8 fad' sold' la' sold'4 |
la' mi' la' |
la' sold'4.\trill la'8 |
la'2 mi''4 |
mi''2 r4 |
mi'' dod'' la' |
si' si'4.\trill la'8 |
la'2 mi''4\forte |
fad''8 mi'' fad'' sol'' fad''4 |
sold''\trill mi'' la'' |
la'' sold''4.\trill la''8 |
la''2 r4 |
r r re''\dolce |
mi''8 re'' mi'' fad'' mi''4 |
fad'' re'' la'' |
la'' r la'' |
la'' fad'' r |
R2. |
r4 r mi''\forte |
fad'' sol'' fad'' |
mi''2\trill dod''4\dolce |
re'' mi'' re'' |
dod''2\trill re''4 |
mi'' mi''4.\trill re''8 |
re''2 la''4 |
la'' r la'' |
la'' fad'' re'' |
mi'' mi''4.\trill re''8 |
re''2 la'4\forte |
si' r si' |
dod''8 si' dod'' re'' dod''4 |
re'' r re'' |
mi''8 re'' mi'' fad'' mi''4 |
fad''4. mi''8 re'' dod'' |
si'4 dod''4.\trill re''8 |
re''4. mi''8 re''4 |
mi''4. fad''8 mi''4 |
fad''8 mi'' fad'' sol'' fad''4 |
mi'' mi''4.\trill re''8 |
re''2 r4 |
R2.*3 |
r4 r si' |
dod''4. re''8 si'4 |
dod'' fad' r |
r r fad''\dolce |
fad'' fad''4. mi''8 |
re''4\trill dod'' dod'' |
re''8 dod'' re'' mi'' re''4 |
dod''\trill r fad'' |
fad'' r fad'' |
fad'' fad''4. mi''8 |
re''4\trill dod'' si' |
si' si' lad' |
si'2 re''4\forte |
mi''8 re'' mi'' fad'' mi''4 |
fad'' si' dod'' |
re'' dod''4.\trill si'8 |
si'2 r4 |
