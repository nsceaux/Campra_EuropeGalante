\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*7\break s2.*10\break s2.*8\break s2.*9\pageBreak
        s2.*9\break s2.*9\break s2.*9\break s2.*9\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
