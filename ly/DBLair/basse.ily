\clef "basse" r4 |
R2. |
r4 r re |
re r re |
re r re'\dolce |
re' r re' |
re' r dod' |
si r si |
la r la |
re'8 dod' re' mi' re'4 |
dod'8 si dod' re' dod'4 |
re' r dod' |
re' dod' re' |
la2 la4\forte |
re' dod' re' |
la2 la4\dolce |
re dod re |
la,2 la4 |
mi r mi |
la r la |
mi r mi |
la dod re |
mi mi,2 |
la, la4 |
sold8 fad sold la sold4 |
la la, dod |
re mi mi, |
la,2 la4\forte |
re' r re' |
mi' dod' re' |
mi' mi2 |
la la4\dolce |
sol r sol |
mi r mi |
re r re' |
dod'8 si dod' re' dod'4 |
re' r dod' |
re' dod' re' |
la2 la4\forte |
re' dod' re' |
la2 la4\dolce |
re dod re |
la,2 fad,4 |
sol, la,2 |
re2 re'4 |
dod'8 si dod' re' dod'4 |
re' re fad |
sol la la, |
re2 fad4^\forte |
sol8 sol la la sol4 |
la r la |
si8 la si dod' si4 |
dod' r dod' |
re'4. dod'8 si la |
sol4 la la, |
re2 re'4 |
dod'8 si dod' re' dod'4 |
re' re fad |
sol la la, |
re2 re4 |
mi r mi |
fad r si |
la4. si8 sol4 |
fad2 si4 |
la4. si8 sol4 |
fad2 fad4 |
si^\dolce lad r |
si lad4. lad8 |
si4 fad fad |
si8 lad si dod' si4 |
lad r lad |
si fad sol |
mi fad2 |
sol4 re mi |
fad fad,2 |
si,2 si4 |
dod'8 si dod' re' dod'4 |
re' re mi |
fad fad,2 |
si, r4 |
