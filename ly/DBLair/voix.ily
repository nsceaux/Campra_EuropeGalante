\clef "vbas-dessus" <>^\markup\character Une femme du bal
re''4 |
fad'' re''8[ mi''] fad''[ sol''] |
fad''4 re'' r |
R2. |
r4 r re'' |
fad'' re''8[ mi''] fad''[ sol''] |
fad''4 re'' la' |
si'8[\melisma la' si' dod''] si'4( |
dod''8)[ si' dod'' re''] dod''4( |
re'')\melismaEnd re'' re'' |
mi''8[\melisma re'' mi'' fad''] mi''4( |
fad''4)\melismaEnd re'' mi'' |
fad'' sol'' fad'' |
mi''2\trill r4 |
R2. |
r4 r mi'' |
fad'' sol'' fad'' |
mi''2\trill la'4 |
si'8[\melisma la' si' dod''] si'4( |
dod'')\melismaEnd la' la' |
si'8[\melisma la' si' dod''] si'4( |
dod''4)\melismaEnd la' si' |
dod''4.\trill si'8 la'4 |
la'4.(\melisma si'8) la'4 |
si'4. dod''8 si'4( |
dod''2)\melismaEnd dod''4 |
fad' sold'4. la'8 |
la'2 r4 |
R2.*3 |
r4 r la' |
si'8[\melisma la' si' dod''] si'4( |
dod''8)[ si' dod'' re''] dod''4( |
re'')\melismaEnd re'' re'' |
mi''8[\melisma re'' mi'' fad''] mi''4( |
fad'')\melismaEnd re'' mi'' |
fad'' sol'' fad'' |
mi''2\trill r4 |
R2. |
r4 r mi'' |
fad'' sol'' fad'' |
mi''2\trill fad''4 |
si' dod''4. re''8 |
re''4.(\melisma mi''8) re''4 |
mi''4. fad''8 mi''4( |
fad''2)\melismaEnd fad''4 |
si'4 dod''4.\trill re''8 |
re''2 r4 |
R2.*10 |
r4 r fad'' |
fad'' mi''4.\trill re''8 |
dod''4 lad'\trill si' |
dod''4. re''8 si'4 |
dod'' fad' r |
R2. |
r4 r fad'' |
fad'' fad''4. mi''8 |
re''4\trill dod'' r |
r r fad'' |
fad'' r fad'' |
fad'' fad''4. mi''8 |
re''4\trill dod'' mi''16[ fad'' sol''8] |
re''4.\trill dod''8 si'4 |
si'2 mi''16[ fad'' sol''8] |
re''4.\trill dod''8 si'4 |
si'2 r4 |
R2.*3 |
r4 r re'' |
