\piecePartSpecs
#`((dessus #:score-template "score-basse-voix")
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#138 #}))
