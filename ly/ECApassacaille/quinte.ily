\clef "quinte" la4 dod'4. dod'8 |
si2 si4 |
la2 la4 |
mi'4 mi'4. re'8 |
dod'4 dod'4. dod'8 |
si2 sold4 |
la2 la4 |
mi'4 mi'4. re'8 |
dod'4 dod' dod' |
si2 si4 |
la2 fad4 |
sold2 sold4 |
r dod' dod' |
si2 si4 |
la2 fad4 |
sold2 sold4 |
r la4. la8 |
mi'2 mi'4 |
dod' la sold |
fad sold mi |
mi la la |
mi'2 mi'4 |
dod' la sold |
fad sold mi |
mi la la |
mi'2 sold4 |
la4. sold8 fad4 |
sold2 mi4 |
r la la |
mi'2 sold4 |
la4. sold8 fad4 |
sold2 sold4 |
r la re' |
dod'2 dod'8 si |
la4 sold la |
la si mi |
mi la re' |
dod'2 dod'4 |
la sold la |
la si mi |
mi r r |
R2.*7 |
r4 dod' la |
si2 si4 |
la la la |
mi' si si |
la dod' la |
si2 si4 |
la la la |
mi' si4. si8 |
dod'4 la mi' |
re' re' dod' |
re' sold la |
si si4.\trill dod'8 |
dod'4 mi'4. mi'8 |
re'4 re' dod' |
re' sold la |
si4 si4.\trill dod'8 |
dod'4 %{%} do' la |
si2 do'4 |
do' la2 |
mi' mi'4 |
r do' la |
si2 do'4 |
do' la2 |
mi'2 mi'4 |
r mi' mi' |
re' re'4. re'8 |
si4 sold la |
la mi4. mi8 |
mi4 mi' mi' |
re' re'4. re'8 |
si4 sold la |
la mi4. mi8 |
mi4 r r |
R2.*3 |
r4 re'4. re'8 |
re'4 si sol |
sol2 do'4 |
do'2.~ |
do'4 la re'~ |
re' si sol |
do'4. do'8 si4 |
la re' sol |
sol r r |
R2.*7 |
r4 mi'4. sol'8 |
re'2 re'4 |
do'4. si8 do' re' |
mi' fa' mi'4 si |
do' mi'4. mi'8 |
re'2 re'4 |
do'4. re'8 do' re' |
mi'4 mi4. mi8 |
mi4 %{%} mi' r |
si r r |
la r r |
mi' r r |
mi' dod' r |
si r r |
la r r |
mi' r r |
mi'8. re'16 dod'8. si16 la8. dod'16 |
re'4. re'8 mi'8. la16 |
si8. dod'16 si4 la |
la8. re'16 si4.\trill la8 |
la8. si16 dod'8. si16 la8. dod'16 |
re'4. re'8 mi'8. la16 |
si8. dod'16 si4 la |
la mi4. mi8 |
mi4 la4. dod'8 |
si4 la mi' |
re'4. mi'8 fad'4 |
mi'2 mi'4 |
mi' dod' mi' |
si2 mi'4 |
re'4. re'8 dod'4 |
re' si4.\trill la8 |
la4 la4. dod'8 |
si4 la mi' |
re'4. mi'8 fad'4 |
mi'2 mi'4 |
mi' dod' mi' |
si2 mi'4 |
re'4. re'8 dod'4 |
la mi' mi |
mi
