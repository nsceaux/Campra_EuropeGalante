\clef "haute-contre" la'4 la'4. la'8 |
si'4 sold' mi' |
fad'8 mi' fad' sold' la' si' |
sold'4\trill mi'8 mi' fad' sold' |
la'4 la'4. la'8 |
si'4 sold' mi' |
fad'8 mi' fad' sold' la' si' |
sold'4\trill mi'4. mi'8 |
mi'4 dod'' la' |
si' mi'' mi'' |
mi'' red''4.(\trill dod''16 red'') |
mi''2 mi''4 |
r dod'' la' |
si' mi'' mi'' |
mi'' red''4.(\trill dod''16 red'') |
mi''2 mi''4 |
r la'4. la'8 |
mi'2 mi'4 |
fad'4. fad'8 sold'4 |
la'4 sold'4.\trill la'8 |
la'4 la'4. la'8 |
mi'2 mi'4 |
fad'4. fad'8 sold'4 |
la'4 sold'4.\trill la'8 |
la'4 mi' mi' |
mi'4. fad'8 sold'4 |
fad'4. sold'8 la'4 |
sold'2\trill sold'4 |
r mi' mi' |
mi'4. fad'8 sold'4 |
fad'4. sold'8 la'4 |
sold'2\trill sold'4 |
r la' sold'8.\trill fad'16 |
mi'4 la'2 |
la'4 si' dod''~ |
dod''8 si' si'4.\trill la'8 |
la'4 la' sold'8.\trill fad'16 |
mi'4 la'2 |
la'4 si' dod''~ |
dod''8 si' si'4.\trill la'8 |
la'4 r r |
R2.*7 |
r4 dod''8 si' dod'' re'' |
si'4\trill mi''4. mi''8 |
mi''4 dod'' re'' |
si'\trill sold'8 la' si' sold' |
la'4 dod''8 si' dod'' re'' |
si'4\trill mi''4. mi''8 |
mi''4 la' re'' |
si'\trill sold'8 la' si' sold' |
la' si' dod'' re'' mi'' dod'' |
re''4 re'' mi'' |
re'' si' dod''~ |
dod''8 si' si'4.\trill la'8 |
la' si' dod'' re'' mi'' dod'' |
re''4 re'' mi'' |
re'' si' dod''~ |
dod''8 re'' si'4.\trill la'8 |
la'4 %{%} la'2 |
mi'2 mi'4 |
fa'8 sol' la'4. si'8 |
sold'2\trill sold'4 |
r la'4. la'8 |
mi'2 mi'4 |
fa'8 sol' la'4. si'8 |
sold'2 sold'4 |
r4 do''8 re'' mi'' do'' |
re''4 re''4. re''8 |
re''4 si' do'' |
la'4 sold'4.\trill la'8 |
la' si' do'' re'' mi'' do'' |
re''4 re''4. re''8 |
re''4 si' do'' |
la'4 sold'4.\trill la'8 |
la'4 r r |
R2.*3 |
r4 si'4. si'8 |
re''2 re''4 |
sol'2 sol'4 |
sol'2 sol'4 |
fa' la'4. la'8 |
sol'4 si'4. si'8 |
la'4. la'8 si'4 |
do''4 si'4.\trill do''8 |
do''4 r r |
R2.*7 |
r4 mi''4. mi''8 |
fa''4. fa''8 mi''\trill re'' |
mi''4 do'' do''~ |
do''8 si' si'4.\trill la'8 |
la'4 do'' mi'' |
fa''4. fa''8 mi''\trill re'' |
mi''4 mi' mi' |
la' sold'4.\trill la'8 |
la'4 %{%} la' r |
si' r r |
red'' r r |
mi'' r r |
mi' la' r |
si' r r |
red'' r r |
mi'' r r |
mi' la'4. la'8 |
la'4 si' mi'8. fad'16 |
sold'8. la'16 fad'4. la'8 |
la'4 mi''4. re''8 |
dod''8.\trill si'16 la'4. la'8 |
la'4 si' mi'8. fad'16 |
sold'8. la'16 fad'4. la'8 |
la'4 sold'4.\trill la'8 |
la'4 dod''4. dod''8 |
re''4 mi'' dod'' |
re''2 re''4 |
mi'' sold'4.\trill sold'8 |
la'2 la'4 |
si'2 mi'4 |
fad' si' la' |
fad' mi'4. mi'8 |
mi'4 dod''4. dod''8 |
re''4 mi'' dod'' |
re''2 re''4 |
mi'' sold'4.\trill sold'8 |
la'2 la'4 |
si'2 mi'4 |
la' si' la' |
la' sold'4.\trill la'8 |
la'4
