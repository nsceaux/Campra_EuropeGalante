\clef "basse" la,4 la2 |
sold2. |
fad |
mi |
la, |
sold, |
fad, |
mi, |
la,4 la2 |
sold2. |
fad |
mi4. re8 dod si, |
la,2. |
sold, |
fad, |
mi, |
la,4 la2 |
sold4 sol2 |
fad mi4 |
re mi mi, |
la,2. |
sold,4 sol,2 |
fad, mi,4 |
re,4 mi,2 |
la,4 la2 |
sold4 mi2 |
fad4. mi8 re4 |
mi mi8 fad sold mi |
la4 la,2 |
sold,4 mi,2 |
fad,4. mi,8 re,4 |
mi, mi,8 fad, sold, mi, |
la,4 la si |
dod' dod2 |
re4. re8 dod4 |
la, mi mi, |
la, la si |
dod' dod2 |
re4. re8 dod4 |
la, mi mi, |
la, r r |
R2.*7 |
r4 la8 sold la si |
sold fad mi fad sold mi |
fad mi re mi fad re |
mi re mi fad sold mi |
la si la sold la si |
sold fad mi fad sold mi |
fad mi re mi fad re |
mi re mi fad sold mi |
la sold la si dod' la |
re' dod' si4 la |
re2 dod4 |
re mi mi, |
la, la8 si dod' la |
re' dod' si4 la |
re2 dod4 |
re mi mi, |
la, %{%} la2 |
sol2. |
fa |
mi4 mi8 re mi mi, |
la,2. |
sol, |
fa, |
mi,4 mi8 re mi mi, |
la,4 la8 si do' la |
re'4. do'8 si4 |
mi4. re8 do4 |
re mi mi, |
la, la8 si do' la |
re'4. do'8 si4 |
mi4. re8 do4 |
re mi mi, |
la, r r |
R2.*3 |
r4 sol4. sol8 |
si,2. |
do4 do4. do8 |
mi,2. |
fa,2 fa8 fa |
sol2 sol8 sol |
la4. la8 sol4 |
fa sol sol, |
do r r |
R2.*7 |
r4 mi'4. mi'8 |
re'4. re'8 do' si |
do'4. si8 la si |
do' re' mi'4 mi |
la4 la4. la8 |
re4. re8 do si, |
do4. si,8 la, si, |
do re mi4 mi, |
la,4 %{%} la, r |
sold, r r |
fad, r r |
mi, r r |
la, la r |
sold r r |
fad r r |
mi r r |
la8. sold16 la8. si16 dod'8. la16 |
re'8. dod'16 si8. la16 sold8. fad16 |
mi8. la,16 si,8. dod16 re8. mi16 |
fad8. re16 mi8. fad16 sold8. mi16 |
la8. sold16 la8. si16 dod'8. la16 |
re'8. dod'16 si8. la16 sold8. fad16 |
mi8. la,16 si,8. dod16 re8. mi16 |
fad8. re16 mi4 mi, |
la, la4. la8 |
si4 dod' la |
re'4. dod'8 si4 |
mi'2 mi4 |
la2 la4 |
sold sol2 |
fad4 sold la |
re mi mi, |
la,4 la,4. la,8 |
si,4 dod la, |
re4. dod8 si,4 |
mi2 mi4 |
la,2 la,4 |
sold, sol,2 |
fad,4 sold,! la, |
re, mi,2 |
la,4
