\score {
  \new StaffGroup <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'flute1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'flute2 \includeNotes "dessus" >>
    >>
    \new Staff << \global \keepWithTag #'violons \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*9\break s2.*8 s4 \bar "" \pageBreak
        s2 s2.*6\break s2.*7\pageBreak
        s2.*8\break s2.*9\pageBreak
        s2.*9\break s2.*9\pageBreak
        s2.*9\break s2.*8\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5\break s2.*9\pageBreak
      }
      \modVersion {
        \repeat unfold 10 { s2.*8\break }
        s2.*20\break
        \repeat unfold 4 { s2.*8\break }
      }
    >>
  >>
  \layout { }
  \midi { }
}
