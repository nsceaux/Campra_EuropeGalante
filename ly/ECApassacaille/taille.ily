\clef "taille" mi'4 mi'4. mi'8 |
mi'2 mi'4~ |
mi' la fad' |
si si mi' |
mi' mi'4. mi'8 |
mi'2 mi'4~ |
mi' la fad' |
si si4.\trill la8 |
la4 mi' mi' |
mi'2 mi'4 |
fad'2 fad'4 |
si2 si4 |
r mi' mi' |
mi'2 mi'4 |
fad'2 fad'4 |
si2 si4 |
r mi'4. mi'8 |
sold'4 mi'2~ |
mi'4 re' re' |
dod'8 la mi'4. mi'8 |
dod'4 dod' la |
si2 mi'4 |
la2 mi'4 |
la4 mi'4. re'8 |
dod'4 dod' la |
si2 mi'4 |
dod' fad'2 |
mi' mi'4 |
r dod' la |
si2 mi'4 |
dod' fad'2 |
mi' mi'4 |
r mi' re' |
mi'2 mi'4 |
re' si mi' |
mi' mi'4. mi'8 |
dod'4 mi' re' |
mi'2 mi'4 |
re' si mi' |
mi' mi'4. mi'8 |
dod'4 r r |
R2.*7 |
r4 la' la' |
mi'8 fad' sold' la' si' sold' |
la'4. sold'8 fad'4 |
sold'8 fad' mi'4 mi' |
mi' la' la' |
mi'8 fad' sold' la' si' sold' |
la'4 la'8 sold' fad'4 |
sold'8 fad' mi'4 mi' |
mi' la' la' |
la' si' dod'' |
sold'\trill mi'2 |
fad'4 mi'4. mi'8 |
mi'4 la'4. la'8 |
la'4 si' dod'' |
sold'\trill mi'2 |
fad'4 mi'4. mi'8 |
mi'4 %{%} mi'4. mi'8 |
mi'2 mi'4 |
mi'2 re'4 |
si2\trill si4 |
r mi'4. mi'8 |
mi'2 mi'4 |
mi'2 re'4 |
si2\trill si4 |
r la la' |
la' si'4. si'8 |
si'2 mi'4 |
fa' mi' si |
do' la'2~ |
la'4 si'4. si'8 |
si'4 mi' mi' |
fa' mi' si |
do'4 r r |
R2.*3 |
r4 sol'2~ |
sol'4 sol' re' |
mi'2 mi'4 |
mi'2 mi'4 |
do'2 fa'4 |
re'2 sol'4 |
mi'2 sol'4 |
do' fa'4. sol'8 |
mi'4\trill r r |
R2.*7 |
r4 si'4. si'8 |
si'4. si'8 la' sold' |
la'2.~ |
la'4 mi'4. mi'8 |
mi'4 la'4. la'8 |
si'4. si'8 la' sold' |
la'2. |
mi'4 re'4. mi'8 |
dod'4 %{%} mi' r |
sold' r r |
la' r r |
si' r r |
la' mi' r |
sold' r r |
la' r r |
si' r r |
la' mi'4. sol'8 |
fad'4 fad' mi'8. red'16 |
mi'4 re'4. re'8 |
dod'8. fad'16 mi'4. mi'8 |
mi'4 mi'4. mi'8 |
fad'4 fad' mi'8. red'16 |
mi'4 re'4. re'8 |
dod'8. fad'16 mi'4. mi'8 |
dod'4\trill mi' la' |
sol' la' la' |
la' fad' si' |
si'2 mi'4 |
mi' mi'4. mi'8 |
mi'2 mi'4 |
la mi' mi' |
re'4 re'4. mi'8 |
dod'4\trill mi' la' |
sol' la' la' |
la' fad' si' |
si'2 mi'4 |
mi' mi'4. mi'8 |
mi'2 mi'4 |
fad' mi' mi' |
fad' mi'4. mi'8 |
dod'4\trill
