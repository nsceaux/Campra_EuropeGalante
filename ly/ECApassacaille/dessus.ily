\clef "dessus"
\tag #'(flute1 flute2 flutes) \startHaraKiri
dod''4 la'4. mi''8 |
mi''4 si'4. dod''8 |
la' sold' la' si' dod'' re'' |
mi''4 sold'8\trill sold' la' si' |
dod''4 la'4. mi''8 |
mi''4 si'4. dod''8 |
la' sold' la' si' dod'' re'' |
mi''4 sold'4.\trill la'8 |
la'4 la''8. la''16 dod''8.\trill re''16 |
mi''4. mi''8 fad'' sold'' |
la''4. fad''8 sold'' la'' |
sold''4.\trill fad''8 mi''4 |
r4 la''8. la''16 dod''8.\trill re''16 |
mi''4. mi''8 fad'' sold'' |
la''4. fad''8 sold'' la'' |
sold''4.\trill fad''8 mi''4 |
r mi'' re''8.\trill dod''16 |
si'8.\trill la'16 si'8. dod''16 dod''8.\trill si'16 |
la'4. la'8 si'4 |
dod''4 si'4.\trill la'8 |
la'4 mi'' re''8.\trill dod''16 |
si'8.\trill la'16 si'8. dod''16 dod''8.\trill si'16 |
la'4. la'8 si'4 |
dod'' si'4.\trill la'8 |
la'4 dod''8 dod'' re'' mi'' |
si'2\trill dod''4 |
la' re''4. dod''8 |
si'2\trill si'4 |
r dod''8 dod'' re'' mi'' |
si'2\trill dod''4 |
la' re''4. dod''8 |
si'2\trill si'4 |
r dod'' si'8.\trill la'16 |
mi''4. dod''8 re'' mi'' |
fad''4 sold'' mi'' |
la'' sold''4.\trill la''8 |
la''4 dod'' si'8.\trill la'16 |
mi''4. dod''8 re'' mi'' |
fad''4 sold'' mi'' |
la'' sold''4.\trill la''8 |
la''4 <<
  \twoVoices #'(flute1 flute2 flutes) <<
    { dod''8 re'' mi'' fad'' |
      mi'' fad'' mi'' dod'' re'' mi'' |
      fad'' sold'' la'' sol'' fad'' mi'' |
      re'' dod'' si' dod'' re'' si' |
      dod''4 dod''8 re'' mi'' fad'' |
      mi'' fad'' mi'' dod'' re'' mi'' |
      fad'' sold'' la'' sol'' fad'' mi'' |
      re'' dod'' si'4.\trill la'8 |
      la'4 }
    { la'8 si' dod'' re'' |
      dod'' re'' dod'' la' si' dod'' |
      re'' mi'' fad'' mi'' re'' dod'' |
      si' la' sold' la' si' sold' |
      la'4 la'8 si' dod'' re'' |
      dod'' re'' dod'' la' si' dod'' |
      re'' mi'' fad'' mi'' re'' dod'' |
      si' la' sold'4.\trill la'8 |
      la'4 }
    { <>^\markup\whiteout Flutes
      \stopHaraKiri s2 s2.*7 s4 \startHaraKiri }
  >>
  \tag #'violons {
    <>^\markup\whiteout Violons
    la'4\doux la' |
    la'4. sol'8 fad' mi' |
    re'4. dod'8 re' mi' |
    fad' re' mi'4 mi' |
    la la' la' |
    la'4. sol'8 fad' mi' |
    re'4. dod'8 re' mi' |
    fad' re' mi'4 mi' |
    la
  }
>> <>^\markup\whiteout Tous mi''4 mi'' |
mi''2 mi''8 mi'' |
la''4 fad''4.\trill si''8 |
sold''4.\trill fad''8 mi'' re'' |
dod''4\trill mi'' mi'' |
mi''2 mi''8 mi'' |
la''4 fad''4.\trill si''8 |
sold''4.\trill fad''8 mi'' re'' |
dod''4\trill mi''4. mi''8 |
fad''4 sold''\trill la'' |
si'' mi'' la''~ |
la''8 si'' sold''4.\trill la''8 |
la''4 mi''4. mi''8 |
fad''4 sold''\trill la'' |
si'' mi'' la''~ |
la''8 si'' sold''4.\trill la''8 |
la''4 %{%} la'4\doux si'8 do'' |
si'4\trill si' mi'' |
la' do'' re''\trill |
mi''2 mi''4 |
r la' si'8 do'' |
si'4\trill si' mi'' |
la' do'' re''\trill |
mi''2 mi''4 |
r mi'' la'' |
fad'' si''4. fad''8 |
sold''4\trill mi'' la''~ |
la''8 do'' si'4.\trill la'8 |
la'4 mi'' la'' |
fad'' si''4. fad''8 |
sold''4\trill mi'' la''~ |
la''8 do'' si'4.\trill la'8 |
la'4 <<
  \twoVoices #'(flute1 flute2 flutes) <<
    { mi''4 do'' |
      sol'' sol''4. sol''8 |
      sol''2 sol''4 |
      la'' la''4.(\trill sol''16 la'') |
      si''4 }
    { do''8 re'' mi'' do'' |
      re'' do'' re'' mi'' fa'' re'' |
      mi'' re'' mi'' fad'' sol''4 |
      sol'' fad''4.\trill( mi''16 fad'') |
      sol''4 }
    { <>^\markup\whiteout Flutes
      \stopHaraKiri s4 s2.*3 s2 \startHaraKiri }
  >>
  \tag #'violons {
    <>^\markup\whiteout Violons
    la'8\doux si' do'' la' |
    si' la' si' do'' re'' si' |
    do''4 do'' si' |
    la'2.\trill |
    sol'4
  }
>> <>^\markup\whiteout Tous sol''4. re''8 |
fa''4 fa''4. sol''8 |
mi''4\trill do''2~ |
do''4 sib'4. do''8 |
la'4\trill fa''4. la'8 |
si'4 sol''4. si'8 |
do''4. do''8 re''4 |
mi''4 re''4.\trill do''8 |
do''4 <<
  \twoVoices #'(flute1 flute2 flutes) <<
    { sol''4. sol''8 |
      do'''4. si''8 la'' sol'' |
      fad'' mi'' fad'' sol'' la'' fad'' |
      sol''4 sol''4. la''8 |
      fad''4 si''2 ~ |
      si''4 la''2 ~ |
      la''8 fad'' sol'' la'' sol''4 \trill~ |
      sol''8 fad'' fad''4.\trill mi''8 |
      mi''4 }
    { mi''4. mi''8 |
      la''4. sol''8 fad'' mi'' |
      red'' dod'' red'' mi'' fad'' red'' |
      mi''4 mi''4. fad''8 |
      red''4\trill si'8 dod'' red'' mi'' |
      dod''4\trill dod''8 red''? mi'' fad'' |
      red''4. red''8 mi''4~ |
      mi''8 fad'' red''4.\trill mi''8 |
      mi''4 }
    { <>^\markup\whiteout Flutes
      \stopHaraKiri s4 s2.*7 s2 \startHaraKiri }
  >>
  \tag #'violons {
    <>^\markup\whiteout Violons
    do''8\doux re'' do'' si' |
    la' sol' la' si' do'' la' |
    si'2 si'4 |
    mi'8 red' mi' fad'? sol' mi' |
    si' la' sol' la' si' sol' |
    la' sol' la' si' do'' la' |
    si'4. si'8 mi' fad' |
    sol' la' si'4 si |
    mi'
  }
>> <>^\markup\whiteout Tous mi''4. mi''8 |
si''2.~ |
si''8 sold'' la'' si'' mi''4 |
la'' sold''4. la''8 |
la''4 mi''4. mi''8 |
si''2.~ |
si''8 sold'' la'' si'' mi''4~ |
mi''8 si' si'4.\trill la'8 |
la'4 %{%} la'8. si'16 dod''8.\trill re''16 |
mi''8. re''16 mi''8. fad''16 sold''8. mi''16 |
fad''8. sold''16 la''8. fad''16 si''8. la''16 |
sold''8. la''16 sold''8. fad''16 mi''8. re''16 |
dod''8.\trill si'16 la'8. si'16 dod''8. re''16 |
mi''8. re''16 mi''8. fad''16 sold''8. mi''16 |
fad''8. sold''16 la''8. fad''16 si''8. la''16 |
sold''8.\trill la''16 sold''8. fad''16 mi''8. re''16 |
dod''8.\trill si'16 dod''8. re''16 mi''8. dod''16 |
fad''8. mi''16 re''8. dod''16 si'8. la'16 |
mi''8. dod''16 re''8. mi''16 fad''8. sold''16 |
la''8. si''16 sold''8.\trill fad''16 mi''4~ |
mi''8. re''16 dod''8. re''16 mi''8. dod''16 |
fad''8. mi''16 re''8. dod''16 si'8.\trill la'16 |
mi''8. dod''16 re''8. mi''16 fad''8. sold''16 |
la''8. dod''16 si'4.\trill la'8 |
la'4 la''4. mi''8 |
sol''4 sol''4. la''8 |
fad''4\trill si''4. fad''8 |
sold''4\trill mi''4. si'8 |
dod''4 la'4. dod''8 |
mi''4 si'4.\trill dod''8 |
re''4 dod''8.\trill si'16 la'4 |
si'8.\trill la'16 sold'4.\trill la'8 |
la'4 la''4. mi''8 |
sol''4 sol''4. la''8 |
fad''4\trill si''4. fad''8 |
sold''4\trill mi''4. si'8 |
dod''4 la'4. dod''8 |
mi''4 si'4.\trill dod''8 |
re''4 dod''8.\trill si'16 dod''4~ |
dod''8 re'' si'4.\trill la'8 |
la'4
