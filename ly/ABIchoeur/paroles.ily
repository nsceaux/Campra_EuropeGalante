Mor -- tels, que l’A -- mour vous en -- traî -- ne,
cé -- dez, cé -- dez à ses dou -- ces ar -- deurs ;
\tag #'vdessus {
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  à ja -- mais dans vos cœurs.
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
}
\tag #'vhaute-contre {
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il re -- gne,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
}
\tag #'vtaille {
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais,
  à ja -- mais dans vos cœurs.
}
\tag #'vbasse {
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
}

Mor -- tels, que l’A -- mour vous en -- traî -- ne,
\tag #'(vdessus vhaute-contre) {
  cé -- dez, cé -- dez à ses dou -- ces ar -- deurs ;
}

\tag #'vdessus {
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
}
\tag #'vhaute-contre {
  Qu’il vous en -- chaî -- ne,
  qu’il re -- gne,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  
  Qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il regne à ja -- mais dans vos cœurs,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
}
\tag #'vtaille {
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais, à ja -- mais dans vos cœurs.
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  
  Qu’il regne à ja -- mais,
  qu’il regne à ja -- mais,
  qu’il re -- gne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
}
\tag #'vbasse {
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  Qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
  
  Qu’il vous bles -- se, qu’il vous en -- chaî -- ne,
  qu’il regne à ja -- mais,
  qu’il regne à ja -- mais dans vos cœurs.
}
