<<
  \tag #'vdessus {
    \clef "vdessus" la'4 |
    re'' re'' do'' |
    si' si' la' |
    la'( sol'4.\trill fad'8) |
    fad'2\trill re''4 |
    re''2 si'4 |
    dod''4 dod''8. si'16( la'4) |
    la'4 sold'4. la'8 |
    la'2 r4 |
    la' la' la' |
    si'8[\melisma la' si' do'' re'' si'] |
    do''[ re'' do'' si' la' sol'] |
    fad'[ mi' fad' sol' la' fad']( |
    sol'4)\melismaEnd mi' si' |
    mi'' mi'' mi'' |
    mi'' mi'' mi'' |
    mi'' red''4.\trill mi''8 |
    mi''4 mi'' mi'' |
    dod''2\trill dod''4 |
    re'' re'' re'' |
    mi''8[\melisma re'' dod'' re'' mi'' dod''] |
    re''[ mi'' fad'' mi'' re'' dod'']( |
    si'4\trill)\melismaEnd si' re'' |
    sol'' sol'' sol'' |
    sol'' fad'' sol'' |
    mi'' mi''4.\trill re''8 |
    re''4 r r |
    R2.*7 |
    r4 r re'' |
    red''\trill red'' mi'' |
    si' si' si' |
    si'2( la'4)\trill |
    si'2 <<
      { \voiceOne
        red''4 |
        mi''2 mi''4 |
        fad'' fad'' mi'' |
        mi'' red''4.\trill mi''8 |
        mi''4 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo
        si'4 |
        si'2 la'4 |
        la' la' sol' |
        sol' fad'4.\trill mi'8 |
        mi'4
      }
    >> mi''4 mi'' |
    dod''2\trill dod''4 |
    fad'' fad'' re'' |
    si'8[ la' si' dod'' re'' mi'']( |
    dod''4) dod'' la' |
    mi'' mi'' mi'' |
    mi'' re'' fad'' |
    si'\trill si' mi'' |
    dod''2\trill la'4 |
    re'' re'' re'' |
    re'' re''4. re''8 |
    re''4 dod''4. dod''8 |
    lad'4 dod'' dod'' |
    fad''2 fad''4 |
    si' si' mi'' |
    dod''8[\trill si' dod'' re'' mi'' dod'']( |
    re''4) si' si' |
    fad'' fad'' fad'' |
    fad'' mi'' sol'' |
    dod'' dod''4.\trill si'8 |
    si'2 r4 |
    si' si' si' |
    dod''8[\melisma si' dod'' re'' mi'' dod''] |
    re''[ dod'' re'' mi'' fad'' re'']( |
    mi''4)\melismaEnd mi'' mi'' |
    la' la' re'' |
    dod''2\trill dod''4 |
    fad'' fad'' fad'' |
    fad'' fad'' sol'' |
    mi'' mi''4.\trill re''8 |
    re''4 r r |
    R2.*2 |
    re''4 re'' re'' |
    mi''8[\melisma re'' mi'' fad'' sol'' mi''] |
    fad''[ sol'' fad'' mi'' re'' dod''] |
    si'[ la' si' dod'' re'' mi''] |
    dod''[\trill si' dod'' re'' mi'' dod''] |
    re''[ dod'' re'' mi'' fad'' re'']( |
    mi''4)\melismaEnd mi'' la' |
    la' la' re'' |
    dod''2\trill dod''4 |
    fad'' fad'' fad'' |
    fad'' fad'' sol'' |
    mi'' mi''4.\trill re''8 |
    re''2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fad'4 |
    fad' sol' la' |
    re' re' re' |
    re'2( dod'4) |
    re'2 re'4 |
    mi'2 mi'4 |
    mi' mi' fad' |
    mi' mi'4. mi'8 |
    mi'4 mi' mi' |
    fad'2 fad'4 |
    re' re' re' |
    la' la' mi' |
    si' si' si' |
    si'2 sol'4 |
    sol' sol' sol' |
    sol' sol' la' |
    fad' fad'4.\trill mi'8 |
    mi'2. |
    mi'4 mi' la' |
    fad'\trill fad' re' |
    la' la' la' |
    la'2 la'4 |
    sol'4 sol' sol' |
    sol'2 si'4 |
    si' si' si' |
    la' la'4. la'8 |
    fad'4\trill r r |
    R2.*7 |
    r4 r si' |
    la' la' sol' |
    fad' fad' sol' |
    mi'2. |
    red'2\trill si4 |
    dod'2 dod'4 |
    red' red' mi' |
    si si si |
    mi'2 r4 |
    mi'4 mi' la' |
    fad'8[\melisma mi' fad' sol' la' si'] |
    sold'[ fad' sold' la' si' sold']( |
    la'4)\melismaEnd la' la' |
    la' la' mi' |
    la' la' la' |
    la' sold'4. la'8 |
    la'2 r4 |
    r r re' |
    sol' sol' sol' |
    sol'2 sol'4 |
    fad' fad' fad' |
    fad' fad'4. fad'8 |
    mi'2 si4 |
    fad' fad' fad' |
    fad'2 fad'4 |
    fad' fad' fad' |
    sol' sol' sol' |
    fad' fad'4. fad'8 |
    red'2 si4 |
    mi' mi' mi' |
    mi' mi' la' |
    fad'2\trill re'4 |
    la' la' la' |
    la' si'8[ la'] sol'[ fad'] |
    mi'2\trill mi'4 |
    la' la' la' |
    fad' fad' mi' |
    mi' mi' la' |
    fad'\trill r r |
    R2.*2 |
    r4 r re' |
    la' la' la' |
    la'2 la'4 |
    la' sol' sol' |
    sol' mi' la' |
    fad'2\trill fad'4 |
    la'4 la' la' |
    la' si'8[ la'] sol'[ fad'] |
    mi'2\trill mi'4 |
    la' la' la' |
    fad' fad' mi' |
    mi' mi'\trill la' |
    fad'2.\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 |
    re' mi' fad' |
    sol' re' re' |
    mi'2. |
    la2 la4 |
    si2 si4 |
    la mi' re' |
    re' si mi' |
    dod'2.\trill |
    r4 re' re' |
    si\trill si sol' |
    mi'2 mi'4 |
    red'8[ dod' red' mi' fad' red']( |
    mi'4) mi' r |
    r r si |
    mi' mi' do' |
    si si4. si8 |
    si2. |
    la4 la la |
    re'8[ dod' re' mi' fad' sol']( |
    mi'4)\trill mi' mi' |
    fad' fad'4. fad'8 |
    re'2 si4 |
    mi' mi' mi' |
    mi' re' re' |
    re' dod'4.\trill re'8 |
    re'4 r r |
    R2.*7 |
    r4 r re' |
    fad' fad' si |
    si si sol |
    sol2( la4) |
    fad2\trill r4 |
    R2.*3 |
    si4 si si |
    la8[ si dod' re' mi' dod']( |
    re'4) re' la |
    mi' mi' mi' |
    mi'2 dod'4 |
    dod' dod' dod' |
    dod' la la |
    mi' mi'4. mi'8 |
    mi'2 r4 |
    r4 re' re' |
    si2\trill si4 |
    mi'4 mi' mi' |
    dod'8[\melisma re' dod' si lad sol] |
    fad[ mi fad sold la si] |
    sold[ fad sold la si dod'] |
    lad[ sold lad si dod' lad]( |
    si4)\melismaEnd si si |
    re' re' re' |
    re' si si |
    si lad4. si8 |
    si2 r4 |
    r mi' mi' |
    dod'2\trill la4 |
    re' re' re' |
    la8[ si dod' re' mi' dod']( |
    re'4) re' la |
    la la dod' |
    dod'2 dod'4 |
    re' re' re' |
    re' dod'4.\trill re'8 |
    re'4 r r |
    R2.*3 |
    r4 r la |
    re' re' re' |
    re'2 si4 |
    dod' dod' la |
    re'2 re'4 |
    la8[ si dod' re' mi' dod']( |
    re'4) re' la |
    la la dod' |
    dod'2 dod'4 |
    re' re' re' |
    re' dod'4.\trill re'8 |
    re'2. |
  }
  \tag #'vbasse {
    \clef "vbasse" re'4 |
    si si la |
    sol sol fad |
    mi2.\trill |
    re2 fad4 |
    sold2\trill sold4 |
    la dod re |
    mi mi8[ re] mi4 |
    la, la la |
    fad2\trill fad4 |
    sol sol sol |
    la8[ sol la si do' la]( |
    si4) si si, |
    mi mi mi |
    mi2 mi4 |
    do'4 do' la |
    si si si, |
    mi2. |
    la4 la la |
    si8[\melisma la si dod' re' si] |
    dod'[ si la si dod' la]( |
    re'4)\melismaEnd re' re |
    sol4 sol sol |
    sol2 mi4 |
    si si sol |
    la la la, |
    re r r |
    R2.*7 |
    r4 r sol |
    fad fad mi |
    red red mi |
    do( do4.\trill si,8) |
    si,2 r4 |
    R2.*2 |
    r4 si si |
    sold2 sold4 |
    la la la |
    re8[ dod re mi fad re]( |
    mi4) mi mi |
    la la la |
    la2 dod4 |
    fad fad re |
    mi mi4. mi8 |
    la,2 r4 |
    R2. |
    r4 sol sol |
    mi2\trill mi4 |
    fad fad fad |
    re8[\melisma dod re mi fad re] |
    mi[ re mi fad sol mi]( |
    fad4)\melismaEnd fad fad |
    si si si |
    si2 si,4 |
    sol sol mi |
    fad fad4. fad8 |
    si2 r4 |
    R2. |
    la4 la la |
    si8[\melisma la si dod' re' si] |
    dod'[ si la si dod' la]( |
    re'4)\melismaEnd re' re |
    la4 la la |
    la2 fad4 |
    si si sol |
    la la la, |
    re r r |
    R2.*3 |
    r4 la la |
    fad2\trill fad4 |
    sol sol sol |
    la8[\melisma sol la si dod' la] |
    si[ la si dod' re' si] |
    dod'[ si la si dod' la]( |
    re'4)\melismaEnd re' re |
    la la la |
    la2 fad4 |
    si si sol |
    la la la, |
    re2. |
  }
>>
