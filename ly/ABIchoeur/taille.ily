\clef "taille" re'4 |
re' sol' fad' |
sol' sol' re' |
mi'2. |
fad'2 fad'4 |
mi'2 mi'4 |
mi' mi' re' |
re' si mi' |
dod' dod' dod' |
re'2 re'4 |
re' re' re' |
do'2 do'4 |
si si si |
si8 do' si la sol fad |
mi2 si4 |
mi' mi' fad' |
fad' fad'4.\trill mi'8 |
mi'2 mi'4 |
mi' mi' la' |
fad'2\trill re'4 |
la' la' la' |
la' re' re' |
re' re' sol' |
sol' si' si' |
si'2 si'4 |
la' la'4. la'8 |
fad'2\trill la'4 |
si' si' si' |
sol'4. fad'8 mi'4 |
fad'8 mi' fad' sol' fad' mi' |
re'4 re' re' |
re'8 dod' re' mi' fa' sol' |
mi'4 la' si' |
la' la'4.\trill sol'8 |
sol'2 sol'4 |
la' fad' sol' |
la' la' sol' |
sol'2 la'4 |
fad'2\trill si4 |
dod'2 dod'4 |
red' red' mi' |
si si si |
mi' mi' mi' |
mi'8 re' dod' re' mi' dod' |
re'4 fad' fad' |
mi' mi' mi' |
mi'2 mi'4 |
mi'2 mi'4 |
la' la' la |
mi' mi'4. mi'8 |
mi'2 r4 |
r r re' |
sol' sol' sol' |
sol'2 mi'4 |
dod'8 si dod' re' mi' dod' |
re'4 fad' fad' |
mi'2 si4 |
fad' fad' fad' |
fad'2 fad'4 |
fad' fad' fad' |
sol'4 sol' sol' |
fad' fad' fad' |
red'2 si4 |
mi' mi' mi' |
mi' mi'4. mi'8 |
re'2 re'4 |
dod'8 re' dod' re' mi' dod' |
re'4 re' re' |
mi'2 mi'4 |
la' la' la' |
fad' fad' si' |
la' la'4. la'8 |
fad' mi' re' mi' fad' sol' |
mi' re' mi' fad' sol' mi' |
fad'4 fad' fad' |
re'4. dod'8 si4 |
mi'8 re' dod' si la4 |
la2 re'4 |
re' sol'4. sol'8 |
sol'4 mi' la' |
la'2 re'4 |
la' la' la' |
la' si'8 la' sol' fad' |
mi'2\trill mi'4 |
la' la' la' |
fad' fad' mi' |
mi' mi'4. fad'8 |
fad'2. |
