\clef "haute-contre" fad'4 |
fad' sol' la' |
re' re' re' |
re'2 dod'4 |
re'2 la'4 |
si'2 si'4 |
la' mi' fad' |
mi' mi'4. mi'8 |
mi'4 mi' mi' |
fad'2 fad'4 |
re' sol' sol' |
mi'2 mi'4 |
red'8 dod' red' mi' fad' red' |
mi' red' mi' fad' sol' la' |
si'4 si' si' |
do''4 do'' do'' |
si' si'4. si'8 |
sold'8 fad' sold' la' si' sold' |
la'4 la' la' |
la'2 sol'4 |
sol' mi' la' |
fad'8 sol' la' sol' la' fad' |
si'4 si' si' |
mi'' mi'' mi'' |
mi'' re'' re'' |
re'' dod''4.\trill re''8 |
re''2 re''4 |
re''8 do'' re'' mi'' fa'' re'' |
mi'' fa'' mi'' re'' do'' si' |
la'4\trill fad' fad' |
sol' sol' sol' |
sol'2 sol'4 |
sol' do'' re'' |
re'' re''4. re''8 |
si'2\trill si'4 |
la' la' sol' |
fad' fad' sol' |
mi'2. |
red'2 si4 |
dod'2 dod'4 |
red' red' mi' |
si si si |
mi' si' si' |
la' la'8 si' la' sol' |
fad' mi' fad' sol' la' si' |
sold' fad' sold' la' si' sold' |
la'4 la' la' |
dod''2 la'4 |
la' la' la' |
la' sold'4. la'8 |
la'2 la'4 |
re'' re'' re'' |
re''4. dod''8 si' la' |
sol'4 sol'4. sol'8 |
fad'4 lad' lad' |
si' fad'8 sold' la' si' |
sold' fad' sold' la' si' dod'' |
lad' sold' lad' si' dod'' lad' |
si'4 si' si' |
si'2 si'4 |
si' si' si' |
si' lad'4. si'8 |
si'2. |
si'4 si' si' |
dod''8 si' la' si' la' sol' |
fad'4 fad' si' |
la'2 la'4 |
la' la' la' |
la'2 la'4 |
la' dod'' dod'' |
re'' re'' re'' |
re'' dod''4.\trill re''8 |
re''2 re''4 |
mi''8 re'' dod'' re'' mi'' dod'' |
re''4 la' la' |
la'4 sold'4. la'8 |
la' si' dod'' re'' mi'' dod'' |
re''4 la' la' |
sol' si' si' |
mi' la' la' |
fad'2\trill si'4 |
sol' mi' mi' |
fad'8 mi' fad' sol' la'4 |
la'2 la'4 |
la' dod'' dod'' |
re'' re'' re'' |
re'' dod''4.\trill re''8 |
re''2. |
