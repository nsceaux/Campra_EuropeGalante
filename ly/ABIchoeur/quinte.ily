\clef "quinte" re'4 |
re' si do' |
re' sol la |
si2 sol4 |
la2 re'4 |
si2 mi'4 |
dod' la si |
si si4.\trill la8 |
la2. |
r4 re' re' |
si si sol' |
sol'2 fad'4 |
fad' si4. la8 |
sol la si do' si la |
sol4 sol mi' |
mi' mi' mi' |
si si4. si8 |
si2. |
la4 la la |
re'8 dod' re' mi' fad' sol' |
mi'4\trill mi' mi' |
re' re' la |
si si si |
si2 si4 |
si re' re' |
la la4. la8 |
la4 re' re' |
re'2 re'4 |
do'8 re' mi'4 mi' |
re'4 re' re' |
re'8 do' si do' si la |
sol2 sol4 |
sol la re |
re re4. re8 |
re2 re'4 |
fad' fad' si |
si si si |
mi2. |
si2 si4 |
dod'2 dod'4 |
red' red' mi' |
si2 si4 |
mi' si mi' |
mi' la4. la8 |
la4 re' re' |
re' si si |
dod' dod' dod' |
dod'2 dod'4 |
la la si |
si si4.\trill la8 |
la2 r4 |
r re' re' |
si2 si4 |
si dod' dod' |
dod' fad fad |
fad2 fad4 |
mi sold4. sold8 |
fad4 fad fad' |
re'8 mi' fad' mi' re' dod' |
si2 si4 |
si si dod' |
dod' dod' fad |
fad2 r4 |
r mi' mi' |
dod'2\trill dod'4 |
si2 si4 |
mi'2 la'8 sol' |
fad' mi' re'4 la |
la la la |
la2 la4 |
si si mi' |
mi' la4. la8 |
la4 re' re' |
la2 la4 |
re' re' re' |
re'8 dod' si4 si |
dod'2 dod'4 |
la re' re' |
re'2 si4 |
dod' dod' la |
re'2 re'4 |
la8 si dod' re' mi' dod' |
re'4 re' la |
la2 dod'4 |
dod' dod' la |
si si si |
la la4. la8 |
la2. |
