\clef "basse" re'4 |
si si la |
sol sol fad |
mi2.\trill |
re2 fad4 |
sold2 sold4 |
la dod re |
mi mi mi, |
la, la la |
fad2\trill fad4 |
sol sol sol |
la8 sol la si do' la |
si4 si si, |
mi mi mi |
mi2 mi4 |
do' do' la |
si si si, |
mi2 mi4 |
la la la |
si8 la si dod' re' si |
dod' si la si dod' la |
re'4 re' re |
sol sol sol |
sol2 mi4 |
si si sol |
la la la, |
re re' re' |
si8 la si do' re' si |
do' si do' re' mi' do' |
re'4 re' re |
sol sol sol |
sol2 si,4 |
do la, sol, |
re re,2 |
sol, sol4 |
fad fad mi |
red red mi |
do2. |
si,2 \clef "alto/bass" si4 |
dod'2 dod'4 |
red' red' mi' |
si \clef "bass" si si |
sold2 sold4 |
la la la |
re8 dod re mi fad re |
mi4 mi mi |
la la la |
la2 dod4 |
fad fad re |
mi mi mi, |
la,2 r4 |
R2. |
r4 sol sol |
mi2 mi4 |
fad fad fad |
re8 dod re mi fad re |
mi re mi fad sol mi |
fad4 fad fad |
si si si |
si2 si,4 |
sol sol mi |
fad fad4. fad8 |
si,2 r4 |
R2. |
la4 la la |
si8 la si dod' re' si |
dod' si la si dod' la |
re'4 re' re |
la la la |
la2 fad4 |
si si sol |
la la la, |
re re' re' |
dod'2 la4 |
fad fad re |
si,2. |
la,4 la la |
fad2 fad4 |
sol sol sol |
la8 sol la si dod' la |
si la si dod' re' si |
dod' si la si dod' la |
re'4 re' re |
la la la |
la2 fad4 |
si si sol |
la la la, |
re2. |
