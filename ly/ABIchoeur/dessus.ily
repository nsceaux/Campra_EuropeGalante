\clef "dessus" la'4 |
re'' re'' do'' |
si' si' la' |
la' sol'4.\trill( fad'8) |
fad'2\trill re''4 |
re''2 si'4 |
dod'' dod'' \appoggiatura si'16 la'4 |
la' sold'4.\trill la'8 |
la'2 r4 |
la' la' la' |
si'8 la' si' do'' re'' si' |
do'' re'' do'' si' la' sol' |
fad' mi' fad' sol' la' fad' |
sol'4 mi' si' |
mi'' mi'' mi'' |
mi'' mi'' mi'' |
mi'' red''4.\trill mi''8 |
mi''4 mi'' mi'' |
dod''2\trill dod''4 |
re'' re'' re'' |
mi''8 re'' dod'' re'' mi'' dod'' |
re'' mi'' fad'' mi'' re'' dod'' |
si'4\trill si' re'' |
sol'' sol'' sol'' |
sol'' fad'' sol'' |
mi'' mi''4.\trill re''8 |
re''2 re''4 |
sol'' sol'' sol'' |
sol''2 sol''4 |
fad''8\trill sol'' fad'' mi'' re'' do'' |
si' la' sol' la' si' sol' |
re''4 re'' re'' |
mi'' fad'' sol'' |
sol'' fad''4.\trill sol''8 |
sol''2 re''4 |
red'' red'' mi'' |
si' si' si' |
si'2 la'4\trill |
si'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { red''4 | mi''2 mi''4 | fad'' fad'' mi'' |
    mi'' red''4.\trill mi''8 | mi''4 }
  { si'4 | si'2 la'4 | la' la' sol' |
    sol' fad'4.\trill mi'8 | mi'4 }
>> mi''4 mi'' |
dod''2\trill dod''4 |
fad'' fad'' re'' |
si'8 la' si' dod'' re'' mi'' |
dod''4 dod'' la' |
mi'' mi'' mi'' |
mi'' re'' fad'' |
si' si'4. mi''8 |
dod''8\trill re'' mi'' fad'' sol'' mi'' |
fad'' mi'' fad'' sol'' la'' fad'' |
sol''4 re''4. re''8 |
re''4 dod''4. dod''8 |
lad'4 dod'' dod'' |
fad''2 fad''4 |
si' si' mi'' |
dod''8\trill si' dod'' re'' mi'' dod'' |
re''4 si' si' |
fad'' fad'' fad'' |
fad'' mi'' sol'' |
dod'' dod''4. si'8 |
si'4 si'' si'' |
sold''8 fad'' sold'' la'' si'' sold'' |
la''4 dod''8 re'' mi'' dod'' |
re'' dod'' re'' mi'' fad'' re'' |
mi''4 mi'' mi'' |
fad'' sol''8( fad'') mi''( re'') |
dod''2\trill dod''4 |
fad'' fad'' fad'' |
fad'' fad'' sol'' |
mi'' mi''4.\trill re''8 |
re''2 re''4 |
la'' la'' la'' |
la''2 la'4 |
si'8 la' si' dod'' re'' mi'' |
dod'' re'' mi'' fad'' sol'' mi'' |
fad'' sol'' fad'' mi'' re'' dod'' |
si' la' si' dod'' re'' mi'' |
dod'' si' dod'' re'' mi'' dod'' |
re'' dod'' re'' mi'' fad'' re'' |
mi''4 mi'' la' |
la' la' re'' |
dod''2\trill dod''4 |
fad'' fad'' fad'' |
fad'' fad'' sol'' |
mi'' mi''4.\trill re''8 |
re''2. |
