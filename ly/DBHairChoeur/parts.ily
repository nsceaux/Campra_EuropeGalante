\piecePartSpecs
#`((dessus #:music , #{ s2 s2.*31 s4\break s2 s2.*31 s4\break #})
   (haute-contre #:music , #{ s2 s2.*31 s4\break s2 s2.*31 s4\break #})
   (taille #:music , #{ s2 s2.*31 s4\break s2 s2.*31 s4\break #})
   (quinte #:music , #{ s2 s2.*31 s4\break s2 s2.*31 s4\break #})
   (basse #:music , #{ s2 s2.*31 s4\break s2 s2.*31 s4\break #})
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#96 #}))
