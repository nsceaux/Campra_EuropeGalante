\clef "dessus" r4 r |
R2.*15 |
r4 re'' la' |
si' dod'' re'' |
dod''\trill re'' mi'' |
fad'' sol'' fad'' |
mi''\trill re'' la' |
si' dod'' re'' |
dod''\trill re'' mi'' |
fad'' sol'' fad'' |
mi''\trill mi'' fad'' |
re'' si' mi'' |
dod''\trill re'' mi'' |
fad'' mi''4.\trill re''8 |
re''4 mi'' fad'' |
re'' si' mi'' |
dod'' re'' mi'' |
fad'' mi''4.\trill re''8 |
re''4
\repeat unfold 4 { r4 r4 R2.*15 r4 }
