\clef "quinte" r4 r |
R2.*15 |
r4 re' re' |
si mi' la |
la la sol |
fad la la |
la re' re' |
si mi' la |
la la sol |
fad la la |
la dod' la |
si si si |
dod' la mi' |
la la4. la8 |
la4 dod' la |
si si si |
dod' la mi' |
la la4. la8 |
la4
\repeat unfold 4 { r4 r4 R2.*15 r4 }
