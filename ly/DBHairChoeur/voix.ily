<<
  \tag #'(vdessus basse) {
    \clef "vbas-dessus"
    <>^\markup\character-text Une Venitienne déguisée
    re''4 la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill re'' la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4 mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4
  }
  \tag #'vhaute-contre { \clef "vhaute-contre" r4 r R2.*15 r4 }
  \tag #'vtaille { \clef "vtaille" r4 r R2.*15 r4 }
  \tag #'vbasse { \clef "vbasse" r4 r R2.*15 r4 }
>>
<<
  \tag #'(vdessus basse) {
    re''4 la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill re'' la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4 mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4
  }
  \tag #'vhaute-contre {
    fad'4 fad' |
    re' sol' fad' |
    mi'\trill fad' sol' |
    la' la' la' |
    la' fad' fad' |
    re' sol' fad' |
    mi'\trill fad' sol' |
    la' la' la' |
    la' la' la' |
    fad' sol' sol' |
    mi' fad' sol' |
    la' la'4. la'8 |
    fad'4 la' la' |
    fad' sol' sol' |
    mi' fad' sol' |
    la' la'4. la'8 |
    fad'4
  }
  \tag #'vtaille {
    re'4 re' |
    si mi' la |
    la re' dod' |
    re' mi' re' |
    dod'\trill re' re' |
    si mi' la |
    la re' dod' |
    re' mi' re' |
    dod'\trill dod' dod' |
    si si si |
    la re' dod' |
    re' dod'4.\trill re'8 |
    re'4 dod' dod' |
    si si si |
    la re' dod' |
    re' dod'4.\trill re'8 |
    re'4
  }
  \tag #'vbasse {
    re'4 fad |
    sol mi re |
    la fad mi |
    re dod re |
    la re' fad |
    sol mi re |
    la fad mi |
    re dod re |
    la la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4 la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4
  }
>>
<<
  \tag #'(vdessus basse) {
    <>^\markup\character La Venitienne
    re''4 dod'' |
    si' si' dod'' |
    lad' si' dod'' |
    re'' dod'' si' |
    lad'\trill fad' sold'8 lad' |
    sold'4 lad' si' |
    lad' si' dod'' |
    re''( dod''2) |
    si'4 re'' dod'' |
    si' dod''\trill re'' |
    mi'' re'' dod'' |
    si' dod'' la' |
    sold' mi'' re''8\trill dod'' |
    si'4 dod'' sold' |
    la' si' dod'' |
    dod''( si'2)\trill |
    la'4
  }
  \tag #'(vhaute-contre vtaille vbasse) { r4 r R2.*15 r4 }
>>
<<
  \tag #'(vdessus basse) {
    re''4 la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill re'' la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4 mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4
  }
  \tag #'(vhaute-contre vtaille) { r4 r R2.*15 r4 }
  \tag #'vbasse {
    re'4 fad |
    sol mi re |
    la fad mi |
    re dod re |
    la re' fad |
    sol mi re |
    la fad mi |
    re dod re |
    la la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4 la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4
  }
>>
<<
  \tag #'(vdessus basse) {
    <>^\markup\character La Venitienne
    fad''4 fad'' |
    sol'' fad'' mi'' |
    red''\trill si' la' |
    sol' fad'\trill mi' |
    si' sol' la'8 si' |
    do''4 si' la' |
    sol' fad' sol' |
    sol'( fad'2\trill) |
    mi'4 sol' fad' |
    mi' fad' sol' |
    la' si' do'' |
    si' dod''! re'' |
    dod''\trill la' la'8 sol' |
    fad'4\trill sol' la' |
    si' dod'' re'' |
    re''2( dod''4) |
    re''
  }
  \tag #'(vhaute-contre vtaille vbasse) { r4 r R2.*15 r4 }
>>
<<
  \tag #'(vdessus basse) {
    re''4 la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill re'' la' |
    si' dod'' re'' |
    dod''\trill re'' mi'' |
    fad'' sol'' fad'' |
    mi''\trill mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4 mi'' fad'' |
    re'' si' mi'' |
    dod''\trill re'' mi'' |
    fad'' mi''4.\trill re''8 |
    re''4
  }
  \tag #'(vhaute-contre vtaille) { r4 r R2.*15 r4 }
  \tag #'vbasse {
    re'4 fad |
    sol mi re |
    la fad mi |
    re dod re |
    la re' fad |
    sol mi re |
    la fad mi |
    re dod re |
    la la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4 la fad |
    si sol mi |
    la fad mi |
    re la,4. re8 |
    re4
  }
>>

