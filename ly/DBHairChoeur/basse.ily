\clef "basse"
\setMusic #'refrain {
  re'4 fad |
  sol mi re |
  la fad mi |
  re dod re |
  la re' fad |
  sol mi re |
  la fad mi |
  re dod re |
  la la fad |
  si sol mi |
  la fad mi |
  re la,2 |
  re4 la fad |
  si sol mi |
  la fad mi |
  re la,2 |
  re,4
}
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue \keepWithTag #'() \refrain
>>
\refrain
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue {
    re4 re |
    sol sol mi |
    fad re dod |
    si, lad, si, |
    fad re mi8 fad |
    mi4 fad sol |
    fad re mi |
    fad fad,2 |
    si,4 re' re' |
    mi' mi' re' |
    dod' si la |
    sold la la, |
    mi dod si,8 la, |
    sold,4 la, mi |
    fad sold la |
    re mi mi, |
    la,
  }
>>
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue \keepWithTag #'() \refrain
>>
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue {
    re'4 re' |
    sol la la |
    si sol red |
    mi re do |
    si, mi fad8 sol |
    la4 si do' |
    la si mi |
    la, si,2 |
    mi,4 mi mi |
    la la sol |
    fad mi re |
    sol mi re |
    la, la si8 dod' |
    re'4 si fad |
    sol mi re |
    la la,2 |
    re4
  }
>>
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue \keepWithTag #'() \refrain
>>
