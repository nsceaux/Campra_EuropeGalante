\tag #'(vdessus basse) {
  For -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer,
  for -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer ;
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
}
For -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer,
for -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer ;
Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
Il n’est per -- mis i -- cy que de rire & d’ai -- mer.

\tag #'(vdessus basse) {
  Ban -- nis -- sons de ces lieux l’im -- por -- tu -- ne rai -- son,
  el -- le vaut moins qu’une ai -- ma -- ble fo -- li -- e :
  Un doux ex -- cés sied bien dans la jeu -- ne sai -- son ;
  Pour être heu -- reux, il faut qu’un cœur s’ou -- bli -- e.
}
\tag #'(vdessus vbasse basse) {
  For -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer,
  for -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer ;
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
}
\tag #'(vdessus basse) {
  Ren -- dez- vous, jeu -- nes cœurs, cé -- dez à vos de -- sirs,
  tout vous ins -- pire un ten -- dre ba -- di -- na -- ge ;
  Ne pre -- fe -- rez ja -- mais la sa -- gesse aux plai -- sirs ;
  il vaut bien mieux estre heu -- reux qu’ê -- tre sa -- ge.
}
\tag #'(vdessus vbasse basse) {
  For -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer,
  for -- mons d’ai -- ma -- bles jeux, lais -- sons- nous en -- fla -- mer ;
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
  Il n’est per -- mis i -- cy que de rire & d’ai -- mer.
}
