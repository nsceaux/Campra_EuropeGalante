\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s2.*7\break s2.*8 s4\break
        s2 s2.*7\pageBreak s2.*8 s4\pageBreak
        s2 s2.*7 s4\break \bar "" s2 s2.*7 s4\break
        s2 s2.*7\break s2.*8 s4\pageBreak
        s2 s2.*7 s4\break \bar "" s2 s2.*7 s4\break
        s2 s2.*7\break s2.*8 s4\pageBreak
      }
      \modVersion {
        s2 s2.*15 s4\break
        s2 s2.*15 s4\break
        s2 s2.*15 s4\break
        s2 s2.*15 s4\break
        s2 s2.*15 s4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
