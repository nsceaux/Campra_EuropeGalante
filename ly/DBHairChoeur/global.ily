\key re \major \midiTempo#160
\digitTime\time 3/4 \partial 2
s2 s2.*15 s4 \bar "||"
\beginMark\markup\smallCaps Chœur
s2 s2.*15 s4 \bar "||"
s2 s2.*15 s4 \bar "||"
\beginMark\markup {
  \smallCaps Chœur \hspace#1
  \vcenter\column\fontsize#-2 \italic {
    \line { On peut se servir de celui-cy en trio, }
    \line { ou bien retourner cy-devant. }
  }
}
s2 s2.*15 s4 \bar "||"
s2 s2.*15 s4 \bar "||"
\beginMark\markup {
  \smallCaps Chœur \hspace#1
  \vcenter\column\fontsize#-2 \italic {
    \line { On peut se servir de celui-cy en trio, }
    \line { ou bien retourner cy-devant. }
  }
}
s2 s2.*15 s4 \bar "|."
