\clef "haute-contre" r4 r |
R2.*15 |
r4 la' la' |
sol' sol' la' |
la' re'' dod'' |
re'' mi'' re'' |
dod''\trill la' la' |
sol' sol' la' |
la' re'' dod'' |
re'' mi'' re'' |
dod''\trill dod'' dod'' |
si' si' si' |
la' re'' dod'' |
re'' dod''4.\trill re''8 |
re''4 dod'' dod'' |
si' si' si' |
la' re'' dod'' |
re'' dod''4.\trill re''8 |
re''4
\repeat unfold 4 { r4 r4 R2.*15 r4 }
