\clef "taille" r4 r |
R2.*15 |
r4 fad' fad' |
re' sol' fad' |
mi' fad' sol' |
la' la' la' |
la' fad' fad' |
re' sol' fad' |
mi' fad' sol' |
la' la' la' |
la' la' la' |
fad' sol' sol' |
mi' fad' sol' |
la' la'4. la'8 |
fad'4 la' la' |
fad' sol' sol' |
mi' fad' sol' |
la' la'4. la'8 |
fad'4
\repeat unfold 4 { r4 r4 R2.*15 r4 }
