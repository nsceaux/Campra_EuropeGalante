\clef "quinte" sib2 sib4 |
sib4. sib8 lab4 |
sol do'4. fa8 |
fa2 fa4 |
fa' fa' fa' |
mib'2 mib'4 |
mib' do'4.\trill( sib16 do') |
re'2 re'4 |
la4 sol fa |
do'2 re'4 |
sib sib do' |
re'2 re'4 |
re' do' sib |
sib la sol |
sol sol la |
la2 sib4 |
sib la sol |
sol2. |
la4 la sol |
fa2. |
sib4 mib' re' |
sol4. fa8 mib4 |
fa2 fa4 |
fa2 fa4 |
