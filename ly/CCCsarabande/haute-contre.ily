\clef "haute-contre" sib'4 sib' fa' |
sol'4. sol'8 re'4 |
mib' mib'4. fa'8 |
fa'2 fa'4 |
fa' sib' sib' |
sib'2 sib'4 |
sib' la'4.\trill( sol'16 la') |
sib'2 sib'4 |
la' la' si' |
do''2 sib'4 |
re'' re'' do'' |
la'2 la'4 |
sib' do'' re'' |
sol' fad' sol' |
sol' sol'4. sol'8 |
fad'2 sol'4 |
sol' la' sib' |
do''2. |
la'4 sib' do'' |
re''2 do''4 |
sib' do'' re'' |
mib'' sol' sol' |
fa' sib' sib' |
la'2\trill sib'4 |
