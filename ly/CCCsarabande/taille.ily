\clef "taille" fa' mib' re' |
mib'4. mib'8 fa'4 |
sib4. la8 sib4 |
la2\trill la4 |
re' re' re' |
mib'2 sib4 |
fa' fa'4. fa'8 |
fa'2 fa'4 |
fa' fa' fa' |
sol'2 sol'4 |
sol' re' sol' |
la'4. sol'8 fad'4 |
sib' fad' sol' |
re' do' sib |
sol do' mib' |
re'2 re'4 |
sol'2. |
mib'4 fa' sol' |
fa'2. |
fa'4 sol' la' |
sib'2 lab'4 |
sol' mib' sol' |
do' re' fa' |
fa'2 re'4 |
