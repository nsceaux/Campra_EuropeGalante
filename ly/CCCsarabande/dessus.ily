\clef "dessus" re''4 mib'' fa'' |
sib'4. do''8 re''4 |
sol' mib''4. re''8 |
do''2\trill do''4 |
fa'' fa'' re'' |
sol''4. fa''8 sol''4 |
do'' fa''4. mib''8 |
re''4.\trill do''8 sib'4 |
do''4 do'' re'' |
mib''2 re''4 |
sib'' la'' sol'' |
fad''4.\trill mi''8 re''4 |
sol'' la'' sib'' |
sib' do'' re'' |
mib'' do''4.\trill sib'8 |
la'2\trill sol'4 |
sib' do'' re'' |
mib''2. |
do''4 re'' mib'' |
fa''2. |
re''4 mib'' fa'' |
sol'' do'' mib'' |
la'\trill re''4. do''8 |
do''2\trill sib'4 |
