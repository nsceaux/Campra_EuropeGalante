\clef "basse" sib4 sib lab |
sol4. sol8 fa4 |
mib8 re do4. sib,8 |
fa,4 fa mib |
re4. do8 sib,4 |
mib4. re8 mib4 |
fa fa,2 |
sib,2 sib,4 |
fa4 mib re |
do2 sol4 |
sol fa mib |
re re' do' |
sib la sol |
sol, la, sib, |
do mib do |
re2 sol,4 |
sol2. |
do4 re mib |
fa2 mib4 |
re mib fa |
sol2 fa4 |
mib2 mib4 |
mib re sib, |
fa fa, sib, |
