\clef "haute-contre" sib'8 sib'4 |
la'2 sib'4 re'2 re'4 |
re'2.~ re'4. sol'8 fad'4 |
sol'2 sol'4 la'4. sib'8 la'4 |
sib'2.~ sib'4. sib'8 la'4 |
sib'4 fa' sol' fa' fa'4. fa'8 |
fa'2.~ fa'4. sib'8 sib'4 |
do''2 do''4 do''2. |
do''2.~ do''4. do''8 la'4 |
la'2 la'4 la'4. fa'8 sol'4 |
la'2 mi'4 fa'4. sol'8 la'4 |
re'2 sib'4 la'2 la'4 |
la'2.~ la'4. la'8 la'4 |
sol'2.~ sol'4. do''8 sib'4 |
la'4. sol'8 fa'4~ fa'4. fa'8 fa'4 |
fa'2 sib'4 sol'2 do''4 |
la' re'' re'' re'' sol' fad' |
sol' re' mib' re'4. re'8 re'4 |
re'2.~ re'4. sib'8 sib'4 |
re'2.~ re'4. sib'8 la'4 |
sol'2.~ sol'4. do''8 sib'4 |
la'4. sol'8 fa'4~ fa'4. fa'8 fa'4 |
fa'2 sib'4 sol'2 do''4 |
la' re'' re'' re'' sol' fad' |
sol' re' mib' re'4. re'8 re'4 |
re'2.~ re'4.
