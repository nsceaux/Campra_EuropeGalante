\score {
  \new StaffGroup <<
    \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4. s1.*5 s2. s4. \break s4. s1.*5\pageBreak
        s1.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
