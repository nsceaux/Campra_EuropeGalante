\clef "quinte" sol8 sol4 |
la2 sol4 la2 la4 |
sol2.~ sol4. sib8 la4 |
sol2 sol4 do'4. sib8 do'4 |
re'2.~ re'4. re'8 do'4 |
sib4 sib sol do' fa4. fa8 |
fa2.~ fa4. sib8 sib4 |
fa4. sib8 la4 do'2 do'4 |
do'4. sib8 la4~ la4. la8 la4 |
la2 re'4 la4. re'8 re'4 |
mi'2 mi'4 la2 la4 |
si?2 re4 la2 la4 |
la2.~ la4. la8 la4 |
si2 si4 do'2 do'4 |
do'2 do'4 fa2 fa4 |
fa sib2 do'4 do'2 |
re'4 re' do' sib sib la |
sol sol sol re' re' la |
sib2.~ sib4. sib8 sib4 |
sib2.~ sib4. sol8 la4 |
si2 si4 do'2 do'4 |
do'2 do'4 fa2 fa4 |
fa sib2 do'4 do'2 |
re'4 re' do' sib sib la |
sol sol sol re' re' la |
sib2.~ sib4.
