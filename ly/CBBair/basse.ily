\clef "basse" sol8 sol,4 |
re4. do8 sib,4 fad,2. |
sol, sol4. sol8 re4 |
sol4. la8 sib4 la4. sol8 fa4 |
sib2. sib,4. sib8 do'4 |
re'4 re mib fa fa,2 |
sib,2.~ sib,4. sib8 sib4 |
la4. sol8 fa4 mi4.\trill re8 do4 |
fa2. fa,4. fa8 dod4 |
re2 re'4 do'4. re'8 sib4 |
la2 la,4 fa,2. |
sol, la, |
re,4. re8 mi4 fad2. |
sol2 sol4 do2. |
fa2 fa4 la,2. |
sib,4. la,8 sol,4 do4. sib,8 la,4 |
re sol, fad, sol, sol la |
sib sib, do re re,2 |
sol,2.~ sol,4. sib8 sib4 |
sol,2.~ sol,4. sol8 fad4 |
sol2 sol4 do2. |
fa2 fa4 la,2. |
sib,4. la,8 sol,4 do4. sib,8 la,4 |
re sol, fad, sol, sol la |
sib sib, do re re,2 |
sol,2.~ sol,4.
