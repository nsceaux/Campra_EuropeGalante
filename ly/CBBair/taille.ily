\clef "taille" re'8 re'4 |
re'2 re'4 re'2 re'4 |
re'2.~ re'4. re'8 re'4 |
re'4. do'8 sib4 fa'2 fa'4 |
fa'2.~ fa'4. fa'8 mib'4 |
re' re' do' do' do'4. do'8 |
re'2.~ re'4. fa'8 sol'4 |
la'4. mi'8 fa'4 sol'4. fa'8 mi'4 |
fa'2.~ fa'4. fa'8 mi'4 |
re'2 re'4 mi'4. re'8 sol'4 |
mi'4. re'8 dod'4 re'2 re'4 |
re'2 sol'4 mi'4. fad'8 sol'4 |
fad'4. mi'8 re'4~ re'4. re'8 re'4 |
re'2. mib'4. fa'8 sol'4 |
fa'2 do'4 do'2 do'4 |
re'2 re'4 mi'2 mi'4 |
re' sib' la' sol' re' do' |
sib sol sol sib la4. sol8 |
sol2.~ sol4. fa'8 sol'4 |
sol2.~ sol4. re'8 re'4 |
re'2. mib'4. fa'8 sol'4 |
fa'2 do'4 do'2 do'4 |
re'2 re'4 mi'2 mi'4 |
re' sib' la' sol' re' do' |
sib sol sol sib la4. sol8 |
sol2.~ sol4.
