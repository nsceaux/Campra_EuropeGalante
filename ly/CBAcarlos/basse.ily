\clef "basse" sol,2 r4 sol |
mib4. mib8 mib4 mib |
si,2 si,4 si, |
do2 do4 do |
sol,2 r4 sol, |
la,2 r4 la, |
sib,2 sib, |
sib, la,4. la,8 |
sol,2 do |
fa, r4 fa |
mi2 mi4 re |
dod2 dod4 dod |
dod2 re |
la,2. la,4 |
re,2 r4 re |
sol,2 r4 sol, |
do2 r4 do |
fa,2 r4 fa, |
sib,2 r4 sib, |
do2 r4 do |
do2 si,4. si,8 |
do4. do8 re4. re,8 |
sol,2 r4 sol\douxSug |
mib4. mib8 mib4 mib |
si,2 si,4 si, |
do2 do4 do |
sol,2 sol, r4 sol |
sib2 sib4 do' |
re'2 re'4 re |
sol2 sol4 sol |
do'2 do'4 sib |
la2\trill sib4 sib8 la sol4 do' |
fa2 fa4 fa |
mi2\trill mi4 re |
dod dod dod dod |
dod2. re4 |
la,1 |
re2 r4 re |
sol,2 r4 sol |
mib4. mib8 mib4 mib |
si,2 si,4 si, |
do2 do4 do |
sol,2 sol,4 r r sol |
sib2 sib4 do' |
re'2 re'4 re |
sol2 r4 sol,^\fortSug |
do2 r4 do |
do2 si,4. si,8 |
do4. do8 re4 re, |
sol,2 sol8\douxSug sol |
re4 re re |
mib mib re |
do\trill do sib, |
fa fa fa8 fa |
do4 do4. do8 |
sol4 sol mib |
fa fa fa |
sib sib r8 re' |
do' sib la4.\trill sol8 |
fad2\trill r8 fad |
sol4. fa8 mi re |
dod2 dod8 dod |
re4 re mi |
fa fa sol |
la la la, |
re2 r8 re' |
do' sib la4.\trill sol8 |
fad2 r8 fad |
sol4. la8 sib do' |
re'2 re8 re |
mib4 mi4. mi8 |
fa4 fad4. fad8 |
sol4 re re, |
sol,2. |
