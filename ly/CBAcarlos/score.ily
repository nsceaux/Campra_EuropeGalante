\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        <>^"Violons"
        \global \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*11\break s1*11 s2 \bar "" \pageBreak
        s2 s1*3 s1. s1*2 s2 \bar "" \break s2 s1 s1. s1*3\pageBreak
        s1*8 \bar "" \break s2 s1*6 s2 \bar "" \pageBreak
        s4 s2.*6\break s2.*6\pageBreak
        s2.*6 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
