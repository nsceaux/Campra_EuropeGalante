\clef "dessus" r2 r4 sib' |
sol'4. sol'8 sol'4 sol' |
fa'2 fa'4 sol' |
mib'2 mib'4 mib' |
re'2 r4 mib' |
mib'2 r4 mib' |
mib'2 re' |
mi'!4. mi'8 fa'2~ |
fa' mi'4.\trill fa'8 |
fa'2 r4 do'' |
dod''2\trill dod''4 re'' |
sol'2 sol'4 sol' |
sol'2 fa'~ |
fa' mi'4.\trill re'8 |
re'2 fa'~ |
fa' re'4.\trill re'8 |
mib'2 mib'~ |
mib'4. mib'8 mib'4. fa'8 |
re'2\trill r4 re' |
mi'!2 r4 mi' |
fad'2 sol'~ |
sol'4. la'8 la'4.\trill sol'8 |
sol'2 r4 sib'\doux |
sol'4. sol'8 sol'4 sol' |
fa'2 fa'4. sol'8 |
mib'2 mib'4 mib' |
re'1 r4 re'' |
re''2 sol''4 sol'' |
sol''2 fad''4.\trill sol''8 |
sol''2 sol''4 sol'' |
mi''2\trill mi''4 mi'' |
fa''2 fa''4 fa'' fa'' mi'' |
fa''2 do''4 do'' |
dod''2\trill dod''4 re'' |
sol'2 sol'4 sol' |
sol'2. fa'4 |
fa'2( mi'4.)\trill re'8 |
re'1 |
r2 r4 sib' |
sol'4. sol'8 sol'4 sol' |
fa'2 fa'4 sol' |
mib'2 mib'4 mib' |
re'1 r4 re'' |
re''2 sol''4 sol'' |
sol''2 fad''4.\trill sol''8 |
sol''2 r4 re'\fort |
mi'2 r4 mi' |
fad'2 sol'4. sol'8 |
sol'4. la'8 la'4.\trill sol'8 |
sol'2 re''8\doux re'' |
fa''4 fa'' fa'' |
sib' sib' sib' |
mib'' mib'' re'' |
do''\trill do'' do''8 re'' |
mib''4 mib'' do'' |
re'' re'' mib'' |
do''\trill do'' fa'' |
re''\trill re'' r8 fa'' |
fa'' sol'' fad''4.\trill sol''8 |
la''2 r8 la'' |
sib''4. la''8 sol'' fa'' |
mi''2\trill mi''8 mi'' |
fa''4 fa'' dod'' |
re'' re'' mi'' |
fa'' mi''4.\trill re''8 |
re''2 r8 fa'' |
fad'' sol'' fad''4.\trill sol''8 |
la''2 r8 la'' |
sib''4. la''8 sol'' la'' |
fad''2\trill re''8 re'' |
re''4 do''4.\trill do''8 |
do''2 la'8 la' |
sib'4 la'4.\trill sol'8 |
sol'2. |
