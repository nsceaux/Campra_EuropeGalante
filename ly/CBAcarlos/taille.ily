\clef "taille" r2 r4 sib |
sib4. sib8 sib4 sib |
sol2 sol4 sol |
sol2 sol4 sol |
sol2 r4 sol |
sol2 r4 fa |
fa2 fa |
sol la |
sib2. sib4 |
la2 r4 do' |
mi'2 mi'4 la |
la2 la4 la |
la2 la |
la2. la4 |
la2 r4 la |
si2 r4 si |
do'2 r4 do' |
la2 r4 la |
sib2 r4 sib |
sol2 r4 la |
la2 la~ |
la la4.\trill la8 |
sib2 r4 sib\douxSug |
sib4. sib8 sib4 sib |
sol2 sol4 sol |
sol2 sol4 sol |
sol1 r4 sol' |
sol'2 sib'4 sib' |
sib'2 la'4.\trill la'8 |
sib'2 sib'4 sib' |
sol'2 sol'4 sol' |
la'2 fa'4 fa'8 la' sib'4 sol' |
la'2 fa'4 re' |
mi'2 mi'4 la |
la2 la4 la |
la2. la4 |
la2. la4 |
la1 |
r2 r4 sib |
sib4. sib8 sib4 sib |
sol2 sol4 sol |
sol2 sol4 sol |
sol1 r4 sol' |
sol'2 sib'4 sib' |
sib'2 la'4.\trill sib'8 |
sib'2 r4 sib\fortSug |
sol2 r4 la |
la2 r4 la |
la2 la4. la8 |
sib2 sol'8\douxSug sol' |
fa'4 fa' fa' |
mib' sol' fa' |
mib' mib' fa' |
fa' fa' fa'8 fa' |
sol'4 sol'4. sol'8 |
sol'4 sol' sol' |
fa' fa' fa' |
fa' fa' r8 fa' |
la' sol' la'4. sib'8 |
la'2 r8 la' |
sol'4. re'8 sol' la' |
sol'2 sol'8 sol' |
fa'4 fa' mi' |
la' la' sib' |
la' la'4. la'8 |
la'2 r8 fa' |
la' sol' la'4. sib'8 |
la'2 r8 la' |
sol'4. do''8 sib' sib' |
la'2 la'8 la' |
sol'4 mi'4.\trill mi'8 |
re'2 re'8 re' |
re'4 re'4. re'8 |
re'2. |
