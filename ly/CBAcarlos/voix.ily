\clef "vbasse" R1*22 |
r2 r4 <>^\markup\character Dom Carlos sol |
mib4. mib8 mib4 mib |
si,2 si,4 si, |
do2 do4 do |
sol,2 sol, r4 sol |
sib2 sib4 do' |
re'2 re'4 re |
sol2 sol4 sol |
do'2 do'4 sib |
la2\trill sib4 sib8 la sol4 do' |
fa2 fa4 fa |
mi2\trill mi4 re |
dod dod dod dod |
dod2. re4 |
la,1 |
re2 r |
r r4 sol |
mib4. mib8 mib4 mib |
si,2 si,4 si, |
do2 do4 do |
sol,2 sol,4 r r sol |
sib2 sib4 do' |
re'2 re'4 re |
sol2 r |
R1*3 |
r4 r <>^\markup\italic à sa troupe sol8 sol |
re4 re re |
mib mib re |
do\trill do sib, |
fa fa fa8 fa |
do4 do4. do8 |
sol4 sol mib |
fa fa fa |
sib sib r8 re' |
do' sib la4.\trill sol8 |
fad2\trill r8 fad |
sol4. fa8 mi re |
dod2\trill dod8 dod |
re4 re mi |
fa fa sol |
la la4. la,8 |
re2 r8 re' |
do' sib la4.\trill sol8 |
fad2 r8 fad |
sol4. la8 sib do' |
re'2 re8 re |
mib4 mi4. mi8 |
fa4 fad4. fad8 |
sol4 re4. re8 |
sol,2. |
