\clef "quinte" r2 r4 sol |
sol4. sol8 sol4 sol |
sol2 fa4 re |
mib2 mib4 do |
re2 r4 re |
do2 r4 do |
re2 re |
do2. do4 |
sib,2 do |
do r4 fa |
sol2 sol4 la |
la2 mi4 mi |
mi2 fa |
sol~ sol4 mi |
fa2 r4 la |
sol2 r4 sol |
sol2 r4 sol |
fa2 r4 fa |
fa2 r4 fa |
mi2 r4 mi |
re2 sol~ |
sol fad4.\trill sol8 |
sol2 r4 sol\douxSug |
sol4. sol8 sol4 sol |
sol2 re4 re |
mib2 mib4 do |
re1 r4 sib |
sib2 sol4 sol' |
re'2 re'4. re'8 |
re'2 re'4 sib |
do'2 do'4 do' |
do'2 sib4 sib8 do' sib4 do' |
do'2 fa4 fa |
sol2 sol4 la |
la2 mi4 mi |
mi2. fa4 |
la2. la4 |
fad1 |
r2 r4 sol |
sol4. sol8 sol4 sol |
sol2 re4 re |
mib2 mib4 do |
re1 r4 sib |
sib2 sol4 sol' |
re'2 re'4. re'8 |
re'2 r4 sol\fortSug |
mi2 r4 mi |
re2 sol~ |
sol fad4.\trill sol8 |
sol2 re'8\douxSug re' |
re'4 re' sib |
sib sib4. sib8 |
la4 la sib |
do' do' fa'8 fa' |
mib'4 mib' mib' |
re' sib do' |
do' do' do' |
re' re' r8 re' |
la sib do'4. sib8 |
re'2 r8 re' |
re'4. re'8 dod' re' |
mi'2 mi'8 mi' |
re'4 fa' sol' |
fa' la' sol' |
fa'8 re' mi'4. la'8 |
fad'2\trill r8 re' |
la sib do'4. sib8 |
re'2 r8 re' |
re'4. fad'8 sol' sol' |
re'2 re'8 re' |
sol4 do'4. do'8 |
la2\trill re'8 re' |
sib sol la4. sib8 |
sib2. |
