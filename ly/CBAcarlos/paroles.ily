La nuit ra -- meine en vain le re -- pos dans le mon -- de,
mon cœur est toû -- jours a -- gi -- té ;
Mais mon trouble & mes soins font ma fé -- li -- ci -- té,
j’ai -- me mieux en joü -- ir que d’u -- ne paix pro -- fon -- de :
La nuit ra -- meine en vain le re -- pos dans le mon -- de,
mon cœur est toû -- jours a -- gi -- té.

C’est à vous de ser -- vir une ar -- deur si cons -- tan -- te,
sou -- met -- tez à l’A -- mour la beau -- té qui m’en -- chan -- te ;
Par vos plus ten -- dres chants, tâ -- chez de la char -- mer :
Ren -- dez- luy le plai -- sir que je sens à l’ai -- mer.
Par vos plus ten -- dres chants, tâ -- chez de la char -- mer :
Ren -- dez- luy le plai -- sir que je sens à l’ai -- mer.
