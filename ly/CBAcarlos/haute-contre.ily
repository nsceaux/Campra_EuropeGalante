\clef "haute-contre" r2 r4 re' |
mib'4. mib'8 mib'4 mib' |
re'2 re'4 re' |
do'2 do'4 do' |
sib2 r4 sib |
do'2 r4 do' |
do'2 sib |
do'2. do'4 |
re'2 do' |
do' r4 la' |
sol'2\trill sol'4 fa' |
mi'2 mi'4 mi' |
mi'2 re'~ |
re' dod'4. re'8 |
re'2 r4 re' |
re'2 r4 re' |
do'2 r4 do' |
do'2 r4 do' |
sib2 r4 sib |
sib2 r4 la |
la2 re'4. re'8 |
mib'2 re'4. re'8 |
re'2 r4 re'\douxSug |
mib'4. mib'8 mib'4 mib' |
re'2 re'4 re' |
do'2 do'4 do' |
sib1 r4 sib' |
sol'2 re''4 mib'' |
re''2 re''4. re''8 |
re''2 re''4 re'' |
do''2 do''4 do'' |
do''2 re''4 re''8 do'' re''4 do'' |
do''2 la'4 la' |
sol'2 sol'4 fa' |
mi'2 mi'4 mi' |
mi'2. re'4 |
re'2( dod'4.)\trill re'8 |
re'1 |
r2 r4 re' |
mib'4. mib'8 mib'4 mib' |
re'2 re'4 re' |
do'2 do'4 do' |
sib1 r4 sib' |
sol'2 re''4 mib'' |
re''2 re''4. re''8 |
re''2 r4 sib\fortSug |
sib2 r4 la |
la2 re'4. re'8 |
mib'2 re'4. re'8 |
re'2 sib'8\douxSug sib' |
la'4 la' sib' |
sol'4 sol'4. sib'8 |
do''4 do'' re'' |
la'4\trill la' la'8 si' |
do''4 sol' la' |
sib' sib' sib' |
sib' sib' la'\trill |
sib' sib' r8 sib' |
do'' re'' do''4. re''8 |
re''2 r8 re'' |
re''4. re''8 mi'' fa'' |
la'2 la'8 la' |
la'4 la' sol' |
la' re'' re'' |
re'' dod''4.\trill re''8 |
re''2 r8 re'' |
do'' re'' do''4. re''8 |
re''2 r8 re'' |
re''4. do''8 re'' mib'' |
re''2 fad'8 fad' |
sol'4 sol'4. sol'8 |
la'2 la'8 la' |
sol'4 fad'4.\trill sol'8 |
sol'2. |
