\clef "dessus" R2.*20 R1*3 R2.*2 R1*4 R2. R1 R2. R1*4 |
r4 \tag #'(dessus1 dessus) <>^\markup\whiteout Violons
\twoVoices #'(dessus1 dessus2 dessus) <<
  { si'4 si' |
    mi'' re'' do'' |
    si'2\trill si'4 |
    do'' do'' re'' |
    mi'' mi'' mi'' |
    fad'' fad'' sol'' |
    la''2 sol''4 |
    fad''4.\trill red''8 mi'' fad'' |
    sol''4 si' si' |
    fad''4. red''8 mi'' fad'' |
    sol''4 sol'' sol'' |
    fad''\trill fad'' sol'' |
    mi''\trill mi''8 re'' mi'' fad'' |
    sol''4 sol'' sol'' |
    mi'' mi'' la'' |
    fad''\trill fad''8 sol'' la'' fad'' |
    sol''4 sol''8 la'' sol'' fad'' |
    mi''4\trill mi''8 mi'' fad'' sol'' |
    la''4 la'' sol'' |
    fad''4\trill red'' mi'' |
    si' si' si' |
    mi'' mi'' mi'' |
    mi'' red''4.\trill mi''8 |
    mi''2 }
  { sold'4 sold' |
    la' sold' la' |
    mi'2 mi'4 |
    mi' la' si' |
    do'' dod'' dod'' |
    red'' red'' mi'' |
    fad''2 mi''4 |
    red''4. si'8 dod'' red'' |
    mi''4 sold' sold' |
    red''4. si'8 dod'' red'' |
    mi''4 si'8 la' si' do'' |
    re''4 re'' si' |
    sol' sol' do'' |
    si'\trill si' re'' |
    sol' sol' do'' |
    la'\trill la' re'' |
    si'4\trill si'8 do'' si' la' |
    sol'4 sol''8 sol'' la'' si'' |
    red''4 red'' mi'' |
    red'' fad'' sol'' |
    la'' fad'' sol'' |
    sol'' sol'' la'' |
    fad'' fad''4.\trill mi''8 |
    mi''2 }
>> r2 |
R1 R2.*2 R1*6 R2.*2 R1*2 R2. R1*6 R2. R1*2 R2.*4 R1 R2. R1 R2.
R1*3 R2.*4 R1*2 R2.*2 R1*3 r2
