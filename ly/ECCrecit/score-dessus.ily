\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag#'dessus2 \includeNotes "dessus" >>
    >>
  >>
  \layout { }
}