\clef "basse" mi4 sol mi |
si4. la8 sol4 |
do' la si |
mi4. re8 do si, |
la,4 si,2 |
mi,2 mi4 |
red2. |
mi |
re |
do |
si, |
mi,2 mi4 |
si,2. |
mi |
fad |
sol |
dod4 red2 |
mi la,4 |
si,2. |
mi,2 si,4 |
mi,2 mi~ |
mi red |
mi1 |
sold,2. |
la,~ |
la,2 re |
sol si, |
do8 si, la, sol, re4 re, |
sol, sol sold2 |
la4. sol8 fad mi |
si,1 |
mi2 re4 |
dod2 do |
si, la, |
sol,4 fad,8 mi, si,2 |
mi,1~ | \allowPageTurn
mi,4 mi' re' |
do' si la |
sold2 sold4 |
la2 la4 |
la, la sol |
fad fad mi |
red2 mi4 |
si,2. |
mi,4 mi' re' |
si,2. |
mi4 mi8 fad sol la |
si4 si si |
do' do' la |
mi' mi' si |
do' do' la |
re' re' re |
sol2. |
do'4 do'8 si la sol |
fad4\trill fad mi |
si si mi' |
red' red' si |
do' do' la |
si si si, |
mi2~ mi~ |
mi1 |
fad4 sol2 |
fad2 red4 |
mi fad si,2 |
sold,1 |
la, |
si,2 si,, |
mi, mi~ |
mi4 dod lad,2 |
si,2. |
mi4 la,2 |
si,1~ |
si,2 fad~ |
fad4 sol2 |
fad fad, |
si,4 si sold2 |
la4 fad sold2 |
sid,1 | \allowPageTurn
dod2 si,4 la, |
sold,2 red |
mi4 mi,8 fad, sold,4 |
dod1 |
sold2 fad4. mi8 |
si2. |
si4 lad2 |
si2. |
mid |
fad4 si, dod2 |
fad2. |
lad2 si |
sold4 fad4. mi8 |
la,4 sold,8. la,16 si,2 |
mi,1~ |
mi, |
la,2 sold,4 |
fad, fad mi |
re2 dod4 |
si,4. si,8 sold,8. sold,16 |
mi,2 mi |
dod re |
red!2. |
mi |
fad8. mi16 red8. dod16 si,2 |
la,4 sold,~ sold,8 fad,16 mi, si,4 |
mi, mi2 red4 |
mi2