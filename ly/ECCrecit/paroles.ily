Vous bril -- lez seule en ces re -- trai -- tes,
vous ef -- fa -- cez tous les au -- tres ap -- pas :
Vous bril -- lez  - pas :
L’A -- mour ne se plaît qu’où vous ê -- tes,
il lan -- guit, il lan -- guit où vous n’ê -- tes pas.
L’A -  pas.
Mon cœur ne sent que trop le plai -- sir que vous fai -- tes.

Quoy ? Sei -- gneur !

C’est de vous que je me sens é -- pris :
De -- puis le jour que je vous vis,
mon cœur, bel -- le Za -- yde, en se -- cret vous a -- do -- re.

He -- las ! s’il es -- toit vray, vous me l’au -- riez a -- pris.

Non, & c’est un se -- cret, que je tai -- rois en -- co -- re,
si vos ten -- dres re -- gards ne me l’a -- voient sur -- pris.
J’es -- pe -- rois af -- fran -- chir mon a -- me
du pe -- ril d’en -- ga -- ger sa foy ;
J’es -- pe -  foy ;
Et je ne vou -- lois pas me per -- mettre u -- ne flâ -- me,
qui prît trop d’em -- pi -- re sur moy.
Et je ne vou -- lois pas me per -- mettre u -- ne flâ -- me,
qui prît trop d’em -- pi -- re sur moy.
J’ay long- temps dif -- fe -- ré de vous ren -- dre les ar -- mes :
Pour é -- vi -- ter d’é -- ter -- nel -- les a -- mours,
des beau -- tez de ces lieux j’em -- prun -- tois le se -- cours ;
Mais, vous tri -- om -- phez de leurs char -- mes,
et je vous aime en -- fin, pour vous ai -- mer toû -- jours.

Ah ! c’en est trop, je céde à cet ou -- tra -- ge :
Ver -- sons le sang que de -- man -- de ma ra -- ge.

Ciel ! que vois- je ? quel -- le fu -- reur !
Mal -- heu -- reu -- se, qu’o -- ses- tu fai -- re ?

Je vou -- lois la pu -- nir d’a -- voir trop sçu te plai -- re,
et de m’a -- voir ra -- vi ton cœur ;
Le de -- ses -- poir dont je suis a -- ni -- mé -- e,
s’en -- flâme en -- cor par tes dis -- cours ;
Tu luy ju -- res, cru -- el, les plus ten -- dres a -- mours,
tu l’ai -- mes cent fois plus que tu ne m’as ai -- mé -- e.
Quand tu for -- mas les nœuds que tu romps pour ja -- mais,
j’é -- prou -- vay ta fier -- té jus -- ques dans ta ten -- dres -- se ;
He -- las ! c’est a -- vec d’au -- tres traits
que l’A -- mour au -- jour -- d’huy te bles -- se,
de -- vant ses yeux ton or -- gueil ces -- se ;
J’ay vou -- lu van -- ger mes at -- traits,
et te pu -- nir de ta foi -- bles -- se.

Quoy, ne crain- tu pas que la mort
soit le prix de ton in -- so -- len -- ce ?

Je n’ay pû rem -- plir ma van -- gean -- ce ;
Ce re -- gret seul sans toy, peut ter -- mi -- ner mon sort.
Mais, toy, Ri -- va -- le trop cru -- el -- le,
pren ce fer in -- fi -- delle à mon jus -- te cour -- roux,
por -- tes- en à mon cœur une at -- tein -- te mor -- tel -- le,
tu m’as dé -- ja por -- té de plus sen -- si -- bles coups.

Qu’on l’ô -- te de mes yeux, & qu’on s’as -- su -- re d’el -- le.
