\score {
  \new ChoirStaff \with { \haraKiriFirst }  <<
    \new GrandStaff <<
      \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag#'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5\break
        s2.*6\break s2.*6\break \grace s16 s2.*3 s1*2\break
        \grace s16 s1 s2.*2\break
        s1*3 s4 \bar "" \break s2. s2. s1\pageBreak
        s2. s1*4\break s2.*9\break s2.*7\pageBreak
        s2.*7 s2 \bar "" \break s2 s1 s2.\break
        s2. s1*2\break s1*3\break s1 s2.*2\pageBreak
        s1*2\break s2. s1 s2 \bar "" \break s2 s1*2\break
        s1*2 s2.\break s1*2 s2.*2\break s2.*2 s1\pageBreak
        s2. s1 s2.\break \grace s16 s1*3\break s2.*2\break
        s2.*2 s1*2\break s2.*2 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}