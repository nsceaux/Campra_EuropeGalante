\key mi \minor
\digitTime\time 3/4 \midiTempo#120 s2.*5
\digitTime\time 3/4 s2. \bar ".!:" s2.*4 \alternatives s2.*2 s2 s4
\bar ".!:" s2.*6 \alternatives s2. { \time 4/4 \midiTempo#80 s4 }
s2. s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 \grace s8  s1*4
\digitTime\time 3/4 \midiTempo#160 \beginMark "Air" s2.
\bar ".!:" s2.*6 \alternatives s2.*2 s2.
s2.*13
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4 s2 \bar "||" \key mi \major s1.
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*4
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 \grace s16 s1*3
\digitTime\time 3/4 s2.*4
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2
