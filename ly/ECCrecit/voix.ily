\clef "vbasse" R2.*5 |
<>^\markup\character-text Zuliman à Zayde
r4 r8 mi mi mi |
si4. la8 sol\trill fad |
sol8. fad16( mi8) sol la si |
fad4\trill si \appoggiatura la16 sol4 |
mi\trill mi( red8) mi |
red2.\trill |
r4 r8 mi mi mi |
red2\trill r8 fad |
sol2 la8 si |
do'2 do'8 \appoggiatura si la |
\appoggiatura la si8. la16( sol8) r16 si si4 |
mi8 r16 la la4.( fad8) |
\appoggiatura fad16 sol2 la8 si |
sol4( fad4.)\trill mi8 |
mi2 r8 fad |
mi4 r8 mi sol sol la si |
la4\trill la8 la la4 si8 fad |
\appoggiatura fad16 sol8 sol
\ffclef "vbas-dessus" <>^\markup\character Zayde
mi''8 r16 mi'' si'4
\ffclef "vbasse" <>^\markup\character Zuliman
r8 si16 si |
mi4 mi8 mi16 re re8 dod |
dod?4\trill r8 mi mi mi |
la sol sol fad fad4\trill r8 re' |
si4\trill si8 si16 re' sol4 sol8. re16 |
mi4 fad8 sol sol4( fad) |
sol
\ffclef "vbas-dessus" <>^\markup\character Zayde
re''4 si'\trill si'8 si'16 mi'' |
dod''4\trill dod''8 dod''16 dod'' red''8 mi'' |
red''4\trill
\ffclef "vbasse" <>^\markup\character Zuliman
si4 r8 fad fad fad16 si |
sold4 mi8 mi16 mi fad8 sold |
\appoggiatura sold?8 la4. la8 r4 la8 fad |
\appoggiatura mi8 red4 red8 mi \appoggiatura mi16 fad2 |
r8 sol la si red4.\trill mi8 |
mi1 |
r4 mi' re' |
do' si la |
sold2 sold4 |
la2 la4 |
r la sol |
fad fad mi |
red2 mi4 |
si,2. |
r4 mi' re' |
si,2. |
mi4 mi8 fad sol la |
si4 si si |
do' do' la |
mi' mi' si |
do' do' la |
re' re' re |
sol2. |
do'4 do'8 si la sol |
fad4\trill fad mi |
si si mi' |
red' red' si |
do' do' la |
si si si, |
mi2 <>^\markup\italic Récitatif r8 si16 si si8 si16 si |
mi'8 sol16 sol sol8 sol16 si mi8 mi r16 mi' mi' mi' |
dod'8\trill dod'16 re' si4\trill si8[ lad16] si |
lad4\trill r8 fad16 fad si8 si16 fad |
sol8 sol16 mi fad8 fad16 fad si,4 si |
r8 si si do' re'4 re'8 \appoggiatura do'16 si8 |
do'8. si16( la8) r la la16 sol fad8 la |
red4\trill fad8 fad16 sol sol4( fad8.)\trill mi16 |
mi2
\ffclef "vbas-dessus" <>^\markup\character-text Roxane vite
mi''4 r8 sold'16 mi' |
<>^\markup\italic { Elle tire son poignard, pour en frapper Zayde }
si'8. mi''16 la' la' la' dod'' fad'8 fad' r fad'' |
red'' fad'' si' si'16 la' la'8 la'16 sold' |
sold'8\trill sold'
\ffclef "vbasse" <>^\markup\character Zuliman
dod'4 r8 fad |
<>^\markup\italic { Il arrache le poignard }
red8\trill red r16 fad fad fad si4 r8 si16 si |
fad8\trill fad fad fad16 sold la8 la
\ffclef "vbas-dessus" <>^\markup\character Roxane
r8 dod''16 dod'' |
fad''8 fad''16 lad' si'8. si'16 si' si' si' dod'' |
lad'8\trill lad' r16 fad' fad' fad' dod''8. dod''16 red''8 mi'' |
red''4\trill r16 si' si' si' mi''8 mi''16 mi'' si'8 si'16 mi'' |
dod''8\trill dod'' r16 fad'' fad'' fad'' sid'4\trill sid'8 sid'16 red'' |
sold'4 r8 sold'16 sold' red''4 red''8 r16 red'' |
mi''4 mi'8 fad' sold'4 sold'8 fad' |
\appoggiatura fad' sold'4 r8 sid' sid'\trill sid' sid' sid' |
dod''4 dod''8 red''16 mi'' red''8.\trill mi''16 |
dod''4\trill dod''8 r sold' sold'16 sold' sold'8 la' |
\appoggiatura la'16 si'4 si'8 mi'' la'4 la'8 sold' |
fad'4\trill r8 si'16 dod'' red''8\trill red''16 si' |
mi''4 dod''8.\trill dod''16 dod''8 red''16 mi'' |
red''4\trill red''8 r fad''4 |
si' r8 si'16 si' si'8 dod''16 sold' |
la'4 re''8 re'' sold'\trill sold'16 sold' sold'8 la' |
fad'\trill fad' r la' la' dod'' |
fad' fad' fad' fad' red'\trill red' r si'16 si' |
si'8 mi'' red''4\trill red''8 mi'' |
\appoggiatura mi''16 fad''4 si'8 si'16 dod'' fad'4\trill fad'8 fad'16 sold' |
mi'8. mi'16
\ffclef "vbasse" <>^\markup\character Zuliman
si4 r16 si si si sold8\trill sold16 si |
mi4 mi8 mi si,8 si,16 si, si,8 mi |
la,8. la,16
\ffclef "vbas-dessus" <>^\markup\character Roxane
mi''16 mi'' mi'' mi'' mid''8 mid''16 fad'' |
fad''8 fad''16 r lad'8 lad'16 lad' lad'8 dod'' |
fad'4 si'8 si'16 si' si'8 lad' |
\appoggiatura lad'16 si'2 r4 |
r4 <>^\markup\italic à Zayde mi''4 sold' r8 dod' |
la'8 la' la' dod'' fad' fad' r fad''16 fad'' |
si'8 si'16 si' fad'8 fad'16 fad' si'8 si'16 si' |
sold'4\trill r8 mi''16 mi'' dod''8\trill dod''16 dod'' |
lad'8 fad'16 fad' si'8 si'16 dod'' red''8 red'' r si' |
si'16 si' dod'' red'' mi''4 mi''8 fad''16 sold'' red''8.\trill mi''16 |
mi''8
\ffclef "vbasse" <>^\markup\character Zuliman
r16 mi mi mi mi mi la8. la16 la la la sold |
sold4\trill sold
