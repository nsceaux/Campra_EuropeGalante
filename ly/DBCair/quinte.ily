\clef "quinte" re'4 mi'8 fa'4 la8 re'4 re'8 |
la4. la4 mi'8 la4 la8 sib4 sib8 |
la4 dod'8 re'4 dod'8 re'4 la8 la4 re8 |
la4. mi'4 mi'8 re'4. re'8 do' si |
do'4. sol8 fa sol la4. sol4 sol8 |
sol4. do'4 re'8 mi'4 mi'8 mi'4 la8 |
mi'4. mi'4 re'8 do' si la mi4 la8 |
si4 sold8 la4 si8 la4 si8 la4 la8 |
si4. do'4 si8 la4 re'8 re' do' si |
do'4. r4 r8 do'4. r4 r8 |
do'4. la4 do'8 sib4 re'8 do' re' mi' |
re'4. fa'4 do'8 re' mi' fa' do'4 sib8 |
la4. la sol si |
la la re'4 sol'8 sol' fa'\trill mi' |
fa'4 la'8 la'4 re'8 la' la sib la4 la8 |
la4.
