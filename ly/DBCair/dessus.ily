\clef "dessus" la''4 sol''8 fa''4\trill mi''8 fa'' mi'' re'' |
mi''4 la'8 re''4 do''8 sib'4\trill la'8 re''4 mi''8 |
dod'' si' la' re''4 mi''8 fa''4 mi''8 fa'' mi'' re'' |
dod''4.\trill la''4 sol''8 fa''\trill mi'' re'' sol''4 fa''8 |
mi'' re'' do'' sol''4 sol''8 sol'' fa'' mi'' re''4\trill do''8 |
do''4. mi''4 re''8 do''4\trill si'8 do'' si' la' |
si'4 mi'8 la'4 si'8 do'' re'' do'' si'4\trill la'8 |
mi''4.~ mi'' r8 r mi'' mi''4 la''8 |
sold'' fad'' mi'' la'4 si'8 do'' re'' do'' si'4\trill la'8 |
la'4. la'4 sib'8 do'' re'' do'' do''4 do''8 |
do'' sib' la' do''4. re'' mi''\trill |
fa'' do''4 do''8 do'' sib' la' sol'4\trill fa'8 |
fa'4. la'8 sol' la' sib'4. si'8\trill la' si' |
do''4. dod''8 si' dod'' re'' mi'' fa'' mi''4\trill re''8 |
re'' fa'' re'' la'' la' re'' dod'' la' fa'' mi''4\trill re''8 |
re''4.
