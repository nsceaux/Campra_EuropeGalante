\clef "basse" re'4 dod'8 re'4 dod'8 re' do'? sib |
la4 sol8 fa4 mi8 re4 do8 sib, la, sol, |
la, la sol fa4 mi8 re4 dod8 re do? sib, |
la,4. la4 la8 re'4 do'8 si la sol |
do'4. mi8 re mi fa mi fa sol4 sol,8 |
do4. do'4 si8 la4 sold8 la sol? fa |
mi4 re8 do4 si,8 la,4 la,8 sol,4 fa,8 |
mi,4 mi8 la4 sold8 la4 sold8 la sol? fa |
mi4. do re mi |
la, r4 r8 fa,4. r4 r8 |
fa,4. la8 sol la sib la sib do' sib do' |
re'4. la sib do' |
fa8 mi fa fa4. sol8 fa sol sol4. |
la8 sol la la4. sib4 sol8 la4 la,8 |
re4 re'8 do'4 sib8 la fa sol la4 la,8 |
re4.
