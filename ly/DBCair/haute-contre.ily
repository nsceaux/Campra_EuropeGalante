\clef "haute-contre" fa''4 mi''8 re''4 la'8 la'4 re''8 |
dod'' si' la' la'4 sol'8 fa'4 fa'8 fa'4 sol'8 |
mi'4.\trill fa'4 sol'8 la'4 la'8 la' fa' sol' |
la'4. dod''4 dod''8 re''4 la'8 si' do''? re'' |
do''4. do''4 do''8 do''4. si'4\trill do''8 |
do''4. sol'4 sold'8 la'4 si'8 mi'4 la'8 |
sold' fad' mi' mi'4 sold'8 la'4 mi'8 mi'4 la'8 |
sold'4 si'8 do''4 si'8 do''4 si'8 do'' si' la' |
mi''4 mi'8 mi'4 la'8 la'4. sold'4 la'8 |
la'4. fa'4 sol'8 la' sib' la' la'4 la'8 |
la' sol' fa' fa'4. fa'4 sib'8 sol'4 do''8 |
la' sol' fa' la'4 la'8 la' sol' fa' mi'4\trill fa'8 |
fa'4. fa'8 mi' fa' re'4. sol'8 fa' sol' |
mi'4. la'8 sol' la' fa'4 sib'8 la'4 la'8 |
la'4 la'8 la'4 sol'8 la'4 re''8 dod''4\trill re''8 |
re''4.
