\clef "taille" la'4 la'8 la'4 la'8 la' fa' sol' |
la'4 mi'8 fa'4 do'8 re'4. re'4 re'8 |
la'4 mi'8 la'4 sol'8 fa'4 sol'8 fa'4 sib'8 |
mi'4. la'4 la'8 la' sol' fa' re'4 re'8 |
mi'4. mi'4 mi'8 mi' re' do' sol'4 sol'8 |
mi'4.\trill mi'4 si8 do'4 re'8 do'4 re'8 |
si4.\trill do'4 re'8 mi'4 mi'8 mi' do' re' |
mi'4. mi' mi' mi'8 do' re' |
mi'4. mi'4 mi'8 re'4 fa'8 mi'4 mi'8 |
mi'4. r4 r8 fa'4. r4 r8 |
fa'4. do'4 fa'8 re'4 sol'8 mi' re' do' |
fa'4. fa' fa'4 re'8 do'4 do'8 |
do'4. re' re' mi'~ |
mi' mi' re'8 dod' re' dod'4 re'8 |
re'4 fa'8 mi'4\trill re'8 mi' fa' re' la'4 mi'8 |
fa'4.
