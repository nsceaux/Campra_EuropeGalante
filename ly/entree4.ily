\newBookPart#'()
\act Quatrième Entrée
\markup\fill-line\fontsize#4 { L’ITALIE }
\markup\vspace#1
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente une Salle magnifique, préparée pour un Bal.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Octavio, Olimpia.
}
%% 4-1
\pieceToc\markup\wordwrap {
  Octavio, Olimpia : \italic { Ne verray-je jamais le jour }
}
\includeScore "DAAprelude"
\includeScore "DABrecit"
\newBookPart #'(full-rehearsal)

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Octavio, Olimpia,
  une Troupe de Masques Galants & Comiques entrent sur la Scene.
}
%% 4-2
\pieceToc "Marche des Masques"
\includeScore "DBAmarche"
\newBookPart #'(full-rehearsal)
%% 4-3
\pieceToc\markup\wordwrap {
  Chœur des Masques : \italic { Tendres amans, rassemblons-nous }
}
\includeScore "DBBchoeur"
%% 4-4
\pieceToc "Air pour les Masques"
\includeScore "DBCair"
\newBookPart #'(full-rehearsal)
%% 4-5
\pieceToc\markup\wordwrap {
  Air italien : \italic { Ad un cuore tutto geloso }
}
\includeScore "DBDair"
\markup\fill-line {
  \line { On reprend l’air des Masques. }
} \noPageBreak
\reIncludeScore "DBCair" "DBEair"
\markup\fill-line {
  \line { Ensuite l’on chante le second Couplet. }
} \noPageBreak
\includeScore "DBFair"
\newBookPart #'(full-rehearsal)
%% 4-6
\pieceToc "Premiere chaconne"
\includeScore "DBGchaconne"
%% 4-7
\pieceToc\markup\wordwrap {
  Une Venitienne, chœur :
  \italic { Formons d'aimables jeux, laissons-nous enflamer }
}
\includeScore "DBHairChoeur"
\newBookPart #'(full-rehearsal)
%% 4-8
\pieceToc "Seconde chaconne"
\includeScore "DBIchaconne"
%% 4-9
\pieceToc\markup\wordwrap {
  Une Venitienne, chœur :
  \italic { Livrons-nous aux plaisirs, il n'est rien de plus doux }
}
\includeScore "DBJairChoeur"
\newBookPart #'(full-rehearsal)
\sceneDescription\markup\justify {
  Pendant le Fête, un des Masques danse avec Olimpia,
  & fait remarquer beaucoup d’empressement pour elle.
  Quand le bal finit, Octavio suit ce Masque qui laisse
  Olimpia surpise de se trouver sans luy.
}
%% 4-10
\pieceToc "Air"
\includeScore "DBKair"
\newBookPart #'(full-rehearsal)
%% 4-11
\pieceToc\markup\wordwrap {
  Air Italien : \italic { Si scherzi, si rida }
}
\includeScore "DBLair"
\newBookPart #'(full-rehearsal)
%% 4-12
\pieceToc "La Forlana"
\includeScore "DBMforlana"
\newBookPart #'(full-rehearsal)
%% 4-13
\pieceToc "Menuet"
\includeScore "DBNmenuet"
\newBookPart #'(full-rehearsal)

\scene "Scene III" "Scene III"
\sceneDescription\markup\wordwrap-center { Olimpia. }
%% 4-14
\pieceToc\markup\wordwrap {
  Olimpia : \italic { Qu’est devenu le jaloux qui m'obsede ? }
}
\includeScore "DCArecit"

\scene "Scene IV" "Scene IV"
\sceneDescription\markup\center-column {
  \line { Olimpia, Octavio. }
  \line { Octavio rentre en remettant son poignard. }
}
%% 4-15
\pieceToc\markup\wordwrap {
  Olimpia, Octavio : \italic { Mais que vois-je ? ô Ciel ! }
}
\includeScore "DDArecit"
\newBookPart #'(full-rehearsal)
\scene "Scene V" "Scene V"
\sceneDescription\markup\wordwrap-center { Octavio. }
%% 4-16
\pieceToc\markup\wordwrap {
  Octavio : \italic { Quel outrage ! mon cœur ne peut le soûtenir }
}
\includeScore "DEArecit"
%% 4-17
\pieceToc "Air des Masques"
\markup\fill-line {
  \line { On reprend l’air des Masques pour finir l’Entrée. }
}
\reIncludeScore "DBCair" "DEBair"
\actEnd "FIN DE LA QUATRIÈME ENTRÉE"
