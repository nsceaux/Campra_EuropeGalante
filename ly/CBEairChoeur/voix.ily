<<
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "basse"
  \tag #'(espagnol basse) {
    \clef "vhaute-contre" <>^\markup\character Un Espagnol
    fa'4 |
    sol' fa'4.\trill mib'8 |
    fa'4 re' fa' |
    fa'4 mib'4.\trill re'8 |
    mib'4 do' re' |
    mib' do'4.\trill sib8 |
    sib2
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r4 R2.*5 r4 r }
>>
\setMusic #'choeur <<
  \tag #'vdessus {
    \clef "vdessus" <>^\markup\character Le Chœur
    fa''4 |
    sol''4 fa''4.\trill mib''8 |
    fa''4 re'' fa'' |
    fa'' mib''4.\trill re''8 |
    mib''4 do'' re'' |
    mib'' do''4.\trill sib'8 |
    sib'2
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sib'4 |
    sib' sib'4. la'8 |
    sib'4 sib' fa' |
    fa' do''4. fa'8 |
    fa'4 fa' fa' |
    sol' fa'4. fa'8 |
    re'2\trill
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 |
    mib' sib4. mib'8 |
    re'4\trill re' re' |
    re' la4.\trill sib8 |
    do'4 do' sib |
    sib la4.\trill sib8 |
    sib2
  }
  \tag #'vbasse {
    \clef "basse" sib4 |
    mib' re'4.\trill do'8 |
    sib4 sib sib |
    re' do'4.\trill sib8 |
    la4\trill la sib |
    mib fa4. fa8 |
    sib,2
  }
  \tag #'(espagnol basse) { r4 R2.*5 r4 r }
>>
\keepWithTag #'(vdessus vhaute-contre vtaille vbasse espagnol basse) \choeur
<<
  \tag #'(espagnol basse) {
    <>^\markup\character L'Espagnol
    fa'4 |
    re'2\trill re'4 |
    re' mi' fa' |
    mi'4.\trill re'8( do'4) |
    re'4 mi' fa' |
    sol' mi'4.\trill fa'8 |
    fa'2 fa'4 |
    sol' fa'4.\trill mib'8 |
    fa'4 re' fa' |
    fa' mib'4.\trill re'8 |
    mib'4 do' re' |
    mib' do'4.\trill sib8 |
    sib2
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r4 R2.*11 r4 r }
>>
\choeur
<<
  \tag #'(espagnol basse) {
    <>^\markup\character L'Espagnol
    re'4 |
    re' mi' fa' |
    mi' fad' sol' |
    fad'4.\trill mi'8( re'4) |
    sol'4 la' sib' |
    \appoggiatura la'8 sol'4 fad'4.\trill sol'8 |
    sol'2 re'4 |
    mib' re'4.\trill do'8 |
    re'4 sol sol |
    sol la sib |
    la\trill sib do' |
    re' re'4. mib'8 |
    do'2\trill fa'4 |
    sol' fa'4.\trill mib'8 |
    fa'4 re' fa' |
    fa' mib'4.\trill re'8 |
    mib'4 do' re' |
    mib' do'4.\trill sib8 |
    sib2
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r4 R2.*17 r4 r }
>>
\choeur
