\key fa \major \midiTempo#160
\digitTime\time 3/4 \partial 4
\beginMark "Air" s4 s2.*5 s2
\beginMark "Chœur" s4 s2.*5 s2
s4 s2.*11 s2
\beginMark "Chœur" s4 s2.*5 s2
s4 s2.*17 s2
\beginMark "Chœur" s4 s2.*5 s2 \bar "|."
 