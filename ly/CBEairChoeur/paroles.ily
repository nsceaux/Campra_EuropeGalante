\tag #'(espagnol basse) {
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
\tag #'choeur {
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
\tag #'(espagnol basse) {
  S’il veut à quel -- que cru -- el -- le,
  faire en -- fin sen -- tir ses coups ;
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
\tag #'choeur {
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
\tag #'(espagnol basse) {
  Si quelque a -- mant, près de sa bel -- le,
  trom -- pe les yeux des ja -- loux ;
  Nuit, soy -- ez fi -- del -- le,
  et ca -- chez à tous,
  des mis -- te -- res si doux ;
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
\tag #'choeur {
  Nuit, soy -- ez fi -- del -- le,
  l’A -- mour ne ré -- ve -- le
  ses se -- cret qu’à vous.
}
