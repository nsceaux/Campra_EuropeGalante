\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'espagnol \includeNotes "voix"
    >> \keepWithTag #'espagnol \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*5 s2 \bar "" \break
        s4 s2.*5 s2 \bar "" \pageBreak
        s4 s2.*7\break s2.*4 s2 \bar "" \break
        s4 s2.*5 s2 \bar "" \pageBreak
        s4 s2.*7\break s2.*8\break s2.*2 s2 \bar "" \break
      }
      \modVersion {
        s4 s2.*5 s2 \bar "" \break
        s4 s2.*5 s2 \bar "" \break
        s4 s2.*11 s2 \bar "" \break
        s4 s2.*5 s2 \bar "" \pageBreak
        s4 s2.*17 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
