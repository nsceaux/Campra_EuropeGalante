\clef "quinte"
r4 R2.*5 r4 r
\setMusic #'choeur {
  re'4 |
  mib' sib4. mib'8 |
  re'4\trill re' re' |
  fa' mib'4. fa'8 |
  fa'4 fa' re' |
  do' do'4. do'8 |
  re'2
}
\keepWithTag #'() \choeur
r4 R2.*11 r4 r
\choeur
r4 R2.*17 r4 r
\choeur
