\clef "basse"
<<
  \tag #'basse { r4 R2.*5 r4 r }
  \tag #'basse-continue {
    sib4 |
    mib' re'4.\trill do'8 |
    sib4 sib sib |
    re' do'4.\trill sib8 |
    la4 la sib |
    mib fa fa, |
    sib,2
  }
>>
\setMusic #'choeur {
  sib4 |
  mib' re'4.\trill do'8 |
  sib4 sib sib |
  re' do'4.\trill sib8 |
  la4\trill la sib |
  mib fa fa, |
  sib,2
}
\keepWithTag #'() \choeur
<<
  \tag #'basse { r4 R2.*11 r4 r }
  \tag #'basse-continue {
    sib,4 |
    sib2 sib4 |
    sib8 la sol4 fa |
    do4. sib,8 la,4 |
    sib,4 do re |
    sib, do do, |
    fa,2 sib4 |
    mib' re'4.\trill do'8 |
    sib4 sib sib |
    re' do'4.\trill sib8 |
    la4 la sib |
    mib fa fa, |
    sib,2
  }
>>
\choeur
<<
  \tag #'basse { r4 R2.*17 r4 r }
  \tag #'basse-continue {
    sib4 |
    sib do' re' |
    do' re' mib' |
    re'4. do'8 sib la |
    sol4 fad sol |
    do re re, |
    sol,2 sol4 |
    do'4 sib lab |
    sol4. sol8 fa4 |
    mi4.\trill re8 do4 |
    fa8 mib re4 do |
    sib,2. |
    fa,2 sib4 |
    mib' re'4.\trill do'8 |
    sib4 sib sib |
    re' do'4.\trill sib8 |
    la4 la sib |
    mib fa fa, |
    sib,2
  }
>>
\choeur
