\clef "haute-contre"
r4 R2.*5 r4 r
\setMusic #'choeur {
  re''4 |
  sib' sib'4. do''8 |
  re''4 re'' re'' |
  re'' la'4.\trill sib'8 |
  do''4 do'' sib' |
  sib' la'4.\trill sib'8 |
  sib'2
}
\keepWithTag #'() \choeur
r4 R2.*11 r4 r
\choeur
r4 R2.*17 r4 r
\choeur
