\clef "taille"
r4 R2.*5 r4 r
\setMusic #'choeur {
  sib'4 |
  sib' sib'4. la'8 |
  sib'4 sib' sib' |
  sib' do''4. fa'8 |
  fa'4 fa' fa' |
  sol' fa'4. fa'8 |
  fa'2
}
\keepWithTag #'() \choeur
r4 R2.*11 r4 r
\choeur
r4 R2.*17 r4 r
\choeur
