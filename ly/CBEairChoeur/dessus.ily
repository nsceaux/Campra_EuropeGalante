\clef "dessus"
r4 R2.*5 r4 r
\setMusic #'choeur {
  fa''4 |
  sol'' fa''4.\trill mib''8 |
  fa''4 re'' fa'' |
  fa'' mib''4.\trill re''8 |
  mib''4 do'' re'' |
  mib'' do''4.\trill sib'8 |
  sib'2
}
\keepWithTag #'() \choeur
r4 R2.*11 r4 r
\choeur
r4 R2.*17 r4 r
\choeur
