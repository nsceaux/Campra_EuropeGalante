\clef "quinte" sib4 sib |
sib sib la4. la8 |
sib4 re' re' re' |
re'4. re'8 do'4 sib |
re'2 re'4 re' |
mi'4. mi'8 re'4 re' |
la'4. dod'8 re'4 la |
la sol fa4. sib8 |
la4 la sol sol |
sib4. re'8 do'4 sol |
do' do'8 sib la sol fad mi |
re4 sib re4. re8 |
re2 re'4 re' |
re2 sol4 sol |
sib4. re'8 do'4 sol |
do' do'8 sib la sol fad mi |
re4 sib re4. re8 |
re2
