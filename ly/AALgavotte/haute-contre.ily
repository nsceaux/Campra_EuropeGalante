\clef "haute-contre" sol'4 sol' |
sol' fa'8 sol' la'4 fad' |
sol' fad' sib' sib' |
sib'4. sib'8 do''4 re'' |
re''2 re''4 la' |
la'4. la'8 la'4 la' |
la'4. la'8 la'4 la' |
la' dod'' re''4. re''8 |
dod''4 re'' sib' sib' |
sib'4. sib'8 do''4 re'' |
sol' la' la' re' |
sol' sol' fad'4. sol'8 |
sol'2 re''4 la' |
sol'2 sib'4 sib' |
sib'4. sib'8 do''4 re'' |
sol' la' la' re' |
sol' sol' fad'4. sol'8 |
sol'2
