\clef "taille" re'4 re' |
sib8 do' re'4 re' re' |
re' re' fa' fa' |
fa'4. sol'8 la'4 sib' |
la'2\trill fa'4 fa' |
sol'4. sol'8 fa'4 sol' |
mi'4.\trill mi'8 fa'4 sol' |
fa' mi' re'4. re'8 |
la'4 fad' mib' mib' |
re'4. re'8 la'4 sol' |
sol'8 fa' mib'4 re'4. la8 |
sib4 mib' re' la |
sib2 fa'4 fa' |
sib2 mib'4 mib' |
re'4. re'8 la'4 sol' |
sol'8 fa' mib'4 re'4. la8 |
sib4 mib' re' la |
sib2
