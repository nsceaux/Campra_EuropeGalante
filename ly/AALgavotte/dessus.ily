\clef "dessus" re''4 sol' |
mib'' re'' do''4.\trill re''8 |
sib'4\trill la' re'' re'' |
re'' mi'' fad'' sol'' |
fad''2\trill la''4 la'' |
mi''4.\trill mi''8 fa''4 re'' |
dod''4.\trill la'8 re''4 mi''\trill |
fa'' sol'' la''4. sib''8 |
mi''4\trill re'' sol'' sol'' |
sol''4. fa''8 fa''4. sol''8 |
mib''4 do'' re'' fad' |
sol'4. la'8 la'4.\trill sol'8 |
sol'2 la''4 la'' |
sol'2 sol''4 sol'' |
sol''4. fa''8 fa''4. sol''8 |
mib''4 do'' re'' fad' |
sol'4. la'8 la'4.\trill sol'8 |
sol'2

