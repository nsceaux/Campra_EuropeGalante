\clef "basse" sol4 sol |
sol8 la sib4 fad re |
sol re8 do sib,4 sib, |
sib,4. sib,8 la,4 sol, |
re2 re'4 re' |
dod'4. dod'8 re'4 sib |
la8 sib la sol fa4 dod |
re mi fa4. sol8 |
la4 re mib mib |
sib, sib la si |
do'8 sib la sol fad mi re do |
sib,4 do re re, |
sol,2 re'4 re' |
sol,2 mib4 mib |
sib, sib la si |
do'8 sib la sol fad mi re do |
sib,4 do re re, |
sol,2
