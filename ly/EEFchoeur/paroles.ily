\tag #'(vbasse basse) {
  Bel -- lo co -- mo star un flor,
  du -- rar quan -- to far ar -- bor,
  du -- rar quan -- to far ar -- bor.
  A l’e -- ne -- mi -- gos su scia -- bo -- la,
  co -- mo à fru -- tas tem -- pe -- sta.
  A l’e -- ne -- mi -- gos su scia -- bo -- la,
  A l’e -- ne -- mi -- gos su scia -- bo -- la,
  co -- mo à fru -- tas tem -- pe -- sta.
  Co -- mo à fru -- tas tem -- pe -- sta.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Bel -- lo co -- mo star un flor,
  du -- rar quan -- to far ar -- bor,
  du -- rar quan -- to far ar -- bor.
}
\tag #'(vbasse basse) {
  La ru -- cia -- da ma -- tu -- ti -- na,
  far flo -- rir __ su jar -- di -- na,
  fa -- vor ce -- le -- sta
  co -- prir, co -- prir su tur -- ban -- ta.
  Fa -- vor ce -- le -- sta
  co -- prir, co -- prir su tur -- ban -- ta.
  Co -- prir, co -- prir su tur -- ban -- ta.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Fa -- vor ce -- le -- sta
  co -- prir, co -- prir su tur -- ban -- ta.
  Co -- prir, co -- prir su tur -- ban -- ta.
}
