\clef "quinte" r2 |
R1*17 |
r2 dod'4 si |
la4. re'8 mi'4 re' |
dod' la la la |
la fad si si |
la la la la |
la fad si si |
la2 r |
R1*16 |
r2 r4 mi' |
re'4. mi'8 fad'4 si |
dod' r r si |
si2 r4 si |
la la mi'2 |
mi'4 r r sol |
la2 r4 la |
la la mi'4. mi'8 |
mi'1 |
