<<
  \tag #'vdessus { \clef "vdessus" r2 R1*17 r2 }
  \tag #'vhaute-contre { \clef "vhaute-contre" r2 R1*17 r2 }
  \tag #'vtaille { \clef "vtaille" r2 R1*17 r2 }
  \tag #'(vbasse basse) {
    \clef "vbasse" <>^\markup\character Le Bostangi
    la4 si |
    dod'4. si8 la4 si |
    dod' dod re8[ mi fad sold] |
    la4 fad re mi |
    la dod re8[ mi fad sold] |
    la4 fad re mi |
    la,2 la4 la8 la |
    fad4 fad8 fad si4. si8 |
    sold2\trill dod'4 dod' |
    la la8 la si4( si,) |
    mi2 mi'4 mi'8 mi' |
    si4 si8 si re'4. re'8 |
    la2 la4 la8 la |
    mi4 mi8 mi sol4. sol8 |
    re2 fad4 fad |
    dod dod8 re mi2 |
    la, la4 la |
    dod dod8 re mi2 |
    la,
  }
>>
<<
  \tag #'basse { r2 R1*5 r2 }
  \tag #'vdessus {
    <>^\markup\character Chœur
    dod''4 re'' |
    mi''4. re''8 dod''4 si'\trill |
    la' mi' fad'8[ sold' la' si'] |
    mi'4 la' la' sold'\trill |
    la' mi' fad'8[ sold' la' si'] |
    mi'4 la' la' sold'\trill |
    la'2
  }
  \tag #'vhaute-contre {
    la'4 sold' |
    la'4. si'8 mi'4 sold'\trill |
    la' la' la'2 |
    la'4 dod' re' si |
    dod' la' la'2 |
    la'4 dod' re' si |
    dod'2
  }
  \tag #'vtaille {
    mi'4 re' |
    dod'4. sold8 la4 re' |
    mi' mi' re'2 |
    dod'4 dod' si si |
    la mi' re'2 |
    dod'4 dod' si si |
    la2
  }
  \tag #'vbasse {
    la4 si |
    dod'4. si8 la4 si |
    dod'4 dod re8[ mi fad sold] |
    la4 fad re mi |
    la dod re8[ mi fad sold] |
    la4 fad re mi |
    la,2
  }
>>
<<
  \tag #'vdessus { r2 R1*16 r2 r4 }
  \tag #'vhaute-contre { r2 R1*16 r2 r4 }
  \tag #'vtaille { r2 R1*16 r2 r4 }
  \tag #'(vbasse basse) {
    <>^\markup\character Le Bostangi
    la4 la |
    si re' dod'4. si8 |
    la4\trill \appoggiatura sold8 fad4 fad mid |
    fad8[ sold la si] dod'4 dod' |
    la fad r la |
    re'4. dod'8 si2\trill |
    la4 fad sol2 |
    r4 mi si2 |
    r4 fad8 sol la2 |
    re r4 la |
    re'4. dod'8 si2\trill |
    la4 mi sol2 |
    r4 re fad2 |
    r4 dod8 re mi2 |
    la,4 mi sol2 |
    r4 re fad2 |
    r4 dod8 re mi2 |
    la, << \tag #'vbasse r4 \tag #'basse r2*1/2 >>
  }
>>
<<
  \tag #'basse { s4 R1*7 r2 }
  \tag #'vdessus {
    <>^\markup\character Chœur
    mi''4 |
    fad''4. mi''8 re''2\trill |
    dod''4 r r re'' |
    fad''2 r4 dod'' |
    mi'' re''8 dod'' si'2\trill |
    la'4 r r re'' |
    fad''2 r4 dod'' |
    mi'' re''8 dod'' si'2\trill |
    la'1 |
  }
  \tag #'vhaute-contre {
    la'4 |
    la'4. la'8 la'4( sold') |
    la' r r sol' |
    la'2 r4 fad' |
    mi' mi'8 re' re'4.( dod'8) |
    dod'4\trill r r sol' |
    la'2 r4 fad' |
    mi' mi'8 re' re'2 |
    dod'1\trill |
  }
  \tag #'vtaille {
    dod'4 |
    re'4. la8 si2 |
    mi'4 r r si |
    re'2 r4 la |
    dod' si8 la sold2\trill |
    la4 r r si |
    re'2 r4 la |
    dod' si8 la sold2\trill |
    la1 |
  }
  \tag #'vbasse {
    la4 |
    re'4. dod'8 si2\trill |
    la4 mi sol2 |
    r4 re fad2 |
    r4 dod8 re mi2 |
    la,4 mi sol2 |
    r4 re fad2 |
    r4 dod8 re mi2 |
    la,1 |
  }
>>