\clef "taille" r2 |
R1*17 |
r2 mi'4 re' |
dod'4. sold8 la4 re' |
mi' mi' la la |
la la re' si |
dod' la la la |
la la re' si |
dod'2 r |
R1*16 |
r2 r4 la' |
la'4. la'8 la'4 sold'\trill |
la'4 r r sol' |
fad'2 r4 fad' |
mi' mi'8 re' re'4. mi'8 |
dod'4\trill r r re' |
re'2 r4 fad' |
mi' mi'8 re' re'4. mi'8 |
dod'1\trill |
