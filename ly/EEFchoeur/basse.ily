\clef "basse"
<<
  \tag #'basse-continue {
    la4\douxSug si |
    dod'4. si8 la4 si |
    dod'4 dod re8 mi fad sold |
    la4 fad re mi |
    la dod re8 mi fad sold |
    la4 fad re mi |
    la,2 la8 si dod' la |
    fad sold la fad si dod' red' si |
    sold la si sold dod' re' mi' dod' |
    la si dod' la si4 si, |
    mi2 mi'4 mi' |
    si si re' re' |
    la2 la8 si dod' la |
    mi fad sol mi sol la si sol |
    re mi fad re fad sold la fad |
    dod la, dod re mi4 mi, |
    la,2 la8 si dod' la |
    dod la, dod re mi4 mi, |
    la,2
  }
  \tag #'basse { r2 R1*17 r2 }
>>
la4 si |
dod'4. si8 la4 si |
dod'4 dod re8 mi fad sold |
la4 fad re mi |
la dod re8 mi fad sold |
la4 fad re mi |
la,2
<<
  \tag #'basse-continue {
    la4\douxSug la |
    si re' dod' dod |
    fad2 fad4 mid |
    fad8 sold la si dod'4 dod |
    fad fad re la |
    re'4. dod'8 si2\trill |
    la4 fad sol2 |
    mi4 mi si2 |
    fad4 fad8 sol la4 la, |
    re re' re' la |
    re'4. dod'8 si2\trill |
    la4 mi sol2 |
    re4 re fad2 |
    dod4 dod8 re mi2 |
    la,4 mi sol2 |
    re4 re fad2 |
    dod4 dod8 re mi2 |
    la,4 la,\fortSug la,
  }
  \tag #'basse { r2 R1*16 r2 r4 }
>>
la4 |
re'4. dod'8 si2\trill |
la4 mi sol2 |
re4 re fad2 |
dod4 dod8 re mi2 |
la,4 mi sol2 |
re4 re fad2 |
dod4 dod8 re mi2 |
la,1 |
