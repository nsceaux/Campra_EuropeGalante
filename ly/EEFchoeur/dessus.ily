\clef "dessus" dod''4\doux re'' |
mi''4. re''8 dod''4 si'\trill |
la' mi' fad'8 sold' la' si' |
mi'4 la' la' sold'\trill |
la' mi' fad'8 sold' la' si' |
mi'4 la' la' sold'\trill |
la'8 si' dod'' re'' mi''4 dod'' |
dod'' la' fad' si' |
si' sold' sold' dod'' |
dod'' la' fad' si' |
sold'8 la' si' sold' mi' fad' sold' la' |
si' dod'' re'' si' fad' sold' la' si' |
dod'' re'' mi'' dod'' la'4 mi'' |
mi'' si' si' re'' |
re'' la' la' fad' |
mi' la' la' sold' |
la'8 si' dod'' re'' mi''4 mi' |
mi' la' la' sold'\trill |
la'2
%%
dod''4\fortSug re'' |
mi''4. re''8 dod''4 si'\trill |
la' mi' fad'8 sold' la' si' |
mi'4 la' la' sold'\trill |
la' mi' fad'8 sold' la' si' |
mi'4 la' la' sold'\trill |
la'2
%%
dod''4\doux dod'' |
si' si' la' sold'\trill |
fad'8 sold' la' si' dod''4 dod'' |
dod'' fad'' fad'' mid'' |
fad'' dod'' re'' mi''\trill |
fad'' la'' la'' sold''\trill |
la'' r r re'' |
sol''2 r4 fad'' |
la'' sol''8 fad'' mi''2\trill |
re''4 re'' re'' mi'' |
fad'' la'' la'' sold''\trill |
la'' r r re'' |
fad''2 r4 dod'' |
mi'' re''8 dod'' si'2\trill |
la'4 r r re'' |
fad''2 r4 dod'' |
mi'' re''8 dod'' si'2\trill |
la'4 dod''\fort dod'' mi'' |
fad''4. mi''8 re''2\trill |
dod''4 r r re'' |
fad''2 r4 dod'' |
mi'' re''8 dod'' si'2\trill |
la'4 r r re'' |
fad''2 r4 dod'' |
mi'' re''8 dod'' si'2\trill |
la'1 |
