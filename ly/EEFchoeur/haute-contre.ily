\clef "haute-contre" r2 |
R1*17 |
r2 la'4 sold' |
la'4. si'8 mi'4 sold'\trill |
la' la la re' |
dod' fad' fad' mi' |
mi' mi' la re' |
dod' fad' fad' mi' |
mi'2 r2 |
R1*16 |
r2 r4 dod'' |
re''4. la'8 si'2 |
mi'4 r r si' |
re''2 r4 la' |
dod'' si'8 la' sold'2\trill |
la'4 r r si' |
re''2 r4 la' |
dod'' si'8 la' sold'2\trill |
la'1 |
