Ah ! que l’A -- mour
pré -- pare en ce jour
de con -- ques -- tes nou -- vel -- les !
Que se ap -- pas
vont soû -- met -- tre de Bel -- les
qui n’y pen -- sent pas !
Il va flé -- chir tous les cœurs re -- bel -- les,
il va pour ja -- mais
les bles -- ser de ses traits :
Loin de les crain -- dre,
cher -- chons leurs coups.
Quel cœur peut se plain -- dre
d’un tour -- ment si doux ?
Au Dieu d’A -- mour cé -- dons la vic -- toi -- re ;
Quand il nous sou -- met à ses de -- sirs,
c’est moins pour sa gloi -- re,
que pour nos plai -- sirs.
