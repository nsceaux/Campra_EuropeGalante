\clef "vbas-dessus" <>^\markup\character Une Grace
re''4 |
do'' sib'2\trill |
la'4. sol'8 fa'4 |
sol' sol'2\trill |
la'4. sib'8 la'4 |
sol' fa'\trill mi' |
fa' re' la' |
sol' fa'2 |
do''4. sib'8 la'4 |
sib' la' sol' |
la'4.\trill sol'8 fa'4 |
sol' sol'4.\trill fa'8 |
fa'2 do''4 |
re'' mi''2\trill |
fa''4. mi''8 re''4 |
mi'' fa''2 |
re''4.\trill do''8 re''4 |
mi''4 fa'' \appoggiatura mi''8 \afterGrace re''4( mi''16) |
mi''4.\trill re''8 do''4 |
re'' re''4.\trill do''8 |
do''2 mi''4 |
si'4 do''2 |
sold'4.\trill la'8 si'4 |
mi' la'2 |
sold' la'4 |
si' do'' re'' |
do''4.\trill si'8 la'4 |
si' si'4.\trill la'8 |
la'2 fa''4 |
mi'' re''2 |
dod''4. re''8 mi''4 |
la' re''2 |
dod''4. si'8 la'4 |
sol'4 fa'\trill mi' |
fa'2 fa'4 |
\afterGrace fa'4( sol'16) sol'2\trill |
la'4. re''8 dod''4\trill |
re'' mi''2\trill |
fa''4. mi''8 re''4 |
mi'' mi''4.\trill re''8 |
re''2
