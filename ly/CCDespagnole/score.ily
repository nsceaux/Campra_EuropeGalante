\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Une Espagnole
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basse-Continue" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*9\break s2.*9\break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
