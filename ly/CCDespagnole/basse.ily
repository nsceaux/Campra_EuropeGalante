\clef "basse" sib4 sib la |
sol la sib |
fa fa,2 |
sib,4. do8 re4 |
mib4. re8 mib4 |
fa sib, mib, |
fa,2. |
sib,2 sib,4 |
fa4. fa8 sol4 |
la mi fa |
do re mi |
fa2. |
fa,4 sol, la, |
sib, la, fa, |
do do,2 |
fa4 fa fa |
fa,2 fa4 |
do sol la |
sib sib,2 |
fa4 fa sol |
lab2. |
sol4 la! sib |
mib fa fa, |
sib, fa2 |
sib,2.\fermata |
