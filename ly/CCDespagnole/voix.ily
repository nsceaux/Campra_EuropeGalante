\clef "vbas-dessus" fa'4 fa' fa' |
sib'2. |
do''4 re'' mib'' |
re''2\trill fa''4 |
sol''4. fa''8 sol''4 |
do'' re''4. mib''8 |
re''4( do''2)\trill |
sib'2 re''4 |
la'4.\trill la'8 sib'4 |
do'' sib' la' |
sol'2.\trill |
la'4 sib' do'' |
do''2. |
sol'4 la'4. sib'8 |
la'4( sol'2)\trill |
fa'2. |
fa'4 do'' re'' |
mib''2 do''4 |
re'' mib''16[ re''8.] do''16[ sib'8 do''16] |
do''2.\trill |
r4 fa'' \appoggiatura mi''8 re''4 |
\appoggiatura do''8 sib'4 mib''4. re''8 |
re''4( do''2)\trill |
sib'4 do'' re'' |
sib'2.\fermata |
