\clef "haute-contre" sol'4 sol' fad' |
sol'4. do''8 sib'4 |
sib' la'2\trill |
sib' fa'4 |
sol' sol' fad' |
sol'4. do''8 sib'4 |
sib' la'2\trill |
sib'2. |
sib'4 sib' do'' |
la'2\trill la'4 |
re' fa' sol' |
fad'8 mi' fad' sol' fad'4 |
sol' fa' mib' |
re' do' re' |
sol' fad'4.\trill sol'8 |
sol'2. |
