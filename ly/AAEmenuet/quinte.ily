\clef "quinte" sib4 sib la |
sol4. fa8 sib4 |
do' do'2\trill |
sib sib4 |
sib sib la |
sol4. fa8 sib4 |
do' do'2\trill |
sib2. |
sib4 sib sol |
la2 la4 |
sol la do' |
la2\trill la4 |
sol sib do' |
re' la sol |
sol re' la |
sib2. |
