\clef "dessus" re''4 sol' la' |
sib'4. do''8 re''4 |
mib'' do''2\trill |
re''8 do'' re'' mib'' re''4 |
re'' sol' la' |
sib'4. do''8 re''4 |
mib'' do''2\trill |
re''2. |
re''4 re'' mib'' |
do''4.\trill sib'8 la'4 |
sib' la'\trill sol' |
la'8 sol' la' sib' la'4 |
do'' sib' la' |
sol' fad' sol' |
la' la'4.\trill sol'8 |
sol'2. |
