\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#24 #}))
