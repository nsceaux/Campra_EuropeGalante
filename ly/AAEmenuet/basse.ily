\clef "basse" sol,2 re4 |
sol4. la8 sib4 |
mib fa2 |
sib4. do'8 sib la |
sol2 re4 |
sol,4. la,8 sib,4 |
mib, fa,2 |
sib,2. |
sib4 sib mib |
fa2 fad4 |
sol fa mib |
re2 re4 |
mib re do |
sib, la, sib, |
do re re, |
sol,2. |
