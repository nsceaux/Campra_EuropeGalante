\clef "taille" re'4 re' re' |
re'4. mib'8 re'4 |
sol' fa'2 |
fa' re'4 |
re' re' re' |
re'4. mib'8 re'4 |
sol' fa'2 |
fa'2. |
fa'4 fa' sol' |
fa' do' re' |
sib do' do' |
re'2 re'4 |
do' re' la |
sib do' sib |
mib' re'4. re'8 |
re'2. |
