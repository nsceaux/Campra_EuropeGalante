\clef "dessus" re''2 re''4 la' |
si' dod'' re'' mi'' |
fad'' re'' mi'' dod'' |
re''2 la'4 la' |
re'' mi'' fad'' sol'' |
la'' sol''8 fad'' mi''4 la'' |
fad''4.\trill mi''8 re''4 la' |
si' dod'' re'' dod'' |
si' dod'' re'' dod'' |
si'2\trill si'4 si' |
mi'' mi''8 mi'' re''4 dod'' |
re''4. dod''8 re''4 si' |
dod'' re'' mi'' fad'' |
si'2 si'4 mi'' |
dod''2\trill mi''4 mi'' |
la' si' dod'' re'' |
mi'' re''8 dod'' si'4 mi'' |
dod''2\trill mi''4 fad'' |
re'' re'' re'' mi'' |
dod''\trill re'' mi'' re'' |
dod''2\trill fad''4 fad'' |
re'' re''8 re'' mi''4 mi'' |
dod''2\trill dod'' |
re''4 re'' mi'' fad'' |
sol''2 dod''4 fad'' |
red''2 fad''4 fad'' |
si'' la'' sol'' fad'' |
mi'' fad''8 sol'' fad''4 si'' |
sol''4.\trill fad''8 mi''4 si' |
mi''2 mi''4 re'' |
dod''2.\trill fad''4 |
si' dod'' re'' mi'' |
fad''2 fad''4 la'' |
sol'' fad'' mi'' fad'' |
re''2 re''4 fad'' |
mi'' dod'' re'' si' |
mi''4. re''8 dod'' si' la'4 |
do''2 do''4 re'' |
si'\trill mi'' dod''\trill la'' |
fad''4.\trill mi''8 mi''4.\trill re''8 |
re''4 re'' re'' la' |
re''2 re''4 do'' |
si'2.\trill re''4 |
sol' fad' sol' la' |
si'2 si'4 re'' |
do'' si' la' re'' |
si'2\trill re''4 re'' |
sol' la' si' do'' |
re'' do''8 si' la'4 re'' |
si'2\trill si'4 si' |
mi'' mi'' mi'' fad'' |
red'' mi'' fad'' sol'' |
fad''2\trill sol''4 sol'' |
mi''4 mi''8 mi'' mi''4 fad'' |
red''2\trill red'' |
mi''4 fad'' sol'' fad'' |
mi''2 mi''4 fad'' |
red''2\trill fad''4 fad'' |
si'' la'' sol'' fad'' |
mi'' fad''8 sol'' fad''4 si'' |
sol''4.\trill fad''8 mi''4 si' |
mi''2 mi''4 re'' |
dod''2.\trill re''4 |
si' dod'' re'' mi'' |
fad''2 fad''4 la'' |
sol'' fad'' mi'' fad'' |
re''2 la'4 la' |
re'' mi'' fad'' sol'' |
la'' sol''8 fad'' mi''4 la'' |
fad''4.\trill mi''8 re''4 la' |
re''2 re''4 do'' |
si'2.\trill la'4 |
si' dod'' re'' mi'' |
fad''2 fad''4 la'' |
sol'' fad'' mi'' fad'' |
re''1 |
