\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << <>^"Violons" \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        <>^\markup\character Chœur
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6 s2 \bar "" \pageBreak
        s2 s1*6\pageBreak
        s1*8\pageBreak
        s1*6 s2 \bar "" \pageBreak
        s2 s1*6\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
      }
      \modVersion {
        s1*7\break
        s1*6\break\noPageBreak s1*7\pageBreak
        s1*7\break\noPageBreak s1*8\pageBreak
        s1*8\break\noPageBreak s1*8\pageBreak
        s1*7\break\noPageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
