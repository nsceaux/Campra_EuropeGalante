\clef "haute-contre" la'2 la'4 la' |
sol' mi' si' la' |
la' si' si' la' |
la'2 fad'4 la' |
si' la' la' sol' |
fad' re'' re'' dod'' |
re''2 la'4 la' |
sold' la' si' la' |
sold' la' si' dod'' |
sold'2 sold'4 sold' |
la' la'8 la' si'4 mi' |
mi'2 mi' |
mi'4 sold' la' la' |
la'2 la'4 sold' |
la'2 la'4 sold' |
fad' mi' mi' sold' |
la' si' si' si' |
la'2 dod''4 re'' |
si' si' si' dod'' |
lad' si' dod'' re'' |
lad'2 re''4 re'' |
si' si'8 si' dod''4 dod'' |
lad'2 lad' |
si'4 si' si' la' |
si'2 si'4 lad' |
si'2 red''4 red'' |
mi'' fad'' si' re'' |
mi'' do'' si' si' |
si' si' si' si' |
dod''2 dod''4 la' |
la'2. la'4 |
sol' sol' sol' sol' |
la'2 la'4 re'' |
dod'' re'' re'' dod'' |
re''2 re''4 re'' |
la' la' si' si' |
dod''4. si'8 la'4 la' |
la'2 la'4 la' |
sol' si' la' la' |
la' re'' dod''4.\trill re''8 |
re''4 la' la' la' |
si'2 si'4 fad' |
sol'2. sol'4 |
mi' mi' mi' la' |
sol'2 sol'4 sol' |
fad' sol' sol' fad' |
sol'2 sol'4 fad' |
mi' la' sol' fad' |
sol' sol' sol' fad' |
sol'2 sol'4 si' |
do'' do'' do'' do'' |
si' mi'' red'' mi'' |
red''2 re''4 re'' |
do''4 do''8 do'' do''4 do'' |
si'2 si' |
mi''4 red'' mi'' si' |
do''2 do''4 do'' |
si'2 red''4 red'' |
mi'' fad'' si' re'' |
mi'' do'' si' si' |
si' si' si' si' |
dod''2 dod''4 la' |
la'2. la'4 |
si' si' si' si' |
la'2 la'4 re'' |
dod'' re'' re'' dod'' |
re''2 fad'4 la' |
si' la' la' sol' |
fad' re'' re'' dod'' |
re'' la' la' la' |
si'2 si'4 fad' |
sol'2. la'4 |
sol' sol' sol' sol' |
fad'2 la'4 re'' |
dod'' re'' re'' dod'' |
re''1 |
