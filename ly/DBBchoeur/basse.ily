\clef "basse" re'2 re'4 fad |
sol la si dod' |
re' si sol la |
re2 re4 dod |
si, dod re mi |
fad sol la la, |
re2 re'4 dod' |
si la sold la |
mi dod si, la, |
mi2 mi'4 re' |
dod' dod'8 dod' si4 la |
sold2 sold |
la4 si dod' re' |
mi'2 mi'4 mi |
la2 la4 mi |
fad sold la si |
dod' re' mi' mi |
la2 la4 fad |
sol sol sol mi |
fad re dod si, |
fad2 re4 re |
sol sol8 sol mi4 mi |
fad2 fad |
si4 la sol fad |
mi2 fad4 fad |
si,2 si4 la |
sol fad mi re |
do la, si, si |
mi' mi' mi' re' |
dod'2\trill dod'4 re' |
la2. fad4 |
sol la si dod' |
re'2 re'4 fad |
mi re la, la, |
re2 re'4 re' |
dod' dod' si si |
la2 la4 la |
fad4. mi8 fad4 re |
sol mi la dod |
re sol, la,2 |
re'4 re' re' do' |
si2 si4 la |
sol2. si,4 |
do re mi fad |
sol2 sol4 si, |
la, sol, re re |
sol,2 sol4 re |
mi fad sol la |
si do' re' re |
sol2 sol4 sol |
do' do' do' la |
si sol fad mi |
si2 sol4 sol |
do'4 do'8 do' la4 la |
si2 si |
sol4 fad mi re |
do2 do4 la, |
si,2 si4 la |
sol fad mi re |
do la, si, si |
mi' mi' mi' re' |
dod'2\trill dod'4 re' |
la2. fad4 |
sol la si dod' |
re'2 re'4 fad |
mi re la, la, |
re2 re4 dod |
si, dod re mi |
fad sol la la, |
re' re' re' do' |
si2 si4 la |
sol2. fad4 |
sol la si dod' |
re'2 re'4 fad |
mi re la, la, |
re1 |
