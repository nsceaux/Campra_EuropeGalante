\clef "taille" fad'2 fad'4 fad' |
re' dod' si mi' |
re' fad' sol' mi' |
fad'2 fad'4 mi' |
re' sol' fad' mi' |
re' re' la' la' |
la'4. sol'8 fad'4 mi' |
re' mi' mi' mi' |
mi' mi' re' mi' |
mi'2\trill mi'4 mi' |
mi' dod'8 dod' re'4 mi' |
re'2 re' |
dod'4 si la la' |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
re' re' dod' si |
mi' la' la' sold' |
la'2 la'4 la' |
sol' sol' sol' sol' |
fad' fad' mi' fad' |
fad'2 la'4 la' |
sol' sol'8 sol' sol'4 sol' |
fad'2 fad' |
fad'4 fad' sol' la' |
sol'2 fad'4 fad' |
fad'2 fad'4 fad' |
sol' red' mi' fad' |
la' fad' fad' fad' |
mi'4. fad'8 sol'4 sol' |
sol'2 sol'4 fad' |
mi'2. re'4 |
re' dod' si mi' |
fad'2 fad'4 fad' |
sol' la' la' la' |
fad'2 fad'4 re' |
mi' la' la' sold' |
la'2 mi'4 mi' |
fad'4. sol'8 la' sol' fad' mi' |
re'4 mi' mi' mi' |
fad' sol' sol' mi' |
fad' fad' fad' fad' |
sol'2 sol4 la |
si2. re'4 |
mi' mi' mi' mi' |
re'2 re'4 sol' |
la' re' re' re' |
re'2 re'4 re' |
si re' re' do' |
si mi' re' re' |
re'2 sol'4 sol' |
sol' sol' sol' la' |
fad' sol' la' si' |
si'2 si'4 sol' |
sol'4 sol'8 sol' la'4 la' |
fad'2\trill fad' |
sol'4 la' si' si |
mi'2 sol'4 fad' |
fad'2 fad'4 fad' |
sol' red' mi' fad' |
la' fad' fad' fad' |
mi' sol' sol' sol' |
sol'2 sol'4 fad' |
mi'2.\trill la'4 |
sol' sol' sol' sol' |
fad'2\trill fad'4 fad' |
sol' la' la' la' |
fad'2\trill fad'4 mi' |
re' sol' fad' mi' |
re' re' la' la' |
la' fad' fad' fad' |
sol'2 sol'4 la' |
re'2. re'4 |
re' dod' si mi' |
la'2 fad'4 fad' |
sol' la' la' la' |
fad'1 |
