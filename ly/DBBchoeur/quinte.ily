\clef "quinte" re'2 re'4 do' |
si mi' re' dod' |
fad' fad' mi' mi' |
re'2 re'4 mi' |
fad' mi' re' dod' |
re' si mi' mi' |
re'2 re'4 mi' |
si mi' si dod' |
mi' la sold la |
si2 si4 si |
dod' dod'8 dod' sold4 la |
si2 si |
la4 re' dod' fad' |
mi'2 mi'4 si |
dod'2 dod'4 si |
la mi' mi' re' |
dod' fad' mi' mi' |
mi'2 mi'4 re' |
re' re' re' dod' |
dod' si lad si |
dod'2 re'4 re' |
re' re'8 re' dod'4 dod' |
dod'2 dod' |
si4 fad si dod' |
dod'2 dod'4 dod' |
si2 si4 si |
si fad' sol' si |
do' mi' mi' red' |
mi' mi' mi' mi' |
mi'2 la4 re' |
mi'2. fad'4 |
re' la re' la |
re'2 re'4 re' |
mi' la la la |
la2 la'4 la' |
la' la re' re' |
dod'2\trill dod'4 dod' |
re'2 re'4 la |
si si dod' mi' |
re' si mi' la |
la re' re' re' |
re'2 si4 do' |
re'2. re'4 |
do' do' do' do' |
si2\trill si4 si |
do' re' re' la |
si2 si4 la |
sol fad si la |
sol sol' re' la |
si2 re'4 re' |
do' sol' sol' fad' |
fad' mi' la mi' |
fad'2 si4 si |
do'4 sol'8 sol' sol'4 fad' |
fad'2 fad' |
si4 la sol re' |
sol2 sol4 la |
fad2\trill si4 si |
si fad' sol' si |
do' mi' mi' red' |
mi' mi' mi' mi' |
mi'2 la4 re' |
mi'2. re'4 |
re' dod' si mi' |
re'2 re'4 re' |
mi' la la la |
la2 re'4 mi' |
fad' mi' re' dod' |
re' si la mi' |
re' re' re' re' |
re'2 si4 do' |
re'2. do'4 |
si la re' la |
re'2 re'4 re' |
mi' la la la |
la1 |
