<<
  \tag #'vdessus {
    \clef "vdessus" re''2 re''4 la' |
    si' dod'' re'' mi'' |
    fad'' re'' mi'' dod'' |
    re''2 r |
    R1*2 |
    re''2 re''4 la' |
    si'4 dod'' re'' dod'' |
    si' dod'' re'' dod'' |
    si'2\trill si'4 si' |
    mi'' mi''8 mi'' re''4 dod'' |
    si'2\trill si' |
    dod''4 re'' mi'' fad'' |
    si'2\trill si'4 mi'' |
    dod''2\trill r |
    R1*2 |
    mi''2 mi''4 fad'' |
    re'' re'' re'' mi'' |
    dod''\trill re'' mi'' re'' |
    dod''2\trill fad''4 fad'' |
    re'' re''8 re'' mi''4 mi'' |
    dod''2\trill dod'' |
    re''4 re'' mi'' fad'' |
    sol''2 dod''4 fad'' |
    red''2 r |
    R1*2 |
    si'4 si' si' si' |
    mi''2 mi''4 re'' |
    dod''2.\trill re''4 |
    si' dod'' re'' mi'' |
    fad''2 fad''4 la'' |
    sol'' fad'' mi''\trill fad'' |
    re''2 r |
    R1*5 |
    re''4 re'' re'' la' |
    re''2 re''4 do'' |
    si'2.\trill re''4 |
    sol' fad' sol' la' |
    si'2 si'4 re'' |
    do'' si' la' re'' |
    si'2\trill r |
    R1*2 |
    si'2 si'4 si' |
    mi'' mi'' mi'' fad'' |
    red'' mi'' fad'' sol'' |
    fad''2\trill sol''4 sol'' |
    mi''4 mi''8 mi'' mi''4 fad'' |
    red''2 red'' |
    mi''4 fad'' sol'' fad'' |
    mi''2 mi''4 fad'' |
    red''2 r |
    R1*2 |
    si'4 si' si' si' |
    mi''2 mi''4 re'' |
    dod''2.\trill re''4 |
    si' dod'' re'' mi'' |
    fad''2 fad''4 la'' |
    sol'' fad'' mi'' fad'' |
    re''2 r |
    R1*2 |
    re''4 re'' re'' la' |
    re''2 re''4 do'' |
    si'2.\trill la'4 |
    si' dod'' re'' mi'' |
    fad''2 fad''4 la'' |
    sol'' fad'' mi'' fad'' |
    re''1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" la'2 la'4 la' |
    sol' mi' re' dod' |
    fad' fad' sol' mi' |
    fad'2\trill r |
    R1*2 |
    la'2 la'4 la' |
    sold' la' mi' mi' |
    mi' mi' re' mi' |
    mi'2 sold'4 sold' |
    la' la'8 la' sold'4 la' |
    si'2 si' |
    la'4 sold' la' la' |
    la'2 la'4 sold' |
    la'2 r |
    R1*2 |
    la'2 la'4 la' |
    sol' sol' sol' sol' |
    fad'4 fad' mi' fad' |
    fad'2 la'4 la' |
    sol' sol'8 sol' sol'4 sol' |
    fad'2 fad' |
    fad'4 fad' sol' la' |
    sol'2 fad'4 fad' |
    fad'2 r |
    R1*2 |
    sol'4 sol' sol' sol' |
    sol'2 sol'4 fad' |
    mi'2.\trill la'4 |
    sol' sol' sol' sol' |
    fad'2 fad'4 fad' |
    sol' la' la' la' |
    fad'2\trill r |
    R1*5 |
    fad'4 fad' fad' fad' |
    sol'2 sol'4 fad' |
    sol'2. sol'4 |
    mi' mi' mi' mi' |
    re'2 re'4 sol' |
    fad' sol' sol' fad' |
    sol'2 r |
    R1*2 |
    sol'2 sol'4 sol' |
    sol' sol' sol' la' |
    fad' sol' red' mi' |
    red'2\trill si'4 si' |
    sol'4 sol'8 sol' la'4 la' |
    fad'2\trill fad' |
    sol'4 red' mi' fad' |
    sol'2 sol'4 la' |
    fad'2\trill r |
    R1*2 |
    sol'4 sol' sol' sol' |
    sol'2 sol'4 fad' |
    mi'2.\trill la'4 |
    sol' sol' sol' sol' |
    fad'2 fad'4 fad' |
    sol' la' la' la' |
    fad'2\trill r |
    R1*2 |
    fad'4 fad' fad' fad' |
    sol'2 sol'4 fad' |
    sol'2. la'4 |
    sol' sol' sol' sol' |
    fad'2 fad'4 fad' |
    sol' la' la' la' |
    fad'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" fad'2 fad'4 fad' |
    re' dod' si la |
    re' re' si la |
    la2 r |
    R1*2 |
    fad'2 fad'4 mi' |
    re' dod' si la |
    sold la si dod' |
    sold2\trill mi'4 mi' |
    mi' la8 la re'4 mi' |
    mi'2 mi' |
    mi'4 re' dod' fad' |
    mi'2 mi'4 mi' |
    mi'2 r |
    R1*2 |
    dod'2 dod'4 re' |
    si si si dod' |
    lad si dod' re' |
    lad2\trill re'4 re' |
    si si8 si dod'4 dod' |
    lad2\trill lad |
    si4 fad si la |
    si2 si4 lad |
    si2 r |
    R1*2 |
    mi'4 mi' mi' mi' |
    mi'2 la4 re' |
    mi'2. re'4 |
    re' dod' si mi' |
    re'2 re'4 re' |
    dod' re' re' dod' |
    re'2 r |
    R1*5 |
    re'4 re' re' re' |
    re'2 si4 do' |
    re'2. re'4 |
    do' do' do' do' |
    si2\trill si4 si |
    do' re' re' re' |
    re'2 r |
    R1*2 |
    re'2 re'4 re' |
    do' do' do' do' |
    si si la si |
    si2 si4 re' |
    do'4 do'8 do' do'4 do' |
    si2 si |
    si4 la sol si |
    do'2 do'4 do' |
    si2 r |
    R1*2 |
    mi'4 mi' mi' si |
    dod'2 la4 re' |
    mi'2. re'4 |
    re' dod' si mi' |
    re'2 re'4 re' |
    dod' re' re' dod' |
    re'2 r |
    R1*2 |
    re'4 re' re' re' |
    re'2 si4 do' |
    re'2. re'4 |
    re' dod' si mi' |
    re'2 re'4 re' |
    dod' re' re' dod' |
    re'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" re'2 re'4 fad |
    sol la si dod' |
    re' si sol la |
    re2 r |
    R1*2 |
    re'2 re'4 dod' |
    si la sold la |
    mi dod si, la, |
    mi2 mi'4 re' |
    dod' dod'8 dod' si4 la |
    sold2 sold |
    la4 si dod' re' |
    mi'2 mi'4 mi |
    la2 r |
    R1*2 |
    la2 la4 fad |
    sol sol sol mi |
    fad re dod si, |
    fad2 re4 re |
    sol sol8 sol sol4 mi |
    fad2 fad |
    si4 la sol fad |
    mi2 fad4 fad |
    si,2 r |
    R1*2 |
    mi'4 mi' mi' re' |
    dod'2\trill dod'4 re' |
    la2. fad4 |
    sol la si dod' |
    re'2 re'4 fad |
    mi re la, la, |
    re2 r |
    R1*5 |
    re'4 re' re' do' |
    si2 si4 la |
    sol2. si,4 |
    do re mi fad |
    sol2 sol4 si, |
    la, sol, re re |
    sol,2 r |
    R1*2 |
    sol2 sol4 sol |
    do' do' do' la |
    si sol fad mi |
    si2 sol4 sol |
    do' do'8 do' la4 la |
    si2 si |
    sol4 fad mi re |
    do2 do4 la, |
    si,2 r |
    R1*2 |
    mi'4 mi' mi' re' |
    dod'2\trill dod'4 re' |
    la2. fad4 |
    sol la si dod' |
    re'2 re'4 fad |
    mi re la, la, |
    re2 r |
    R1*2 |
    re'4 re' re' do' |
    si2 si4 la |
    sol2. fad4 |
    sol la si dod' |
    re'2 re'4 fad |
    mi re la, la, |
    re1 |
  }
>>
