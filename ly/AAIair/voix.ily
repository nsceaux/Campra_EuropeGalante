<<
  \tag #'(grace1 basse) {
    \clef "vbas-dessus" <>^\markup\character { Deux Graces et le chœur }
    re''4 do'' si' |
    do'' si' la' |
    si'4.\trill la'8( sol'4) |
    si' dod'' re'' |
    re''4. mi''8 dod''4 |
    re''2. |
    la'4 si' do'' |
    si' la'\trill sol' |
    la' la' re'' |
    re''2 si'4 |
    do'' la'4.\trill sol'8 |
    sol'2. |
  }
  \tag #'grace2 {
    \clef "vbas-dessus" si'4 la' sol' |
    la' sol' fad' |
    sol'2 re'4 |
    sol' sol' fad' |
    mi' fad' sol' |
    fad'2.\trill |
    fad'4 sol' la' |
    sol' fad' mi' |
    fad' fad' sol' |
    la'2 sol'4 |
    la' fad'4.\trill sol'8 |
    sol'2. |
  }
>>