\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'grace1 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'grace2 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*8\break }
    >>
  >>
  \layout { }
  \midi { }
}