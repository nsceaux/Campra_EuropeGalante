\clef "basse" sol4 fad sol |
do re re, |
sol,2. |
sol8 fad mi4 re |
la la,2 |
re2. |
re4 sol fad |
sol fad mi |
re re' si |
fad2 sol4 |
do re re, |
sol,2. |
