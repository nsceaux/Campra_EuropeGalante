\clef "basse" r4 |
R2. |
r4 r la |
fa re2 |
dod4. si,8 la,4 |
re sol, la, |
re,2 re'4 |
la sib2 |
fa4. sol8 la4 |
sib sol fa |
do4. sib,8 la,4 |
sib, la, sol, |
fa,4. sol,8 la,4 |
sib, do do, |
fa,2 fa4 |
fa mi2 |
re sol4 |
do' fa2 |
sol4. la8 si4 |
do' fa sol |
do4. re8 mi4 |
fa sol sol, |
do2 do'4 |
do' la2 |
mi'4. mi'8 re'4 |
do'4 la2 |
mi'4. re'8 do'4 |
si la sold |
la2 la4 |
la fa2\trill |
mi4. re8 do4 |
si, la, sold, |
la,4. si,8 do4 |
re mi mi, |
la,2 re'4 |
do' sib2 |
la4. si8 dod'4 |
re' re2 |
la,4. si,8 dod4 |
re mi fa |
dod2 re4 |
do sib,2 |
la, sib,4 |
sol, la,2 |
re4 la, sib, |
sol, la,2 |
re2 fa4 |
fa mi2 |
re2 re'4 |
do' sib2 |
la4. si8 dod'4 |
re' re2 |
la,4. si,8 dod4 |
re mi fa |
dod2 re4 |
do sib,2 |
la, sib,4 |
sol, la,2 |
re4 la, sib, |
sol, la,2 |
re2. |
