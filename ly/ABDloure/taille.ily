\clef "taille" r4 |
R2. |
r4 r dod' |
re' re'2 |
la'4. re'8 la'4 |
la' sib' la' |
la'2 fa'4 |
fa' re'2 |
do' fa'4 |
re' sol' la' |
sol'2 la'4 |
mi' fa' sol' |
la' fa' fa' |
fa' mi'4.\trill fa'8 |
fa'2 fa'4 |
fa' sol'2 |
fa' sol'4 |
sol' la'2 |
sol' sol'4 |
mi' re' re' |
do' mi' mi' |
re' re'4. re'8 |
mi'2 sol'4 |
sol' do''2 |
si'4. la'8 sold'4 |
la' la'2 |
si' la'4 |
si' do'' mi' |
mi'2 mi'4 |
mi' re'2 |
si\trill do'4 |
re' mi' mi' |
mi'4. re'8 do'4 |
fa' mi'4. mi'8 |
mi'2 re'4 |
mi' fa' sol' |
mi'2\trill mi'4 |
re' fa'2 |
mi' la'4 |
la' sol' fa' |
mi'2 re'4 |
mi' sol'2 |
mi' re'4 |
sib' la'2 |
la'4. sol'8 fa'4 |
sol' sol' mi' |
fa'2 fa'4 |
fa' sol'2 |
fa'2 re'4 |
mi' fa' sol' |
mi'2\trill mi'4 |
re' fa'2 |
mi' la'4 |
la' sol' fa' |
mi'2 re'4 |
mi' sol'2 |
mi' re'4 |
sib' la'2 |
la'4. sol'8 fa'4 |
sol' sol' mi' |
fa'2. |
