\clef "quinte" r4 |
R2. |
r4 r la |
la re'2 |
la mi'4 |
re' mi' mi' |
re'2 re'4 |
do' sib2 |
do'2 do'4 |
sib sib la |
do'2 do'4 |
sib fa mi |
fa do' do' |
sib sol do' |
do'2 la4 |
la sol2 |
la si4 |
do' do'2 |
sol' sol4 |
do' la sol |
sol2 do'4 |
la re' sol |
sol2 mi'4 |
mi' mi'2 |
mi' mi'4 |
mi' mi'2 |
mi' la4 |
re' mi' re' |
do'2 do'4 |
do' la2 |
mi'2 la4 |
si do' re' |
do'4. re'8 mi'4 |
re' si mi' |
dod'2 re'4 |
la re'2 |
mi' la4 |
re' re'2 |
mi'4. fa'8 sol'4 |
fa' sol' la' |
la2 la4 |
la re'2 |
la'4 la re' |
mi' mi'2 |
la4 la re' |
re' dod'4.\trill re'8 |
re'2 la4 |
la sol2 |
re'2 re'4 |
la re'2 |
mi' la4 |
re' re'2 |
mi'4. fa'8 sol'4 |
fa' sol' la' |
la2 la4 |
la re'2 |
la'4 la re' |
mi' mi'2 |
la4 la re' |
re' dod'4.\trill re'8 |
re'2. |
