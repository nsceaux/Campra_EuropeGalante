\clef "haute-contre" r4 |
R2. |
r4 r la' |
la' la'2 |
la'4. si'8 dod''4 |
re'' re'' dod'' |
re''2 la'4 |
la' fa'2 |
fa' fa'4 |
fa' sib' do'' |
do''2 do''4 |
re'' do'' sib' |
do''2 do''4 |
re'' do''4. do''8 |
la'2 la'4 |
si' do''2 |
do''4. do''8 si'4 |
sol' do''2 |
si'4.\trill la'8 sol'4 |
sol' la' si' |
do'' sol' sol' |
do'' si'4.\trill do''8 |
do''2 do''4 |
si' mi''2 |
mi''4. mi''8 si'4 |
do'' mi''2 |
mi''4. si'8 do''4 |
sold' la' si' |
la'2 la'4 |
la' la'2 |
sold' la'4 |
sold' la' si' |
la'4. sold'8 la'4 |
la' sold'4.\trill la'8 |
la'2 la'4 |
la' la'( sol') |
la'2 mi'4 |
fa' la'2 |
la' la'4 |
re'' dod'' re'' |
mi''2 fa''4 |
mi'' re''2 |
dod'' re''4 |
re'' dod''2 |
re''4 do'' sib' |
sib'? la'4. la'8 |
la'2 la'4 |
si' do''2 |
la'2 la'4 |
la' la'( sol') |
la'2 mi'4 |
fa' la'2 |
la' la'4 |
re'' dod'' re'' |
mi''2 fa''4 |
mi'' re''2 |
dod'' re''4 |
re'' dod''2 |
re''4 do'' sib' |
sib'? la'4. la'8 |
la'2. |
