\clef "dessus" la''4 |
fa'' re''2 |
dod''4.\trill re''8 mi''4 |
la' fa''2 |
mi''4.\trill re''8 mi''4 |
fa'' sol'' mi'' |
fa'' re'' fa'' |
do'' re''2 |
la'4.\trill sib'8 do''4 |
re'' mi'' fa'' |
mi''\trill do'' fa'' |
sol'' la'' sib'' |
la''4.\trill sol''8 fa''4 |
sol'' sol''4.\trill fa''8 |
fa''2 do''4 |
re'' mi''2\trill |
fa''4. mi''8 re''4 |
mi'' fa''2 |
re''4.\trill do''8 re''4 |
mi'' fa'' re'' |
mi''4.\trill re''8 do''4 |
re'' re''4.\trill do''8 |
do''2 mi''4 |
mi'' la''2 |
sold''4.\trill la''8 si''4 |
mi''4 la''2 |
sold''4.\trill fa''8 mi''4 |
re'' do''\trill si' |
do''2 do''4 |
do'' re''2\trill |
mi''4. fa''8 mi''4 |
re'' do''\trill si' |
do''4. si'8 la'4 |
si' si'4.\trill la'8 |
la'2 fa''4 |
mi'' re''2 |
dod''4.\trill re''8 mi''4 |
la' re''2 |
dod''4.\trill re''8 mi''4 |
fa'' mi''\trill re'' |
la''2 la''4 |
la'' sol''4.\trill( fa''16 sol'') |
la''4. sol''8 fa''4 |
sol'' mi''2\trill |
fa''4. mi''8 re''4 |
mi'' mi''4.\trill re''8 |
re''2 do''4 |
re'' mi''2\trill |
re''2 fa''4 |
mi'' re''2 |
dod''4.\trill re''8 mi''4 |
la' re''2 |
dod''4.\trill re''8 mi''4 |
fa'' mi''\trill re'' |
la''2 la''4 |
la'' sol''4.\trill( fa''16 sol'') |
la''4. sol''8 fa''4 |
sol'' mi''2\trill |
fa''4. mi''8 re''4 |
mi'' mi''4.\trill re''8 |
re''2. |
