\clef "quinte" re'4 |
re' sib sib |
do' do' fa |
fa2 do'8 do' |
sib2 re'8 do' |
sib4 sib sol |
do' fa4. fa8 |
fa2 sib4 |
fa'4 mib' re' |
do' sib la |
sol2 sol'8 sol' |
la'2 la'8 la' |
sib'4 sib do' |
re' re' re |
sol2 si4 |
do' do' do' |
do'4 sol la |
do'2 do'8 do' |
la2 do'8 sib |
la4 do' sib |
sol4. la8 sib4 |
la2\trill fa'4 |
re'2\trill sol'4 |
re' sib sib |
sib sib sib |
re'2 sib4 |
fa' mib' re' |
do' sib la |
sol2 sib8 fa |
sib2 re'8 do' |
sib4 sib sol |
do'4 do'4.\trill sib8 |
sib2 fa4 |
fa fa fa' |
mib' sol do' |
sib2 r4 |
R2.*3 |
r4 r si |
do' do' do' |
do' sol la |
do'2 do'8 do' |
la2 do'8 sib |
la4 do' sib |
sol4. la8 sib4 |
la2\trill la4 |
do'2 do'4 |
sol' sol' fa' |
mib' re' do' |
si2\trill si8 si |
do'2 do'8 do' |
sib4 sib do' |
lab lab8 sol lab4 |
sol2\trill re'8 re' |
do'2 mib'8 re' |
do'4 do' do' |
sol sol4. sol8 |
sol2 do'4 |
re' re' do' |
sib4 do' fa |
fa2 do'8 do' |
sib2 re'8 do' |
sib4 sib sol |
do' fa4. fa8 |
fa2 fa'4 |
fa' sib sib |
sib sib lab |
sol2 r4 |
R2.*3 |
r4 r re' |
re' sib sib |
do' do' fa |
fa2 do'8 do' |
sib2 re'8 do' |
sib4 sib sol |
do' fa4. fa8 |
fa2. |
