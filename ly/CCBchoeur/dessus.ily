\clef "dessus" sib'4 |
fa'' re'' sib' |
mib'' mib'' re'' |
do''2\trill do''8 do'' |
re''2 re''8 mib'' |
fa''4 fa' sib' |
sib'4. do''8 la'4\trill |
sib'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    do''\trill do'' re'' |
    mib'' re'' do'' |
    re''2 sol''8 sol'' |
    sol''2 fa''8 fa'' |
    fa''4 re'' sol'' |
    sol''4. sol''8 fad''4 |
    sol''2 }
  { sib'4 la'\trill la' sib' |
    do'' sol' la' |
    sib'2 sib'8 sib' |
    do''2 do''8 do'' |
    re''4 re'' mib'' |
    la'\trill la' re'' |
    si'2 }
>> sol''4 |
mi''\trill fa'' sol'' |
do'' sib' la' |
sol'2\trill sol'8 sol' |
la'2 la'8 sib' |
do''4 fa'' fa'' |
fa''4. sol''8 mi''4\trill |
fa''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { do''4 | fa''2 }
  { la'4 | re''2 }
>> fa''4 |
sib' sib' sib' |
mib'' mib'' mib'' |
re''2\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    do''\trill do'' re'' |
    mib'' re'' do'' |
    re''2 }
  { sib'4 |
    la'\trill la' si' |
    do'' sol' la' |
    sib'2 }
>> sib'8 do'' |
re''2 re''8 mib'' |
fa''4 fa' sib' |
sib'4. do''8 la'4 |
sib'2 <>^\markup\whiteout "Violons" re''4 |
do''\trill do'' re'' |
mib'' re'' do'' |
re''2\twoVoices #'(dessus1 dessus2 dessus) <<
  << <>^"Flutes" 
    { sib''8 sib'' |
      sib''2 la''8 la'' |
      la''4 la'' la'' |
      sib'' la''4.\trill sol''8 |
      sol''2 } \\
    { re''8 re'' |
      mi''2 mi''8 mi'' |
      fad''4 fad'' fad'' |
      sol'' fad''4. sol''8 |
      sol''2 }
  >>
  { <>^\markup\whiteout "Violons" sol'8\doux sol' |
    do''2 do''8 do'' |
    re''4 re'' re'' |
    do'' re'' re' |
    sol'2 }
>> <>^\markup\whiteout "Tous" sol''4 |
mi''\trill fa'' sol'' |
do'' sib' la' |
sol'2\trill sol'8 sol' |
la'2 la'8 sib' |
do''4 fa'' fa'' |
fa''4. sol''8 mi''4\trill |
fa''2 do''4 |
mib''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mib''4 |
    re''\trill re'' re'' |
    sol'' fa'' mib'' |
    re''2\trill re''8 re'' |
    mib''2 mib''8 mib'' |
    re''4\trill re'' mib'' |
    do''4 do''8( si') do''4 |
    si'2\trill }
  { do''4 |
    si' si' si' |
    do'' si' do'' |
    sol'2 sol'8 sol' |
    sol'2 lab'8 lab' |
    lab'4 sol' sol' |
    sol'4. sol'8 fa'4 |
    sol'2 }
>> re''8 re'' |
mib''2 mib''8 fa'' |
sol''4 sol' do'' |
do''4. re''8 si'4 |
do''2 mib''4 |
re''\trill re'' mib'' |
fa'' mib'' re'' |
do''2\trill do''8 do'' |
re''2 re''8 mib'' |
fa''4 fa' sib' |
sib'4. do''8 la'4\trill |
sib'2 fa''4 |
re''\trill re'' fa'' |
sib' sib' re'' |
sol'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  << <>^"Flutes"
    { sol''8 sol'' |
      sol''2 sol''8 sol'' |
      la''4 la'' la'' |
      sib'' la''4.\trill sib''8 |
      sib''2 } \\
    { mib''8 mib'' |
      mib''2 mib''8 mib'' |
      mib''4 mib'' mib'' |
      re'' do''4.\trill sib'8 |
      sib'2 }
  >>
  { <>^\markup\whiteout "Violons" mib'8\doux mib' |
    mib'2 mib'8 mib' |
    fa'4 fa' fa' |
    mib' fa'4. fa'8 |
    sib2 }
>> <>^\markup\whiteout "[Tous]" sib'4 |
fa'' re'' sib' |
mib'' mib'' re'' |
do''2\trill do''8 do'' |
re''2 re''8 mib'' |
fa''4 fa' sib' |
sib'4. do''8 la'4\trill |
sib'2. |
