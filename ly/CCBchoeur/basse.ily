\clef "basse" sib4 |
re re mib |
do\trill do sib, |
fa2 fa8 fa |
sib2 sib8 do' |
re'4 re mib |
fa fa fa, |
sib,2 \clef "alto" sib4 |
fa' mib' re' |
do' sib la |
sol2 sol'8 sol' |
la'2 la'8 la' |
sib'4 sib do' |
re' re' re |
sol2 \clef "bass" sol4 |
do' do' sib |
la sol fa |
mi2\trill mi8 mi |
fa2 fa8 sol |
la4 la, sib, |
do do do, |
fa,2 \clef "alto" fa'4 |
re'2 \clef "bass" re4 |
sol sol sol |
mib mib mib |
sib2 \clef "alto" sib4 |
fa' mib' re' |
do' sib la |
sol2 \clef "bass" sol8 la |
sib2 sib8 do' |
re'4 re mib |
fa fa fa, |
sib,2 sib,4 |
fa mib re |
do sib, la, |
sol,2 r4 |
R2.*3 |
r4 r sol |
do' do' sib |
la sol fa |
mi2\trill mi8 mi |
fa2 fa8 sol |
la4 la, sib, |
do do do, |
fa,2 fa4 |
do2 \clef "alto" do'4 |
sol' sol' fa' |
mib' re' do' |
si2 si8 si |
do'2 do'8 do' |
sib4 sib do' |
lab4 lab8 sol lab4 |
sol2\trill \clef "bass" sol8 sol |
do'2 do'8 re' |
mib'4 mib fa |
sol sol8 fa sol4 |
do2 do4 |
sol fa mib |
re do sib, |
fa,2 fa8 fa |
sib2 sib8 do' |
re'4 re mib |
fa fa fa, |
sib,2 fa4 |
sib sib lab |
sol sol fa |
mib2 r4 |
R2.*3 |
r4 r sib |
re re mib |
do\trill do sib, |
fa2 fa8 fa |
sib2 sib8 do' |
re'4 re mib |
fa fa fa, |
sib,2. |
