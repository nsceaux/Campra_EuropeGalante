\clef "haute-contre" sib'4 |
sib' sib' sib' |
la' la' sib' |
la'2\trill la'8 la' |
sib'2 sib'8 la' |
sib'4 fa' sol' |
fa' fa' fa' |
fa'2 sib4 |
fa'4 mib' re' |
do' sib la |
sol2 sol'8 sol' |
la'2 la'8 la' |
sib'4 sib do' |
re' re'4. re'8 |
sol2 re''4 |
do'' do'' sol' |
la' mi' fa' |
sol'2 sol'8 sol' |
fa'2 fa'8 mi' |
fa'4 fa' fa' |
do'' do'' do'' |
do''2 fa'4 |
re'2\trill la'4 |
sol' sol' sol' |
sol' sib' sib' |
sib'2 sib4 |
fa' mib' re' |
do' sib la |
sol2 sol'8 fa' |
fa'2 sib'8 la' |
sib'4 fa' sol' |
fa' fa'4. fa'8 |
fa'2 sib'4 |
la'\trill la' si' |
do'' sol' la' |
sib'2 r4 |
R2.*3 |
r4 r re'' |
do'' do'' sol' |
la' mi' fa' |
sol'2 sol'8 sol' |
fa'2 fa'8 mi' |
fa'4 fa' fa' |
do'' do''4. do''8 |
do''2 do''4 |
do''2 do'4 |
sol' sol' fa' |
mib' re' do' |
si2 si8 si |
do'2 do'8 do' |
sib4 sib do' |
lab4 lab8 sol lab4 |
sol2 si'8 si' |
do''2 do''8 si' |
do''4 sol' lab' |
sol' sol' sol' |
sol'2 sol'4 |
sib' sib' do'' |
re'' la' sib' |
la'2\trill la'8 la' |
sib'2 sib'8 la' |
sib'4 fa' sol' |
fa'4 fa'4. fa'8 |
fa'2 la'4 |
sib' sib' fa' |
sol' sol' re' |
mib'2 r4 |
R2.*3 |
r4 r sib' |
sib' sib' sib' |
la' la' sib' |
la'2\trill la'8 la' |
sib'2 sib'8 la' |
sib'4 fa' sol' |
fa' fa'4. fa'8 |
fa'2. |
