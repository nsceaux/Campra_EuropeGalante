\clef "taille" fa'4 |
fa' fa' sol' |
mib' mib' fa' |
fa'2 fa'8 fa' |
fa'2 fa'8 mib' |
re'4 re' re' |
do'4. re'8 mib'4 |
re'2\trill sib4 |
fa' mib' re' |
do' sib la |
sol2 sol'8 sol' |
la'2 la'8 la' |
sib'4 sib do' |
re' re' re' |
sol2 sol'4 |
sol' fa' mi' |
fa' sol' do' |
do'2 do'8 do' |
do'2 la'8 sol' |
fa'4 fa' re' |
do' do'4. do'8 |
do'2 fa'4 |
re'2\trill re'4 |
re'4 re' re' |
sol' sol' sol' |
fa'2 sib4 |
fa' mib' re' |
do' sib la |
sol2 re'8 do' |
sib2 fa'8 mib' |
re'4 re' re' |
do'4.\trill re'8 mib'4 |
re'2\trill fa'4 |
fa' fa' fa' |
sol' sol' fad' |
sol'2 r4 |
R2.*3 |
r4 r sol' |
sol' fa' mi' |
fa' sol' do' |
do'2 do'8 do' |
do'2 la'8 sol' |
fa'4 fa' re' |
do'4 do'4. do'8 |
do'2 fa'4 |
sol'2 do'4 |
sol' sol' fa' |
mib' re' do' |
si2 si8 si |
do'2 do'8 do' |
sib4 sib do' |
lab4 lab8 sol lab4 |
sol2\trill sol'8 sol' |
sol'2 sol'8 fa' |
mib'4 mib' mib' |
mib' re'4.\trill re'8 |
mib'2 sol'4 |
sol' sol' la' |
sib' mib' fa' |
fa'2 fa'8 fa' |
fa'2 fa'8 mib' |
re'4 re' re' |
do'4. re'8 mib'4 |
re'2\trill do'4 |
re' re' re' |
mib' mib' fa' |
sib2 r4 |
R2.*3 |
r4 r fa' |
fa' fa' sol' |
mib' mib' fa' |
fa'2 fa'8 fa' |
fa'2 fa'8 mib' |
re'4 re' re' |
do'4.\trill re'8 mib'4 |
re'2.\trill |
