<<
  \tag #'vdessus {
    \clef "vdessus" sib'4 |
    fa'' re'' sib' |
    mib'' mib'' re'' |
    do''2\trill do''8 do'' |
    re''2 re''8 mib'' |
    fa''4 fa' sib' |
    sib'4. do''8 la'4\trill |
    sib'2 <<
      { \voiceOne re''4 |
        do''4\trill do'' re'' |
        mib'' re'' do'' |
        re''2 sol''8 sol'' |
        sol''2 fa''8 fa'' |
        fa''4 re'' sol'' |
        sol''4. sol''8 fad''4 |
        sol''2
        \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'4 |
        la'\trill la' si' |
        do'' sol' la' |
        sib'2 sib'8 sib' |
        do''2 do''8 do'' |
        re''4 re'' mib'' |
        la'\trill la' re'' |
        si'2
      }
    >> sol''4 |
    mi''\trill fa'' sol'' |
    do'' sib' la' |
    sol'2\trill sol'8 sol' |
    la'2 la'8 sib' |
    do''4 fa'' fa'' |
    fa''4. sol''8 mi''4\trill |
    fa''2 <<
      { \voiceOne do''4 fa''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo la'4 re''2
      }
    >> fa''4 |
    sib' sib' sib' |
    mib'' mib'' mib'' |
    re''2\trill <<
      { \voiceOne re''4 |
        do''\trill do'' re'' |
        mib'' re'' do'' |
        re''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'4 |
        la'\trill la' si' |
        do'' sol' la' |
        sib'2
      }
    >> sib'8 do'' |
    re''2 re''8 mib'' |
    fa''4 fa' sib' |
    sib'4. do''8 la'4\trill |
    sib'2 r4 |
    R2.*6 |
    r4 r sol'' |
    mi''\trill fa'' sol'' |
    do'' sib' la' |
    sol'2\trill sol'8 sol' |
    la'2 la'8 sib' |
    do''4 fa'' fa'' |
    fa''4. sol''8 mi''4\trill |
    fa''2 do''4 |
    mib''2 <<
      { \voiceOne mib''4 |
        re''\trill re'' re'' |
        sol'' fa'' mib'' |
        re''2\trill re''8 re'' |
        mib''2 mib''8 mib'' |
        re''4\trill re'' mib'' |
        do''4 do''( si'8) do'' |
        si'2\trill \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo do''4 |
        si' si' si' |
        do'' si' do'' |
        sol'2 sol'8 sol' |
        sol'2 lab'8 lab' |
        lab'4 sol' sol' |
        sol'4. sol'8 fa'4 |
        sol'2
      }
    >> re''8 re'' |
    mib''2 mib''8 fa'' |
    sol''4 sol' do'' |
    do''4. re''8 si'4\trill |
    do''2 r4 |
    R2.*6 |
    r4 r fa'' |
    re''\trill re'' fa'' |
    sib' sib' re'' |
    sol'2 r4 |
    R2.*3 |
    r4 r sib' |
    fa'' re'' sib' |
    mib'' mib'' re'' |
    do''2\trill do''8 do'' |
    re''2 re''8 mib'' |
    fa''4 fa' sib' |
    sib'4. do''8 la'4\trill |
    sib'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fa'4 |
    fa' fa' sol' |
    mib' do' fa' |
    fa'2 la'8 la' |
    sib'2 sib'8 la' |
    sib'4 fa' sol' |
    fa' fa' fa' |
    fa'2 sib4 |
    fa'4 mib' re' |
    do' sib la |
    sol2 sol'8 sol' |
    la'2 la'8 la' |
    sib'4 sib do' |
    re' re'8[ do'] re'4 |
    sol2 sol'4 |
    sol' la' mi' |
    fa'4 mi' fa' |
    sol'2 mi'8 mi' |
    do'2 fa'8 mi' |
    fa'4 fa' fa |
    do' do' do' |
    do'2 fa'4 |
    re'2\trill fa'4 |
    sol'4 sol' sol' |
    sol' sol' sol' |
    fa'2 sib4 |
    fa' mib' re' |
    do' sib la |
    sol2 sol'8 fa' |
    fa'2 sib'8 la' |
    sib'4 fa' sol' |
    fa' fa' fa' |
    fa'2 r4 |
    R2.*6 |
    r4 r sol'4 |
    sol' fa' mi' |
    fa' mi' fa' |
    sol'2 mi'8 mi' |
    do'2 fa'8 mi' |
    fa'4 fa' fa |
    do' do' do' |
    do'2 fa'4 |
    sol'2 do'4 |
    sol' sol' fa' |
    mib' re' do' |
    si2 si8 si |
    do'2 do'8 do' |
    sib4 sib do' |
    lab4 lab8[ sol] lab4 |
    sol2\trill sol'8 sol' |
    sol'2 sol'8 fa' |
    mib'4 mib' mib' |
    mib' re'4.\trill re'8 |
    mib'2 r4 |
    R2.*6 |
    r4 r fa' |
    fa' fa' fa' |
    sol' sol' re' |
    mib'2 r4 |
    R2.*3 |
    r4 r fa' |
    fa' fa' sol' |
    mib' do' fa' |
    fa'2 la'8 la' |
    sib'2 sib'8 la' |
    sib'4 fa' sol' |
    fa' fa'4. fa'8 |
    fa'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 |
    re' sib sib |
    la\trill la sib |
    la2\trill fa'8 fa' |
    fa'2 fa'8 mib' |
    re'4 re' re' |
    do'4.\trill re'8 mib'4 |
    re'2\trill r4 |
    R2.*6 |
    r4 r si |
    do' do' sol |
    la sib do' |
    do'2 do'8 do' |
    do'2 do'8 sib |
    la4 la re' |
    sol4. la8 sib4 |
    la2\trill r4 |
    r r re' |
    re'4 re' re' |
    sib sib sib |
    sib2 r4 |
    R2.*2 |
    r4 r re'8 do' |
    sib2 fa'8 mib' |
    re'4 re' re' |
    do'4. re'8 mib'4 |
    re'2\trill r4 |
    R2.*6 |
    r4 r si |
    do' do' sol |
    la sib do' |
    do'2 do'8 do' |
    do'2 do'8 sib |
    la4 la re' |
    sol4.\trill la8 sib4 |
    la2\trill la4 |
    do'2 r4 |
    R2.*6 |
    r4 r si8 si |
    do'2 do'8 si |
    do'4 do' lab |
    sol sol sol |
    sol2 r4 |
    R2.*6 |
    r4 r la |
    sib re' re' |
    mib' mib' fa' |
    sib2 r4 |
    R2.*3 |
    r4 r re' |
    re' sib sib |
    la\trill la sib |
    la2\trill fa'8 fa' |
    fa'2 fa'8 mib' |
    re'4 re' re' |
    do'4.\trill re'8 mib'4 |
    re'2.\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" sib4 |
    re re mib |
    do\trill do sib, |
    fa2 fa8 fa |
    sib2 sib8 do' |
    re'4 re mib |
    fa fa8([ mib]) fa4 |
    sib,2 r4 |
    R2.*6 |
    r4 r sol |
    do' do' sib |
    la sol fa |
    mi2\trill mi8 mi |
    fa2 fa8 sol |
    la4 la, sib, |
    do do8[ sib,] do4 |
    fa,2 r4 |
    r r re |
    sol4 sol sol |
    mib mib mib |
    sib2 r4 |
    R2.*2 |
    r4 r sol8 la |
    sib2 sib8 do' |
    re'4 re mib |
    fa fa8[ mib] fa4 |
    sib,2 r4 |
    R2.*6 |
    r4 r sol |
    do' do' sib |
    la sol fa |
    mi2\trill mi8 mi |
    fa2 fa8 sol |
    la4 la, sib, |
    do do8[ sib,] do4 |
    fa,2 fa4 |
    do2 r4 |
    R2.*6 |
    r4 r sol8 sol |
    do'2 do'8 re' |
    mib'4 mib fa |
    sol sol8[ fa] sol4 |
    do2 r4 |
    R2.*6 |
    r4 r fa |
    sib sib lab |
    sol sol fa |
    mib2 r4 |
    R2.*3 |
    r4 r sib |
    re re mib |
    do\trill do sib, |
    fa2 fa8 fa |
    sib2 sib8 do' |
    re'4 re mib |
    fa fa8[ mib] fa4 |
    sib,2. |
  }
>>