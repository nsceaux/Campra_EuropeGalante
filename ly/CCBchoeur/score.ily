\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff <<
          <>^"Violons"
          \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff <<
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        <>^\markup\character Chœur
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*7\pageBreak
        s2.*8\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*9 s2 \bar "" \pageBreak
        s4 s2.*6 s2 \bar "" \pageBreak
        s4 s2.*6\pageBreak
        s2.*7\pageBreak
        s2.*8 s2 \bar "" \pageBreak
        s4 s2.*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
