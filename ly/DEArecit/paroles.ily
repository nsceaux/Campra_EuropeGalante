Quel ou -- tra -- ge ! mon cœur ne peut le soû -- te -- nir ;
El -- le me lais -- se, el -- le rit de ma pei -- ne,
Dieux ! quand l’Hy -- men est prest à nous u -- nir,
La per -- fide à ses nœuds op -- pose une au -- tre chaî -- ne.
Non, non, je ne puis luy par -- don -- ner,
je me livre aux trans -- ports de ma fu -- reur ex -- trê -- me,
je sui -- vray les con -- seils qu’el -- le vient me don -- ner,
Im -- mo -- lons mon ri -- val, son a -- mante & moy- mê -- me.
Ne vau -- droit- il pas mieux rompre un fa -- tal li -- en ?
Mais, le puis- je ? In -- sen -- sé, quel vain es -- poir me flat -- te ?
Sans l’ob -- jet de mes feux, je n’es -- pe -- re plus rien ;
C’est sa seu -- le ri -- gueur qu’il faut que je com -- bat -- te :
Al -- lons tom -- ber en -- core aux ge -- noux de l’in -- grat -- te,
pour at -- ten -- drir son cœur, ou pour per -- cer le mien.
