\clef "basse" re1\douxSug~ |
re1 |
re4 mi^\fortSug fa2 |
sib,1\douxSug~ |
sib,2 do4 mi,8^\fortSug fa, |
do,2. do'4\douxSug |
la2. |
sib4. sol8 do'4 mi8 fa |
sib,2. la,8 sib, |
do2 re4 la,8^\fortSug sib, |
do2 fa, |
fa2\douxSug re4 |
sol4 mi2 |
fa sol8 sol, |
do2. |
re |
mi2 dod4 |
re2 mi4 |
fa re mi8^\fortSug mi, |
la,2~ la,4 sol, |
fad,1^\douxSug |
sol,2 do,4 |
re, r re r8 re |
mib4 r do |
re r re,2 |
sol,1 |
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Source : \raise#0.5 \score {
    { \tinyQuote \clef "bass" do2 dod }
    \layout { \quoteLayout }
  }
}
mi2 %{do2%} dod |
re dod^\fortSug |
<< re1 { s2. s4^\douxSug } >> |
dod4 do si, sib, |
la,2 sib,^\fortSug |
<< la,1 { s2 s\douxSug } >> |
fad,1 |
sol, |
sol,4 r fa,8 mi,16 re, la,4 |
re,1\fermata |
