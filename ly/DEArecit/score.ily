\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s1*2 s2. s1\break \grace s16 s1*3 s2.\pageBreak
        s2.*4\break s2.*3 s1*2\pageBreak
        \grace s16 s2. s1 s2. s1\break s1*4\pageBreak
        \grace s8 s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
