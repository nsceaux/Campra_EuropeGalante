\clef "quinte" fa1\douxSug |
la2\fortSug sol\douxSug |
fa4 do'\fortSug do'2 |
sib2.\douxSug fa4 |
fa4 sol sol4. do8\fortSug |
do2 do'4.\douxSug do'8 |
do'2 do'4 |
re' sib do'4. do'8 |
sib4 fa fa4. fa8 |
do2 fa |
do4.\fortSug do8 do4 r |
do'\douxSug la2 |
sol2 sol4 |
fa4 la8 re' sol8. sol16 |
sol2 do'4 |
do' la re'8 si |
si2 mi4 |
la2 mi4 |
la2 mi8.\fortSug mi16 |
mi2 r |
r4 la\douxSug la4. la8 |
re4 sol la |
la r fa r8 fa |
mib4 r sol |
re4 r re4. re8 |
re2 sol~ |
sol4 mi2 la4 |
la2 mi4\fortSug la |
la2 r4 la\douxSug |
mi4 la re' re |
la2 re\fortSug |
la2. la4\douxSug |
la2. la4 |
sol2. sol4 |
la4 r la8 mi'16 la la8. la16 |
la1\fermata |
