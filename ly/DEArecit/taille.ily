\clef "taille" la1\douxSug |
re'2.\fortSug re'4\douxSug |
re'4 sol\fortSug la4. la8\douxSug |
re'2 fa'4. fa'8 |
fa'4 re' mi' do'\fortSug |
do'2 mi'4.\douxSug mi'8 |
fa'2 fa'4 |
fa' re' do'4. fa'8 |
re'4. re'8 re'4 do'8 sib |
sib4. do'8 la4.\trill fa8\fortSug |
do'4. do'8 do'4 r |
fa'2\douxSug fa'4 |
re'4 do'2 |
do'4 re'4. re'8 |
mi'2. |
re'2 si4 |
si2 la4 |
la2 mi'4 |
do' fa' mi'8.\fortSug mi'16 |
dod'2 r |
r4 re'\douxSug re'4. re'8 |
re'4 sib la |
la r sib r8 sib |
sib4 r sol8 la |
la4 r la4. la8 |
sol2. sib4 |
la2. la4 |
la2 la4.\fortSug la8 |
la2 r4 re'\douxSug |
mi'4 do' re'2 |
mi'4 la\fortSug fa4. sol8 |
mi2 dod'4.\douxSug dod'8 |
re'2. re'4 |
re'4 mi'2 mi'4 |
mi' r re'8 dod'16 re' mi'8 sol' |
fad'1\fermata |
