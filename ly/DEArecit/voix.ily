\clef "vhaute-contre" <>^\markup\character Octavio
r4 r8 re'16 re' la8 la r4 |
r8 fa' \appoggiatura mi'16 re'8. la16 sib4 sib8 sib16 la |
la4\trill r r fa'8 fa'16 fa' |
re'8\trill re' r4 r re'8 fa' |
sib4 sib8 la sol\trill sol r4 |
r sol'4 r do'8 do' |
fa' fa' do' do' re' mib' |
re'4\trill re'8 sol' mi'8.\trill re'16( do'8) sib16[ la] |
\appoggiatura la16 sib4. sib8 sib sib do' re' |
la4( sol)\trill fa r |
r2 r4 <>^\markup\italic Vîte do'8 r |
fa' r r la16 la re' re' re' re' |
si4\trill r8 sol'16 sol' mi'8 mi'16 mi' |
la'4 fa'8 fa'16 fa' si8.\trill do'16 |
do'8 do' r mi'16 mi' mi'8 mi'16 mi' |
la4. la16 la si8 si16 si |
sold4 r8 mi'16 mi' la'8 la'16 mi' |
fa'4 r8 re'16 fa' si8\trill si16 do' |
la8 la r4 r |
R1 |
r4 r8 <>^\markup\italic Tendrement do' do' do' re' la |
\appoggiatura la16 sib4 sib8 sib16 sib do'8. re'16 |
fad4 r sib r8 sib |
sol\trill sol r4 mib'8 do' |
la4\trill r la8 la16 la re'8 la |
sib8. la16( sol8) r sol' sol' mi'8\trill mi'16 mi' |
dod'4\trill la8 sol sol4 sol8 fad? |
fad4\trill r r2 |
r4 la8 la re'4 re'8 la |
\appoggiatura la8 sib4 r8 la la la la sol |
\appoggiatura sol la la r4 r2 |
r4 r8 mi' mi' mi' dod' mi' |
la4 la8 la la4 si8 do' |
si\trill si r mi' mi' re' re' dod'16[ si] |
\appoggiatura si8 dod'4 r fa'8 sol'16 la' dod'8.\trill re'16 |
re'1\fermata |
