\clef "haute-contre" re'2\douxSug~ re'4. fa'8\fortSug |
fa'2 sol'\douxSug |
la'4 sol'\fortSug fa'4. fa'8\douxSug |
fa'2 sib'~ |
sib'4 re'' do'' sol'8\fortSug fa' |
mi'2\trill do''4.\douxSug sol'8 |
la'4 do''2 |
sib'4 sol'2 sol'8 fa' |
fa'4. fa'8 fa' sol' la' fa' |
do''4. do''8 do'' fa'[\fortSug fa' fa'] |
fa'4 mi'8.\trill fa'16 fa'4 r |
la'2\douxSug la'4 |
si'4 do''4. do''8 |
do''4 la' sol' |
sol'2 sol'4 |
fa'2 fa'4 |
mi'2 mi'4 |
re' fa' mi' |
do' la' sold'8.\fortSug\trill la'16 |
la'2 r |
r4 la'\douxSug la'4. la'8 |
sol'2 mib'4 |
re' r fa' r8 fa' |
sol'4 r mib' |
re' r re'4. re'8 |
re'2. mi'4 |
mi'2 mi'4. mi'8 |
re'2 mi'4.\fortSug mi'8 |
re'2 r4 fa'\douxSug |
sol'4 la' re'4. sol'8 |
mi'4.\trill fa'8\fortSug re'4. mi'8 |
dod'2 mi'4.\douxSug mi'8 |
re'2 la'4. la'8 |
si'2 si'4. si'8 |
la'4 r la'8 sol'16 fa' la'8. la'16 |
la'1\fermata |
