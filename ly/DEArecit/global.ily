\key la \minor
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 3/4 s2.*8
\time 4/4 s1*2
\digitTime\time 3/4 \grace s16 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*12 \bar "|."
