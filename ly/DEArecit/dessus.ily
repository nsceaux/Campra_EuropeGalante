\clef "dessus" fa'4.\doux fa'8 fa'4 re''8\fort re'' |
la'2 re''4.\doux re''8 |
re''4 sib'8\fort sib'16 la' la'4.\trill la'8\doux |
sib'4 fa''8\fort fa''16 fa'' re''4.\trill re''8\doux |
re''4 sol''8 fa'' mi''4\trill sib'8\fort la' |
sol'2\trill sol''4.\doux sol''8 |
do''4 fa''4. fa''8 |
fa''4 sib' sol'\trill do''8. do''16 |
re''4. re''8 re'' mi'' fa'' fa'' |
fa''4( mi''8.)\trill fa''16 fa''8 sib'[\fort do'' re''] |
la'4( sol'8.)\trill fa'16 fa'4 r |
do''4\doux fa''4. fa''8 |
re''4\trill mi''4. mi''8 |
do''4. fa''8 re''8.\trill sol''16 |
mi''2~\trill mi''8. mi''16 |
fa''4. fa''8 re''8. re''16 |
si'4\trill mi''4. mi''8 |
la'2 sold'8.\trill la'16 |
la'4 r8 re''16\fort fa'' si'8\trill si'16 do'' |
la'2 r |
r4 la'\doux re''4. re''8 |
re''2 do''8. sib'16 |
la'4\trill r re'' r8 re'' |
sib'4 r la'8 la' |
fad'4 r fad'4. fad'8 |
sol'2 sib'4. sol'8 |
mi'2\trill mi''4. mi''8 |
la'4 la'8\fort sol' sol'4 sol'8. fad'16 |
fad'2\trill r4 la'8\doux la' |
mi''4. fa''8 re''4. mi''8 |
dod''4.\trill la'8\fort la' la' la' sol' |
\appoggiatura sol'16 la'2 la'4.\doux la'8 |
re''2 re''4. re''8 |
re''4 sol'' mi''4.\trill mi''8 |
mi''4 r re''8 mi''16 fa'' mi''8.\trill re''16 |
re''1\fermata |
