\clef "vbas-dessus" <>^\markup\character Doris
R1 |
la'8. la'16 la'8 re'' \appoggiatura sol'16 fad'4 fad'8 sol' |
la'4 la' mi'' r8 la' |
fad'\trill fad' si' si'16 si' mi'4 r8 mi'' |
dod''4\trill r8 la'16 la' re''8 re''16 la' |
si'8 si' r re''16 dod'' si'8 la'16 sol' |
fad'4\trill fad'8 si' mi'4\trill mi'8. fad'16 |
re'4 r r |
R1 |
r4 r8 la' la' la' |
re'' re'' mi'' mi'' fad'' sol'' |
fad''4\trill fad'' re''8 si' |
\appoggiatura la'8 sold'8. la'16 la'4 la'8 sold' |
la'4. la'16 la' re''8 re''16 fad' |
sol'8 sol'16 sol' sol'8 la'16 si' mi'8\trill mi' r mi' |
mi' fad' sol' sol'16 la' si'8 si'16 dod'' |
re''8 re'' r4 sol'4.( fad'8) |
fad'2\trill r8 la' la' la' |
mi''4. fad''16 sol'' dod''4\trill dod''8. re''16 |
re''2 r |
R1*2 |
r4 r8 la' la' la' fad'\trill fad' |
si'4 sol'8 sol'16 sol' sol'8 fad' |
fad'\trill fad' la'4 r8 re'' si'\trill si'16 mi'' |
dod''4\trill dod''8 dod''16 dod'' dod''8 mi'' |
la'8 r16 mi' mi' fad' sol' la' si'8 si'16 re'' sol'8 sol'16 si' |
mi'4 mi'8 r do'' do'' |
\appoggiatura la'8 sold'4 sold'8 la' la' sold' |
la'4 r8 mi''16 mi'' dod''8 dod''16 la' |
re''4 re''8 re''16 re'' re''8 fad'' |
si'\trill si' mi'' mi''16 mi'' dod''8 mi'' |
lad'\trill fad' si' si' si' lad'! |
si'4 r re''8 r si' si' |
sold'4 mi'8 mi'16 mi' fad'8\trill sold' |
\appoggiatura sold'?16 la'4 la'8 r mi'' r dod''\trill r |
r8 la' re'' fad'' si' si'16 re'' sol'8 sol'16 fad' |
fad'4\trill r8 re''16 re'' la'8 la'16 re'' |
si'8\trill si' si' la' sol' fad' |
mi'\trill mi' r mi' mi' fad' sol' la' |
si' si' dod'' re'' dod''8.\trill re''16 |
mi''4 r8 la' la' la' |
la'4. la'8 si' fad' |
\appoggiatura fad'16 sol'4. si'8 si' mi'' |
dod''\trill re'' mi'' fad'' fad''4( mi'')\trill |
re''1\fermata |
