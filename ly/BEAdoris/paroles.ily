Quel fu -- nes -- te coup pour mon a -- me !
Quoy ? Sil -- van -- dre, tu me tra -- his ?
In -- grat, qu’as- tu fait de ta flâ -- me ?
C’est Do -- ris qui te cherche, & c’est toy qui la fuis ?
Tu me ju -- rois que l’As -- tre qui m’é -- clai -- re,
s’é -- tein -- droit a -- vant ton a -- mour.
Au de -- là du tom -- beau je de -- vois t’ê -- tre che -- re,
ja -- mais ar -- deur ne pa -- rût plus sin -- ce -- re,
He -- las ! que de ser -- ments tu tra -- his en un jour !
Tu crois trou -- ver ail -- leurs u -- ne plus dou -- ce chaî -- ne ;
Mais, per -- fi -- de, croi- tu que je t’y laisse en paix ?
J’i -- ray trou -- bler sans cesse en ri -- vale in -- hu -- mai -- ne,
Les dou -- ceurs que tu te pro -- mets :
Mon a -- mour ou -- tra -- gé me tien -- dra lieu de hai -- ne,
et je te ren -- dray bien les maux que tu me fais :
Mais ses tour -- ments cal -- me -- ront- ils ma pei -- ne ?
Non, non, il faut plû -- tôt luy ca -- cher mon cour -- roux ;
Que dans d’au -- tres li -- ens un nou -- veau feu l’en -- traî -- ne :
Il ne joü -- i -- ra point de mon dé -- pit ja -- loux ;
Et j’at -- ten -- dray qu’à mes ge -- noux,
son in -- cons -- tan -- ce le ra -- mei -- ne.
