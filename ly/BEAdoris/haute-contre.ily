\clef "haute-contre" r2 fad'4. fad'8 |
re'2. re'4 |
la2. la4 |
la si la2 |
mi'4 fad'4. fad'8 |
re'2 re'8 dod' |
re'4 si la4. la8 |
la4 r8 fad' sol' fad'16 mi' |
re'4 re'2 dod'8.\trill re'16 |
re'2 mi'4 |
re' la'4. la'8 |
la'4 re'2 |
si4 dod' re'8 si |
dod'2 re'4 |
re' si la2~ |
la4 si mi' |
re'2 mi' |
la re'4 fad' |
sol'2 mi'4.\trill re'8 |
re'2 mi' |
re' fad'4. fad'8 |
si'2 la'8. sol'16 sol'8. la'16 |
fad'2\trill re'4. re'8 |
mi'4 la4. la8 |
la2 re'4. sol'8 |
mi'2. |
dod'4 si~ si2 |
do'2 do'4 |
si4. do'8 si8.\trill la16 |
la2 dod'4\trill |
re'2 fad'4 |
re' mi'4. mi'8 |
dod'4\trill re'8 re' mi' dod' |
re'2 r4 re' |
mi'2.~ |
mi'2. mi'4 |
mi' re'2 mi'4 |
fad' re'4. re'8 |
re'4 si2 |
la dod'4 si8 la |
re'4 mi'8 fad' mi'8.\trill re'16 |
dod'2\trill mi'4 |
si2. |
si4 mi'4. mi'8 |
mi'8 la' sol' la' la'4. la'8 |
la'1\fermata |
