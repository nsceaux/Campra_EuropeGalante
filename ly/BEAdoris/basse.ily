\clef "basse" re1~ |
re |
dod |
re4 sol, la,2 |
la4 fad2 |
sol sol,8 la, |
si,4. sol,8 la,2 |
re, sol,8 la, |
si,4. sol,8 la,2 |
fad,4 re dod |
si, la,2 |
re2.~ |
re4 dod si, |
la,2 fad,4 |
si, sol, la,2~ |
la,4 mi, mi |
si,2 dod |
re1 |
sol,2 la, |
re,4 re dod2\trill |
re1 sol,2 la, |
re, re |
re4 dod2 |
re2. sol8 mi |
la2. |
la,4 mi,8. fad,16 sol,2 |
do la,4 |
mi4. la,8 mi,4 |
la,2 la4 |
fad2. |
sol4 mi2 |
fad4 re8 si, fad,4 |
si,1 |
mi2~ mi8 re |
dod2 la, |
la4 fad sol8. fad16 mi4 |
re2. |
sol, |
la, mi8 fad |
sol fad mi re la8. re16 |
la,2 la4 |
red2. |
mi |
la8 fad mi re la,2 |
re,1\fermata |
