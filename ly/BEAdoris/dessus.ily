\clef "dessus2" r2 la'8.\doux la'16 la'8 re'' |
fad'2 re'4. re'8 |
mi'2. mi'4 |
re'4. re'8 dod'2\trill |
la'4 la'4. la'8 |
sol'4. si'16 la' sol'8 fad'16 mi' |
re'4. re'8 re'4 dod'8.\trill re'16 |
re'4 r8 re''16\fort dod'' si'8 la'16 sol' |
fad'4\trill fad'8 si' mi'4\trill mi'8 fad' |
re'2 la'4\doux |
si'8 re'' re''4 dod''\trill |
re''2 fad'4 |
mi'2 re'8. mi'16 |
dod'2\trill fad'4 |
re'2 dod'4.\trill dod'8 |
dod'8 re' mi'8. fad'16 sol'4 |
fad'2 mi'\trill |
re'4. re'8 fad'2 |
si' la'8. sol'16 sol'8. la'16 |
fad'2\trill sol'4.(\fort fad'8) |
fad'2\trill r8 la' la' la' |
mi''4. fad''16 sol'' dod''4\trill dod''8. re''16 |
re''2 fad'4.\doux fad'8 |
mi'2 mi'4 |
re'2 fad'4 sol' |
mi'2.~ |
mi'2 re' |
do'4 mi'4. mi'8 |
mi'4. mi'8 re'8. mi'16 |
dod'2\trill la'4 |
la' la'4. la'8 |
sol'4 sol'4. sol'8 |
fad'4. fad'8 fad' fad' |
fad'2 r4 fad' |
mi'2~ mi'8 mi' |
mi'2. la'4 |
la'4. la'8 re'4 re'8 dod' |
re'4 fad'4. re'8 |
sol'4 sol'8 fad' mi' re' |
dod'2\trill mi'4. re'8 |
re'4 sol'8 la' la'4 |
la'2 la'4 |
fad'2. |
mi'2 si'4 |
la'8 re'' dod'' re'' re''4( dod''8.)\trill re''16 |
re''1\fermata |
