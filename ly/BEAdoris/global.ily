\key re \major
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*7
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*6
\time 4/4 s2 \tempo "Lentement" s2
\digitTime\time 3/4 s2.
\time 4/4 \grace s16 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*4
\time 4/4 s1*2 \bar "|."
