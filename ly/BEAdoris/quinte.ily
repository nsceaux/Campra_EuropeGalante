\clef "quinte" r2 la4. la8 |
la2. fad4 |
mi2. mi4 |
fad si, dod2 |
dod'4 re'4. re'8 |
re'2 re8 la |
re4 sol2 mi4 |
fad4. la8 si dod' |
re'4. re8 la4 sol8. la16 |
fad2\trill mi4 |
si8 sol sol4 mi |
fad2 re4 |
mi2 fad8 re |
la4 mi fad~ |
fad mi mi2 |
mi mi4 |
fad4. re8 la2 |
la1 |
sol4 mi mi4. mi8 |
fad2 mi4. mi8 |
fad2. la4 |
sol re la4. la8 |
la2. la4 |
sol mi2 |
fad2. si4 |
la2. |
la4 sol~ sol2~ |
sol4 mi2 |
mi4. mi8 mi8. mi16 |
mi2 mi'4 |
re'4 re'4. re'8 |
re'4 dod'2 |
dod'4 fad8. re'16 dod'8.\trill si16 |
si2 r4 fad |
sold2 mi4 |
mi2 la |
la sol8 si mi'4 |
la2 re4 |
re2 mi4 |
mi2 mi8 la sol fad |
si la sol fad la8. la16 |
la2 la4 |
si2 si4 |
sol2 sol4 |
la mi'8 la la4. la8 |
la1\fermata |
