\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >> \keepWithTag #'vdessus \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s2.*2 s1\break s2. s1 s2.*3\pageBreak
        \grace s8 s2.*2 s1\break s2. s1*3\pageBreak
        s1*4 s2.\break s1 s2. s2 \bar "" \pageBreak
        s2 s2.*3\break s2.*3 s1\pageBreak
        s2. s1*2\break s2.*2 s1 s2.\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
