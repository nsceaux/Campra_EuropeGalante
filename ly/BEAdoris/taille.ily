\clef "taille" r2 re'4. re'8 |
re'2. la4 |
la2. la4 |
la sol mi2 |
la4 re'4. re'8 |
re'2 si8 dod' |
si4 re' la4. la8 |
la4. re'8 re' dod' |
si2 la4. la8 |
la2 la4 fad8 si mi4 la |
la2 si4 |
si4 la si |
mi la2 |
si2 dod'4. dod'8 |
la4 sol4. si8 |
si2 mi |
fad2. re'4~ |
re' re la4. la8 |
la2. la4 |
la2 re'4 fad' |
sol'2 mi'4.\trill re'8 |
re'2. re'4 |
si dod'4. dod'8 |
la2. si4 |
dod'2. |
la4 re'~ re'2 |
sol la4 |
si4. la8 si mi |
mi2 la4 |
la2 la4 |
si dod'2 |
dod'4 si dod'8 fad |
fad2 r4 si |
si2~ si8 si |
dod'2. dod'4 |
dod' fad' re'8 sol sol4 |
la2. |
si2 si8 si |
dod'2 la4 si8 re' |
si4 mi'8 la la8. la16 |
la2 dod'4 |
si2 si4 |
si2. |
dod'8 fad' sol' fad' la'4 sol'8. la'16 |
fad'1\fermata |
