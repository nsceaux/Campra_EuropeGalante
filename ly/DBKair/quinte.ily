\clef "quinte" re'4 |
re' r re' |
re' r mi' |
fad' r si |
mi' r la |
re' r re' |
sol r sol |
la re' la |
re' mi' re' |
mi'2 dod'4 |
si r si |
la r dod' |
si r si |
la r mi' |
re' r fad' |
mi' mi' fad' |
mi' mi'4. mi'8 |
mi'2 la4 |
sol r si |
la r la |
re' r si |
mi' r mi' |
re'2 re'4 |
re' la4. la8 |
la2 re'4 |
sol' r sol' |
fad' re' re' |
si la4. la8 |
la2
