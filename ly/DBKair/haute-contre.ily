\clef "haute-contre" re''4 |
re'' r re'' |
re'' r la' |
la' r sold' |
la' r la' |
la' r re'' |
dod''8 si' dod'' re'' dod''4 |
re'' la' la' |
la' la' re'' |
dod''2\trill la'4 |
sold'8 fad' sold' la' sold'4 |
la' la' la' |
sold'8 fad' sold' la' sold'4 |
la' la' dod'' |
re'' r re'' |
re'' dod'' la' |
mi'' mi''4. mi''8 |
dod''2\trill re'4 |
re' r sol' |
mi' r la' |
fad' r si' |
la' r la' |
la'2 si'4 |
si' la'4. la'8 |
la'2 la'4 |
la' r la' |
la'2 re''4 |
re'' dod''4.\trill re''8 |
re''2
