\clef "taille" la'4 |
la' fad'8 sol' la' si' |
la'4 fad' la' |
re'8 dod' re' mi' re'4 |
mi'8 re' mi' fad' mi'4 |
fad' re' re' |
sol' r sol' |
fad' fad' sol' |
fad' la' la' |
la'2 mi'4 |
mi' r mi' |
mi' r mi' |
mi' r mi' |
mi' r la' |
la' r si' |
si' la'4. si'8 |
dod''4 si'4.\trill la'8 |
la'2 do'4 |
si r mi' |
mi' r mi' |
re' r re' |
sol' r sol' |
fad' re' re' |
mi' mi'4.\trill mi'8 |
fad' mi' fad' sol' fad'4 |
mi' r mi' |
re'2 re'4 |
mi' mi'4.\trill fad'8 |
fad'2
