\clef "basse" re'4 |
re' r re' |
re' r dod' |
si r si |
la r la |
fad r fad |
mi r mi |
re re' dod' |
re' dod' re' |
la2 la4 |
mi r mi |
la r la |
mi r mi |
la r la |
re' r re' |
mi' dod' re' |
mi' mi2 |
la fad4 |
sol8 fad sol la sol4 |
la r la |
si8 la si dod' si4 |
dod' r dod' |
re'4. dod'8 si la |
sol4 la la, |
re2 re'4 |
dod'8 si dod' re' dod'4 |
re' re fad |
sol la la, |
re2
