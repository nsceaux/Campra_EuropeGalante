\clef "dessus" re''4 |
fad'' re''8 mi'' fad'' sol'' |
fad''4 re'' la' |
si'8 la' si' dod'' si'4 |
dod''8 si' dod'' re'' dod''4 |
re'' re'' re'' |
mi''8 re'' mi'' fad'' mi''4 |
fad'' re'' mi'' |
fad'' sol'' fad'' |
mi''2\trill la'4 |
si'8 la' si' dod'' si'4 |
dod'' la' la' |
si'8 la' si' dod'' si'4 |
dod'' la' mi'' |
fad''8 mi'' fad'' sol'' fad''4 |
sold'' mi'' la'' |
la'' sold''4.\trill la''8 |
la''2 la'4 |
si' r si' |
dod''8 si' dod'' re'' dod''4 |
re''4 r re'' |
mi''8 re'' mi'' fad'' mi''4 |
fad''4. mi''8 re'' dod'' |
si'4 dod''4.\trill re''8 |
re''4. mi''8 re''4 |
mi''4. fad''8 mi''4 |
fad''8 mi'' fad'' sol'' fad''4 |
mi''4 mi''4.\trill re''8 |
re''2
