\key re \major \midiTempo#160 \tempo "Gay" \beginMark "Ritournelle"
\digitTime\time 3/4 \partial 4 s4 s2.*8 s2 \bar ":|."
s4 s2.*7 s2 \bar ".!:" s4 s2.*10 s2 \bar ":!."
