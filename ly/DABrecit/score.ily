\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3\break s1*4\break s1*4\break
        s1*3\break s2. s1 s2. s1\break s1 s2.*4\pageBreak
        \grace s8 s2.*8\break \grace s8 s2.*7\break
        \grace s8 s2.*7\break s2. s1*4\break
        s1*3\break s1*3 s2 \bar "" \pageBreak
        s2 s2. s1\break s1*2 s2.*3\break s2.*5\break
        s2.*5\break s2. s1*4\break s1*6\pageBreak
        s1*5 s2 \bar "" \break s2 s1*5 s2 \bar "" \break
        s2 s1*2 s2.\break s1*3 s2 \bar "" \break s2 s1*3\break
        s1 s2. s1*2 s2 \bar "" \pageBreak
        s2 s1*4\break s1*4\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
