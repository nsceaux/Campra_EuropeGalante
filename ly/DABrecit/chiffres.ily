<_+>2 <"">8.\figExtOn <"">16\figExtOff <6+>4 <_+>4.\figExtOn <_+>8\figExtOff <6>4 <_->8 <6> <6+>2 <7 _+>4. <6 _->8 <4>4 \new FiguredBass <3+>
<"">4..\figExtOn <"">16\figExtOff s2 <6> <7>4 <6> <"">4.\figExtOn <"">8\figExtOff <6>2 <6>4 <5> <4> <3>
<"">4..\figExtOn <"">16\figExtOff <_->4 <_+> <"">8.\figExtOn <"">16\figExtOff <6+>4 <_+>4.\figExtOn <_+>8\figExtOff <6>4 <_->8. <6>16 <6+>2 <7 _+>4. <6 5 _->8 <4>4 \new FiguredBass <3+>
s1 <6>2 <6>4 <7>8 <6> <_+>2\figExtOn <_+>\figExtOff
<6>4 <7> <6+> s1 s2 <6>4 <_+>2 <6>
<_+>4 <6+> <4> <3+> <_+>2\figExtOn <_+>4\figExtOff s2 s8 <5/> s4 <6>2 s4 <5/>2
s2 <6>4 <6> <6 5/>2 <7>4 <4> \new FiguredBass <3> s2. s4 <6 _+> <6+> <_+> <_+> <6> <_+>2. <7>4 <6>2
<_+>4 <4> <3+> s2 s8 <5/> s4 <6> <6> s2 <6>4 <6> <6>2 <"">2\figExtOn <"">8\figExtOff <6+> s2 <6 5/>4
s4. <6>8 <6>4 s4. <6>8 <6+>4 s2. <6>2\figExtOn <6>4 <"">8*5 <"">8\figExtOff <6>4 <7> <6> s2 <6>4
s4 <4> <3> <"">2..\figExtOn <"">8\figExtOff s1 s2 <6> s1
<6> <6>2 <_+> <6> <_+>4.\figExtOn <_+>8\figExtOff
<6+>4 <_+> <_+>2 s2 <7>4 <6> <_+>8\figExtOn <_+>\figExtOff <6> <6+> s4 <4>8 <3+> <_+>2
<_+>2 <6>2. <7>4 <6+> s2
<5/>1 <6>4 <7>8 <6+> s2 s2. <6>2\figExtOn <6>8\figExtOff <4> <6>4 <7 4> <3>
s2 s8 <6> <6+>4 <_+>2 s2. s2 s8 <6> s2 <6>4
s4. <6>8 <6>4 <6> <7> <6+> s2 s8 <6> s8 <6> <6>4 <5/> s4. <6 5/>
<6>4 <4> \new FiguredBass <3> <_->1 <6+>4 <6> <6>2 s2. <6>8 <6+> <_->2 <6->4 <6>
<7>2 <6> <_+>1 <_-> <_+> <"">4.\figExtOn <"">8 <5/>4 <5/> <"">4. <"">8\figExtOff <6>4 <6 _->
<5/>2 <6 5> s2. <_+>4 <_->4. <6>8 <6+>2 <6> <6>4 <6> <_+>2.\figExtOn <_+>4 <6>4. <6>8\figExtOff
<6>4 <7 5 _-> <4>2 \new FiguredBass <3+> <"">4.\figExtOn <"">8\figExtOff s4 <_+> <_->4. <6>8 <6+>2 <6>2 <6>4 <6> <_+>2.\figExtOn <_+>4\figExtOff <6>2
<6 4+>4 <7 5 _-> <4>2 \new FiguredBass <3+> s1 <6>2. s1
<6+>4.\figExtOn <6+>8\figExtOff <6>4. <6+>8 <_->1 <7 _+>2
s4 <6 5/> <6 5>1 s1 s2 <6>4 <5/>
<4> <3> <_->2 s2 <6->8 <6> <7>4 <6> <_+>2 s1 <6>2
<7>4 <6> s1 <6+>4 <6>8 <6 5> <4>4 <3+> <_+>4.\figExtOn <_+>8\figExtOff <6>2 s <_+>
<6>1 s4 <\markup\triangle-down 6 7 \center-column { \number 5 \figure-flat } >4 <4> \new FiguredBass <3+> s1 s2.
<6>4 s <6> <4> <3>
