\clef "vhaute-contre" <>^\markup\character Octavio
r4 re'8. re'16 |
\appoggiatura re'8 mi' mi'16 mi' fad'8 sol' fad'2\trill |
sol'8 sol'16 fa' mib'8 re' do'4\trill la8 re' |
fad4\trill sol8 la sib4( la)\trill |
sol4 r8 sol' re'\trill re' r re'16 mib' |
fa'4 do'8 do'16 re' \appoggiatura do'8 sib4( la8) sib |
la4\trill la r8 fa' fa' re' |
sol'4 mib'8 re' do'4\trill re'8 mib' |
re'2\trill r4 re'8 re' |
mi' mi'16 mi' fad'8 sol' fad'2\trill |
sol'8 sol'16 fa' mib'8 re' do'4\trill la8 re' |
fad4\trill sol8 la sib4( la)\trill |
sol4
\ffclef "vbas-dessus" <>^\markup\character Olimpia
r8 sib' sib' sib' do'' re'' |
la'4\trill la'8 la' sib'4 sib'8 do'' |
\appoggiatura do''8 re''4 r8 la'16 la' fad'4\trill fad'8 fad' |
sol' sol' la'\trill la' sib' do'' |
sib'8.\trill la'16( sol'4) r4 re''8 re''16 re'' |
sol''4. fa''8 mi'' re'' |
dod''2\trill la'8 la' si' do''? |
si'4 dod''8 re'' re''4( dod'') |
re''2. |
\ffclef "vhaute-contre" <>^\markup\character Octavio
r4 r sol8 la si do' si4( la8.)\trill[ sol16] |
fad4\trill r8 re' re' sol' |
\appoggiatura fad'8 mi'2 re'8 mi' |
do'4\trill \appoggiatura si8 la4 si |
si( la2)\trill |
sol si4 |
si dod' re' |
dod' red' mi' |
red'2 sol'4 |
\afterGrace sol'( fad'8) fad'4 fad'8 fad' |
\appoggiatura mi'8 red' mi' mi'4( red') |
mi'2 sol'8 re' |
mi'4 re' do' |
si\trill si do' |
re' do'4.\trill si8 |
la2\trill la8 si |
do'4 do' re' |
\appoggiatura re'8 mi'4. mi'8 fad'4 |
sol' si4.\trill dod'8 |
\appoggiatura dod'16 re'2 re'4 |
r la2 |
fad\trill r8 re' |
sol' fad'16[ mi'] mi'4( fad'8) sol' |
fad'8.\trill mi'16( re'4) sol8 la |
si do' si4( la)\trill |
sol2 r |
r4 si si8 si si si |
mi'4 mi'8 mi' fad'4\trill fad'8 fad' |
sol'8. fad'16( mi'4) r4 sol'8 fad' |
mi'4 re'8\trill do'16[ si] \appoggiatura si8 do'4 do'8 si |
la4\trill la8 sol fad4\trill r8 si16 si |
si4 dod'8 re' dod'4.\trill dod'8 |
red' mi' red'\trill mi' \appoggiatura mi'8 fad'4 si |
r sol'8 sol' sol'4 fad'8 fad' |
red'4\trill mi'8 fad' sol'4 fad'8 sol' |
mi'4
\ffclef "vbas-dessus" <>^\markup\character Olimpia
r8 mi''16 mi'' dod''4 dod''16 dod'' dod'' re'' |
re''8 re'' r la'16 la' re''8 re''16 fad' |
sol'8 sol'16 sol' sol'8\trill sol'16 fad' fad'4\trill r16 la' la' si' |
do''8 do'' do'' si' si'\trill si' r sol' |
re'' re''16 re'' mi''8 fad''16 sol'' fad''2\trill |
\ffclef "vhaute-contre" <>^\markup\character Octavio
r4 si si8 sol |
re'2 la8 si |
do'4 do'4.*5/6 si16[ la si] |
si4\trill sol8 si si si |
dod' re' dod'4. re'8 |
re'2. |
re'4 la8 si do' re' |
mi'4 si8 do' re'([ do'16 si]) |
la4\trill r8 re' do' si |
la si sol4\trill( fad8) sol |
fad4\trill la8 si do' re' |
mi'4 fad'8 sol' re'([ do'16 si]) |
si4\trill r8 re' do' si |
la si si4( la8.)\trill sol16 |
sol4
\ffclef "vbas-dessus" <>^\markup\character Olimpia
sol'4 sib'4. sib'8 |
do''4 re'' do''\trill la' |
sib' sol' sib' do''8 re'' |
mib''2 mib''4 re'' |
do''2\trill do''8[ sib'] do'' do''( |
re''1) |
r4 sol'4 sib'4. sib'8 |
re''2\repeatTie r4 la' |
sib' do''8 re'' sol'4 la'8 sib' |
la'2\trill sib'4 do''8 re'' |
mib''4 re'' mib'' do'' |
\appoggiatura do''16 re''8. do''16( sib'4) r re'' |
mib''4. re''8 do''4.\trill sib'8 |
la'4 sib' do''8[ sib'] la'[ sol'] |
fad'2\trill r4 la' |
re''2 la'4 la'8 sib' |
sib'2( la')\trill |
sol' r4 re'' |
mib''4. re''8 do''4.\trill sib'8 |
la'4 sib' do''8[ sib'] la'[ sol'] |
fad'2\trill r4 la' |
re''2 la'4 la'8 sib' |
sib'2( la')\trill |
sol'4
\ffclef "vhaute-contre" <>^\markup\character Octavio
sol'4 r re'16 re' re' mi' |
fa'4 sib8 sib16 sib sib8 re' |
sol4 r8 sol mib' mib' mib' mib' |
si4\trill si8 si do'4 do'8 re' |
\appoggiatura re'16 mib'4 mib'8 r sol sol sol la |
sib4 sib8 la la4\trill fa'8 fa'16 re' |
sol'4 mib'8 mib' la4\trill la8 sib |
sib4 sib r re'8 re' |
re'4 re'8 mib' do'4\trill do'8 do' |
do'4 sib8. la16 la4\trill r16 la la la |
re'8 la sib sib do' re' |
sol2 fad |
r re'4 re'8 sib |
fa'4 fa'8 fa' fa'4\trill fa'8 mi' |
fa'2 r4 fa'8 fa' |
dod'8\trill dod'16 dod' re'8 mi' fa'4( mi')\trill |
re'2 re'4 re'8 sol' |
mi'4\trill mi'8 la' fad'4\trill fad'8 la' |
re'2 r4 re'8 sib |
sol sol16 sol la8 sib sib4( la)\trill |
sol2 r4 r8 re' |
si4\trill r16 si si si mi'8 mi'16 sol' do'8 do'16 do' |
la4\trill re'8 re'16 mi' la8 la si do' |
si1\trill |
