\clef "basse" sol4. sol8 |
do'8. sib16 la8. sol16 re4 re8 do |
si,4 do8 sib,? la,4. sol,8 |
re4 mib8 do re4 re, |
sol,4 sol8. la16 sib2 |
la sol |
fa4. mib8 re2 |
mib fa4 fa, |
sib,8. do16 sib,8. la,16 sol,4 sol |
do'8. sib16 la8. sol16 re4 re8 do |
si,4 do8. sib,?16 la,4. sol,8 |
re4 mib8 do re4 re, |
sol,2 sol |
fad sol8 fa? mib4 |
re2 do |
sib,4 la,2 |
sol,1~ |
sol,2. | \allowPageTurn
la,4 la8 sol fad2 |
sol8 fa mi re la4 la, |
re4. do8 si, la, |
sol,2 sol8 fad |
sol do dod2 |
re4 si,2 |
do2 si,8 do |
la,4 fad, sol, |
do re re, |
sol,2. |
sol4 la si |
la si do' |
si2 mi4 |
la2 la4 |
si8 mi si,2 |
mi mi'8 si |
do'4 si la |
sol4. sol,8 la,4 |
si, la,4.\trill sol,8 |
re4. re8 do si, |
la,4 la si |
do'4. si8 la4 |
sol4. fad8 mi4 |
re2. |
dod4. si,8 la,4 |
re4. mi8 re do |
si, do la,4.\trill sol,8 |
re2 mi8 fad |
sol do re4 re, |
sol,2 sol8 la sol fad |
mi1~ |
mi2 red |
mi1 |
sold,2 la,~ |
la, si, |
sold, la,4 la8 sol |
fad mi si mi si,2 |
mi2 la |
si8 la sol fad mi la, si,4 |
mi2 la |
fad2. | \allowPageTurn
mi2 re |
fad, sol, |
fad,4 mi, re,2 |
sol,4 sol2 |
fad4 fa2 |
mi4 re re, |
sol,4. sol8 sol fad |
mi re la4 la, |
re re8 do si, la, |
re2 la,8 si, |
do re mi4 si,8 do |
re re' do' si la sol |
fad sol mi2 |
re la8 si |
do' si la sol fad re |
sol sol, la, si, do re |
mi do re4 re, |
sol,2 sol4. sol8 |
la4 sib fad2 |
sol sol8 fa mib re |
do2 sol4 fa |
mib1 |
re4. mib8 re do sib, la, |
sol,2 sol4. sol8 |
re4 re8 mi fad4 re |
sol4. fa8 mi4 do |
fa4 fa8 mib re4 do8 sib, |
la,4 sib, mib, fa, |
sib,2 sib4 sol |
do'4. sib8 la4. sol8 |
fad4 sol fa mib |
re4. mib8 re4 do |
sib, do8 re mib4 do |
re2 re, |
sol,4 sol8 la sib4 sol |
do'4. sib8 la4. sol8 |
fad4 sol fa? mib |
re4. mib8 re4 do |
sib, do8 re mib4 do |
re2 re, |
sol,1 | \allowPageTurn
re2. |
mib1 |
re4 sol8 fa mib4. re8 |
do1~ |
do2 fa4 re |
mib2 fa4 fa, |
sib,1 |
sib2 mi4 fad |
sol sol, re2~ |
re4 sol4. fa8 |
mib2 re~ |
re4. do8 sib,4 sib |
la2 sol |
fa2. fa4 |
mi4 fa8 sol la4 la, |
re4. do8 si,2 |
do4. la,8 re2 |
fad, sol, |
mib4 do re re, |
sol,1 |
sol2 do |
re4 si,8. do16 re4 re, |
sol,1 |
