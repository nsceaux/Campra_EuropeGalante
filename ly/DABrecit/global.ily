\set Score.currentBarNumber = 13
\key re \minor \beginMark "Air"
\time 2/2 \midiTempo#80 \partial 2 s2 s1*14
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 \midiTempo#120 s2. \bar "||"
\beginMark "Air" \key sol \major s2.*26
\time 4/4 \midiTempo#80 s1*11
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\beginMark "Air" \digitTime\time 3/4 \midiTempo#120
\bar ".!:" s2.*5 \alternatives s2. s2. s2.*7 \bar "||"
\time 2/2 \midiTempo#160 \key re \minor
s4 \beginMark "Air" s2. \bar ".!:" s1*4 \alternatives s1*2 s1
s1*15
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*8
\digitTime\time 3/4 s2.
\time 4/4 s1*9 \bar "||"
\key sol \major s1*4 \bar "|."
