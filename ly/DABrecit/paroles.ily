Ne ver -- ray- je ja -- mais le jour,
où je se -- ray con -- tent de l’ar -- deur de vôtre a -- me ?
In -- grat -- te, vous brû -- lez d’u -- ne trop foi -- ble flâ -- me,
vous of -- fen -- sez & l’A -- mant & l’A -- mour.
Ne ver -- ray- je ja -- mais le jour,
où je se -- ray con -- tent de l’ar -- deur de vôtre a -- me ?

De quel re -- proche en -- cor ve -- nez- vous m’al -- lar -- mer ?
Vos soup -- çons plus long- temps ne peu -- vent se con -- train -- dre :
Que sert, in -- grat, de vous ai -- mer ?
Vous ne ces -- sez point de vous plain -- dre.

Je ne me plain -- drois __ pas,
si vous m’ai -- miez, comme il faut que l’on ai -- me ;
A sui -- vre sans ces -- se vos pas,
je trouve u -- ne dou -- ceur ex -- trê -- me :
Tous les au -- tres plai -- sirs sont pour moy sans ap -- pas ;
Du bon -- heur de vous voir, je fais mon bien su -- prê -- me :
He -- las ! si vous m’ai -- miez de mes -- me,
je ne me plain -- drois pas.
Mais, que vous ê -- tes loin de l’ar -- deur qui m’en -- fla -- me ;
Mon bon -- heur ne fait pas le plus doux de vos soins ;
Et de tous les plai -- sirs que peut goû -- ter vôtre a -- me :
mon a -- mour est ce -- luy qui la tou -- che le moins.

Je con -- nois ce qui vous ir -- ri -- te,
vous souf -- frez à re -- gret que je vienne en ces lieux ;
Et le spec -- tacle ou l’on m’in -- vi -- te
of -- fen -- se peut- ê -- tre vos yeux ?

C’est le su -- jet de mes jus -- tes al -- lar -- mes,
vous re -- con -- nois -- sez mal ma foy ;
foy ;
Je re -- nonce à tout pour vos char -- mes,
et vous ne qui -- tez rien pour moy.
Je re -- nonce à tout pour vos char -- mes,
et vous ne qui -- tez rien pour moy.

Sor -- tez de l’a -- mou -- reux em -- pi -- re,
ou de -- ve -- nez plus tran -- quile en ai -- mant :
Sor -- tez de  - mant :
Un cœur qui s’al -- larme ai -- sé -- ment,
n’est point heu -- reux, quand il soû -- pi -- re.
Pour moy, l’a -- mour est un plai -- sir char -- mant ;
Pour vous, c’est un mar -- ti -- re.
Pour moy, l’a -- mour est un plai -- sir char -- mant ;
Pour vous, c’est un mar -- ti -- re.

Ah ! ne mur -- mu -- rez point de mes trans -- ports ja -- loux !
L’ex -- cés de mon a -- mour fait ce -- luy de mes crain -- tes ;
Tout ce qui s’ap -- pro -- che de vous
porte à mon cœur de sen -- si -- bles at -- tein -- tes.
Que ne som -- mes- nous seuls en des lieux re -- ti -- rez !
Je ces -- se -- rois peut- ê -- tre de me plain -- dre ;
Plus vos at -- traits y se -- roient i -- gno -- rez,
moins j’au -- rois de Ri -- vaux à crain -- dre.
Plus vos at -- traits y se -- roient i -- gno -- rez,
moins j’au -- rois de Ri -- vaux à crain -- dre.
On vient. Son -- gez du moins que je suis prés de vous,
et mé -- na -- gez un cœur ja -- loux.
