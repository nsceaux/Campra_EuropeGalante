\include "common.ily"
\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "L'Europe Galante" }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Notes
%\bookpart {
%  \paper { #(define page-breaking ly:minimal-breaking) }
%  \include "notes.ily"
%}
%% Livret
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}
%% Musique
\include "ly/entree1.ily"
\include "ly/entree2.ily"
\include "ly/entree3.ily"
\include "ly/entree4.ily"
\include "ly/entree5.ily"
