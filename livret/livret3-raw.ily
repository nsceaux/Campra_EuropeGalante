\livretAct L’ESPAGNE
\livretScene TROISIÈME ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente une Place publique, que l’on discerne
  à peine, parce que l’action se passe dans la nuit.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Dom Pedro, Cavalier Espagnol sous le bacon de sa Maîtresse.
}
\livretPers Dom Pedro
\livretRef#'CAApedro
%# Sommeil, qui chaque nuit joü=issez de ma belle,
%# Ne versez point encor vos pavots sur ses yeux,
%# Attendez pour regner sur elle
%# Qu'elle ait appris mes tendres feux.
%# Je vais parler, c'est assez me contraindre,
%# C'est trop cacher les maux qu'elle me fait souffrir;
%# Du moins il est temps de m'en plaindre
%# Lorsque je suis prest d'en mourir.
%# Ah! s'il plaisoit aux beaux yeux que j'adore,
%# De soulager mon amoureux tourment,
%# Le sort fatal que je déplore
%# Deviendroit un destin charmant.
%# Mais ma mort est toûjours certaine,
%# Quelque succés qu'Amour daigne me préparer,
%# Que Lucile soit inhumaine,
%# Ou sensible à l'ardeur que je viens declarer,
%# Il faudra toûjours expirer
%# De mon plaisir ou de ma peine.
%# Quelle troupe s'avance? & qui l'ameine ici?
%# Restons, j'en veux estre éclairci.

\livretScene SCENE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Dom Carlos ameine avec luy une troupe de Musiciens
  & de Danceurs. Le Théatre est éclairé.
}
\livretPers Dom Carlos
\livretRef#'CBAcarlos
%# La nuit rameine en vain le repos dans le monde,
%# Mon cœur est toûjours agité;
%# Mais mon trouble & mes soins font ma félicité,
%# J'aime mieux en joü=ir que d'une paix profonde:
%# La nuit rameine en vain le repos dans le monde,
%# Mon cœur est toûjours agité.
\livretDidasPPage à sa troupe.
%# C'est à vous de servir une ardeur si constante,
%# Soumettez à l'Amour la beauté qui m'enchante;
%# Par vos plus tendres chants tâchez de la charmer,
%# Rendez-luy le plaisir que je sens à l'aimer.
\livretRef#'CBBair
\livretDidasPage On commence la Serenade.
\livretPersDidas Une Musicienne chante ces paroles Espagnolles
\livretRef#'CBCespagnole
%#12 El esperar en amor es merecer.
%#12 El persistir es un esforçar el hádo,
%#12 En gozar suele mudarse el padecer.
%#12 Al fin es Amante quien está amado.
%#12 El esperar en amor es merecer.
\livretDidasP Sens de l’Espagnol
%# Un cœur dans l'Empire d'amour
%# Merite les biens qu'il espere;
%# Sa constance ameine le jour
%# Où l'objet qu'il trouvoit severe
%# S'attendrit & brûle à son tour:
%# Un cœur dans l'Empire d'amour
%# Merite les biens qu'il espere;
\livretPers Un Musicien
\livretRef#'CBEairChoeur
%# Nuit soy=ez fidelle,
%# L'Amour ne revele
%# Ses secret qu'à vous.
\livretPers Chœur
%# Nuit soy=ez fidelle,
%# L'Amour ne revele
%# Ses secret qu'à vous.
\livretPers Le Musicien
%# S'il veut à quelque cru=elle
%# Faire enfin sentir ses coups;
%# Nuit soy=ez fidelle,
%# L'Amour ne revele
%# Ses secret qu'à vous.
\livretDidasPPage\justify {
  Le chœur repete ces trois derniers Vers.
}
\livretPers Le Musicien
%# Si quelque amant prés de sa belle
%# Trompe les yeux des jaloux;
%# Nuit soy=ez fidelle,
%# Et cachez à tous
%# Des misteres si doux:
%# Nuit soy=ez fidelle,
%# L'Amour ne revele
%# Ses secret qu'à vous.
\livretDidasPPage\justify {
  Le chœur repete ces trois derniers Vers.
}
\livretPers Dom Carlos
\livretRef#'CBFcarlos
%# Vous ne paroissez point, ingrate Le=onore,
%# Méprisez-vous qui vous adore?
%# Se peut-il que mon tendre amour
%# Ne fléchisse jamais vostre ame?
%# Quoy? la nuit, si propice à l'amoureuse flame,
%# Ne me sert pas mieux que le jour?
%# N'est-il pas temps qu'un sort heureux réponde
%# Aux soins trop éprouvez de ma sincere ardeur?
%# Le plus fidelle amant du monde
%# N'a-t'il pas droit sur vostre cœur?

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center {
  Dom Pedro, Dom Carlos.
}
\livretPers Dom Pedro
\livretRef#'CCArecit
%# Moderez le transport que vous faites paroître;
%# Il faut s'expliquer autrement.
%# N'usurpez point le nom de plus fidelle amant,
%# C'est moy qui me pique de l'être.
\livretPers Dom Carlos
%# En vain l'avez-vous prétendu,
%# On ne peut égaler mes feux ni ma constance;
%# Bannissez l'injuste esperance
%# De me ravir un titre qui m'est dû.
\livretPers Dom Pedro
%# Puisque Lucile est l'objet de ma flâme,
%# Peut-il estre des feux plus ardens que les miens?
%# L'Amour par d'autres yeux peut-il blesser une ame
%# Si vivement que par les siens?
\livretPers Dom Carlos
%# Lucile est digne qu'on l'adore,
%# Elle enchaîne les cœurs des plus aimables nœuds,
%# Si je n'avois vû Le=onore
%# Nous brûlerions des mêmes feux.
\livretPers Dom Pedro & Dom Carlos
%# Que nostre ardeur soit éternelle,
%# L'Amour nous promet mille attraits;
%# Disputons à jamais
%# A qui sera plus tendre & plus fidelle.
\livretPersDidas Dom Carlos à sa troupe
%# Vous, chantez, celebrez de si belles ardeurs,
%# Que vos voix, que vos chants attendrissent les cœurs.
\livretPers Le Chœur
\livretRef#'CCBchoeur
%# Chantons de si belles ardeurs,
%# Que nos voix, que nos chants attendrissent les cœurs.
\livretPers Une Musicienne
\livretRef#'CCDespagnole
%# Soy=ez constans dans vos amour,
%# Amans, on est prest à se rendre:
%# Un cœur qu'on attaque toûjours
%# Se lasse enfin de se deffendre;
%# Tost ou tard il vient d'heureux jours
%# A qui sçait les attendre.
\livretPers Le Chœur
\livretRef#'CCFchoeur
%# Chantons de si belles ardeurs,
%# Que nos voix, que nos chants attendrissent les cœurs.
\livretFinAct FIN DE LA TROISIÈME ENTRÉE
\null
\sep
\null
