\livretAct LA FRANCE
\livretScene SECONDE ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente un Boccage,
  & dans le fonds un Hameau.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Silvandre, Philene. }
\livretPers Philene
\livretRef#'BAAphileneSilvandre
%# Quoy? pour l'objet de vôtre ardeur
%# Vous préparez encore une Fête nouvelle?
%# Tant de fidelité doit fléchir sa rigueur;
%# En vain Doris affecte une fierté cru=elle,
%# Elle se lassera de refuser son cœur
%# Aux soins que vous prenez pour elle.
\livretPers Silvandre
%# Ce n'est plus de Doris que j'attens mon bonheur.
\livretPers Philene
%#- Ciel! qu'enten-je?
\livretPers Silvandre
%#= L'Amour m'offre un nouveau vainqueur,
%# Et me force d'estre infidelle.
%# Je romps mes premiers nœuds pour des nœuds plus charmans,
%# Mon infidelité m'est chere,
%# Et j'ay plus de plaisir à trahir mes sermens,
%# Que je n'en sentis à les faire.
\livretPers Philene
%# A qui donc offrez-vous, vostre *hommage nouveau?
\livretPers Silvandre
%# A l'indifferente Cephise.
%# Que mon tri=omphe seroit beau
%# Si je la soûmettois au Dieu qu'elle méprise!
\livretPers Philene
%# Vous desiriez avec la mesme ardeur
%# Qu'un jour Doris partageât vôtre flame.
\livretPers Silvandre
%# Hé-bien je vous apprens que j'ay soûmis son cœur,
%# Les feux dont je brulois ont passé dans son ame.
%# Mes sermens, mes pleurs, mes soupirs
%# M'ont obtenu l'aveu que je demandois d'elle.
\livretPers Philene
%# Pourquoy donc brûlez-vous d'une flame nouvelle?
\livretPers Silvandre
%# L'Amour en comblant nos desirs
%# A de nouveaux nœuds nous appelle.
%# Plus de fois on est infidelle,
%# Et plus on goûte de plaisirs.
%# L'Amour en comblant nos desirs
%# A de nouveaux nœuds nous appelle.
%# Cephise se plaît en ces lieux.
\livretPers Philene
%# C'est elle-même qui s'avance.
\livretPers Silvandre
%# Allons, Philene, évitons sa présence,
%# La Fête en ma faveur doit prévenir ses yeux.

\livretScene SCENE DEUXIÉME
\livretPers Céphise
\livretRef#'BBAcephise
%# Paisibles lieux, agré=ables retraites,
%# Je n'aimeray jamais que vous.
%# En vain mille Bergers viennent à mes genoux
%# Me jurer des ardeurs parfaites.
%# Beaux lieux, n'en soy=ez point jaloux,
%# Je méprise leur flâme, & je les quitte tous
%# Pour le plaisir que vous me faites.
%# Paisibles lieux, agré=ables retraites,
%# Je n'aimeray jamais que vous.
%# Pour forcer mon cœur à se rendre
%# On fait des efforts chaque jour;
%# Mais quelques pleurs que je fasse répandre,
%# Quelques sermens que l'on me fasse entendre,
%# Ce sont les pieges de l'Amour,
%# Je me garderay bien de m'y laisser surprendre.

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center {
  Céphise, troupe de Bergers, de Bergeres & des Pastres,
  qui l'interrompent par leurs Dances.
}
\livretPers Céphise
\livretRef#'BCBrecit
%# Que voy-je? quel spectacle! & quels nouveaux concerts!
%# A qui ces jeux sont-ils offerts?
\livretPers Chœurs de Bergers
\livretRef#'BCCchoeur
%# Aimez, aimez, belle Bergere,
%# Laissez-vous enflamer,
%# Que sert l'avantage de plaire,
%# Sans le plaisir d'aimer?
\livretPers Une Bergere
\livretRef#'BCDair
%# Soûpirez, jeunes cœurs,
%# Suivez ce qu'Amour vous inspire;
%# Cent nouvelles douceurs
%# Vous attendent dans son empire:
%# Soûpirez, jeunes cœurs,
%# Devroit-on vous le dire?
\livretPers Chœurs de Bergers
\livretRef#'BCEchoeur
%# Aimez, aimez, belle Bergere,
%# Laissez-vous enflamer,
%# Que sert l'avantage de plaire,
%# Sans le plaisir d'aimer?
\livretPers Une Bergere
\livretRef#'BCFair
%# Aimons dans la jeune saison,
%# Cédons, cédons à la tendresse.
%# Nous en faut-il d'autre raison
%# Que le penchant qui nous en presse?
%# En vain une erreur extrême
%# Nous deffend de nous enflamer;
%# Nôtre cœur sent assez luy-même
%# Le besoin qu'il a d'aimer.
\livretPers Chœurs de Bergers
\livretRef#'BCGchoeur
%# Aimez, aimez, belle Bergere,
%# Laissez-vous enflamer,
%# Que sert l'avantage de plaire,
%# Sans le plaisir d'aimer?
\livretPers Un Berger
\livretRef#'BCJair
%# Soupirons tous,
%# Suivons l'Amour sans nous contraindre;
%# Il est plus doux
%# De le sentir que de le craindre.
%# Qui sent ses coups,
%# Les chérit au lieu de s'en plaindre;
%# L'Amour rend les Amans
%# Jaloux de leurs tourmens.
%# Ses feux sont charmans,
%# Gardons-nous bien de les éteindre,
%# C'est des tendres soupirs
%# Que naissent les plaisirs.
\livretPers Cephise
\livretRef#'BCOrecit
%# Que je sçache du moins d'où me vient cet *hommage;
%# Quel Amant me poursuit jusques dans ce Boccage?

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  Cephise, Silvandre.
}
\livretPers Silvandre
\livretRef#'BDArecit
%# Voy=ez à vos genoux cet Amant empressé:
%# Je découvre en tremblant l'ardeur qui me possede;
%# Mais pardonnez aux maux dont je me sens pressé,
%# C'est dans les yeux qui m'ont blessé
%# Que j'en viens chercher le remede.
\livretPers Cephise
%# Qu'entends-je? quels discours? vous seriez-vous mépris?
%# Vous me prenez, peut-être, pour Doris.
\livretPers Silvandre
%# Non: Cephise, c'est vous à qui je viens apprendre
%# Le vi=olent amour dont je ressens les coups.
%# Helas! Doris a-t'elle autant d'attraits que vous,
%# Et peut-on s'y méprendre?
\livretPers Cephise
%# Ce n'est donc que depuis deux jours
%# Que vos yeux la trouvent moins belle?
%# Vous luy juriez alors une flâme éternelle.
%# Quoy? pouvez-vous si-tost démentir vos discours?
\livretPers Silvandre
%# Lorsque Doris me parut belle,
%# Je ne connoissois pas encore vos attraits,
%# Il faudroit pour estre fidelle
%# Vous avoir toûjours veuë ou ne vous voir jamais.
\livretPers Cephise
%# Que n'adressez-vous mieux un langage si tendre,
%# De quelqu'autre Bergere il surprendroit la foy;
%# Pour moy je fuis l'amour, & je veux m'en deffendre;
%# Mais s'il me contraignoit quelque jour à me rendre,
%#12 Du moins voudrois-je un cœur que n'eût aimé que moy.
\livretPers Silvandre
%# Eh bien, vous serez satisfaite.
%# J'ay senti pour vous seule une flâme parfaite,
%# Je n'ay jamais aimé comme j'aime en ce jour:
%# Doris estoit ma derniere amourette,
%# Vous estes mon premier amour.
\livretPers Cephise
%# Laissez-moy, c'est trop vous entendre,
%# Redonnez vostre cœur à l'aimable Doris.
\livretPers Silvandre
%#- Je vous suivray par tout.
\livretPersDidas Doris qui survient.
%#= Silvandre, cher Silvandre;
%# Ah! je l'appelle en vain, il est sourd à mes cris.

\livretScene SCENE CINQUIÉME
\livretPers Doris
\livretRef#'BEAdoris
%# Quel funeste coup pour mon ame!
%# Quoy? Silvandre, tu me trahis?
%# Ingrat qu'as-tu fait de ta flâme?
%# C'est Doris qui te cherche, & c'est toy qui la fuis.
%# Tu me jurois que l'Astre qui m'éclaire
%# S'éteindroit avant ton amour;
%# Au delà du tombeau je devois t'estre chere,
%# Jamais ardeur ne parut plus sincere,
%# Helas! que de sermens tu trahis en un jour!
%# Tu crois trouver ailleurs une plus douce chaîne;
%# Mais, perfide, crois-tu que je t'y laisse en paix?
%# J'iray troubler sans cesse en rivale inhumaine,
%# Les douceurs que tu te promets:
%# Mon amour outragé me tiendra lieu de haine,
%# Et je te rendray bien les maux que tu me fais.
%# Mais ses tourmens calmeront-ils ma peine?
%# Non, non, il faut plûtost luy cacher mon courroux;
%# Que dans d'autres li=ens un nouveau feu l'entraîne?
%# Il ne joü=ira point de mon dépit jaloux;
%# Et j'attendray qu'à mes genoux
%# Son inconstance le rameine.
\livretFinAct FIN DE LA SECONDE ENTRÉE
\null
\sep
\null
