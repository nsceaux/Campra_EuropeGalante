\livretAct L’ITALIE
\livretScene QUATRIÈME ENTRÉE
\livretDescAtt\wordwrap-center {
  Le Théatre represente une Salle magnifique preparée pour un Bal
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Octavio, Olimpia
}
\livretPers Octavio
\livretRef#'DABrecit
%# Ne verray-je jamais le jour
%# Où je seray content de l'ardeur de vôtre ame?
%# Ingrate, vous brûlez d'une trop foible flame;
%# Vous offensez & l'Amant & l'Amour.
%# Ne verray-je jamais le jour
%# Où je seray content de l'ardeur de vôtre ame?
\livretPers Olimpia
%# De quel reproche encor venez-vous m'allarmer?
%# Vos soupçons plus long-tems ne peuvent se contraindre,
%# Que sert ingrat de vous aimer?
%# Vous ne cessez point de vous plaindre.
\livretPers Octavio
%# Je ne me plaindrois pas,
%# Si vous m'aimiez comme il faut que l'on aime;
%# A suivre sans cesse vos pas
%# Je trouve une douceur extrême:
%# Tous les autres plaisirs sont pour moy sans appas;
%# Du bonheur de vous voir je fais mon bien suprême:
%# Helas! si vous m'aimiez de mesme
%# Je ne me plaindrois pas.
%# Mais que vous estes loin de l'ardeur qui m'enflame;
%# Mon bonheur ne fait pas le plus doux de vos soins;
%# Et de tous les plaisirs que peut goûter vostre ame
%# Mon amour est celuy qui la touche le moins.
\livretPers Olimpia
%# Je connois ce qui vous irrite,
%# Vous souffrez à regret que je vienne en ces lieux;
%# Et le spectacle ou l'on m'invite
%# Offense peut-estre vos yeux.
\livretPers Octavio
%# C'est le sujet de mes justes allarmes,
%# Vous reconnoissez mal ma foy;
%# Je renonce à tout pour vos charmes,
%# Et vous ne quitez rien pour moy.
\livretPers Olimpia
%# Sortez de l'amoureux empire,
%# Ou devenez plus tranquile en aimant
%# Un cœur qui s'allarme aisément
%# N'est point heureux quand il soûpire;
%# Pour moy l'Amour est un plaisir charmant,
%# Pour vous c'est un martire.
\livretPers Octavio
%# Ah! ne murmurez point de mes transports jaloux!
%# L'excés de mon amour fait celuy de mes craintes;
%# Tout ce qui s'approche de vous
%# Porte à mon cœur de sensibles atteintes.
%# Que ne sommes-nous seuls en des lieux retirez
%# Je cesserois peut-estre de me plaindre;
%# Plus vos attraits y seroient ignorez,
%# Moins j'aurois de Rivaux à craindre.
%# On vient. Songez du moins que je suis prés de vous,
%# Et menagez un cœur jaloux.

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Octavio, Olimpia, Troupe de Masques.
}
\livretPers Le Chœur des Masques
\livretRef#'DBBchoeur
%# Tendres amans, rassemblons-nous.
%# Pour les cœurs que l'Amour enchaîne,
%# Quel séjour peut estre plus doux?
%# S'il se trouve icy des jaloux;
%# L'Amour ne les ameine
%# Que pour les tromper tous.
\livretPers Une Venitienne
\livretRef#'DBDair
%#9 Ad un cuore tutto geloso
%#7 Deve amor negar pieta.
%#4 La sua face
%#5 Ch’alleta è piace
%#8 Vuol dolcezza non crudelta.
\livretRef#'DBFair
%#9 Un bel viso tutto vezzoso
%#7 Merta un laci di lealta.
%#4 Che Cupido
%#5 Quel nume infido
%#8 Aborrisce la ferita.
\livretDidasPPage Sens de l’Italien.
%# Sur les jaloux l'Amour épuise
%# Ses plus redoutables rigueurs;
%# Il veut qu'on engage les cœurs,
%# Et défend qu'on les tirannise,
%# Belles, prenez de douces chaînes,
%# Tout doit répondre à vos desirs;
%# Le Dieu d'Amour garde ses peines
%# Pour qui troublera vos plaisirs.
\livretPersDidas Une Venitienne déguisée
\livretRef#'DBHairChoeur
%# Formons d'aimables jeux, laissons-nous enflamer;
%# Il n'est permis icy que de rire & d'aimer.
\livretDidasP\line { Le Chœur repete ces deux derniers Vers. }
\livretPers La Venitienne
%# Bannissons de ces lieux l'importune raison,
%# Elle vaut moins qu'une aimable folie;
%# Un doux excés sied bien dans la jeune saison,
%# Pour estre *heureux il faut qu'un cœur s'oublie.
\livretPers Le Chœur
%# Formons d'aimables jeux, laissons-nous enflamer;
%# Il n'est permis icy que de rire & d'aimer.
\livretPers La Venitienne
%# Rendez-vous, jeunes cœurs, cedez à vos desirs,
%# Tout vous inspire un tendre badinage;
%# Ne preferez jamais la sagesse aux plaisirs,
%# Il vaut bien mieux estre *heureux qu'estre sage.
\livretPers Le Chœur
%# Formons d'aimables jeux, laissons-nous enflamer;
%# Il n'est permis icy que de rire & d'aimer.
\livretPersDidas Une autre Venitienne déguisée
\livretRef#'DBJairChoeur
%# Livrons-nous aux plaisirs, il n'est rien de plus doux;
%# Pour qui seroient-ils faits si ce n'estoit pour nous?
\livretDidasP\line { Le Chœur repete ces deux Vers. }
\livretPers La Venitienne
%# Mille amours déguisez dans ce charmant séjour
%# Comblent nos cœurs d'une douceur extrême;
%# Si quelqu'un en ces lieux est entré sans amour,
%# Ne craignons pas qu'il en sorte de mesme.
\livretPers Le Chœur
%# Livrons-nous aux plaisirs, il n'est rien de plus doux;
%# Pour qui seroient-ils faits si ce n'estoit pour nous?
\livretPers La Venitienne
%# L'Amour, jeunes beautez, accompagne vos pas,
%# Pour tout soumettre il vous prête ses armes;
%# C'est vainement qu'aux yeux vous cachez mille appas,
%# A tous les cœurs il revéle vos charmes.
\livretDidasP\line { Le Chœur repete \normal-text Livrons-nous, &c. }
\livretRef#'DBKair
\livretDidasPPage\justify {
  Pendant la Feste un des Masques danse avec Olimpia,
  & fait remarquer beaucoup d’empressement pour elle.
  Quand le bal finit, Octavio suit ce Masque,
  & Olimpia reste surpise de se trouver sans luy.
}
\livretPers Air Italien
\livretRef#'DBLair
%#6 Si scherzi, si rida
%#6 Si pensi =à goder
%#6 Gia sotto le piume
%#6 D'Aligero nume
%#6 Per noi si matura
%#6 L'acerbo piacer
\livretDidasPPage Sens de l’Italien.
%# Rions & folâtrons ne songeons qu'aux plaisirs,
%# L'Amour sous ses aîles
%# Au gré de nos desirs
%# Meurit mille douceurs nouvelles.

\livretScene SCENE TROISIÈME
\livretPers Olimpia
\livretRef#'DCArecit
%# Qu'est devenu le jaloux qui m'obsede?
%# Ciel! quel est le sujet de son éloignement?
%# Auroit-il reconnu l'ardeur qui me possède?
%# Mes regards n'ont-ils pas découvert mon amant?
%# Peut-estre de nos yeux la douce intelligence
%# N'a pû garder le secret de nos cœurs;
%# Ces indiscrets témoins de nos tendres langueurs
%# Ont enfin rompu le silence.
%# Ah! faut-il qu'une injuste loy
%# Destine à ce jaloux le reste de ma vie;
%# Les soins que son Rival a laissé voir pour moy
%# Me font redouter sa furie;
%#- Que je crains…

\livretScene SCENE QUATRIÈME
\livretDescAtt\center-column {
  \line { Octavio, Olimpia }
  \wordwrap-center {
    Octavio rentre en remettant son Poignard.
  }
}
\livretPers Olimpia
\livretRef#'DDArecit
%#= Mais que vois-je? ô Ciel!
%# Cru=el! quelle rage vous guide?
%# De quels affreux transports éteincellent vos yeux;
\livretPers Octavio
%# Gemi, pleure à ton tour, perfide;
%# Va, cours de ton amant recevoir les adieux;
%# Il expire prés de ces lieux.
\livretPersDidas Olimpia en s’évanoüissant
%#- Ciel!
\livretPers Octavio
%#= Eh bien, malheureux, en douterois-je encore?
%# Sa douleur m'en dit plus que je n'en veux sçavoir;
%# Me voilà donc certain du feu qui la devore;
%# Cependant je n'ay pû vanger mon desespoir
%# Sur celuy que son cœur adore.
%# En vain je l'ay suivi, ce trop heureux Amant.
%# Fatale Feste, nuit trop sombre,
%# C'est vous dont le tumulte & l'ombre
%# Ont dérobé ses jours à mon ressentiment.
\livretDidasPPage à Olimpia
%# Tu reprens tes esprits, cru=elle, à ce langage,
%# Je suis le seul qui souffre icy:
%# De tous ses mouvements je sens croistre ma rage;
%# Je voulois luy surprendre un secret qui m'outrage;
%# Je n'ay que trop bien ré=üssi.
\livretPers Olimpia
%# Vous voy=ez mon ardeur, il n'est plus temps de feindre,
%# Mon secret se découvre à vos soupçons jaloux;
%# C'est à l'Amour qu'il faut vous plaindre,
%# Je l'aurois écouté s'il m'eust parlé pour vous.
\livretPers Octavio
%# Quoy! perfide, mes feux, le devoir, ma tendresse,
%# Mes pleurs n'ont pu vous attendrir?
%# Ah! je veux désormais reparer ma foiblesse,
%# Je mettray tous mes soins à vous faire souffrir:
%# Puisque vous brulez pour un autre,
%# Mon Rival en perdra le jour;
%# Ma fureur dans son sang éteindra son amour,
%# Et punira le vôtre.
\livretPers Olimpia
%# Cru=el, cessez de m'allarmer,
%# N'ecoutez point une injuste colere;
%# C'estoit à moy de vous aimer,
%# Mais c'estoit à vous de me plaire.
\livretPers Octavio
%# Ingrate, ce discours vient encor animer
%# Mon desespoir & ma vengeance.
\livretPers Olimpia
%# Pour vous aider à les calmer
%# Il faut fuïr de vostre presence.

\livretScene SCENE CONQUIÈME
\livretPers Octavio
\livretRef#'DEArecit
%# Quel outrage! mon cœur ne peut le soûtenir,
%# Elle me laisse, elle rit de ma peine;
%# Dieux! quand l'*Hymen est prest à nous unir,
%# La perfide à ses nœuds oppose une autre chaîne.
%# Non, je ne puis luy pardonner,
%# Je me livre aux transports de ma fureur extréme,
%# Je suivray les conseils qu'elle me vient donner.
%# Immolons mon rival, son amante & moy-même.
%#12 Ne vaudroit-il pas mieux rompre un fatal lien?
%# Mais le puis-je? Insensé, quel vain espoir me flatte?
%# Sans l'objet de mes feux je n'espere plus rien;
%# C'est sa seule rigueur qu'il faut que je combatte:
%# Allons tomber encore aux genoux de l'ingratte,
%# Pour attendrir son cœur, ou pour percer le mien.
\livretFinAct FIN DE LA QUATRIÈME ENTRÉE
\null
\sep
\null
