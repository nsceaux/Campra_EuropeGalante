\livretAct L’EUROPE GALANTE
\livretScene PREMIERE ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente une Forge galante, où les Graces,
  les Plaisirs & les Ris sont occupez à forger les traits
  de l’Amour. Venus y descend pour les exciter au travail.
}
\livretPers Venus
\livretRef#'AABvenusChoeur
%# Frappez, frappez, ne vous lassez jamais,
%# Qu'à vos travaux l'Echo réponde.
%# Pour le fils de Venus forgez de nouveaux traits;
%# Qu'ils portent dans les cœurs une atteinte profonde.
%# Frappez, frappez, ne vous lassez jamais,
%# Vous travaillez pour le bonheur du monde.
\livretPersDidas Le chœur des Graces, des Plaisirs & des Ris
%# Frappons, Frappons, ne nous lassons jamais,
%# Qu'à nos travaux l'Echo réponde.
%# Pour le fils de Venus forgeons de nouveaux traits;
%# Qu'ils portent dans les cœurs une atteinte profonde.
%# Frappons, frappons, ne nous lassons jamais,
%# Nous travaillons pour le bonheur du monde.
\livretPers Venus
\livretRef#'AACvenus
%# C'est Vulcain qui fait le Tonnerre,
%# Dont le Maître des Dieux épouvante le Terre;
%# Mais ce sont les Plaisirs, les Graces & les Ris
%# Qui forment les traits de mon fils.
%# Jeunes cœurs essay=ez la douceur de ses armes;
%# Qui s'en laisse blesser éprouve mille charmes.
\livretPersDidas Deux Graces, à qui le chœur répond
\livretRef#'AAGair
%# Souffrez que l'Amour vous blesse:
%# Belles, chassez la fierté:
%# Apprenez que la tendresse
%# Est l'ame de la Beauté.
\livretRef#'AAIair
%# Si vous voulez que les Graces
%# Vous accompagnent toûjours,
%# Pour les voir suivre vos traces
%# Suivez celles des Amours.
\livretPers Une Grace
\livretRef#'AAKgrace
%# C'est dans une tendresse extrême
%# Qu'on trouve des plaisirs parfaits.
%# On n'est content que quand on aime,
%# Les autres biens sont sans attraits;
%# Pour être *heureux l'Amour luy-même
%# S'est blessé de ses traits.
\livretDidasPPage\justify {
  Ce Divertissement est troublé par une Symphonie qui annonce la
  Discorde.
}
\livretPers Venus
\livretRef#'ABAvenusDiscorde
%# Quelle soudaine *horreur! & quels terribles bruits!
%# Ciel! qui peut amener la Discorde où je suis?
\livretPers La Discorde
%# C'est en vain qu'à tes Loix tu prétends qu'on réponde.
%# Dé=esse, fais cesser d'inutiles travaux.
%# A quel coin reculé du Monde
%# L'Amour veut-il tenter des tri=omphes nouveaux?
%# Pour qui destine-t’il les traits qu’on luy prepare?
%# De tous côtez je le fais dédaigner;
%# Lorsque de tous les cœurs la Discorde s’empare,
%# Sur qui veut-il encor regner?
%# Tout ressent la fureur dont je suis animée,
%# A mes sanglants Autels tout vient sacrifier,
%# Et ton fils se voit oublier ;
%# Je l’ay du moins banny de l'Europe allarmée,
%# S’il ne l’est pas du monde entier.
%%% De quels traits impuissans menace-t’il la terre?
%%% Quoy déja son pouvoir veut succéder au mien?
%%% A peine a-t'on éteint le flambeau de la Guerre,
%%% Qu'il prétend rallumer le sien.
%%% Non, non, j'ay pour toûjours trompé son esperance,
%%% J'ay détruit, j'ay brisé les Autels & les fers:
%%% J'ay du moins arraché l'Europe à sa puissance,
%%% Si ce n'est pas tout l'Univers.
\livretPers Venus
%# Tu t'applaudis d'une fausse Victoire,
%# L'Amour a dans l'Europe une nouvelle Gloire.
%# Il recueille le fruit de tes noires fureurs,
%# Il regne au milieu de la Guerre.
%%% Il a tri=omphé de la Guerre.
%# Malgré tes vains efforts il rassemble deux cœurs
%%% Malgré tous tes efforts il rassemble deux cœurs
%# Qui feront quelque jour le destin de la Terre.
%# Le Héros qui les joint commence à dénoü=er
%%% Le Héros qui les joint sçait enfin dénoü=er
%# Ce nœud que tu formas avec un soin funeste.
\livretPers La Discorde
%# C'en est assez; épargne-moy le reste;
%# Et ne me force pas à t'entendre loü=er
%# Un Roy qui me déteste.
\livretPers Venus
%# Je te feray souffrir de plus cru=els tourmens,
%# Tu méprises l'Amour, tu verras sa victoire:
%# Et je veux que ces lieux par divers changemens
%# Servent de Thé=atre à sa gloire.
%# L’Europe que tu crois attentive à ta voix,
%# Va chanter à tes yeux la douceur de ses loix;
%%% C'est luy qui dans l'Europe a ramené la Paix,
%%% Ses Peuples à tes yeux vont chanter ses attraits,
%# Tu vas voir que des cœurs l'Amour seul est le Maître.
\livretPers La Discorde
%# Ah! ne te flatte pas de m'en rendre témoin.
\livretPers Venus
%# Je veux te contraindre de l'être,
%# Tu prends pour t'en deffendre un inutile soin.
\livretPers La Discorde
%# Puisque dans ces lieux on m'arrête,
%# Fureurs, secondez-moy, troublons au moins la Fête.
%# Faisons des Inconstans, des Jaloux odi=eux,
%# Jettons dans tous les cœurs les soupçons & les craintes:
%# Qu'on reconnoisse à mille plaintes
%%% Que l'on connoisse à mille plaintes
%# Que la Discorde est dans ces lieux.
\livretPers Venus
\livretRef#'ABBvenus
%# Tu ne peux exciter que de vaines allarmes;
%# Tu rendras mon tri=omphe encor plus glori=eux.
\livretRef#'ABCvenusChoeur
%# Faisons regner l'Amour, faisons briller ses charmes,
%# Les doux plaisirs sont ses plus fortes armes.
\livretPers Le Choeur
%# Faisons regner l'Amour, faisons briller ses charmes,
%# Les doux plaisirs sont ses plus fortes armes.
\livretPers Une Grace
\livretRef#'ABEair
%# Ah! que l'Amour
%# Prépare en ce jour
%%% Ah! que ce jour
%%% Va faire à l'Amour
%# De conquêtes nouvelles!
%# Que se appas
%# Vont soûmettre de Belles
%# Qui n'y pensent pas!
%# Il va fléchir tous les cœurs rebelles,
%# Il va pour jamais
%# Les blesser de ses traits;
%# Loin de les craindre,
%# Cherchons leurs coups.
%# Quel cœur peut se plaindre
%# D'un tourment si doux?
%# Au Dieu d'Amour cédons la victoire;
%# Quand il nous soûmet à ses desirs,
%# C'est moins pour sa gloire
%# Que pour nos plaisirs.
\null
\livretRef#'ABGair
%# Que tes faveurs
%# Vont charmer les cœurs!
%# Amour, que de cru=elles
%# Tu vas dompter!
%# Et que d'Amans fidéles
%# Vont en profiter!
%# Tu vas fléchir tous les cœurs rebelles,
%# Tu vas pour jamais
%# Les blesser de tes traits:
%# Loin de les craindre
%# Cherchons leurs coups.
%# Que cœur peut se plaindre
%# D'un tourment si doux?
%# Au Dieu d'Amour cédons la victoire;
%# Quand il nous soûmet à ses desirs,
%# C'est moins pour sa gloire
%# Que pour nos plaisirs.
\livretPers Le Chœur
\livretRef#'ABIchoeur
%# Mortels, que l'Amour vous entraîne,
%# Cédez à ses douces ardeurs:
%# Qu'il vous blesse, qu'il vous enchaîne,
%# Qu'il regne à jamais dans vos cœurs.
\livretPersDidas Venus à la Discorde
\livretRef#'ABJrecit
%# Commence à ressentir l'effet de ma vengeance;
%# Discorde, voy l'Amour tri=ompher de la France.
\livretFinAct FIN DE LA PREMIERE ENTRÉE
\null
\sep
\null
\livretAct AVIS
\null
\justify {
  On a choisi des Nations de l’Europe,
  celles dont les caractéres se contrastent davantage & promettent plus
  de jeu pour le Théatre : La France, l’Espagne, l’Italie & la Turquie.
  On a suivi les idées ordinaires qu’on a du genie de leurs Peuples.
  Le François est peint volage, indiscret & coquet ;
  l’Espagnol, fidéle & romanesque ;
  l’Italien, jaloux, fin & violent ;
  Et enfin, l’on a exprimé, autant que le Théatre l’a pû permettre,
  la hauteur & la souveraineté des Sultans, & l’emportement des Sultanes.
}
\null
\sep
\null


