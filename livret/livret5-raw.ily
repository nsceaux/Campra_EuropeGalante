\livretAct LA TURQUIE
\livretScene CINQUIÈME ENTRÉE
\livretDescAtt\wordwrap-center {
  Le Théatre represente les Jardins du Serail, & dans le fonds
  le Palais des Sultanes.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Zayde seule.
}
\livretPers Zayde
\livretRef#'EAAzaide
%# Mes yeux ne pourrez-vous jamais
%# Forcer mon vainqueur à se rendre?
%# Faut-il avec un cœur si tendre
%# Avoir de si foibles attraits?
%# Mes yeux ne pourrez-vous jamais
%# Forcer mon vainqueur à se rendre?
%# Au moment de mon esclavage,
%# Quand on me conduisit dans ce riche Palais,
%# Il parut à mes yeux l'Antre le plus sauvage;
%# Je le fis retentir de mes tristes regrets;
%# Je me fis une image affreuse
%# Du Souverain que j'adore aujourd'hui;
%# Mais sa presence enfin dissipa mon ennuy;
%# Et je me trouvay trop heureuse
%# D'estre captive auprés de luy.
%# Les beautez dont il est le maistre,
%# Par son ordre bien-tost s'assemblent dans ces lieux,
%# Amour, Amour, fais-luy connoistre
%# Le cœur qui le merite mieux.
%# Mais c'est luy que je voy, gardons-nous de paroistre,
%# Il n'est pas tems encor de m'offrir à ses yeux.

\livretScene SCENE DEUIXÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Roxane.
}
\livretPers Roxane
\livretRef#'EBAzulimanRoxane
%# Quoy pour d'autres appas vostre ame est enflamée?
%# Mes soupirs désormais vont estre superflus;
%# Ah! pourquoy m'avez-vous aimée?
%# Ou pourquoy ne m'aimez vous plus?
\livretPers Zuliman
%# Je ne romprois pas nostre chaîne
%# Si vous sçaviez m'y retenir;
%# Mon cœur s'accorde sans peine
%# A qui sçait mieux l'obtenir.
\livretPers Roxane
%# Que vostre inconstance est cru=elle!
%# Helas! vous m'ostez vostre cœur,
%# Et malgré toute ma douleur
%# Je n'ose vous traiter d'ingrat & d'infidelle.
%# Je vois avec horreur mépriser mes appas,
%# Je sens les plus vives allarmes;
%# Mais le respect me force à murmurer tout bas,
%# Et me fait devorer mes soupirs & mes larmes.
\livretPers Zuliman
%# Vous meritez un sort plus doux,
%# Et mon cœur à regret se détache du vostre;
%# La pitié parle encor pour vous,
%# Mais l'Amour parle pour un autre.
\livretPers Roxane
%# C'en est donc fait, Seigneur, mes beaux jours sont passez?
\livretPers Zuliman
%# Je n'oublieray jamais que vous me fûtes chere.
\livretPers Roxane
%# Vous ne m'aimez plus, c'est assez
%# Tout le reste me desespere;
%# Que ne puis-je oubli=er que je vous ay sceu plaire!
%# Je ne sentirois pas que vous me trahissez.
\livretPers Zuliman
%# On approche, cessez une plainte trop vaine;
%# Celles qu'icy mon ordre ameine
%# Vont par leurs jeux répondre à mes desirs;
%# Dissimulez vostre peine,
%# Et respectez mes plaisirs.
\livretPers Roxane
%# Voy=ons du moins l'objet de ses nouveaux soupirs;
%# Sçachons à qui je dois ma haine.

\livretScene SCENE TROISIÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Roxane, Zayde, & les autres Sultanes.
}
\livretRef#'ECApassacaille
\livretDidasPage\wordwrap-center {
  Les sultanes forment plusieurs dances pour plaire à Zuliman.
}
\livretPers Zayde
\livretRef#'ECBair
%# Que l'Amour dans nos cœurs fasse naistre
%# Mille ardeurs pour nostre auguste Maistre;
%# Que nos tendres soupirs
%# Préviennent ses desirs.
\livretDidasP\wordwrap {
  Le Chœur des Sultanes repete ces quatres vers.
}
\livretPers Zayde
%# Dans ces lieux tout doit le satisfaire;
%# Pour ce charmant Vainqueur laissons-nous enflamer;
%# Attendons le bonheur de luy plaire
%# En joü=issant toujours du plaisir de l'aimer.
\livretDidasP\wordwrap {
  Le Chœur repete ces quatres vers.
}
\livretPersDidas Zuliman à Zaïde
\livretRef#'ECCrecit
%# Vous brillez seule en ces retraites,
%# Vous effacez tous les autres appas;
%# L'Amour ne se plaist qu'où vous estes,
%# Il languit où vous n'estes pas:
%# Mon cœur ne sent que trop le plaisir que vous faites.
\livretPers Zayde
%#- Quoy? Seigneur…
\livretPers Zuliman
%#= C'est de vous que je me sens épris;
%# Depuis le jour que je vous vis
%# Mon cœur, belle Za=ïde, en secret vous adore.
\livretPers Zaïde
%# Helas! s'il estoit vray vous me l'auriez appris.
\livretPers Zuliman
%# Non, & c'est un secret que je tairois encore
%# Si vos tendres regards ne me l'avoient surpris.
%# J'esperois affranchir mon ame
%# Du peril d'engager sa foy;
%# Et je ne voulois pas me permettre une flame
%# Qui prist trop d'empire sur moy.
%# J'ay long-temps differé de vous rendre les armes:
%# Pour éviter d'éternelles amours,
%# Des beautez de ces lieux j'empruntois le secours;
%# Mais vous tri=omphez de leurs charmes,
%# Et je vous aime, enfin, pour vous aimer toujours.
\livretPersDidas Roxane tirant son poignard & voulant fraper Zayde
%# Ah c'en est trop, je céde à cet outrage,
%# Versons le sang que demande ma rage.
\livretPersDidas Zuliman luy arrachant son poignard.
%# Ciel! que vois-je? quelle fureur!
%# Malheureuse, qu'oze-tu faire?
\livretPers Roxane
%# Je voulois la punir d'avoir trop sceu te plaire,
%# Et de m'avoir ravi ton cœur.
%# Le desespoir dont je suis animée
%# S'enflame encor par tes discours;
%# Tu luy jures, cru=el, les plus tendres amours,
%# Tu l'aimes cent fois plus que tu ne m'as aimée.
%# Quand tu formas les nœuds que tu romps pour jamais
%# J'éprouvay ta fierté jusque dans ta tendresse;
%# Helas! c'est avec d'autres traits
%# Que l'Amour aujourd'huy te blesse,
%# Devant ses yeux ton orgueil cesse;
%# J'ay voulu vanger mes attraits,
%# Et te punir de ta foiblesse.
\livretPers Zuliman
%# Quoy, ne crains-tu pas que la mort
%# Soit le prix de ton insolence?
\livretPers Roxane
%# Je n'ay pû remplir ma vangeance;
%# Ce regret seul sans toy peut terminer mon sort:
%# Mais toy, Rivale trop cru=elle,
%# Prens ce fer infidele à mon juste courroux;
%# Portes-en à mon cœur une atteinte mortelle;
%# Tu m'as déja porté de plus sensibles coups.
\livretPers Zuliman
%# Qu'on l'ôte de mes yeux & qu'on s'assûre d'elle.

\livretScene SCENE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Zayde, & les autres Sultanes.
}
\livretPers Zayde
\livretRef#'EDAzaideZuliman
%# Au nom de nos tendres ardeurs
%# Oubli=ez sa jalouse rage,
%# Ne vous vangez de ses fureurs
%# Qu'en m'aimant d'avantage.
\livretPers Zuliman
%# Je suis épris de vos attraits
%# Autant qu'on le peut être;
%# Mon feu ne sçauroit croistre,
%# Ni s'affoiblir jamais.
\livretPers Zuliman & Zayde
%# Livrons nos cœurs à la tendresse
%# Ne formons que d'*heureux desirs;
%# Aimons-nous, aimons-nous sans cesse,
%# Comptons nos jours par nos plaisirs.
\livretPers Zuliman
%# Que tout signale icy nos ardeurs mutu=elles,
%# Qu'on offre à nos regards les Festes les plus belles.

\livretScene SCENE CINQUIÈME
\livretDescAtt\center-column {
  \wordwrap-center {
    Zuliman, Zayde, les Sultanes,
    & les Bostangis ou Jardiniers du Serail.
  }
  \wordwrap-center {
    Ils forment plusieurs Jeux suivant leur caractere.
  }
}
\livretPersDidas Le Chef des Bostangis à qui le Chœur répond
\livretRef#'EEBchoeur
%# Vivir, vivir, gran Sultana.
\livretRef#'EECchoeur
%# Unir unir li cantara.
%# Mille volte =exclamara,
\livretRef#'EEDchoeur
%# Vivir, vivir, gran Sultana.
\null
\livretRef#'EEFchoeur
%# Bello como star un flor;
%# Durar quanto far arbor.
%# Al enemigos su sciabola
%# Como a frutas tempesta.
%# La rusciada matutina
%# Far florir su jardina.
%# Favor celesta
%# Coprir su turbanta.
\null
\livretRef#'EEIchoeur
%# Star contento,
%# Star potento,
%# Del mondo star l'amor o la spavento.
%# En regnar,
%# En amar,
%# Far tributir
%# L'occidento, l'Oriento.
%# En regnar,
%# En amar,
%# Sempre sentir
%# Plazer sensa tormento.
%# Dir e far
%# O disfar
%# Subito, subito
%#5 Sú ló momento.
\livretRef#'EEKchoeur
%# Star contento,
%# Star potento,
%# Del mondo star l'amor, ó lo spavento.
\livretDidasP\wordwrap { Le sens des paroles Franques. }
%# Vive le Souverain qui nous donne des Loix;
%# Chantons, chantons, repetons mille fois,
%# Vive le souverain qui nous donne des Loix.
%# Qu'il ignore à jamais les peines,
%# Qu'il éprouve mille douceurs,
%# Qu'il brille autant que les fleurs,
%# Qu'il dure autant que les chesnes.
%# Qu'il ré=ünisse en luy la force & le courage;
%# Que ses voisins jaloux
%# Craignent plus son couroux
%# Que nos fruits ne craignent l'orage.
%# Qu'au devant de ses vœux les cœurs viennent s'offrir
%# Que pour son bon-heur tout conspire;
%# Et que le Ciel fasse toujours fleurir
%# Et ses jardins & son Empire.

\livretScene SCENE DERNIERE
\livretDescAtt\wordwrap-center {
  Venus, la Discorde.
}
\livretPers La Discorde
\livretRef#'EFArecit
%# C'en est trop, Dé=esse inhumaine,
%# Laisse-moy fuïr de ce fatal séjour;
%# Tu n'as que trop joü=i de ma cru=elle peine:
%# O Ciel! tout échape à ma haine,
%# Et tout céde à l'Amour.
%# J'excitois vainement le Dépit & la Rage;
%# La force de l'Amour en brilloit d'avantage.
%# Fuy=ons, fuy=ons de l'Univers,
%# Allons du moins regner dans les Enfers.
\livretDidasPPage Elle s’abisme.
\livretPers Venus
%# La Discorde à l'Amour céde enfin la victoire.
%# Vous, Jeux chamans, tendres Plaisirs,
%# Volez de toutes parts pour servir ses desirs;
%# Allez accroître encor son Empire & sa Gloire.
\livretDidasPPage\wordwrap {
  Les Plaisir partent pour satisfaire à ses Ordres.
}
\null
\livretFinAct FIN DE L’EUROPE GALANTE
