\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct L’EUROPE GALANTE
\livretScene PREMIERE ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente une Forge galante, où les Graces,
  les Plaisirs & les Ris sont occupez à forger les traits
  de l’Amour. Venus y descend pour les exciter au travail.
}
\livretPers Venus
\livretRef#'AABvenusChoeur
\livretVerse#10 { Frappez, frappez, ne vous lassez jamais, }
\livretVerse#8 { Qu’à vos travaux l’Echo réponde. }
\livretVerse#12 { Pour le fils de Venus forgez de nouveaux traits ; }
\livretVerse#12 { Qu’ils portent dans les cœurs une atteinte profonde. }
\livretVerse#10 { Frappez, frappez, ne vous lassez jamais, }
\livretVerse#10 { Vous travaillez pour le bonheur du monde. }
\livretPersDidas Le chœur des Graces, des Plaisirs & des Ris
\livretVerse#10 { Frappons, Frappons, ne nous lassons jamais, }
\livretVerse#8 { Qu’à nos travaux l’Echo réponde. }
\livretVerse#12 { Pour le fils de Venus forgeons de nouveaux traits ; }
\livretVerse#12 { Qu’ils portent dans les cœurs une atteinte profonde. }
\livretVerse#10 { Frappons, frappons, ne nous lassons jamais, }
\livretVerse#10 { Nous travaillons pour le bonheur du monde. }
\livretPers Venus
\livretRef#'AACvenus
\livretVerse#8 { C’est Vulcain qui fait le Tonnerre, }
\livretVerse#12 { Dont le Maître des Dieux épouvante le Terre ; }
\livretVerse#12 { Mais ce sont les Plaisirs, les Graces & les Ris }
\livretVerse#8 { Qui forment les traits de mon fils. }
\livretVerse#12 { Jeunes cœurs essayez la douceur de ses armes ; }
\livretVerse#12 { Qui s’en laisse blesser éprouve mille charmes. }
\livretPersDidas Deux Graces, à qui le chœur répond
\livretRef#'AAGair
\livretVerse#7 { Souffrez que l’Amour vous blesse : }
\livretVerse#7 { Belles, chassez la fierté : }
\livretVerse#7 { Apprenez que la tendresse }
\livretVerse#7 { Est l’ame de la Beauté. }
\livretRef#'AAIair
\livretVerse#7 { Si vous voulez que les Graces }
\livretVerse#7 { Vous accompagnent toûjours, }
\livretVerse#7 { Pour les voir suivre vos traces }
\livretVerse#7 { Suivez celles des Amours. }
\livretPers Une Grace
\livretRef#'AAKgrace
\livretVerse#8 { C’est dans une tendresse extrême }
\livretVerse#8 { Qu’on trouve des plaisirs parfaits. }
\livretVerse#8 { On n’est content que quand on aime, }
\livretVerse#8 { Les autres biens sont sans attraits ; }
\livretVerse#8 { Pour être heureux l’Amour luy-même }
\livretVerse#6 { S’est blessé de ses traits. }
\livretDidasPPage\justify {
  Ce Divertissement est troublé par une Symphonie qui annonce la
  Discorde.
}
\livretPers Venus
\livretRef#'ABAvenusDiscorde
\livretVerse#12 { Quelle soudaine horreur ! & quels terribles bruits ! }
\livretVerse#12 { Ciel ! qui peut amener la Discorde où je suis ? }
\livretPers La Discorde
\livretVerse#12 { C’est en vain qu’à tes Loix tu prétends qu’on réponde. }
\livretVerse#12 { Déesse, fais cesser d’inutiles travaux. }
\livretVerse#8 { A quel coin reculé du Monde }
\livretVerse#12 { L’Amour veut-il tenter des triomphes nouveaux ? }
\livretVerse#12 { Pour qui destine-t’il les traits qu’on luy prepare ? }
\livretVerse#10 { De tous côtez je le fais dédaigner ; }
\livretVerse#12 { Lorsque de tous les cœurs la Discorde s’empare, }
\livretVerse#8 { Sur qui veut-il encor regner ? }
\livretVerse#12 { Tout ressent la fureur dont je suis animée, }
\livretVerse#11 { A mes sanglants Autels tout vient sacrifier, }
\livretVerse#7 { Et ton fils se voit oublier  ; }
\livretVerse#12 { Je l’ay du moins banny de l’Europe allarmée, }
\livretVerse#8 { S’il ne l’est pas du monde entier. }
%%% De quels traits impuissans menace-t’il la terre?
%%% Quoy déja son pouvoir veut succéder au mien?
%%% A peine a-t'on éteint le flambeau de la Guerre,
%%% Qu'il prétend rallumer le sien.
%%% Non, non, j'ay pour toûjours trompé son esperance,
%%% J'ay détruit, j'ay brisé les Autels & les fers:
%%% J'ay du moins arraché l'Europe à sa puissance,
%%% Si ce n'est pas tout l'Univers.
\livretPers Venus
\livretVerse#10 { Tu t’applaudis d’une fausse Victoire, }
\livretVerse#12 { L’Amour a dans l’Europe une nouvelle Gloire. }
\livretVerse#12 { Il recueille le fruit de tes noires fureurs, }
\livretVerse#8 { Il regne au milieu de la Guerre. }
%%% Il a tri=omphé de la Guerre.
\livretVerse#12 { Malgré tes vains efforts il rassemble deux cœurs }
%%% Malgré tous tes efforts il rassemble deux cœurs
\livretVerse#12 { Qui feront quelque jour le destin de la Terre. }
\livretVerse#12 { Le Héros qui les joint commence à dénoüer }
%%% Le Héros qui les joint sçait enfin dénoü=er
\livretVerse#12 { Ce nœud que tu formas avec un soin funeste. }
\livretPers La Discorde
\livretVerse#10 { C’en est assez ; épargne-moy le reste ; }
\livretVerse#12 { Et ne me force pas à t’entendre loüer }
\livretVerse#6 { Un Roy qui me déteste. }
\livretPers Venus
\livretVerse#12 { Je te feray souffrir de plus cruels tourmens, }
\livretVerse#12 { Tu méprises l’Amour, tu verras sa victoire : }
\livretVerse#12 { Et je veux que ces lieux par divers changemens }
\livretVerse#8 { Servent de Théatre à sa gloire. }
\livretVerse#12 { L’Europe que tu crois attentive à ta voix, }
\livretVerse#12 { Va chanter à tes yeux la douceur de ses loix ; }
%%% C'est luy qui dans l'Europe a ramené la Paix,
%%% Ses Peuples à tes yeux vont chanter ses attraits,
\livretVerse#12 { Tu vas voir que des cœurs l’Amour seul est le Maître. }
\livretPers La Discorde
\livretVerse#12 { Ah ! ne te flatte pas de m’en rendre témoin. }
\livretPers Venus
\livretVerse#8 { Je veux te contraindre de l’être, }
\livretVerse#12 { Tu prends pour t’en deffendre un inutile soin. }
\livretPers La Discorde
\livretVerse#8 { Puisque dans ces lieux on m’arrête, }
\livretVerse#12 { Fureurs, secondez-moy, troublons au moins la Fête. }
\livretVerse#12 { Faisons des Inconstans, des Jaloux odieux, }
\livretVerse#12 { Jettons dans tous les cœurs les soupçons & les craintes : }
\livretVerse#8 { Qu’on reconnoisse à mille plaintes }
%%% Que l'on connoisse à mille plaintes
\livretVerse#8 { Que la Discorde est dans ces lieux. }
\livretPers Venus
\livretRef#'ABBvenus
\livretVerse#12 { Tu ne peux exciter que de vaines allarmes ; }
\livretVerse#12 { Tu rendras mon triomphe encor plus glorieux. }
\livretRef#'ABCvenusChoeur
\livretVerse#12 { Faisons regner l’Amour, faisons briller ses charmes, }
\livretVerse#10 { Les doux plaisirs sont ses plus fortes armes. }
\livretPers Le Choeur
\livretVerse#12 { Faisons regner l’Amour, faisons briller ses charmes, }
\livretVerse#10 { Les doux plaisirs sont ses plus fortes armes. }
\livretPers Une Grace
\livretRef#'ABEair
\livretVerse#4 { Ah ! que l’Amour }
\livretVerse#5 { Prépare en ce jour }
%%% Ah! que ce jour
%%% Va faire à l'Amour
\livretVerse#6 { De conquêtes nouvelles ! }
\livretVerse#3 { Que se appas }
\livretVerse#6 { Vont soûmettre de Belles }
\livretVerse#5 { Qui n’y pensent pas ! }
\livretVerse#9 { Il va fléchir tous les cœurs rebelles, }
\livretVerse#5 { Il va pour jamais }
\livretVerse#6 { Les blesser de ses traits ; }
\livretVerse#4 { Loin de les craindre, }
\livretVerse#4 { Cherchons leurs coups. }
\livretVerse#5 { Quel cœur peut se plaindre }
\livretVerse#5 { D’un tourment si doux ? }
\livretVerse#9 { Au Dieu d’Amour cédons la victoire ; }
\livretVerse#9 { Quand il nous soûmet à ses desirs, }
\livretVerse#5 { C’est moins pour sa gloire }
\livretVerse#5 { Que pour nos plaisirs. }
\null
\livretRef#'ABGair
\livretVerse#4 { Que tes faveurs }
\livretVerse#5 { Vont charmer les cœurs ! }
\livretVerse#6 { Amour, que de cruelles }
\livretVerse#4 { Tu vas dompter ! }
\livretVerse#6 { Et que d’Amans fidéles }
\livretVerse#5 { Vont en profiter ! }
\livretVerse#9 { Tu vas fléchir tous les cœurs rebelles, }
\livretVerse#5 { Tu vas pour jamais }
\livretVerse#6 { Les blesser de tes traits : }
\livretVerse#4 { Loin de les craindre }
\livretVerse#4 { Cherchons leurs coups. }
\livretVerse#5 { Que cœur peut se plaindre }
\livretVerse#5 { D’un tourment si doux ? }
\livretVerse#9 { Au Dieu d’Amour cédons la victoire ; }
\livretVerse#9 { Quand il nous soûmet à ses desirs, }
\livretVerse#5 { C’est moins pour sa gloire }
\livretVerse#5 { Que pour nos plaisirs. }
\livretPers Le Chœur
\livretRef#'ABIchoeur
\livretVerse#8 { Mortels, que l’Amour vous entraîne, }
\livretVerse#8 { Cédez à ses douces ardeurs : }
\livretVerse#8 { Qu’il vous blesse, qu’il vous enchaîne, }
\livretVerse#8 { Qu’il regne à jamais dans vos cœurs. }
\livretPersDidas Venus à la Discorde
\livretRef#'ABJrecit
\livretVerse#12 { Commence à ressentir l’effet de ma vengeance ; }
\livretVerse#12 { Discorde, voy l’Amour triompher de la France. }
\livretFinAct FIN DE LA PREMIERE ENTRÉE
\null
\sep
\null
\livretAct AVIS
\null
\justify {
  On a choisi des Nations de l’Europe,
  celles dont les caractéres se contrastent davantage & promettent plus
  de jeu pour le Théatre : La France, l’Espagne, l’Italie & la Turquie.
  On a suivi les idées ordinaires qu’on a du genie de leurs Peuples.
  Le François est peint volage, indiscret & coquet ;
  l’Espagnol, fidéle & romanesque ;
  l’Italien, jaloux, fin & violent ;
  Et enfin, l’on a exprimé, autant que le Théatre l’a pû permettre,
  la hauteur & la souveraineté des Sultans, & l’emportement des Sultanes.
}
\null
\sep
\null


\livretAct LA FRANCE
\livretScene SECONDE ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente un Boccage,
  & dans le fonds un Hameau.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Silvandre, Philene. }
\livretPers Philene
\livretRef#'BAAphileneSilvandre
\livretVerse#8 { Quoy ? pour l’objet de vôtre ardeur }
\livretVerse#12 { Vous préparez encore une Fête nouvelle ? }
\livretVerse#12 { Tant de fidelité doit fléchir sa rigueur ; }
\livretVerse#12 { En vain Doris affecte une fierté cruelle, }
\livretVerse#12 { Elle se lassera de refuser son cœur }
\livretVerse#8 { Aux soins que vous prenez pour elle. }
\livretPers Silvandre
\livretVerse#12 { Ce n’est plus de Doris que j’attens mon bonheur. }
\livretPers Philene
\livretVerse#11 { Ciel ! qu’enten-je ? }
\livretPers Silvandre
\livretVerse#11 { \transparent { Ciel ! qu’enten-je ? } L’Amour m’offre un nouveau vainqueur, }
\livretVerse#8 { Et me force d’estre infidelle. }
\livretVerse#12 { Je romps mes premiers nœuds pour des nœuds plus charmans, }
\livretVerse#8 { Mon infidelité m’est chere, }
\livretVerse#12 { Et j’ay plus de plaisir à trahir mes sermens, }
\livretVerse#8 { Que je n’en sentis à les faire. }
\livretPers Philene
\livretVerse#12 { A qui donc offrez-vous, vostre hommage nouveau ? }
\livretPers Silvandre
\livretVerse#8 { A l’indifferente Cephise. }
\livretVerse#8 { Que mon triomphe seroit beau }
\livretVerse#12 { Si je la soûmettois au Dieu qu’elle méprise ! }
\livretPers Philene
\livretVerse#10 { Vous desiriez avec la mesme ardeur }
\livretVerse#10 { Qu’un jour Doris partageât vôtre flame. }
\livretPers Silvandre
\livretVerse#12 { Hé-bien je vous apprens que j’ay soûmis son cœur, }
\livretVerse#12 { Les feux dont je brulois ont passé dans son ame. }
\livretVerse#8 { Mes sermens, mes pleurs, mes soupirs }
\livretVerse#12 { M’ont obtenu l’aveu que je demandois d’elle. }
\livretPers Philene
\livretVerse#12 { Pourquoy donc brûlez-vous d’une flame nouvelle ? }
\livretPers Silvandre
\livretVerse#8 { L’Amour en comblant nos desirs }
\livretVerse#8 { A de nouveaux nœuds nous appelle. }
\livretVerse#8 { Plus de fois on est infidelle, }
\livretVerse#8 { Et plus on goûte de plaisirs. }
\livretVerse#8 { L’Amour en comblant nos desirs }
\livretVerse#8 { A de nouveaux nœuds nous appelle. }
\livretVerse#8 { Cephise se plaît en ces lieux. }
\livretPers Philene
\livretVerse#8 { C’est elle-même qui s’avance. }
\livretPers Silvandre
\livretVerse#10 { Allons, Philene, évitons sa présence, }
\livretVerse#12 { La Fête en ma faveur doit prévenir ses yeux. }

\livretScene SCENE DEUXIÉME
\livretPers Céphise
\livretRef#'BBAcephise
\livretVerse#10 { Paisibles lieux, agréables retraites, }
\livretVerse#8 { Je n’aimeray jamais que vous. }
\livretVerse#12 { En vain mille Bergers viennent à mes genoux }
\livretVerse#8 { Me jurer des ardeurs parfaites. }
\livretVerse#8 { Beaux lieux, n’en soyez point jaloux, }
\livretVerse#12 { Je méprise leur flâme, & je les quitte tous }
\livretVerse#8 { Pour le plaisir que vous me faites. }
\livretVerse#10 { Paisibles lieux, agréables retraites, }
\livretVerse#8 { Je n’aimeray jamais que vous. }
\livretVerse#8 { Pour forcer mon cœur à se rendre }
\livretVerse#8 { On fait des efforts chaque jour ; }
\livretVerse#10 { Mais quelques pleurs que je fasse répandre, }
\livretVerse#10 { Quelques sermens que l’on me fasse entendre, }
\livretVerse#8 { Ce sont les pieges de l’Amour, }
\livretVerse#12 { Je me garderay bien de m’y laisser surprendre. }

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center {
  Céphise, troupe de Bergers, de Bergeres & des Pastres,
  qui l'interrompent par leurs Dances.
}
\livretPers Céphise
\livretRef#'BCBrecit
\livretVerse#12 { Que voy-je ? quel spectacle ! & quels nouveaux concerts ! }
\livretVerse#8 { A qui ces jeux sont-ils offerts ? }
\livretPers Chœurs de Bergers
\livretRef#'BCCchoeur
\livretVerse#8 { Aimez, aimez, belle Bergere, }
\livretVerse#6 { Laissez-vous enflamer, }
\livretVerse#8 { Que sert l’avantage de plaire, }
\livretVerse#6 { Sans le plaisir d’aimer ? }
\livretPers Une Bergere
\livretRef#'BCDair
\livretVerse#6 { Soûpirez, jeunes cœurs, }
\livretVerse#8 { Suivez ce qu’Amour vous inspire ; }
\livretVerse#6 { Cent nouvelles douceurs }
\livretVerse#8 { Vous attendent dans son empire : }
\livretVerse#6 { Soûpirez, jeunes cœurs, }
\livretVerse#6 { Devroit-on vous le dire ? }
\livretPers Chœurs de Bergers
\livretRef#'BCEchoeur
\livretVerse#8 { Aimez, aimez, belle Bergere, }
\livretVerse#6 { Laissez-vous enflamer, }
\livretVerse#8 { Que sert l’avantage de plaire, }
\livretVerse#6 { Sans le plaisir d’aimer ? }
\livretPers Une Bergere
\livretRef#'BCFair
\livretVerse#8 { Aimons dans la jeune saison, }
\livretVerse#8 { Cédons, cédons à la tendresse. }
\livretVerse#8 { Nous en faut-il d’autre raison }
\livretVerse#8 { Que le penchant qui nous en presse ? }
\livretVerse#7 { En vain une erreur extrême }
\livretVerse#8 { Nous deffend de nous enflamer ; }
\livretVerse#8 { Nôtre cœur sent assez luy-même }
\livretVerse#7 { Le besoin qu’il a d’aimer. }
\livretPers Chœurs de Bergers
\livretRef#'BCGchoeur
\livretVerse#8 { Aimez, aimez, belle Bergere, }
\livretVerse#6 { Laissez-vous enflamer, }
\livretVerse#8 { Que sert l’avantage de plaire, }
\livretVerse#6 { Sans le plaisir d’aimer ? }
\livretPers Un Berger
\livretRef#'BCJair
\livretVerse#4 { Soupirons tous, }
\livretVerse#8 { Suivons l’Amour sans nous contraindre ; }
\livretVerse#4 { Il est plus doux }
\livretVerse#8 { De le sentir que de le craindre. }
\livretVerse#4 { Qui sent ses coups, }
\livretVerse#8 { Les chérit au lieu de s’en plaindre ; }
\livretVerse#6 { L’Amour rend les Amans }
\livretVerse#6 { Jaloux de leurs tourmens. }
\livretVerse#5 { Ses feux sont charmans, }
\livretVerse#8 { Gardons-nous bien de les éteindre, }
\livretVerse#6 { C’est des tendres soupirs }
\livretVerse#6 { Que naissent les plaisirs. }
\livretPers Cephise
\livretRef#'BCOrecit
\livretVerse#12 { Que je sçache du moins d’où me vient cet hommage ; }
\livretVerse#12 { Quel Amant me poursuit jusques dans ce Boccage ? }

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  Cephise, Silvandre.
}
\livretPers Silvandre
\livretRef#'BDArecit
\livretVerse#12 { Voyez à vos genoux cet Amant empressé : }
\livretVerse#12 { Je découvre en tremblant l’ardeur qui me possede ; }
\livretVerse#12 { Mais pardonnez aux maux dont je me sens pressé, }
\livretVerse#8 { C’est dans les yeux qui m’ont blessé }
\livretVerse#8 { Que j’en viens chercher le remede. }
\livretPers Cephise
\livretVerse#12 { Qu’entends-je ? quels discours ? vous seriez-vous mépris ? }
\livretVerse#10 { Vous me prenez, peut-être, pour Doris. }
\livretPers Silvandre
\livretVerse#12 { Non : Cephise, c’est vous à qui je viens apprendre }
\livretVerse#12 { Le violent amour dont je ressens les coups. }
\livretVerse#12 { Helas ! Doris a-t’elle autant d’attraits que vous, }
\livretVerse#6 { Et peut-on s’y méprendre ? }
\livretPers Cephise
\livretVerse#8 { Ce n’est donc que depuis deux jours }
\livretVerse#8 { Que vos yeux la trouvent moins belle ? }
\livretVerse#12 { Vous luy juriez alors une flâme éternelle. }
\livretVerse#12 { Quoy ? pouvez-vous si-tost démentir vos discours ? }
\livretPers Silvandre
\livretVerse#8 { Lorsque Doris me parut belle, }
\livretVerse#12 { Je ne connoissois pas encore vos attraits, }
\livretVerse#8 { Il faudroit pour estre fidelle }
\livretVerse#11 { Vous avoir toûjours veuë ou ne vous voir jamais. }
\livretPers Cephise
\livretVerse#12 { Que n’adressez-vous mieux un langage si tendre, }
\livretVerse#12 { De quelqu’autre Bergere il surprendroit la foy ; }
\livretVerse#12 { Pour moy je fuis l’amour, & je veux m’en deffendre ; }
\livretVerse#12 { Mais s’il me contraignoit quelque jour à me rendre, }
\livretVerse#12 { Du moins voudrois-je un cœur que n’eût aimé que moy. }
\livretPers Silvandre
\livretVerse#8 { Eh bien, vous serez satisfaite. }
\livretVerse#12 { J’ay senti pour vous seule une flâme parfaite, }
\livretVerse#12 { Je n’ay jamais aimé comme j’aime en ce jour : }
\livretVerse#10 { Doris estoit ma derniere amourette, }
\livretVerse#8 { Vous estes mon premier amour. }
\livretPers Cephise
\livretVerse#8 { Laissez-moy, c’est trop vous entendre, }
\livretVerse#12 { Redonnez vostre cœur à l’aimable Doris. }
\livretPers Silvandre
\livretVerse#12 { Je vous suivray par tout. }
\livretPersDidas Doris qui survient.
\livretVerse#12 { \transparent { Je vous suivray par tout. } Silvandre, cher Silvandre ; }
\livretVerse#12 { Ah ! je l’appelle en vain, il est sourd à mes cris. }

\livretScene SCENE CINQUIÉME
\livretPers Doris
\livretRef#'BEAdoris
\livretVerse#8 { Quel funeste coup pour mon ame ! }
\livretVerse#8 { Quoy ? Silvandre, tu me trahis ? }
\livretVerse#8 { Ingrat qu’as-tu fait de ta flâme ? }
\livretVerse#12 { C’est Doris qui te cherche, & c’est toy qui la fuis. }
\livretVerse#10 { Tu me jurois que l’Astre qui m’éclaire }
\livretVerse#8 { S’éteindroit avant ton amour ; }
\livretVerse#12 { Au delà du tombeau je devois t’estre chere, }
\livretVerse#10 { Jamais ardeur ne parut plus sincere, }
\livretVerse#12 { Helas ! que de sermens tu trahis en un jour ! }
\livretVerse#12 { Tu crois trouver ailleurs une plus douce chaîne ; }
\livretVerse#12 { Mais, perfide, crois-tu que je t’y laisse en paix ? }
\livretVerse#12 { J’iray troubler sans cesse en rivale inhumaine, }
\livretVerse#8 { Les douceurs que tu te promets : }
\livretVerse#12 { Mon amour outragé me tiendra lieu de haine, }
\livretVerse#12 { Et je te rendray bien les maux que tu me fais. }
\livretVerse#10 { Mais ses tourmens calmeront-ils ma peine ? }
\livretVerse#12 { Non, non, il faut plûtost luy cacher mon courroux ; }
\livretVerse#12 { Que dans d’autres liens un nouveau feu l’entraîne ? }
\livretVerse#12 { Il ne joüira point de mon dépit jaloux ; }
\livretVerse#8 { Et j’attendray qu’à mes genoux }
\livretVerse#8 { Son inconstance le rameine. }
\livretFinAct FIN DE LA SECONDE ENTRÉE
\null
\sep
\null
\livretAct L’ESPAGNE
\livretScene TROISIÈME ENTRÉE
\livretDescAtt\justify {
  Le Théatre represente une Place publique, que l’on discerne
  à peine, parce que l’action se passe dans la nuit.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Dom Pedro, Cavalier Espagnol sous le bacon de sa Maîtresse.
}
\livretPers Dom Pedro
\livretRef#'CAApedro
\livretVerse#12 { Sommeil, qui chaque nuit joüissez de ma belle, }
\livretVerse#12 { Ne versez point encor vos pavots sur ses yeux, }
\livretVerse#8 { Attendez pour regner sur elle }
\livretVerse#8 { Qu’elle ait appris mes tendres feux. }
\livretVerse#10 { Je vais parler, c’est assez me contraindre, }
\livretVerse#12 { C’est trop cacher les maux qu’elle me fait souffrir ; }
\livretVerse#8 { Du moins il est temps de m’en plaindre }
\livretVerse#8 { Lorsque je suis prest d’en mourir. }
\livretVerse#10 { Ah ! s’il plaisoit aux beaux yeux que j’adore, }
\livretVerse#10 { De soulager mon amoureux tourment, }
\livretVerse#8 { Le sort fatal que je déplore }
\livretVerse#8 { Deviendroit un destin charmant. }
\livretVerse#8 { Mais ma mort est toûjours certaine, }
\livretVerse#12 { Quelque succés qu’Amour daigne me préparer, }
\livretVerse#8 { Que Lucile soit inhumaine, }
\livretVerse#12 { Ou sensible à l’ardeur que je viens declarer, }
\livretVerse#8 { Il faudra toûjours expirer }
\livretVerse#8 { De mon plaisir ou de ma peine. }
\livretVerse#12 { Quelle troupe s’avance ? & qui l’ameine ici ? }
\livretVerse#8 { Restons, j’en veux estre éclairci. }

\livretScene SCENE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Dom Carlos ameine avec luy une troupe de Musiciens
  & de Danceurs. Le Théatre est éclairé.
}
\livretPers Dom Carlos
\livretRef#'CBAcarlos
\livretVerse#12 { La nuit rameine en vain le repos dans le monde, }
\livretVerse#8 { Mon cœur est toûjours agité ; }
\livretVerse#12 { Mais mon trouble & mes soins font ma félicité, }
\livretVerse#12 { J’aime mieux en joüir que d’une paix profonde : }
\livretVerse#12 { La nuit rameine en vain le repos dans le monde, }
\livretVerse#8 { Mon cœur est toûjours agité. }
\livretDidasPPage à sa troupe.
\livretVerse#12 { C’est à vous de servir une ardeur si constante, }
\livretVerse#12 { Soumettez à l’Amour la beauté qui m’enchante ; }
\livretVerse#12 { Par vos plus tendres chants tâchez de la charmer, }
\livretVerse#12 { Rendez-luy le plaisir que je sens à l’aimer. }
\livretRef#'CBBair
\livretDidasPage On commence la Serenade.
\livretPersDidas Une Musicienne chante ces paroles Espagnolles
\livretRef#'CBCespagnole
\livretVerse#12 { El esperar en amor es merecer. }
\livretVerse#12 { El persistir es un esforçar el hádo, }
\livretVerse#12 { En gozar suele mudarse el padecer. }
\livretVerse#12 { Al fin es Amante quien está amado. }
\livretVerse#12 { El esperar en amor es merecer. }
\livretDidasP Sens de l’Espagnol
\livretVerse#8 { Un cœur dans l’Empire d’amour }
\livretVerse#8 { Merite les biens qu’il espere ; }
\livretVerse#8 { Sa constance ameine le jour }
\livretVerse#8 { Où l’objet qu’il trouvoit severe }
\livretVerse#8 { S’attendrit & brûle à son tour : }
\livretVerse#8 { Un cœur dans l’Empire d’amour }
\livretVerse#8 { Merite les biens qu’il espere ; }
\livretPers Un Musicien
\livretRef#'CBEairChoeur
\livretVerse#5 { Nuit soyez fidelle, }
\livretVerse#5 { L’Amour ne revele }
\livretVerse#5 { Ses secret qu’à vous. }
\livretPers Chœur
\livretVerse#5 { Nuit soyez fidelle, }
\livretVerse#5 { L’Amour ne revele }
\livretVerse#5 { Ses secret qu’à vous. }
\livretPers Le Musicien
\livretVerse#7 { S’il veut à quelque cruelle }
\livretVerse#7 { Faire enfin sentir ses coups ; }
\livretVerse#5 { Nuit soyez fidelle, }
\livretVerse#5 { L’Amour ne revele }
\livretVerse#5 { Ses secret qu’à vous. }
\livretDidasPPage\justify {
  Le chœur repete ces trois derniers Vers.
}
\livretPers Le Musicien
\livretVerse#8 { Si quelque amant prés de sa belle }
\livretVerse#7 { Trompe les yeux des jaloux ; }
\livretVerse#5 { Nuit soyez fidelle, }
\livretVerse#5 { Et cachez à tous }
\livretVerse#6 { Des misteres si doux : }
\livretVerse#5 { Nuit soyez fidelle, }
\livretVerse#5 { L’Amour ne revele }
\livretVerse#5 { Ses secret qu’à vous. }
\livretDidasPPage\justify {
  Le chœur repete ces trois derniers Vers.
}
\livretPers Dom Carlos
\livretRef#'CBFcarlos
\livretVerse#12 { Vous ne paroissez point, ingrate Leonore, }
\livretVerse#8 { Méprisez-vous qui vous adore ? }
\livretVerse#8 { Se peut-il que mon tendre amour }
\livretVerse#8 { Ne fléchisse jamais vostre ame ? }
\livretVerse#12 { Quoy ? la nuit, si propice à l’amoureuse flame, }
\livretVerse#8 { Ne me sert pas mieux que le jour ? }
\livretVerse#10 { N’est-il pas temps qu’un sort heureux réponde }
\livretVerse#12 { Aux soins trop éprouvez de ma sincere ardeur ? }
\livretVerse#8 { Le plus fidelle amant du monde }
\livretVerse#8 { N’a-t’il pas droit sur vostre cœur ? }

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center {
  Dom Pedro, Dom Carlos.
}
\livretPers Dom Pedro
\livretRef#'CCArecit
\livretVerse#12 { Moderez le transport que vous faites paroître ; }
\livretVerse#8 { Il faut s’expliquer autrement. }
\livretVerse#12 { N’usurpez point le nom de plus fidelle amant, }
\livretVerse#8 { C’est moy qui me pique de l’être. }
\livretPers Dom Carlos
\livretVerse#8 { En vain l’avez-vous prétendu, }
\livretVerse#12 { On ne peut égaler mes feux ni ma constance ; }
\livretVerse#8 { Bannissez l’injuste esperance }
\livretVerse#10 { De me ravir un titre qui m’est dû. }
\livretPers Dom Pedro
\livretVerse#10 { Puisque Lucile est l’objet de ma flâme, }
\livretVerse#12 { Peut-il estre des feux plus ardens que les miens ? }
\livretVerse#12 { L’Amour par d’autres yeux peut-il blesser une ame }
\livretVerse#8 { Si vivement que par les siens ? }
\livretPers Dom Carlos
\livretVerse#8 { Lucile est digne qu’on l’adore, }
\livretVerse#12 { Elle enchaîne les cœurs des plus aimables nœuds, }
\livretVerse#8 { Si je n’avois vû Leonore }
\livretVerse#8 { Nous brûlerions des mêmes feux. }
\livretPers Dom Pedro & Dom Carlos
\livretVerse#8 { Que nostre ardeur soit éternelle, }
\livretVerse#8 { L’Amour nous promet mille attraits ; }
\livretVerse#6 { Disputons à jamais }
\livretVerse#10 { A qui sera plus tendre & plus fidelle. }
\livretPersDidas Dom Carlos à sa troupe
\livretVerse#12 { Vous, chantez, celebrez de si belles ardeurs, }
\livretVerse#12 { Que vos voix, que vos chants attendrissent les cœurs. }
\livretPers Le Chœur
\livretRef#'CCBchoeur
\livretVerse#8 { Chantons de si belles ardeurs, }
\livretVerse#12 { Que nos voix, que nos chants attendrissent les cœurs. }
\livretPers Une Musicienne
\livretRef#'CCDespagnole
\livretVerse#8 { Soyez constans dans vos amour, }
\livretVerse#8 { Amans, on est prest à se rendre : }
\livretVerse#8 { Un cœur qu’on attaque toûjours }
\livretVerse#8 { Se lasse enfin de se deffendre ; }
\livretVerse#8 { Tost ou tard il vient d’heureux jours }
\livretVerse#6 { A qui sçait les attendre. }
\livretPers Le Chœur
\livretRef#'CCFchoeur
\livretVerse#8 { Chantons de si belles ardeurs, }
\livretVerse#12 { Que nos voix, que nos chants attendrissent les cœurs. }
\livretFinAct FIN DE LA TROISIÈME ENTRÉE
\null
\sep
\null
\livretAct L’ITALIE
\livretScene QUATRIÈME ENTRÉE
\livretDescAtt\wordwrap-center {
  Le Théatre represente une Salle magnifique preparée pour un Bal
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Octavio, Olimpia
}
\livretPers Octavio
\livretRef#'DABrecit
\livretVerse#8 { Ne verray-je jamais le jour }
\livretVerse#12 { Où je seray content de l’ardeur de vôtre ame ? }
\livretVerse#12 { Ingrate, vous brûlez d’une trop foible flame ; }
\livretVerse#10 { Vous offensez & l’Amant & l’Amour. }
\livretVerse#8 { Ne verray-je jamais le jour }
\livretVerse#12 { Où je seray content de l’ardeur de vôtre ame ? }
\livretPers Olimpia
\livretVerse#12 { De quel reproche encor venez-vous m’allarmer ? }
\livretVerse#12 { Vos soupçons plus long-tems ne peuvent se contraindre, }
\livretVerse#8 { Que sert ingrat de vous aimer ? }
\livretVerse#8 { Vous ne cessez point de vous plaindre. }
\livretPers Octavio
\livretVerse#6 { Je ne me plaindrois pas, }
\livretVerse#10 { Si vous m’aimiez comme il faut que l’on aime ; }
\livretVerse#8 { A suivre sans cesse vos pas }
\livretVerse#8 { Je trouve une douceur extrême : }
\livretVerse#12 { Tous les autres plaisirs sont pour moy sans appas ; }
\livretVerse#12 { Du bonheur de vous voir je fais mon bien suprême : }
\livretVerse#8 { Helas ! si vous m’aimiez de mesme }
\livretVerse#6 { Je ne me plaindrois pas. }
\livretVerse#12 { Mais que vous estes loin de l’ardeur qui m’enflame ; }
\livretVerse#12 { Mon bonheur ne fait pas le plus doux de vos soins ; }
\livretVerse#12 { Et de tous les plaisirs que peut goûter vostre ame }
\livretVerse#12 { Mon amour est celuy qui la touche le moins. }
\livretPers Olimpia
\livretVerse#8 { Je connois ce qui vous irrite, }
\livretVerse#12 { Vous souffrez à regret que je vienne en ces lieux ; }
\livretVerse#8 { Et le spectacle ou l’on m’invite }
\livretVerse#8 { Offense peut-estre vos yeux. }
\livretPers Octavio
\livretVerse#10 { C’est le sujet de mes justes allarmes, }
\livretVerse#8 { Vous reconnoissez mal ma foy ; }
\livretVerse#8 { Je renonce à tout pour vos charmes, }
\livretVerse#8 { Et vous ne quitez rien pour moy. }
\livretPers Olimpia
\livretVerse#8 { Sortez de l’amoureux empire, }
\livretVerse#10 { Ou devenez plus tranquile en aimant }
\livretVerse#8 { Un cœur qui s’allarme aisément }
\livretVerse#8 { N’est point heureux quand il soûpire ; }
\livretVerse#10 { Pour moy l’Amour est un plaisir charmant, }
\livretVerse#6 { Pour vous c’est un martire. }
\livretPers Octavio
\livretVerse#12 { Ah ! ne murmurez point de mes transports jaloux ! }
\livretVerse#12 { L’excés de mon amour fait celuy de mes craintes ; }
\livretVerse#8 { Tout ce qui s’approche de vous }
\livretVerse#10 { Porte à mon cœur de sensibles atteintes. }
\livretVerse#12 { Que ne sommes-nous seuls en des lieux retirez }
\livretVerse#10 { Je cesserois peut-estre de me plaindre ; }
\livretVerse#10 { Plus vos attraits y seroient ignorez, }
\livretVerse#8 { Moins j’aurois de Rivaux à craindre. }
\livretVerse#12 { On vient. Songez du moins que je suis prés de vous, }
\livretVerse#8 { Et menagez un cœur jaloux. }

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Octavio, Olimpia, Troupe de Masques.
}
\livretPers Le Chœur des Masques
\livretRef#'DBBchoeur
\livretVerse#8 { Tendres amans, rassemblons-nous. }
\livretVerse#8 { Pour les cœurs que l’Amour enchaîne, }
\livretVerse#8 { Quel séjour peut estre plus doux ? }
\livretVerse#8 { S’il se trouve icy des jaloux ; }
\livretVerse#6 { L’Amour ne les ameine }
\livretVerse#6 { Que pour les tromper tous. }
\livretPers Une Venitienne
\livretRef#'DBDair
\livretVerse#9 { Ad un cuore tutto geloso }
\livretVerse#7 { Deve amor negar pieta. }
\livretVerse#4 { La sua face }
\livretVerse#5 { Ch’alleta è piace }
\livretVerse#8 { Vuol dolcezza non crudelta. }
\livretRef#'DBFair
\livretVerse#9 { Un bel viso tutto vezzoso }
\livretVerse#7 { Merta un laci di lealta. }
\livretVerse#4 { Che Cupido }
\livretVerse#5 { Quel nume infido }
\livretVerse#8 { Aborrisce la ferita. }
\livretDidasPPage Sens de l’Italien.
\livretVerse#8 { Sur les jaloux l’Amour épuise }
\livretVerse#8 { Ses plus redoutables rigueurs ; }
\livretVerse#8 { Il veut qu’on engage les cœurs, }
\livretVerse#8 { Et défend qu’on les tirannise, }
\livretVerse#8 { Belles, prenez de douces chaînes, }
\livretVerse#8 { Tout doit répondre à vos desirs ; }
\livretVerse#8 { Le Dieu d’Amour garde ses peines }
\livretVerse#8 { Pour qui troublera vos plaisirs. }
\livretPersDidas Une Venitienne déguisée
\livretRef#'DBHairChoeur
\livretVerse#12 { Formons d’aimables jeux, laissons-nous enflamer ; }
\livretVerse#12 { Il n’est permis icy que de rire & d’aimer. }
\livretDidasP\line { Le Chœur repete ces deux derniers Vers. }
\livretPers La Venitienne
\livretVerse#12 { Bannissons de ces lieux l’importune raison, }
\livretVerse#10 { Elle vaut moins qu’une aimable folie ; }
\livretVerse#12 { Un doux excés sied bien dans la jeune saison, }
\livretVerse#10 { Pour estre heureux il faut qu’un cœur s’oublie. }
\livretPers Le Chœur
\livretVerse#12 { Formons d’aimables jeux, laissons-nous enflamer ; }
\livretVerse#12 { Il n’est permis icy que de rire & d’aimer. }
\livretPers La Venitienne
\livretVerse#12 { Rendez-vous, jeunes cœurs, cedez à vos desirs, }
\livretVerse#10 { Tout vous inspire un tendre badinage ; }
\livretVerse#12 { Ne preferez jamais la sagesse aux plaisirs, }
\livretVerse#10 { Il vaut bien mieux estre heureux qu’estre sage. }
\livretPers Le Chœur
\livretVerse#12 { Formons d’aimables jeux, laissons-nous enflamer ; }
\livretVerse#12 { Il n’est permis icy que de rire & d’aimer. }
\livretPersDidas Une autre Venitienne déguisée
\livretRef#'DBJairChoeur
\livretVerse#12 { Livrons-nous aux plaisirs, il n’est rien de plus doux ; }
\livretVerse#12 { Pour qui seroient-ils faits si ce n’estoit pour nous ? }
\livretDidasP\line { Le Chœur repete ces deux Vers. }
\livretPers La Venitienne
\livretVerse#12 { Mille amours déguisez dans ce charmant séjour }
\livretVerse#10 { Comblent nos cœurs d’une douceur extrême ; }
\livretVerse#12 { Si quelqu’un en ces lieux est entré sans amour, }
\livretVerse#10 { Ne craignons pas qu’il en sorte de mesme. }
\livretPers Le Chœur
\livretVerse#12 { Livrons-nous aux plaisirs, il n’est rien de plus doux ; }
\livretVerse#12 { Pour qui seroient-ils faits si ce n’estoit pour nous ? }
\livretPers La Venitienne
\livretVerse#12 { L’Amour, jeunes beautez, accompagne vos pas, }
\livretVerse#10 { Pour tout soumettre il vous prête ses armes ; }
\livretVerse#12 { C’est vainement qu’aux yeux vous cachez mille appas, }
\livretVerse#10 { A tous les cœurs il revéle vos charmes. }
\livretDidasP\line { Le Chœur repete \normal-text Livrons-nous, &c. }
\livretRef#'DBKair
\livretDidasP\justify {
  Pendant la Feste un des Masques danse avec Olimpia,
  & fait remarquer beaucoup d’empressement pour elle.
  Quand le bal finit, Octavio suit ce Masque,
  & Olimpia reste surpise de se trouver sans luy.
}
\livretPers Air Italien
\livretRef#'DBLair
\livretVerse#6 { Si scherzi, si rida }
\livretVerse#6 { Si pensi à goder }
\livretVerse#6 { Gia sotto le piume }
\livretVerse#6 { D’Aligero nume }
\livretVerse#6 { Per noi si matura }
\livretVerse#6 { L’acerbo piacer }
\livretDidasPPage Sens de l’Italien.
\livretVerse#11 { Rions & folâtrons ne songeons qu’aux plaisirs, }
\livretVerse#5 { L’Amour sous ses aîles }
\livretVerse#6 { Au gré de nos desirs }
\livretVerse#8 { Meurit mille douceurs nouvelles. }

\livretScene SCENE TROISIÈME
\livretPers Olimpia
\livretRef#'DCArecit
\livretVerse#10 { Qu’est devenu le jaloux qui m’obsede ? }
\livretVerse#12 { Ciel ! quel est le sujet de son éloignement ? }
\livretVerse#12 { Auroit-il reconnu l’ardeur qui me possède ? }
\livretVerse#12 { Mes regards n’ont-ils pas découvert mon amant ? }
\livretVerse#12 { Peut-estre de nos yeux la douce intelligence }
\livretVerse#10 { N’a pû garder le secret de nos cœurs ; }
\livretVerse#12 { Ces indiscrets témoins de nos tendres langueurs }
\livretVerse#8 { Ont enfin rompu le silence. }
\livretVerse#8 { Ah ! faut-il qu’une injuste loy }
\livretVerse#12 { Destine à ce jaloux le reste de ma vie ; }
\livretVerse#12 { Les soins que son Rival a laissé voir pour moy }
\livretVerse#8 { Me font redouter sa furie ; }
\livretVerse#8 { Que je crains… }

\livretScene SCENE QUATRIÈME
\livretDescAtt\center-column {
  \line { Octavio, Olimpia }
  \wordwrap-center {
    Octavio rentre en remettant son Poignard.
  }
}
\livretPers Olimpia
\livretRef#'DDArecit
\livretVerse#8 { \transparent { Que je crains… } Mais que vois-je ? ô Ciel ! }
\livretVerse#8 { Cruel ! quelle rage vous guide ? }
\livretVerse#12 { De quels affreux transports éteincellent vos yeux ; }
\livretPers Octavio
\livretVerse#8 { Gemi, pleure à ton tour, perfide ; }
\livretVerse#12 { Va, cours de ton amant recevoir les adieux ; }
\livretVerse#8 { Il expire prés de ces lieux. }
\livretPersDidas Olimpia en s’évanoüissant
\livretVerse#12 { Ciel ! }
\livretPers Octavio
\livretVerse#12 { \transparent { Ciel ! } Eh bien, malheureux, en douterois-je encore ? }
\livretVerse#12 { Sa douleur m’en dit plus que je n’en veux sçavoir ; }
\livretVerse#12 { Me voilà donc certain du feu qui la devore ; }
\livretVerse#12 { Cependant je n’ay pû vanger mon desespoir }
\livretVerse#8 { Sur celuy que son cœur adore. }
\livretVerse#12 { En vain je l’ay suivi, ce trop heureux Amant. }
\livretVerse#8 { Fatale Feste, nuit trop sombre, }
\livretVerse#8 { C’est vous dont le tumulte & l’ombre }
\livretVerse#12 { Ont dérobé ses jours à mon ressentiment. }
\livretDidasPPage à Olimpia
\livretVerse#12 { Tu reprens tes esprits, cruelle, à ce langage, }
\livretVerse#8 { Je suis le seul qui souffre icy : }
\livretVerse#12 { De tous ses mouvements je sens croistre ma rage ; }
\livretVerse#12 { Je voulois luy surprendre un secret qui m’outrage ; }
\livretVerse#8 { Je n’ay que trop bien réüssi. }
\livretPers Olimpia
\livretVerse#12 { Vous voyez mon ardeur, il n’est plus temps de feindre, }
\livretVerse#12 { Mon secret se découvre à vos soupçons jaloux ; }
\livretVerse#8 { C’est à l’Amour qu’il faut vous plaindre, }
\livretVerse#12 { Je l’aurois écouté s’il m’eust parlé pour vous. }
\livretPers Octavio
\livretVerse#12 { Quoy ! perfide, mes feux, le devoir, ma tendresse, }
\livretVerse#8 { Mes pleurs n’ont pu vous attendrir ? }
\livretVerse#12 { Ah ! je veux désormais reparer ma foiblesse, }
\livretVerse#12 { Je mettray tous mes soins à vous faire souffrir : }
\livretVerse#8 { Puisque vous brulez pour un autre, }
\livretVerse#8 { Mon Rival en perdra le jour ; }
\livretVerse#12 { Ma fureur dans son sang éteindra son amour, }
\livretVerse#6 { Et punira le vôtre. }
\livretPers Olimpia
\livretVerse#8 { Cruel, cessez de m’allarmer, }
\livretVerse#10 { N’ecoutez point une injuste colere ; }
\livretVerse#8 { C’estoit à moy de vous aimer, }
\livretVerse#8 { Mais c’estoit à vous de me plaire. }
\livretPers Octavio
\livretVerse#12 { Ingrate, ce discours vient encor animer }
\livretVerse#8 { Mon desespoir & ma vengeance. }
\livretPers Olimpia
\livretVerse#8 { Pour vous aider à les calmer }
\livretVerse#8 { Il faut fuïr de vostre presence. }

\livretScene SCENE CONQUIÈME
\livretPers Octavio
\livretRef#'DEArecit
\livretVerse#12 { Quel outrage ! mon cœur ne peut le soûtenir, }
\livretVerse#10 { Elle me laisse, elle rit de ma peine ; }
\livretVerse#10 { Dieux ! quand l’Hymen est prest à nous unir, }
\livretVerse#12 { La perfide à ses nœuds oppose une autre chaîne. }
\livretVerse#8 { Non, je ne puis luy pardonner, }
\livretVerse#12 { Je me livre aux transports de ma fureur extréme, }
\livretVerse#12 { Je suivray les conseils qu’elle me vient donner. }
\livretVerse#12 { Immolons mon rival, son amante & moy-même. }
\livretVerse#12 { Ne vaudroit-il pas mieux rompre un fatal lien ? }
\livretVerse#12 { Mais le puis-je ? Insensé, quel vain espoir me flatte ? }
\livretVerse#12 { Sans l’objet de mes feux je n’espere plus rien ; }
\livretVerse#12 { C’est sa seule rigueur qu’il faut que je combatte : }
\livretVerse#12 { Allons tomber encore aux genoux de l’ingratte, }
\livretVerse#12 { Pour attendrir son cœur, ou pour percer le mien. }
\livretFinAct FIN DE LA QUATRIÈME ENTRÉE
\null
\sep
\null
\livretAct LA TURQUIE
\livretScene CINQUIÈME ENTRÉE
\livretDescAtt\wordwrap-center {
  Le Théatre represente les Jardins du Serail, & dans le fonds
  le Palais des Sultanes.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Zayde seule.
}
\livretPers Zayde
\livretRef#'EAAzaide
\livretVerse#8 { Mes yeux ne pourrez-vous jamais }
\livretVerse#8 { Forcer mon vainqueur à se rendre ? }
\livretVerse#8 { Faut-il avec un cœur si tendre }
\livretVerse#8 { Avoir de si foibles attraits ? }
\livretVerse#8 { Mes yeux ne pourrez-vous jamais }
\livretVerse#8 { Forcer mon vainqueur à se rendre ? }
\livretVerse#8 { Au moment de mon esclavage, }
\livretVerse#12 { Quand on me conduisit dans ce riche Palais, }
\livretVerse#12 { Il parut à mes yeux l’Antre le plus sauvage ; }
\livretVerse#12 { Je le fis retentir de mes tristes regrets ; }
\livretVerse#8 { Je me fis une image affreuse }
\livretVerse#10 { Du Souverain que j’adore aujourd’hui ; }
\livretVerse#12 { Mais sa presence enfin dissipa mon ennuy ; }
\livretVerse#8 { Et je me trouvay trop heureuse }
\livretVerse#8 { D’estre captive auprés de luy. }
\livretVerse#8 { Les beautez dont il est le maistre, }
\livretVerse#12 { Par son ordre bien-tost s’assemblent dans ces lieux, }
\livretVerse#8 { Amour, Amour, fais-luy connoistre }
\livretVerse#8 { Le cœur qui le merite mieux. }
\livretVerse#12 { Mais c’est luy que je voy, gardons-nous de paroistre, }
\livretVerse#12 { Il n’est pas tems encor de m’offrir à ses yeux. }

\livretScene SCENE DEUIXÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Roxane.
}
\livretPers Roxane
\livretRef#'EBAzulimanRoxane
\livretVerse#12 { Quoy pour d’autres appas vostre ame est enflamée ? }
\livretVerse#12 { Mes soupirs désormais vont estre superflus ; }
\livretVerse#8 { Ah ! pourquoy m’avez-vous aimée ? }
\livretVerse#8 { Ou pourquoy ne m’aimez vous plus ? }
\livretPers Zuliman
\livretVerse#8 { Je ne romprois pas nostre chaîne }
\livretVerse#8 { Si vous sçaviez m’y retenir ; }
\livretVerse#7 { Mon cœur s’accorde sans peine }
\livretVerse#7 { A qui sçait mieux l’obtenir. }
\livretPers Roxane
\livretVerse#8 { Que vostre inconstance est cruelle ! }
\livretVerse#8 { Helas ! vous m’ostez vostre cœur, }
\livretVerse#8 { Et malgré toute ma douleur }
\livretVerse#12 { Je n’ose vous traiter d’ingrat & d’infidelle. }
\livretVerse#12 { Je vois avec horreur mépriser mes appas, }
\livretVerse#8 { Je sens les plus vives allarmes ; }
\livretVerse#12 { Mais le respect me force à murmurer tout bas, }
\livretVerse#12 { Et me fait devorer mes soupirs & mes larmes. }
\livretPers Zuliman
\livretVerse#8 { Vous meritez un sort plus doux, }
\livretVerse#12 { Et mon cœur à regret se détache du vostre ; }
\livretVerse#8 { La pitié parle encor pour vous, }
\livretVerse#8 { Mais l’Amour parle pour un autre. }
\livretPers Roxane
\livretVerse#12 { C’en est donc fait, Seigneur, mes beaux jours sont passez ? }
\livretPers Zuliman
\livretVerse#12 { Je n’oublieray jamais que vous me fûtes chere. }
\livretPers Roxane
\livretVerse#8 { Vous ne m’aimez plus, c’est assez }
\livretVerse#8 { Tout le reste me desespere ; }
\livretVerse#12 { Que ne puis-je oublier que je vous ay sceu plaire ! }
\livretVerse#12 { Je ne sentirois pas que vous me trahissez. }
\livretPers Zuliman
\livretVerse#12 { On approche, cessez une plainte trop vaine ; }
\livretVerse#8 { Celles qu’icy mon ordre ameine }
\livretVerse#10 { Vont par leurs jeux répondre à mes desirs ; }
\livretVerse#7 { Dissimulez vostre peine, }
\livretVerse#7 { Et respectez mes plaisirs. }
\livretPers Roxane
\livretVerse#12 { Voyons du moins l’objet de ses nouveaux soupirs ; }
\livretVerse#8 { Sçachons à qui je dois ma haine. }

\livretScene SCENE TROISIÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Roxane, Zayde, & les autres Sultanes.
}
\livretRef#'ECApassacaille
\livretDidasPage\wordwrap-center {
  Les sultanes forment plusieurs dances pour plaire à Zuliman.
}
\livretPers Zayde
\livretRef#'ECBair
\livretVerse#9 { Que l’Amour dans nos cœurs fasse naistre }
\livretVerse#9 { Mille ardeurs pour nostre auguste Maistre ; }
\livretVerse#6 { Que nos tendres soupirs }
\livretVerse#6 { Préviennent ses desirs. }
\livretDidasP\wordwrap {
  Le Chœur des Sultanes repete ces quatres vers.
}
\livretPers Zayde
\livretVerse#9 { Dans ces lieux tout doit le satisfaire ; }
\livretVerse#12 { Pour ce charmant Vainqueur laissons-nous enflamer ; }
\livretVerse#9 { Attendons le bonheur de luy plaire }
\livretVerse#12 { En joüissant toujours du plaisir de l’aimer. }
\livretDidasP\wordwrap {
  Le Chœur repete ces quatres vers.
}
\livretPersDidas Zuliman à Zaïde
\livretRef#'ECCrecit
\livretVerse#8 { Vous brillez seule en ces retraites, }
\livretVerse#10 { Vous effacez tous les autres appas ; }
\livretVerse#8 { L’Amour ne se plaist qu’où vous estes, }
\livretVerse#8 { Il languit où vous n’estes pas : }
\livretVerse#12 { Mon cœur ne sent que trop le plaisir que vous faites. }
\livretPers Zayde
\livretVerse#12 { Quoy ? Seigneur… }
\livretPers Zuliman
\livretVerse#12 { \transparent { Quoy ? Seigneur… } C’est de vous que je me sens épris ; }
\livretVerse#8 { Depuis le jour que je vous vis }
\livretVerse#12 { Mon cœur, belle Zaïde, en secret vous adore. }
\livretPers Zaïde
\livretVerse#12 { Helas ! s’il estoit vray vous me l’auriez appris. }
\livretPers Zuliman
\livretVerse#12 { Non, & c’est un secret que je tairois encore }
\livretVerse#12 { Si vos tendres regards ne me l’avoient surpris. }
\livretVerse#8 { J’esperois affranchir mon ame }
\livretVerse#8 { Du peril d’engager sa foy ; }
\livretVerse#12 { Et je ne voulois pas me permettre une flame }
\livretVerse#8 { Qui prist trop d’empire sur moy. }
\livretVerse#12 { J’ay long-temps differé de vous rendre les armes : }
\livretVerse#10 { Pour éviter d’éternelles amours, }
\livretVerse#12 { Des beautez de ces lieux j’empruntois le secours ; }
\livretVerse#8 { Mais vous triomphez de leurs charmes, }
\livretVerse#12 { Et je vous aime, enfin, pour vous aimer toujours. }
\livretPersDidas Roxane tirant son poignard & voulant fraper Zayde
\livretVerse#10 { Ah c’en est trop, je céde à cet outrage, }
\livretVerse#10 { Versons le sang que demande ma rage. }
\livretPersDidas Zuliman luy arrachant son poignard.
\livretVerse#8 { Ciel ! que vois-je ? quelle fureur ! }
\livretVerse#8 { Malheureuse, qu’oze-tu faire ? }
\livretPers Roxane
\livretVerse#12 { Je voulois la punir d’avoir trop sceu te plaire, }
\livretVerse#8 { Et de m’avoir ravi ton cœur. }
\livretVerse#10 { Le desespoir dont je suis animée }
\livretVerse#8 { S’enflame encor par tes discours ; }
\livretVerse#12 { Tu luy jures, cruel, les plus tendres amours, }
\livretVerse#12 { Tu l’aimes cent fois plus que tu ne m’as aimée. }
\livretVerse#12 { Quand tu formas les nœuds que tu romps pour jamais }
\livretVerse#12 { J’éprouvay ta fierté jusque dans ta tendresse ; }
\livretVerse#8 { Helas ! c’est avec d’autres traits }
\livretVerse#8 { Que l’Amour aujourd’huy te blesse, }
\livretVerse#8 { Devant ses yeux ton orgueil cesse ; }
\livretVerse#8 { J’ay voulu vanger mes attraits, }
\livretVerse#8 { Et te punir de ta foiblesse. }
\livretPers Zuliman
\livretVerse#8 { Quoy, ne crains-tu pas que la mort }
\livretVerse#8 { Soit le prix de ton insolence ? }
\livretPers Roxane
\livretVerse#8 { Je n’ay pû remplir ma vangeance ; }
\livretVerse#12 { Ce regret seul sans toy peut terminer mon sort : }
\livretVerse#8 { Mais toy, Rivale trop cruelle, }
\livretVerse#12 { Prens ce fer infidele à mon juste courroux ; }
\livretVerse#12 { Portes-en à mon cœur une atteinte mortelle ; }
\livretVerse#12 { Tu m’as déja porté de plus sensibles coups. }
\livretPers Zuliman
\livretVerse#12 { Qu’on l’ôte de mes yeux & qu’on s’assûre d’elle. }

\livretScene SCENE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Zuliman, Zayde, & les autres Sultanes.
}
\livretPers Zayde
\livretRef#'EDAzaideZuliman
\livretVerse#8 { Au nom de nos tendres ardeurs }
\livretVerse#8 { Oubliez sa jalouse rage, }
\livretVerse#8 { Ne vous vangez de ses fureurs }
\livretVerse#6 { Qu’en m’aimant d’avantage. }
\livretPers Zuliman
\livretVerse#8 { Je suis épris de vos attraits }
\livretVerse#6 { Autant qu’on le peut être ; }
\livretVerse#6 { Mon feu ne sçauroit croistre, }
\livretVerse#6 { Ni s’affoiblir jamais. }
\livretPers Zuliman & Zayde
\livretVerse#8 { Livrons nos cœurs à la tendresse }
\livretVerse#8 { Ne formons que d’heureux desirs ; }
\livretVerse#8 { Aimons-nous, aimons-nous sans cesse, }
\livretVerse#8 { Comptons nos jours par nos plaisirs. }
\livretPers Zuliman
\livretVerse#12 { Que tout signale icy nos ardeurs mutuelles, }
\livretVerse#12 { Qu’on offre à nos regards les Festes les plus belles. }

\livretScene SCENE CINQUIÈME
\livretDescAtt\center-column {
  \wordwrap-center {
    Zuliman, Zayde, les Sultanes,
    & les Bostangis ou Jardiniers du Serail.
  }
  \wordwrap-center {
    Ils forment plusieurs Jeux suivant leur caractere.
  }
}
\livretPersDidas Le Chef des Bostangis à qui le Chœur répond
\livretRef#'EEBchoeur
\livretVerse#8 { Vivir, vivir, gran Sultana. }
\livretRef#'EECchoeur
\livretVerse#8 { Unir unir li cantara. }
\livretVerse#8 { Mille volte exclamara, }
\livretRef#'EEDchoeur
\livretVerse#8 { Vivir, vivir, gran Sultana. }
\null
\livretRef#'EEFchoeur
\livretVerse#7 { Bello como star un flor ; }
\livretVerse#7 { Durar quanto far arbor. }
\livretVerse#9 { Al enemigos su sciabola }
\livretVerse#7 { Como a frutas tempesta. }
\livretVerse#8 { La rusciada matutina }
\livretVerse#7 { Far florir su jardina. }
\livretVerse#5 { Favor celesta }
\livretVerse#6 { Coprir su turbanta. }
\null
\livretRef#'EEIchoeur
\livretVerse#4 { Star contento, }
\livretVerse#4 { Star potento, }
\livretVerse#11 { Del mondo star l’amor o la spavento. }
\livretVerse#3 { En regnar, }
\livretVerse#3 { En amar, }
\livretVerse#4 { Far tributir }
\livretVerse#7 { L’occidento, l’Oriento. }
\livretVerse#3 { En regnar, }
\livretVerse#3 { En amar, }
\livretVerse#4 { Sempre sentir }
\livretVerse#7 { Plazer sensa tormento. }
\livretVerse#3 { Dir e far }
\livretVerse#3 { O disfar }
\livretVerse#6 { Subito, subito }
\livretVerse#5 { Sú ló momento. }
\livretRef#'EEKchoeur
\livretVerse#4 { Star contento, }
\livretVerse#4 { Star potento, }
\livretVerse#10 { Del mondo star l’amor, ó lo spavento. }
\livretDidasP\wordwrap { Le sens des paroles Franques. }
\livretVerse#12 { Vive le Souverain qui nous donne des Loix ; }
\livretVerse#10 { Chantons, chantons, repetons mille fois, }
\livretVerse#12 { Vive le souverain qui nous donne des Loix. }
\livretVerse#8 { Qu’il ignore à jamais les peines, }
\livretVerse#8 { Qu’il éprouve mille douceurs, }
\livretVerse#7 { Qu’il brille autant que les fleurs, }
\livretVerse#7 { Qu’il dure autant que les chesnes. }
\livretVerse#12 { Qu’il réünisse en luy la force & le courage ; }
\livretVerse#6 { Que ses voisins jaloux }
\livretVerse#6 { Craignent plus son couroux }
\livretVerse#8 { Que nos fruits ne craignent l’orage. }
\livretVerse#12 { Qu’au devant de ses vœux les cœurs viennent s’offrir }
\livretVerse#8 { Que pour son bon-heur tout conspire ; }
\livretVerse#10 { Et que le Ciel fasse toujours fleurir }
\livretVerse#8 { Et ses jardins & son Empire. }

\livretScene SCENE DERNIERE
\livretDescAtt\wordwrap-center {
  Venus, la Discorde.
}
\livretPers La Discorde
\livretRef#'EFArecit
\livretVerse#8 { C’en est trop, Déesse inhumaine, }
\livretVerse#10 { Laisse-moy fuïr de ce fatal séjour ; }
\livretVerse#12 { Tu n’as que trop joüi de ma cruelle peine : }
\livretVerse#8 { O Ciel ! tout échape à ma haine, }
\livretVerse#6 { Et tout céde à l’Amour. }
\livretVerse#12 { J’excitois vainement le Dépit & la Rage ; }
\livretVerse#12 { La force de l’Amour en brilloit d’avantage. }
\livretVerse#8 { Fuyons, fuyons de l’Univers, }
\livretVerse#10 { Allons du moins regner dans les Enfers. }
\livretDidasPPage Elle s’abisme.
\livretPers Venus
\livretVerse#12 { La Discorde à l’Amour céde enfin la victoire. }
\livretVerse#8 { Vous, Jeux chamans, tendres Plaisirs, }
\livretVerse#12 { Volez de toutes parts pour servir ses desirs ; }
\livretVerse#12 { Allez accroître encor son Empire & sa Gloire. }
\livretDidasPPage\wordwrap {
  Les Plaisir partent pour satisfaire à ses Ordres.
}
\null
\livretFinAct FIN DE L’EUROPE GALANTE
}
