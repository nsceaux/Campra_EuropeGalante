\version "2.19.54"

\header {
  copyrightYear = "2017"
  composer = "André Campra"
  poet = "Antoine Houdar de La Motte"
  date = "1697"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
%% Staff size
#(set-global-staff-size
  (cond ((eqv? (ly:get-option 'urtext) #t) 14)
        ((not (symbol? (ly:get-option 'part))) 16)
        ((memq (ly:get-option 'part) '(basse-continue)) 18)
        (else 20)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((dessus       "Dessus"        () (#:notes "dessus"))
   (haute-contre "Hautes-contre" () (#:notes "haute-contre" #:clef "alto"))
   (taille       "Tailles"       () (#:notes "taille" #:clef "alto"))
   (quinte       "Quintes"       () (#:notes "quinte" #:clef "alto"))
   (basse        "Basses"        () (#:notes "basse" #:tag-notes basse #:clef "basse"))
   (basse-continue
    "Basse continue" ()
    (#:notes "basse" #:tag-notes basse-continue #:clef "basse")))
\opusTitle "L'Europe Galante"

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.simphonie-du-marais.org" \line {
      La Simphonie du Marais – Hugo Reyne
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
}

forte =
#(make-music
  'TextScriptEvent
  'text #{\markup\whiteout\italic\general-align #X #-0.5 forte #})

dolce =
#(make-music
  'TextScriptEvent
  'text #{\markup\whiteout\italic\general-align #X #-0.5 dolce #})
